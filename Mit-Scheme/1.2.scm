;;; 1.2 Linear Recursion and Iteration

;; Factorial
( define ( factorial n )
         ( if ( = n 1 )
              1
              ( * n ( factorial ( - n 1 )))))
( factorial 5 ) ; 5 * 4 * 3 * 2 * 1 = 120
( factorial 6 )

( define ( factorial n )
         ( define ( fact-iter product counter max-count )
             ( if ( > counter max-count )
                  product
                  ( fact-iter ( * counter product )
                              ( + 1 counter )
                              ( max-count ))))
         ( fact-iter 1 1 n ))

#| ( define ( + a b )
         ( if ( = 0 a )
              b
              ( inc ( + ( dec a ) b ))))
( define ( + a b )
         ( if ( = a 0 )
              b
              ( + ( dec a ) ( inc b ))))
( define ( inc n )
         ( + 1 n ))
( define ( dec n )
         ( - n 1 )) |#

;; describe the type of processes involved when running ( + 4 5 ) with each procedure
;; The first procedure yields a recursive process that calls itself within a nested part of the expression so each call to + will be nested within the overall substituted procedure
#| ( + 4 5 )
( inc ( + ( dec 4 ) 5 ))
( inc ( + 3 5 ))
( inc ( inc ( + ( dec 3 ) 5 )))
( inc ( inc ( inc ( + ( dec 2 ) ) 5 )))
( inc ( inc ( inc ( inc ( + ( dec 1 ) 5 )))))
( inc ( inc ( inc ( inc ( + 0 5 )))))
( inc ( inc ( inc ( inc 5 ))))
( inc ( inc ( inc 6 )))
( inc ( inc 7 ))
( inc 8 )
( 9 ) |#

;; The second procedure also calls itself in the body of the procedure however this time it is a iterative process. Each loop merely substitutes the new values of 'a' and 'b' until the predicate evaluates to true and the loop ends
#| ( + 4 5 )
( + ( dec 4 ) ( inc 5 ))
( + ( dec 3 ) ( inc 6 ))
( + ( dec 2 ) ( inc 7 ))
( + ( dec 1 ) ( inc 8 ))
( + 0 9 ) ;return 'b' which is 9
( 9 ) |#

;; Ackermann's function
( define ( A x y )
         ( cond (( = y 0 ) 0 )
                (( = x 0 ) ( * 2 y ))
                (( = y 1 ) 2 )
                ( else ( A ( - x 1 )
                           ( A x ( - y 1 ))))))
#| ( A 1 10 )
( A 0 ( A 1 ( 9 )))
( A 0 ( A 0 ( A 1 8 )))
( A 0 ( A 0 ( A 0 ( A 1 7 ))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 1 6 )))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 1 5 ))))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 1 4 )))))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 1 3 ))))))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 1 2 )))))))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 1 1))))))))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 2 )))))))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 4 ))))))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 8 )))))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 0 ( A 0 16 ))))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 0 32 )))))
( A 0 ( A 0 ( A 0 ( A 0 64 ))))
( A 0 ( A 0 ( A 0 128 )))
( A 0 ( A 0 256 ))
( A 0 512 )
( 1024 ) |#

#| ( A 2 4 )
( A 1 ( A 2 3 ))
( A 1 ( A 1 ( A 2 2 )))
( A 1 ( A 1 ( A 1 ( A 2 1 ))))
( A 1 ( A 1 ( A 1 2 )))
( A 1 ( A 1 ( A 0 ( A 1 1 ))))
( A 1 ( A 1 ( A 0 2 )))
( A 1 ( A 1 4 ))
( A 1 ( A 0 ( A 1 3 )))
( A 1 ( A 0 ( A 0 ( A 1 2 ))))
( A 1 ( A 0 ( A 0 ( A 0 ( A 1 1 )))))
( A 1 ( A 0 ( A 0 ( A 0 2 ))))
( A 1 ( A 0 ( A 0 4 )))
( A 1 ( A 0 8 ))
( A 1 16 )
( A 0 ( A 1 15 )) |#
;; and on and on until y is equal to 1 and you end up running 2 to the power 16

#| ( A 3 3 )
( A 2 ( A 3 2 ))
( A 2 ( A 2 ( A 3 1 )))
( A 2 ( A 2 2 ))
( A 2 ( A 1 ( A 2 1 )))
( A 2 ( A 1 2 ))
( A 2 ( A 0 ( A 1 1 )))
( A 2 ( A 0 2 ))
( A 2 4 ) |#

( define ( f n ) ( A 0 n )) ; will double n
( define ( g n ) ( A 1 n )) ; will raise 2 to the power n
#| ( g 5 )
( A 1 5 )
( A 0 ( A 1 4 ))
( A 0 ( A 0 ( A 1 3 )))
( A 0 ( A 0 ( A 0 ( A 1 2 ))))
( A 0 ( A 0 ( A 0 ( A 0 ( A 1 1 )))))
( A 0 ( A 0 ( A 0 ( A 0 2 ))))
( A 0 ( A 0 ( A 0 4 )))
( A 0 ( A 0 8 ))
( A 0 16 )
( 32 ) |#
( define ( h n ) ( A 2 n )) ; will raise 2 to the power 2 ( n - 1 ) times

( f 10 ) ;20
( g 10 ) ;1024
( h 3 ) ;16

;; 1.2.2 Tree Recursion
( define ( fib n )
         ( cond (( = n 0 ) 0 )
                (( = n 1 ) 1 )
                ( else ( + ( fib ( - n 1 ))
                           ( fib ( - n 2 ))))))
( define ( fib n )
         ( fib-iter 1 0 n ))
( define ( fib-iter a b count )
         ( if ( = count 0 )
              b 
              ( fib-iter ( + a b ) a ( - count 1 ))))
( define ( count-change amount )
         ( cc amount 5 ))
( define ( cc amount kinds-of-coins )
         ( cond (( = amount 0 ) 1 )
                (( or ( < amount 0 )
                      ( = kinds-of-coins 0 ))
                 0 )
                ( else
                  ( + ( cc amount ( - kinds-of-coins 1 ))
                       ( cc ( - amount ( first-denomination kinds-of-coins ))
                             kinds-of-coins )))))
( define ( first-denomination kinds-of-coins )
         ( cond (( = kinds-of-coins 1 ) 1 )
                (( = kinds-of-coins 2 ) 5 )
                (( = kinds-of-coins 3 ) 10 )
                (( = kinds-of-coins 4 ) 25 )
                (( = kinds-of-coins 5 ) 50 )))
( count-change 50 )

;; f( n ) = n if n < 3
;; f( n ) = f( n - 1 ) + 2f( n - 2 ) + 3f( n - 3 ) if n >= 3
;; Recursive Process
( define ( f n )
         ( if ( < n 3 ) n
              ( + ( f ( - n 1 ))
                  ( * 2 ( f ( - n 2 )))
                  ( * 3 ( f ( - n 3 ))))))

;; Iterative Process
; The smallest number that x can begin with if it is not less than 3 is 2
; y starts as 1 as it is 3 - 2 while z is 0 ( 3 - 0 )
; these parameters shift along for each loop while n is reduced by 1 until it is less than 3
( define ( foo n )
         ( define ( foo-iter x y z n )
                  ( if ( < n 3 ) x
                       ( foo-iter ( + x ( * 2 y ) ( * 3 z )) x y ( - n 1 ))))
         ( if ( < n 3 ) n
              ( foo-iter 2 1 0 n )))

;; Pascal's triangle 
; Compute elements of triangle to a certain depth
( define ( pascal col depth )
         ( cond 
           ; out-of-bounds numbers 
           (( or ( > col depth ) ( < col 0 ) ( < depth 0 )) 0 )
           (( = col 0 ) 1 )
           (( = col depth ) 1 )
           ( else ( + ( pascal ( - col 1 ) ( - depth 1 ))
                      ( pascal col ( - depth 1 ))))))
;; Back to Fib
;; Fib( n ) is the closest integer to Ωª / √5
;; where Ω = 1 + √5 / 2 ≈ 1.6180
;; which satisfies the equation Ω² = Ω + 1

( define ( cube x ) ( * x x x ))
( define ( p x ) ( - ( * 3 x ) ( * 4 ( cube x ))))
( define ( sine angle )
         ( if ( not ( > ( abs angle ) 0.1 ))
              angle
              ( p ( sine ( / angle 3.0 )))))

( sine 12.15 )

( define ( expt b n )
         ( if ( = n 1 ) b
              ( * b ( expt b ( - n 1 )))))
( define ( expt b n )
         ( define ( expt-iter b counter product )
                  ( if ( = counter 0 )
                       product
                       ( expt-iter b
                                   ( - counter 1 )
                                   ( * b product ))))
         ( expt-iter b n 1 ))

( define ( fast-expt b n )
         ( cond (( = n 0 ) 1 )
                (( even? n )
                 ( square ( fast-expt b ( / n 2 ))))
                ( else 
                  ( * b ( fast-expt b ( - n 1 ))))))
( define ( even? n )
         ( = ( remainder n 2 ) 0 ))
( fast-expt 5 3 )
( fast-expt 10 5 )

( define ( fast-expt b n )
         ( define ( fast-expt-iter b n a )
                  ( cond (( = n 0 ) a )
                         (( even? n )
                          ( fast-expt-iter b ( - n 2 ) ( * a ( square b ))))
                          ( else 
                            ( fast-expt-iter b ( - n 1 ) ( * a b )))))
         ( fast-expt-iter b n 1 ))
( fast-expt 10 5 )
( expt 10 5 )


#| ( define ( * a b )
         ( if ( = b 0 ) 0
              ( + a ( * a ( - b 1 )))))

( define ( double i )
         ( + i i ))
( define ( halve i )
         ( if ( even? i ) ( / i 2 ) i ))
( * 4 8 )
( * 4 11 )
( define ( * a b )
         ( cond (( = b 0 ) 0 )
                (( even? b )
                 ( double ( * a ( halve b ))))
                ( else 
                  ( + a ( * a ( - b 1 ))))))
( * 4 8 )
( * 4 11 )

( define ( * a b )
         ( define ( *-iter amount a b )
                  ( cond (( = b 0 ) amount )
                         (( even? b )
                          ( *-iter ( amount )
                                   ( double a )
                                   ( halve b )))
                         ( else 
                           ( *-iter ( + amount a )
                                    ( double a )
                                    ( halve ( - b 1 ))))))
         ( *-iter 0 a b )) |#

( define ( fib n )
         ( fib-iter 1 0 0 1 n ))

( define ( fib-iter a b p q count )
         ( cond (( = count 0 ) b )
                (( even? count )
                 ( fib-iter a 
                            b
                            ( + ( square p ) ( square q ))
                            ( + ( * 2 p q ) ( square q ))
                            ( / count 2 )))
                ( else ( fib-iter ( + ( * b q )
                                      ( * a q )
                                      ( * a p ))
                                  ( + ( * b p )
                                      ( * a q ))
                                  p
                                  q
                                  ( - count 1 )))))

( define ( gcd a b )
         ( if ( = b 0 ) a
              ( gcd b ( remainder a b ))))
( gcd 206 40 )

( define ( smallest-divisor n )
         ( find-divisor n 2 ))
( define ( find-divisor n test-divisor )
         ( cond (( > ( square test-divisor ) n ) n )
                (( divides? test-divisor n ) test-divisor )
                ( else ( find-divisor 
                         n
                         ( + test-divisor 1 )))))
( define ( divides? a b )
         ( = ( remainder b a ) 0 ))

( define ( prime? n )
         ( = n ( smallest-divisor n )))

( define ( expmod base exp m )
         ( cond (( = exp 0 ) 1 )
                (( even? exp )
                 ( remainder 
                   ( square ( expmod base ( / exp 2 ) m ))
                   m ))
                ( else
                  ( remainder 
                    ( * base ( expmod base ( - exp 1 ) m ))
                    m ))))
( define ( fermat-test n )
         ( define ( try-it a )
                  ( = ( expmod a n n ) a ))
         ( try-it ( + 1 ( random ( - n 1 )))))
( define ( fast-prime? n times )
         ( cond (( = times 0 ) true )
                (( fermat-test n )
                 ( fast-prime? n ( - times 1 )))
                ( else false )))
( smallest-divisor 199 ) ;199
( smallest-divisor 1999 ) ;1999
( smallest-divisor 19999 ) ;7

( define ( timed-prime-test n )
         ( newline )
         ( display n )
         ( start-prime-test n ( runtime )))
( define ( start-prime-test n start-time )
         ( if ( prime? n )
              ( report-prime ( - ( runtime )
                                start-time ))))
( define ( report-prime elapsed-time )
         ( display " *** " )
         ( display elapsed-time ))

( define ( search-for-primes a b )

