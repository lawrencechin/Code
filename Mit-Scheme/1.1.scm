;;; Exercises 1

10 ;10
( + 5 3 4 ) ;12
( - 9 1 ) ;8
( / 6 2 ) ;3
( + ( * 2 4 ) ( - 4 6 )) ;6
( define a 3 ) ;a
( define b ( + a 1 )) ;b
( + a b ( * a b )) ;19
( = a b ) ;#f
( if ( and ( > b a ) ( < b ( * a b )))
     b
     a ) ;4
( cond (( = a 4 ) 6 )
       (( = b 4 ) ( + 6 7 a ))
       ( else 25 )) ;16
( + 2 ( if ( > b a ) b a )) ;6
( * ( cond (( > a b ) a )
           (( < a b ) b )
           ( else -1 ))
    ( + a 1 )) ;16

( / ( + 5 4 ( - 2 ( - 3 ( + 6 ( / 4 5 )))))
    ( * 3 ( - 6 2 )( - 2 7 ))) 

( define ( larger-num x y )
         ( if ( > x y ) x y ))
( define ( square x ) ( * x x ))
( define ( sum-of-squares x y )
         ( + ( square x )( square y )))
( define ( largest-num-sum-of-squares x y z )
         ( if ( = x ( larger-num x y ))
                  ( sum-of-squares x ( larger-num y z ))
                  ( sum-of-squares y ( larger-num x z ))
                  ))
( largest-num-sum-of-squares 1 2 3 ) ;13
( largest-num-sum-of-squares 10 12 15 ) ;369
( largest-num-sum-of-squares 8 2 6 ) ;100

;; if parameter b is greater than zero then use a plus sign in the expression ( + a b ) otherwise a minus sign ( - a b )
( define ( a-plus-abs-b a b )
         (( if ( > b 0 ) + - ) a b ))

( define ( p ) ( p )) ;procedure 'p' calls itself in the body
( define ( test x y )
         ( if ( = x 0 ) 0 y ))
;; In applicative-order evaluation the procedure will run indefinitely because all arguments are evaluated first. When 'y' is evaluated it will run the procedure and never end. In normal-order evaluation it will run the arguments from left to right and determine that 'x' is equal to 0 and thus return 0 before checking y. Basically don't run this procedure while using the Mit-Scheme interpreter!

( define ( sqrt-iter guess x )
         ( if ( good-enough? guess x )
              guess
              ( sqrt-iter ( improve guess x ) x )))
( define ( improve guess x )
         ( average guess ( / x guess )))
( define ( average x y )
         ( / ( + x y ) 2 ))
( define ( good-enough? guess x )
         ( < ( abs ( - ( square guess ) x )) ( * guess 0.001  )))
( define ( abs x )
         ( if ( < x 0 ) ( - x ) x ))

( define ( sqrt x )
         ( sqrt-iter 1.0 x ))

;; sqrt refactored as an enclosed procedure with lexical scoping
( define ( sqrt x )
         ( define tolerance 0.001 )
         ( define ( good-enough? guess )
                  ( < ( abs ( - ( square guess ) x )) ( * guess tolerance )))
         ( define ( improve guess )
                  ( average guess ( / x guess )))
         ( define ( sqrt-iter  guess )
                  ( if ( good-enough? guess )
                       guess
                       ( sqrt-iter ( improve guess ))))
         ( sqrt-iter 1.0 ))

( sqrt 9 )
( sqrt ( + 100 37 ))
( sqrt ( + ( sqrt 2 ) (sqrt 3 )))
( square ( sqrt 1000 ))

( define ( sqr x )
         ( exp ( double ( log x ))))
( define ( double x )
         ( + x x ))

( sqr 5 )
;; The new form of 'if', which isn't a special form, evaluates the second clause of the if statement ending up calling itself recursively. This will not end! The special form 'if' evaluates the predicate and then determines the result rather than the usual applicative-order execution.

( sqrt 0.0001 )
( sqrt 1000000000000 )
( sqrt 10000000000000 )

; cube root
( define ( cube-iter guess x )
         ( if ( cube-good-enough? guess x )
              ( cube-improve guess x )
              ( cube-iter ( cube-improve guess x ) x )))
( define ( cube-improve guess x )
         ( cube-average ( / x ( square guess )) ( * 2 guess )))
( define ( cube-average x y )
         ( / ( + x y ) 3 ))
( define ( cube-good-enough? guess x )
         ( < ( abs ( - ( cube guess ) x )) ( abs ( * guess 0.001 ))))
( define ( cube x )
         ( * x x x ))
( define ( cube-root x )
         ( cube-iter 1.0 x ))

( cube-root 1 )
( cube-root -8 )
( cube-root -1000 )
( cube-root 1e-30 )
( cube-root 1e60 )
( cube-root 27 )
( cube-root -27 )
