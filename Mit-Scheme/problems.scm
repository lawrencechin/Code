;; Additional Problems solved in Scheme

;; The sum of all multiples of 3 & 5 up to 1000
( define ( multiples_3_5 total counter max-count )
         ( if ( > counter max-count ) total
              ( multiples_3_5 
                ( if ( or ( = 0 ( modulo counter 3 ))( = 0 ( modulo counter 5 )))
                          ( + total counter )
                          total )

                ( + counter 1 ) 
                max-count )))

( multiples_3_5 0 1 999 )
