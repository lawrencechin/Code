/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stacks;

/**
 *
 * @author lawrencechin
 */
public class stack {
    int stackNum;
    Stacks rootPointer;
    stack nextStack;
    static int count = 0;
    
    public stack(){
        count++;
        this.stackNum = 1;
        this.rootPointer = null;
        this.nextStack = null;
    }
    
    public void push(stack s, Stacks rootOfList){
        if(this.rootPointer == rootOfList){
            this.rootPointer = null;
            s.rootPointer = rootOfList;
            s.nextStack = this;
            s.stackNum = this.stackNum +1;
            rootOfList.stack = s;
            System.out.println("Pushing "+s.stackNum+"…"); 
        }
    }
    
    public boolean pop(Stacks rootOfList){
        
        if(this.nextStack == null){
            
            if(count == 0){
                this.empty();
                return false;
            }
            
            count--;
            this.rootPointer = null;
            System.out.println("Popping… It's a "+this.stackNum);
            return false;
        }
        
        if(this.rootPointer == rootOfList){
            rootOfList.stack = this.nextStack;
            this.nextStack.rootPointer = rootOfList;
            count--;
            System.out.println("Popping… It's a "+this.stackNum);
            return false;
        }else{
            return this.nextStack.pop(rootOfList);
        }
    }
    
    public boolean empty(){
        if(count == 0){
            System.out.println("Stack is empty");
            return true;
        }else{
            this.size();
            return false;
        }
    }
    
    public void size(){
        System.out.println("The stack has "+count+" elements within");
    }
}
