/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stacks;

/**
 *
 * @author lawrencechin
 */
public class Stacks {

    stack stack;
    Stacks rootOfList = this;
    
    public void push(stack s, Stacks rootOfList){
        if(this.stack != null && this.stack.rootPointer == null){
            this.stack = null;
        }
        if(this.stack == null){
            this.stack = s;
            this.stack.rootPointer = rootOfList;
            System.out.println("Pushing "+s.stackNum+"…");
        }else{
            this.stack.push(s, rootOfList);
        }
    }
    
    public void pop(){
        if(this.stack == null){
            System.out.println("There are no elements in the stack");
        }else{
            this.stack.pop(this.rootOfList);
        }
    }
    
    public void empty(){
        this.stack.empty();
    }
    
    public static void main(String[] args) {
        Stacks jam = new Stacks();
        jam.push(new stack(), jam.rootOfList);
        jam.empty();
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.empty();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.empty();
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.empty();
        jam.pop();
        jam.push(new stack(), jam.rootOfList);
        jam.push(new stack(), jam.rootOfList);
        jam.pop();
        jam.empty();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.pop();
        jam.empty();
        jam.push(new stack(), jam.rootOfList);
        jam.pop();
        jam.empty();
    }
    
}
