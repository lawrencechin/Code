/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hashutilities;

/**
 *
 * @author lawrencechin
 */
public class HashMapMaster implements HashMap{

    private HashMapHead headList;
    
    public HashMapMaster(){
        headList = null;
    }
    
    public String put(int key, String name){
        HashMapHead current = runList(key, headList, true);
        if(current != null){
            current.insertNode(name, current.getFirst());
        }
        return name;
    }
    // used to parse list of keys, boolean switch for get or set(if we do not
    // add the switch then an irroneous call to remove an object that doesn't exist
    // will actually create that object)
    private HashMapHead runList(int key, HashMapHead head, boolean insert){
        HashMapHead newHead = new HashMapHead(key);
        if(head == null){
            if(insert){
                headList = newHead;
            }
            return headList;
        }else if(head.getKey() == key){
            return head;
        }
        
        if(head.getNext() == null){
            if(insert){
                head.setNext(newHead);
            }
            return head.getNext();
        }else{
            if(insert){
                return runList(key, head.getNext(), true);
            }else{
                return runList(key, head.getNext(), false);
            }
            
        }
    }
    
    public String[] get(int key){
        String[] nameArray;
        HashMapHead current = runList(key, headList, false);
        if(current != null){
            nameArray = current.getNames(current.getFirst());
            return nameArray;
        }else{
            return null;
        }
                       
    }
    
    public void remove(int key, String name){
        HashMapHead current = runList(key, headList, false);
        if(current != null){
            current.removeNode(name, current.getFirst());
        }
        
        if(headList.getFirst() == null){
            headList = headList.getNext();
        }
        
    }
    
    public boolean isEmpty(){
        return headList == null;
    }
}
