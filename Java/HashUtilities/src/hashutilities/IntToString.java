/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hashutilities;

/**
 *
 * @author lawrencechin
 */
public class IntToString implements SimpleMap{
    private int size;
    final private int DEFAULT_CAPACITY = 10;
    private mapNode [] newMap = new mapNode[DEFAULT_CAPACITY];
    
    public void put(int key, String value){
        boolean newEntry = true;
        for(int i=0; i < size; i++){
            if(newMap[i].getKey() == key){
                newEntry = false;
            }
        }
        if(newEntry){
            resizeArray();
            newMap[size++] = new mapNode(key, value);
        }
    }
    
    private void resizeArray(){
        if(size == newMap.length){
            int newSize = newMap.length * 2;
            mapNode [] tempArray = new mapNode[newSize];
            
            for(int i=0; i < size; i++){
                tempArray[i] = newMap[i];
            }
            
            newMap = tempArray;
        }
    }
        
    public String get(int key){
        for(int i=0; i < size; i++){
            if(newMap[i] != null){
                if(newMap[i].getKey() == key){
                    return newMap[i].getValue();
                }
            } 
        }
        return null;
    }
    
    public void remove(int key){
        for(int i = 0; i < size; i++){
            if(newMap[i].getKey() == key){
                newMap[i] = null;
                size--;
                resetArray(i);
            }
        }
    }
    
    private void resetArray(int entry){
        for(int i = entry; i < size; i++){
            newMap[i] = newMap[i+1];
        }
    }
    
    public boolean isEmpty(){
        if(size == 0){
            return true;
        }else{
            return false;
        }
    }
    
    public int getSize(){
        return size;
    }
}
