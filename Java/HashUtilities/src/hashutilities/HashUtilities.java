/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hashutilities;

import java.util.Arrays;

/**
 *
 * @author lawrencechin
 */
public class HashUtilities {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //HashUtilities.shortHashTest();
        //HashUtilities.simpleMapTest();
        HashUtilities.hashMapTest();
    }
    
    static int shortHash(int num){
        /**
         * Takes in any number and returns an integer between 0 - 1000
         * @return
         */
        return Math.abs(num%1000);
    }
    
    static void shortHashTest(){
        System.out.println ("Give me a string and I will calculate its hash code"); 
        String str = "Trevor";
        int hash = str.hashCode();
        int smallHash = HashUtilities.shortHash(hash); 
        System.out.println("0 < " + smallHash + " < 1000");  
    }
    
    static void simpleMapTest(){
        IntToString test = new IntToString();
        
        System.out.println("Is empty? "+test.isEmpty());
        System.out.println("Size: "+test.getSize());
        test.put(10,"King");
        System.out.println("Size: "+test.getSize());
        System.out.println("Is empty? "+test.isEmpty());
        test.put(10,"King");
        test.put(10,"Tring");
        test.put(15,"Drama");
        test.remove(10);
        test.put(10, "Super");
        System.out.println("Value: "+test.get(15));
        test.put(16,"Jam");
        test.put(17,"Monster");
        test.remove(16);
        System.out.println("Value: "+test.get(17));
        test.put(18,"Yikes");
        System.out.println("Size: "+test.getSize());
        test.put(19,"Paste");
        test.put(20,"Timmy");
        test.put(21,"Paisley");
        System.out.println("Value: "+test.get(20));
        test.put(22,"More Kings");
        test.put(23,"Less King");
        test.put(24,"Mo Drama");
        test.remove(21);
        test.remove(15);
        test.remove(16);
        test.remove(22);
        System.out.println("Size: "+test.getSize());
        test.put(16,"Jam");
        test.put(15,"Drama");
        test.put(21,"Paisley");
        System.out.println("Size: "+test.getSize());
        System.out.println("Value: "+test.get(20));
        test.put(200,"J");
        test.put(220,"D");
        test.put(300,"P");
        System.out.println("Size: "+test.getSize());
        test.remove(200);
        test.put(13, "Captain");
        System.out.println("Value: "+test.get(300));
    }
    
    static void hashMapTest(){
        HashMapMaster bloom = new HashMapMaster(); 
        System.out.println("is empty? "+bloom.isEmpty());
        bloom.put(1, "Sammy");
        bloom.put(2, "Jessie");
        bloom.put(1, "Bobby");
        bloom.put(2, "Balmmy");
        bloom.put(1, "Trevor");
        bloom.put(2, "Dang");
        System.out.println(Arrays.toString(bloom.get(1)));
        bloom.put(3,"Tracey");
        bloom.put(4,"Basic Instinct");
        bloom.put(3,"Sliver");
        bloom.put(2,"Tom Berringer");
        bloom.remove(2, "Dang");
        bloom.remove(1, "Sammy");
        bloom.put(1, "Blobby");
        bloom.remove(7, "Test");
        bloom.remove(1, "No Name Johnson");
        bloom.put(4, "Django");
        System.out.println("is empty? "+bloom.isEmpty());
        System.out.println(Arrays.toString(bloom.get(1)));
        System.out.println(Arrays.toString(bloom.get(3)));
        bloom.remove(1, "Blobby");
        bloom.remove(2, "Jessie");
        bloom.remove(1, "Bobby");
        bloom.remove(2, "Balmmy");
        bloom.remove(1, "Trevor");
        bloom.remove(2, "Tom Berringer");
        System.out.println("is empty? "+bloom.isEmpty());
        System.out.println(Arrays.toString(bloom.get(1)));
        System.out.println(Arrays.toString(bloom.get(2)));
        bloom.remove(3,"Tracey");
        bloom.remove(4,"Basic Instinct");
        bloom.remove(3,"Sliver");
        bloom.remove(2,"Tom Berringer");
        bloom.remove(2, "Dang");
        bloom.remove(1, "Sammy");
        bloom.remove(1, "Blobby");
        bloom.remove(7, "Test");
        bloom.remove(1, "No Name Johnson");
        bloom.remove(4, "Django");
        System.out.println(Arrays.toString(bloom.get(1)));
        bloom.put(1, "Gratious");
        System.out.println(Arrays.toString(bloom.get(1)));
    }
}
