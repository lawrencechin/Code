/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hashutilities;

/**
 *
 * @author lawrencechin
 */
/**
* Map from integer to Strings: one to many */
public interface HashMap { 
    /**
    * Puts a new String in the map.
    * @param key
    * @param name
    * @return
    */
    String put(int key, String name);
    /**
    * Returns all the names associated with that key, 
    * or null if there is none.
    * @param key
    * @return
    */
    String[] get(int key);
    /**
    * Removes a name from the map. 
    * @param key
    * @param name
    */
    void remove(int key, String name);
    /**
    * Returns true if there are no workers in the map, 
    * false otherwise
    * @return
    */
    boolean isEmpty(); 

}
