/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hashutilities;

/**
 *
 * @author lawrencechin
 */
public class hashMapNode {
    private String value;
    private hashMapNode next;
    
    public hashMapNode(String value){
        this.value = value;
        this.next = null;
    }
    
    public String getValue(){
        return value;
    }
    
    public hashMapNode getNext(){
        return next;
    }
    
    public void setNext(hashMapNode newNode){
        next = newNode;
    }
}
