/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hashutilities;

/**
 *
 * @author lawrencechin
 */
public class HashMapHead{
    
    private int key;
    private int size;
    private hashMapNode head;
    private HashMapHead next;
    private String temp;
    
    public HashMapHead(int key){
        this.key = key;
        head = null;
        next = null;
        temp = "";
    }
    
    //always pass in 'head' node to start with
    public void removeNode(String value, hashMapNode node){
        if(node == null){ 
        }else if(node.getValue().equals(value)){
            size--;
            head = node.getNext();
        }else if(node.getNext() != null){
            if(node.getNext().getValue().equals(value)){
                size--;
                node.setNext(node.getNext().getNext());
            }else{
                removeNode(value, node.getNext());
            }
        }
        
    }
    //always pass in 'head' node to start with
    public void insertNode(String value, hashMapNode node){
        hashMapNode newNode = new hashMapNode(value);
        if(node == null){
            size++;
            head = newNode;
        }else if(node.getNext() == null){
            size++;
            node.setNext(newNode);
        }else{
            insertNode(value, node.getNext());
        }
    }
    
    public int getKey(){
        return key;
    }
    
    public hashMapNode getFirst(){
        return head;
    }
    
    public HashMapHead getNext(){
        return next;
    }
    
    public void setNext(HashMapHead newHead){
        next = newHead;
    }
    
    public String[] getNames(hashMapNode node){
        String [] names = new String[size];
        populateTemp(node);
        names = temp.split(",");
        temp = "";
        return names;
    }
    
    private void populateTemp(hashMapNode node){
        if(node == null){
        }else{
            temp += (node.getValue()+",");
            populateTemp(node.getNext());
        }
    }
    
}
