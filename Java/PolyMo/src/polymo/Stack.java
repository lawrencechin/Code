/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 * 
 * @author lawrencechin
 * @param <T> 
 */
public class Stack<T extends Number> implements StackInt<T>{
    
    private ListNode head;
    
    public Stack(){
        this.head = null;
    }
    
    @Override
    public void pop(){
        if(head == null){
            System.out.println("Stack is empty");
        }else{
            System.out.println("<<Popping: "+head.getValue());
            head = head.getNextPrev(false);
        }
    }
    
    @Override
    public void push(T value){
        ListNode newNode = new ListNode(value);
        if(head == null){
            System.out.println(">>Pushing: "+newNode.getValue());
            head = newNode;
        }else{
           System.out.println(">>Pushing: "+newNode.getValue());
           newNode.setNextPrev(head, false);
           head = newNode;
        }
    }
    
    @Override 
    public Boolean isEmpty(){
        return head == null ? true : false;
    }
    
    //non-interface method, downcast object if needed
    public void printStack(){
        if(head == null){
            System.out.println("Stack is empty");
        }else{
            printStackInternal(head);
        }
    }
    
    private void printStackInternal(ListNode head){
        if(head == null){
            return;
        }else{
            System.out.println(head.getValue());
            printStackInternal(head.getNextPrev(false));
        }
    }
}
