/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 * 
 * @author lawrencechin
 * adds sorting capabilities to LinkedList overriding
 * the add method
 * @param <T> 
 */
public class SortedLinkedList<T> extends LinkedList<T>{

    public SortedLinkedList(String name) {
        super(name);
    }
    
    @Override 
    public void add(T value){
        ListNode newNode = new ListNode(value);
        //quick note, originally wanted to just call super.add() but since it 
        //calls a private method that method cannot be accessed in this 
        //subclass. So we need to write both methods again here.
        
        if(super.getFirst() == null){
            super.setFirst(newNode);
        }else{
            addInternal(super.getFirst(), newNode);
        }
    }
    
    private void addInternal(ListNode head, ListNode newNode){
        //cast Objects to Comparable(s), can't pass Obj to Comparable method
        Comparable val1 = (Comparable) head.getValue();
        Comparable val2 = (Comparable) newNode.getValue();
        
        if(compareValue(val1, val2)){
            //if newNode is less than head
            //set newNode pointers to head(next) and head's prev(prev)
            newNode.setNextPrev(head, false);
            newNode.setNextPrev(head.getNextPrev(true), true);
            //set head's prev to newNode
            head.setNextPrev(newNode, true);
            //if first then set newNode to first and leave prev(null)
            if(head == super.getFirst()){
                super.setFirst(newNode);
            }else{
                //set newNode prev's next pointer to newNode
                newNode.getNextPrev(true).setNextPrev(newNode, false);
            }
            
        }else if(head.getNextPrev(false) == null){
            newNode.setNextPrev(head, true);
            head.setNextPrev(newNode, false);
            
        }else{
            addInternal(head.getNextPrev(false), newNode);
        }
    }
    
    //uses generic Comparable method to compare objects of unknown class
    private <T extends Comparable> boolean compareValue(T value1, T value2){
        return value1.compareTo(value2) > 0 ? true : false;
    }
}
