/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 * 
 * @author lawrencechin
 * @param <K>
 * @param <V> 
 */
public interface MapInt<K,V> {
    /**
     * links value with key. If value already exists at key then command is ignored
     * @param key
     * @param value 
     */
    void put(K key, V value);
    /**
     * returns value linked with key
     * @param key
     * @return value
     */
    V get(K key);
    /**
     * replaces value linked with key with null, if empty does nothing
     * @param key 
     */
    void remove(K key);
}
