/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 * 
 * @author lawrencechin
 * @param <T> 
 */
public interface StackInt<T> {
    /**
     * remove element from Stack
     * @param obj 
     */
    void pop();
    /**
     * add element to Stack
     * @param obj 
     */
    void push(T obj);
    /**
     * check if the Stack is empty
     * @return 
     */
    Boolean isEmpty();
}
