/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 * 
 * @author lawrencechin
 * @param <T> 
 */
public class ListNode<T> {
    private T value;
    private ListNode next;
    private ListNode prev;
    
    public ListNode(T value){
        this.value = value;
        next = null;
        prev = null;
    }
    
    public T getValue(){
        return value;
    }
    
    //utilises a boolean to determine whether we return prev or next(true = prev)
    public ListNode getNextPrev(Boolean nextPrev){
        return nextPrev? prev : next;
    }
    
    public void setNextPrev(ListNode newNode, Boolean nextPrev){
        if(nextPrev){
            prev = newNode;
        }else{
            next = newNode;
        }
    }
}
