/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;
import java.util.ArrayList;
/**
 * 
 * @author lawrencechin
 * @param <T> 
 */
public class LinkedList implements LinkedListInt{
    //name refers to the name of the list and doesn't need to be generic
    //eg.Employee Name or Employee NI Numbers
    final private String name;
    private ListNode head;
    private LinkedList next;
    
    public LinkedList(String name){
        this.name = name;
        this.head = null;
        this.next = null;
    }
    
    @Override
    //takes in the VALUE eg. String "Name" or Integer 10
    public <T> void add(T value){
        ListNode newNode = new ListNode(value);
        if(head == null){
            head = newNode;
        }else{
            addInternal(head, newNode);
        }
    }
    
    //private method to recursively loop through list
    private void addInternal(ListNode head, ListNode newNode){
        //check if next is null
        if(head.getNextPrev(false) == null){
            //set prev pointer
            newNode.setNextPrev(head, Boolean.TRUE);
            //set heads next pointer to newNode
            head.setNextPrev(newNode, false);
        }else{
            addInternal(head.getNextPrev(false), newNode);
        }
    }
    
    @Override
    public <T> void delete(T value){
        if(head == null){
            return;
        }else{
            deleteInternal(head, value);
        }
    }
    
    private <T> void deleteInternal(ListNode head, T value){
        if(head == null){
            return;
        }else if(head.getValue() == value){
            System.out.println("Value: "+value+" found!!!!");
            if(head.getNextPrev(false) == null){
                if(head == this.head){
                    this.head = null;
                    return;
                }
                head.getNextPrev(true).setNextPrev(head.getNextPrev(false), false);
            }else{
                if(head == this.head){
                    this.head = this.head.getNextPrev(false);
                    this.head.setNextPrev(null, true);
                    return;
                }
                head.getNextPrev(true).setNextPrev(head.getNextPrev(false), false);
                head.getNextPrev(false).setNextPrev(head.getNextPrev(true), true);
            }
        }else{
            deleteInternal(head.getNextPrev(false), value);
        }
    }
    
    @Override
    public void returnList(){
        //run internal list parser wiith direction set to false(traverse forward)
        System.out.println("\n-----*Start*-----");
        returnInternal(head, false);
        System.out.println("-----*End*-----\n");
    }
 
    
    private void returnInternal(ListNode test, Boolean direction){
        //check if next is null, set direction to true/reverse and run in reverse
        if(test.getNextPrev(direction) == null){
            System.out.println(test.getValue());
            direction = true;
            //if prev is null and direction is set to true/reverse, end 
            if(test.getNextPrev(direction) == null && direction){
                return;
            }else{
                returnInternal(test.getNextPrev(direction), direction);
            }
        }else{
            System.out.println(test.getValue());
            returnInternal(test.getNextPrev(direction), direction);
        }
    }
    
    public String getName(){
        return name;
    }
    
    public ListNode getFirst(){
        return head;
    }
    
    public void setFirst(ListNode newNode){
        head = newNode;
    }
    
    public void setNext(LinkedList newList){
        this.next = newList;
    }
    
    public LinkedList getNext(){
        return next;
    }
    
    public int numberTest(ArrayList<?> num) {
	return num.size();
    } 
}
