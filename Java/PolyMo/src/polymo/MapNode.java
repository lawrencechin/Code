/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 * 
 * @author lawrencechin
 * @param <K>
 * @param <V> 
 */
public class MapNode<K, V> {
    final private K key;
    private V value;
    private MapNode next;
    
    public MapNode(K key, V value){
        this.key = key;
        this.value = value;
        this.next = null;
    }
    
    public K getKey(){
        return key;
    }
    
    public V getValue(){
        return value;
    }
    
    public void setValue(V value){
        this.value = value;
    }
    
    public MapNode getNext(){
        return next;
    }
    
    public void setNext(MapNode newNode){
        this.next = newNode;
    }
}
