/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 * 
 * @author lawrencechin
 * @param <K>
 * @param <V> 
 */
public class SimpleMap<K, V> implements MapInt<K, V>{
    
    private MapNode<K,V> head;
    
    public SimpleMap(){
        head = null;
    }
    
    @Override
    public void put(K key, V value){
        MapNode<K, V> newNode = new MapNode(key, value);
        Boolean test = false;
        if(head == null){
            head = newNode;
        }else{
            Comparable var1 = (Comparable) newNode.getKey();
            Comparable var2 = (Comparable) head.getKey();
            if(var2.compareTo(var1) > 0){
                newNode.setNext(head);
                head = newNode;
            }else{
                test = true;
            }
            
        }
        
        if(test){
            putInternal(head, newNode);
        }
        
    }
    
    private void putInternal(MapNode head, MapNode newNode){
        
        if(newNode.getKey() == head.getKey()){
            return; //same key, return, no action
        }else if(head.getNext() != null){
            Comparable var1 = (Comparable) newNode.getKey();
            Comparable var2 = (Comparable) head.getNext().getKey();
            
            if(var1.compareTo(var2) > 0){
                putInternal(head.getNext(), newNode);
            }else{
                newNode.setNext(head.getNext());
                head.setNext(newNode);
            }
        }else{
            head.setNext(newNode);
        }
    }
    
    @Override
    public V get(K key){
      if(head == null){
          return null;
      }else if(head.getKey().equals(key)){
          return head.getValue();
      }else{
          return getInternal(head, key);
      }  
    }
    
    private V getInternal(MapNode<K, V> head, K key){
        if(head.getKey().equals(key)){
            return head.getValue();
        }else if(head.getNext() == null){
            return null;
        }else{
            //can't run this in the method, strange, annoying!
            MapNode<K,V> test = head.getNext();
            return getInternal(test, key);
        }
    }
    
    @Override 
    public void remove(K key){
        if(head == null){
            return;
        }else if(head.getKey().equals(key)){
            if(head.getNext() == null){
                head = null;
            }else{
                head = head.getNext();
            }
        }else{
            removeInternal(head, key);
        }
    }
    
    private void removeInternal(MapNode head, K key){
        if(head.getNext() != null){
            if(head.getNext().getKey().equals(key)){
                head.setNext(head.getNext().getNext());
            }else{
                removeInternal(head.getNext(), key);
            }
        }
    }
}
