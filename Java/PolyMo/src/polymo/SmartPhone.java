/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 *
 * @author lawrencechin
 */
public class SmartPhone extends MobilePhone{
    
    
    public SmartPhone(String brand){
        super(brand);
    }
    
    public void browseWeb(String str){
        System.out.println("\n You are browsing: "+str);
    }
    
    public void findPosition(){
        System.out.println("\n Searching for current location…");
        System.out.println("Found! You are currently in Mozambique");
    }
    //a better way to do this?
    public void playGame(String str){
        System.out.println("\n Player one has entered game: "+str);
    }
    
    @Override 
    public void call(String number){
        if(number.indexOf("00") == 0){
            System.out.println("Calling "+number+" through the internet to save money");
        }else{
            super.call(number);
        }
    }
}
