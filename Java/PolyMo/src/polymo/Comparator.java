/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 *
 * @author lawrencechin
 * 
 */
public class Comparator {
    /**
     * 
     * @param <T>
     * @param var1
     * @param var2
     * @return 
     */
    public static <T extends Comparable> T getMax(T var1, T var2){
        if(var1.getClass() != var2.getClass()){
            System.out.println("Incompatible types, please try again");
            return var1;
        }else if(var1.compareTo(var2) > 0){
            return var1;
        }else{
            return var2;
        }
    }
}
