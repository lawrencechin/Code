/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 *
 * @author lawrencechin
 */
public class Company {
    private LinkedList listHead;
    
    public Company(){
        listHead = null;
    }
    
    private LinkedList returnNamedList(String name){
        if(listHead.getName().equals(name)){
            return listHead;
        }else if(listHead.getNext().getName().equals(name)){
            return listHead.getNext();
        }else{
            return null;
        }
    }
    public <T> void addList(String name){
        //LinkedList<T> newList = new LinkedList<T>(name);
        //calling a new Sorted object to test sorting
        LinkedList newList = new SortedLinkedList(name);
        //A degree of cheating! We know that there will only be two lists so 
        // I haven't written a fully fledged add method here that potentially 
        //loops through many more list objects
        if(listHead == null){
            listHead = newList;
        }else if(listHead.getNext() == null){
            listHead.setNext(newList);
        }
    }
    
    public <T> void addDelete(String name, T value, String addDelete){
        if(returnNamedList(name) != null){
            //check if we want to add or delete using addDelete param
            if(addDelete.equals("add")){
                returnNamedList(name).add(value);
            }else if(addDelete.equals("delete")){
                returnNamedList(name).delete(value);
            }
        }else{
            System.out.println("List not found, please try again");
        }
    }
    
    public void returnList(String name){
        if(returnNamedList(name) != null){
            returnNamedList(name).returnList();
        }
    }

}
