/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

/**
 * 
 * @author lawrencechin
 */
public interface LinkedListInt {
    
    <T> void add(T obj);
    
    <T> void delete(T obj);
    
    void returnList();
    
}
