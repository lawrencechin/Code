 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polymo;

import java.util.ArrayList;

public class PolyMo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(Comparator.getMax(1.67, 1.77));
        //phone();
        //testPhone(new SmartPhone("Motorola"));
        //testPhone(new MobilePhone("Sony Ericsson")); -cannot downcast MobilePhone to SmartPhone
        
        LinkedList();
        
        //companyTest();
        
        //Stacks();
        
        //StringMap();

    }

    
    public static <T> int shortHash(T value){
        //use this to generate KEY from VALUE in map exercises
        return Math.abs((value.hashCode()))%1000;
    }
    
    public static void phone(){
        MobilePhone myPhone = new SmartPhone("Nokia");
        myPhone.call("00727762227");
        myPhone.call("123123123");
        myPhone.call("12345678987654");
        myPhone.call("5792749 93837");
        myPhone.call("009 8674 766");
        myPhone.call("7867 763 373");
        myPhone.call("056787");
        myPhone.call("890");
        myPhone.call("999");
        myPhone.call("911");
        myPhone.call("001");
        myPhone.call("34");
        myPhone.call("911");
        
        //these methods belong to smartphone so one cannot call them
        //we downcast myPhone to a SmartPhone and now have access to 
        //Smartphone methods 
        System.out.println("\n> Downcasting myPhone to a SmartPhone obj <");
        SmartPhone cleverPhone = (SmartPhone) myPhone;
        cleverPhone.browseWeb("http://www.jam.com");
        cleverPhone.findPosition();
        cleverPhone.playGame("Battle arena: Monster Slash");
        
        cleverPhone.ringAlarm("7:00 Wake Up Call");
        cleverPhone.printLastNumbers();
        System.out.println("\n Brand: "+myPhone.getBrand());
    }
    
    public static void testPhone(Phone phone){
        System.out.println("\nRunning testPhone");
        phone.call("000 678 978");
        //the only method avaliable to phone is call even though we've created
        //a smartphone object
        System.out.println("\n> Downcasting myPhone to a SmartPhone obj <");
        SmartPhone smarty = (SmartPhone) phone;
        //now we have downcasted the object revealing Smartphone methods
        smarty.browseWeb("http://supercaptains.com");
    }
    
    public static void LinkedList(){
        //used to test the linked list implementation before using it with
        //the company class
        
        LinkedListInt IntTest = new LinkedList("Another test");
        //IntTest is cast(?) from the interface and doesn't have access to the 
        //getName() method so we downcast to LinkedList to gain access to that 
        //method
        LinkedList downCastList = (LinkedList) IntTest;
        downCastList.getName();
        IntTest.add(10);
        IntTest.add(20);
        IntTest.add(30);
        IntTest.add(50);
        IntTest.returnList();
        IntTest.delete(30);
        IntTest.delete(10);
        IntTest.returnList();
        IntTest.add(500);
        IntTest.delete(4000);
        IntTest.returnList();
        
        LinkedList StringTest = new LinkedList("The best test");
        LinkedList downCastStr = (LinkedList) StringTest;
        downCastStr.getName();
        StringTest.add("10");
        StringTest.add("20");
        StringTest.add("30");
        StringTest.add("50");
        IntTest.returnList();
        StringTest.delete("30");
        StringTest.delete("10");
        StringTest.returnList();
        StringTest.add("500");
        StringTest.delete("4000");
        StringTest.returnList();
        
        ArrayList<Integer> test = new ArrayList(10);
        test.add(1);
        test.add(2);
        test.add(5);
        System.out.println(StringTest.numberTest(test));
    }
    
    public static void companyTest(){
        Company KingInc = new Company();
        KingInc.addList("Employees");
        KingInc.addList("National Insurance");
        KingInc.addList("Born to fail");//won't be inserted
        KingInc.addDelete("Employees", "Dave", "add");
        KingInc.addDelete("Employees", "Jane", "add");
        KingInc.addDelete("Employees", "Barry", "add");
        KingInc.addDelete("Employees", "Julie", "add");
        KingInc.addDelete("Employees", "Trevor", "add");
        KingInc.addDelete("Employees", "Gina", "add");
        KingInc.addDelete("Employees", "Sebastian", "add");
        KingInc.addDelete("Employees", "Rebecca", "add");
        //output will be sorted if calling SortedLinkedList
        KingInc.returnList("Employees");
        KingInc.addDelete("National Insurance", 5, "add");
        KingInc.addDelete("National Insurance", 7, "add");
        KingInc.addDelete("National Insurance", 2, "add");
        KingInc.addDelete("National Insurance", 6, "add");
        KingInc.addDelete("National Insurance", 1, "add");
        KingInc.addDelete("National Insurance", 3, "add");
        KingInc.addDelete("National Insurance", 4, "add");
        KingInc.returnList("National Insurance");
        //output will be sorted if calling SortedLinkedList
        KingInc.addDelete("Employees", "Jane", "delete");
        KingInc.addDelete("Employees", "Dave", "delete");
        KingInc.returnList("Employees");
        
        KingInc.addDelete("National Insurance", 2, "delete");
        KingInc.addDelete("National Insurance", 5, "delete");
        KingInc.addDelete("National Insurance", 7, "delete");
        KingInc.returnList("National Insurance");
    }
    
    public static void Stacks(){
        StackInt newStack = new Stack();
        
        newStack.pop();
        System.out.println("Stack is empty? - "+newStack.isEmpty());
        newStack.push(10);
        //newStack.push("Test"); //should throw an exception, does throw an exception 
        newStack.push(50);
        newStack.push(45.7);
        Stack castStack = (Stack) newStack;
        castStack.printStack();
        newStack.push(1);
        newStack.push(500);
        newStack.push(56.89);
        newStack.push(34.8);
        newStack.push(12);
        castStack.printStack();
        castStack.pop();
        castStack.pop();
        castStack.pop();
        castStack.pop();
        castStack.pop();
        castStack.pop();
        castStack.pop();
        castStack.pop();
        System.out.println("Stack is empty? - "+castStack.isEmpty());
    }
    
    public static void simpleMap(){
        MapInt<Integer, String> newMap = new SimpleMap();
        newMap.put(shortHash("Super"), "Super");
        newMap.put(shortHash("I am simple"), "I am simple");
        newMap.put(shortHash("Captain"), "Captain");
        newMap.put(shortHash("Super"), "Super");
        newMap.put(shortHash("Paolo"), "Paolo");
        newMap.put(shortHash("Susyanne"), "Susyanne");
        newMap.put(shortHash("Ming"), "Ming");
        System.out.println(newMap.get(shortHash("Super")));
        System.out.println(newMap.get(shortHash("Ming")));
        System.out.println(newMap.get(shortHash("Shouldn't work")));
        newMap.remove(shortHash("Super"));
        newMap.remove(shortHash("Super"));
        newMap.remove(shortHash("Super"));
        System.out.println(newMap.get(shortHash("Super")));
    }
    
    public static void StringMap(){
        MapInt<String, Integer> newMap = new SimpleMap();
        newMap.put("Super", shortHash("Super"));
        newMap.put("I am simple", shortHash("I am simple"));
        newMap.put("Captain", shortHash("Captain"));
        newMap.put("Super", shortHash("Super"));
        newMap.put("Paolo", shortHash("Paolo"));
        newMap.put("Susyanne", shortHash("Susyanne"));
        newMap.put("Ming", shortHash("Ming"));
        System.out.println(newMap.get("Super"));
        System.out.println(newMap.get("Ming"));
        System.out.println(newMap.get("Shouldn't work"));
        newMap.remove("Super");
        newMap.remove("Super");
        System.out.println(newMap.get("Super"));
    }
}
