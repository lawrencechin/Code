const AJAX_JSON_Req = ( url, callback ) => {
    const AJAX_req = new XMLHttpRequest( );
    AJAX_req.open( "GET", url, true );
    AJAX_req.setRequestHeader( "Content-type", "application/json" );
 
    AJAX_req.onreadystatechange = function(){
        if( AJAX_req.readyState == 4 && AJAX_req.status == 200 ){
            const response = JSON.parse( AJAX_req.responseText );
            callback( response );
        }
    };

    AJAX_req.send();
};

const tally = () => {
    let total = 0;

    const increment = () => { total += 1; };
    const getTotal = () => total;

    return { increment, getTotal };
};

const buildXs = () => {
    const xs = [];

    const addToList = ( ...value ) => { 
        value.forEach( v => xs.push( v ));
    };
    const getValue = idx => xs[ idx ];
    const getList = () => xs;

    return { addToList, getValue, getList };
};

const printData = ( elem, data ) => {
    elem.innerHTML = data;
};
 
const compareAnswer = ( userAns, correctAns ) => {
    let correct = false;

    if( correctAns instanceof Array )
        correct = userAns.length === correctAns.length 
            && correctAns.every(( v, i ) => userAns[ i ] == v );
    else
        correct = userAns[ 0 ] == correctAns;

    return correct;
};


const revealAnsContainer = ( ansContElem, startBtn, contBtn ) => {
    ansContElem.classList.remove( "hide" );
    startBtn.classList.add( "hide" );
    contBtn.classList.remove( "hide" );
};

const populateAnswers = ( qNumberElem, qContainer, ansElems, inputElemParent ) => question => {
    inputElemParent.children[ 0 ].value = "";
    printData( qNumberElem, question.number ); 
    printData( qContainer, question.question );

    if( question.qType !== "multiple_choice" ){
        ansElems.forEach( ae => {
            printData( ae, "" );
            ae.parentElement.classList.add( "hide" );
        });
        inputElemParent.classList.add( "show" );
    } else {
        inputElemParent.classList.remove( "show" );
            ansElems.forEach(( ae, i ) => {
                if( question.answers[ i ] === undefined ){
                    printData( ae, "" ); 
                } else {
                    printData( ae, question.answers[ i ]);
                }

                ae.parentElement.classList.remove( "hide" );
            });
    }
};

const selectAnswer = evt => {
    evt.preventDefault();
    const tar = evt.target;
    tar.classList.toggle( "active" );
    if( tar.classList.contains( "active" ))
        tar.nextSibling.checked = true;
    else
        tar.nextSibling.checked = false;
};

const deselect = ( checkBoxElems, answerElems ) => { 
    checkBoxElems.forEach( cb => cb.checked = false );
    answerElems.forEach( a => a.classList.remove( "active" ));
};

const submit = ( checkboxElems, inputElem, questionType ) => {
    const answerVal = [];

    if( questionType !== "multiple_choice" )
        answerVal.push( inputElem.value );
    else
        checkboxElems.forEach( cb => {
            if( cb.checked )
                answerVal.push( cb.value );
        });
    
    return answerVal ? answerVal : false;
};

const printCorrectAnswers = ( answers, correct ) => correct.reduce(( str, a ) => str += answers[ parseInt( a )] + ", ", "" );

const collectAnswers = ( score, answers, questions ) => {
    let finalStr = `<p id="finalScore">You scored <span id="score">${ score } / ${ questions.length }</span></p>`;

    questions.forEach(( q, i ) => {
        finalStr += `
            <div class="question"><p><span>${ q.number }:</span> ${ q.question }</p><br>
            <span class="userAnswer"><b>Answer: </b>${ answers ? printCorrectAnswers( q.answers, answers[ i ]) : "N/A" }<span>
            <span class="correctAnswer"> <b>Correct: </b>
        `;

        if( q.correct instanceof String )
            finalStr = `${ finalStr }${ q.correct }</span></div>`;
        else if( q.correct instanceof Array )
            finalStr = `${ finalStr }${ printCorrectAnswers( q.answers, q.correct)}</span></div>`;
        else
           finalStr = `${ finalStr }${ q.answers[ q.correct ]}</span></div>`;
    });

    return finalStr;
};

const init = res => {
    // Object init
    const questions = res.questions;
    const answers = buildXs();
    const score = tally();
    let iterator = 0;

    // DOM elements
    const startBtn = document.getElementById( "start" );
    const contBtn = document.getElementById( "contButton" );
    const skipBtn = document.getElementById( "skip_ahead" );
    const ansContainer = document.getElementById( "answers" );
    const checkBoxes = document.querySelectorAll( "input[type=checkbox]" );
    const answerElems = document.querySelectorAll( "#answers a" );
    const lastAnsElem = document.getElementById( "last" );
    const questionElem = document.getElementById( "questionReplace" );
    const questionNumElem = document.getElementById( "number" );
    const main = document.getElementById( "main" );

    // Curried Functions
    const _populateAnswers = populateAnswers( questionNumElem, questionElem, answerElems, lastAnsElem );

    startBtn.addEventListener( "click", () => {
        revealAnsContainer( ansContainer, startBtn, contBtn );
        _populateAnswers( questions[ iterator ]);
    });

    contBtn.addEventListener( "click", () => {
        const ans = submit( checkBoxes, lastAnsElem.children[ 0 ], questions[ iterator ].qType );

        if( !ans ) alert( "Please select an answer" );
        else {
            answers.addToList( ans );
            if( compareAnswer( ans, questions[ iterator ].correct ))
                score.increment();
            if( iterator === questions.length - 1 ){
                printData( main, collectAnswers(
                    score.getTotal(),
                    answers.getList(),
                    questions
                ));
            } else {
                deselect( checkBoxes, answerElems );
                _populateAnswers( questions[ ++iterator ]);
            }
        }
    });

    skipBtn.addEventListener( "click", () => {
        printData( main, collectAnswers(
            0,
            false,
            questions
        ));
    });

    answerElems.forEach( a => a.addEventListener( "click", selectAnswer ));
};

AJAX_JSON_Req( './js/json/questions.json', init ); 
