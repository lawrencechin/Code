/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package listutilities;

/**
 *
 * @author lawrencechin
 */
public class listItem {
    private int num;
    private listItem nextItem;
    //used for cocktail sort to go backwards through the list
    private listItem previousItem;
    //used for bubble and cocktailSort functions, once the counter deincrements
    //to zero then we know we've traversed and ordered every element in the list
    static int arrayLength;
    //used for cocktailSort - must be static as we only want one shared boolean,
    //if we create a boolean for each object the logic of the while loop fails
    static boolean reverseDirection;
    
    public listItem(int num){
        this.num = num;
        nextItem = null;
        previousItem = null;
        reverseDirection = false;
    }   
    
    public void setArrayCount(int count){
        arrayLength = count;
    }
    
    public void quickSort(){
    
    }
    
    //traverse forward setting highest number then back setting lowest number
    public void cocktailSort(){
            //secondary while checking for direction boolean
            while(!reverseDirection && arrayLength != 0){
                
                if(this.nextItem == null){
                    arrayLength--;
                    reverseDirection = true;
                    //recursive call from last listItem not first!
                    this.previousItem.cocktailSort();
                    
                }else if(this.num > this.nextItem.num){
                    int tempNum = this.num;
                    this.num = this.nextItem.num;
                    this.nextItem.num = tempNum;
                    this.nextItem.cocktailSort();
                    
                }else{
                    this.nextItem.cocktailSort();
                }
            }
            
            //switch to reversedirection traversal
            while(reverseDirection && arrayLength != 0){
                //checks for firstItem
                if(this.previousItem == null){
                    arrayLength--;
                    //set direction boolean back to false
                    reverseDirection = false;
                    //run from start again
                    this.nextItem.cocktailSort();
                    
                }else if(this.num < this.previousItem.num){
                    int tempNum = this.num;
                    this.num = this.previousItem.num;
                    this.previousItem.num = tempNum;
                    this.previousItem.cocktailSort();
                }else{
                    
                    this.previousItem.cocktailSort();
                }
            }
        }
    
    public void bubbleSort(listItem first){
        while(arrayLength != 0){
            
            if(this.nextItem == null){
                arrayLength--;
                //run again from the firstItem, crucial
                first.bubbleSort(first);
                
            }else if(this.num > this.nextItem.num){
                int tempNum = this.num;
                this.num = this.nextItem.num;
                this.nextItem.num = tempNum;
                this.nextItem.bubbleSort(first);
                
            }else{
                this.nextItem.bubbleSort(first);
            }
        }
        
    }
    
    //array to list unsorted - UPDATE - added a prevItem field for cocktail sort
    public void add(listItem newItem){
        if(this.nextItem == null){
            newItem.previousItem = this;
            this.nextItem = newItem;
        }else{
            this.nextItem.add(newItem);
        }
    }
    
    //array to list sorted low to high
    public void reOrder(listItem newItem){
        if(this.nextItem == null){
            this.nextItem = newItem;
        }else{
            this.nextItem.reOrder(newItem);
        }  
        
        if(this.num > this.nextItem.num){
            int tempNum = this.num;
            this.num = this.nextItem.num;
            this.nextItem.num = tempNum;           
        }     
    }

    //parse data for test purposes
    public boolean getData(){
        if(this.nextItem == null){
            System.out.println(this.num+"\n");
            return false;
        }else{
            System.out.println(this.num);
            return this.nextItem.getData();
        }
    }
}
