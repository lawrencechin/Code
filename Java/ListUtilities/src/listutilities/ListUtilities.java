/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package listutilities;

/**
 *
 * @author lawrencechin
 */
public class ListUtilities {
    private listItem firstItem;
    
    static ListUtilities arrayToList(int[] array, String option){
        ListUtilities arrayList = new ListUtilities();
        
        Boolean choice = option.toLowerCase().equals("sorted") ? true : false;
        
        arrayList.firstItem = new listItem(array[0]);
        arrayList.firstItem.setArrayCount(array.length);
        
        for(int i=1; i < array.length; i++){
            listItem newItem = new listItem(array[i]);
            if(!choice){
                arrayList.firstItem.add(newItem);
            }else{
                arrayList.firstItem.reOrder(newItem); 
            }
        }
        
//        arrayList.getData();
        return arrayList;
    }
    
    static void bubbleSort(ListUtilities list){
        //run arrayToList to generate object as parameter, array must be 'unsorted'
        System.out.println("Bubble Sorted List: (time to run - "+System.currentTimeMillis()+")\n");
        list.firstItem.bubbleSort(list.firstItem);
        list.getData();
    }
    
    static void cocktailSort(ListUtilities list){
        System.out.println("Cocktail Sorted List: (time to run -"+System.currentTimeMillis()+")\n");
        list.firstItem.cocktailSort();
        list.getData();
    }
    
    static void quickSort(ListUtilities list){
        System.out.println("Quick Sorted List: (time to run -"+System.currentTimeMillis()+")\n");
        list.firstItem.quickSort();
        list.getData();
    }
    
    public void getData(){
       //helper method, runs listItem method but makes it easier to type
       this.firstItem.getData();
   }
    
    public static void main(String[] args) {
        
        int[] test = newRandomArray.randomArray();
        
        //options: "unsorted" || "sorted"
        ListUtilities best = ListUtilities.arrayToList(test, "unsorted");
        ListUtilities.bubbleSort(best);
        ListUtilities.cocktailSort(best);
    }
    
}
