/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package listutilities;

/**
 *
 * @author lawrencechin
 */
public class listItem {
    private int num;
    private listItem nextItem;
    private listItem previousItem;
    static boolean reverseDirection;
    static boolean swap;
    
    public listItem(int num){
        this.num = num;
        nextItem = null;
        previousItem = null;
        reverseDirection = false;
        swap = false;
    }   
    
    public void quickSort(){
    
    }
    
    public void cocktailSort(){
        //uses if statement rather than while, while loop breaks using new method
        //with recursion
        if(!reverseDirection){
            if(this.nextItem == null){
                //set direction at last list element    
                reverseDirection = true;
                //check if swap hasn't occured
                if(!swap){return;}
                //set swap to false for next loop
                swap = false;
                //run on previousItem (this current item is highest number)
                this.previousItem.cocktailSort();
                    
            }else if(this.num > this.nextItem.num){
                int tempNum = this.num;
                this.num = this.nextItem.num;
                this.nextItem.num = tempNum;
                //swap occured! set bool to true
                swap = true;
                this.nextItem.cocktailSort();
            }else{
                //no swap occured, boo! run next item
                this.nextItem.cocktailSort();
            }
        }else{
            if(this.previousItem == null){
                //start of list, reverseDirection
                reverseDirection = false;
                //check if swap occured during loop
                if(!swap){return;}
                //set back to false
                swap = false;
                //run nextitem(we know the first item is now the smallest element)
                this.nextItem.cocktailSort();
                    
            }else if(this.num < this.previousItem.num){
                int tempNum = this.num;
                this.num = this.previousItem.num;
                this.previousItem.num = tempNum;
                //swap occured! change bool to true
                swap = true;
                this.previousItem.cocktailSort();
            }else{
                //no swap, drat! run previous item
                this.previousItem.cocktailSort();
            }
        }    
    }
    
    public void bubbleSort(listItem first, ListUtilities list){
        //swap is only set in two places covering a single loop
        if(this.nextItem == null){
            System.out.println(swap);
            //it is only set to true if a 'swap' is made
            //if no swaps are made in a single loop then the list is sorted
            if(!swap){
                return;
            }
            //when we get to the end of a loop, we check for swap otherwise
            //set it to false to start a new loop
            swap = false;
            first.bubbleSort(first, list);
            
        }else if(this.num > this.nextItem.num){
            swap = true;
            this.nextItem.bubbleSort(first, list);
        }else{
            this.nextItem.bubbleSort(first, list);
        }
        
    }
    
    public void add(listItem newItem){
        if(this.nextItem == null){
            newItem.previousItem = this;
            this.nextItem = newItem;
        }else{
            this.nextItem.add(newItem);
        }
    }
    
    public void reOrder(listItem newItem){
        if(this.nextItem == null){
            this.nextItem = newItem;
        }else{
            this.nextItem.reOrder(newItem);
        }  
        
        if(this.num > this.nextItem.num){
            int tempNum = this.num;
            this.num = this.nextItem.num;
            this.nextItem.num = tempNum;
        }     
    }
    
    public boolean getData(){
        if(this.nextItem == null){
            System.out.println(this.num+"\n");
            return false;
        }else{
            System.out.print(this.num+" | ");
            return this.nextItem.getData();
        }
    }
}
