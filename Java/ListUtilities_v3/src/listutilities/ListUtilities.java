/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package listutilities;

/**
 *
 * @author lawrencechin
 */
//version 3 of listUtilities - apparently it's more efficient to switch nodes 
//rather than node data so let's see if we can increase array size by reverting

public class ListUtilities {
    private listItem firstItem;
    
    static ListUtilities arrayToList(int[] array, String option){
        ListUtilities arrayList = new ListUtilities();
        
        Boolean choice = option.toLowerCase().equals("sorted") ? true : false;
        
        arrayList.firstItem = new listItem(array[0]);
        
        for(int i=1; i < array.length; i++){
            listItem newItem = new listItem(array[i]);
            if(!choice){
                arrayList.firstItem.add(newItem);
            }else{
                arrayList.firstItem.reOrder(newItem); 
            }
        }
        
        return arrayList;
    }
    
    static void bubbleSort(ListUtilities list){
        list.firstItem.bubbleSort(list.firstItem, list);
        list.firstItem.getData();
        System.out.println("Bubble Sorted List: (time to run - "+System.currentTimeMillis()+")\n");
    }
    
    static void cocktailSort(ListUtilities list){
        list.firstItem.cocktailSort();
        list.firstItem.getData();
        System.out.println("Cocktail Sorted List: (time to run - "+System.currentTimeMillis()+")\n");
    }
    
    static void quickSort(ListUtilities list){
        System.out.println("Quick Sorted List: (time to run - "+System.currentTimeMillis()+")\n");
        list.firstItem.quickSort();
        list.firstItem.getData();
    }
    
    public void setFirstItem(listItem newItem){
        this.firstItem = newItem;
    }
    
    public static void main(String[] args) {
        
//        int[] test = newRandomArray.randomArray();
        
        int[] test = {56,23,10,14,60,2};
        
        //options: "unsorted" || "sorted"
        ListUtilities best = ListUtilities.arrayToList(test, "unsorted");
        ListUtilities.bubbleSort(best);
//        ListUtilities.cocktailSort(best);
    }
}
