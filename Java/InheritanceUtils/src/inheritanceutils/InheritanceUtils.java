/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inheritanceutils;

/**
 *
 * @author lawrencechin
 */
public class InheritanceUtils {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        phone();
        guitar();
        teaching("Dave", "Java City", "Computer Science");
    }
    
    public static void phone(){ 
        RestrictedSmartPhone iPhone = new RestrictedSmartPhone("Apple");
        iPhone.call("00727762227");
        iPhone.call("123123123");
        iPhone.call("12345678987654");
        iPhone.call("5792749 93837");
        iPhone.call("009 8674 766");
        iPhone.call("7867 763 373");
        iPhone.call("056787");
        iPhone.call("890");
        iPhone.call("999");
        iPhone.call("911");
        iPhone.call("001");
        iPhone.call("34");
        iPhone.call("911");
        
        iPhone.browseWeb("http://www.jam.com");
        iPhone.findPosition();
        iPhone.playGame("Battle arena: Monster Slash");
        iPhone.ringAlarm("7:00 Wake Up Call");
        iPhone.printLastNumbers();
        System.out.println("\n Brand: "+iPhone.getBrand());
    }
    
    public static void guitar(){
        Guitar spanishGuitar = new Guitar();
        
        spanishGuitar.play();
        spanishGuitar.burn();
    }
    
    public static void teaching(String name, String lessonName, String topic){
        Lecturer king = new Lecturer(name);
        System.out.println("\nName: "+king.getName());
        king.teach(lessonName);
        king.doResearch(topic);
    }
    
}
