/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inheritanceutils;

/**
 *
 * @author lawrencechin
 */
public class Guitar {
    final private WoodenObject wo;
    final private MusicalInstrument mi;
    
    public Guitar(){
        wo = new WoodenObject();
        mi = new MusicalInstrument();
    }
    
    public void play(){
        mi.play();
    }
    
    public void burn(){
        wo.burn();
    }
    
}
