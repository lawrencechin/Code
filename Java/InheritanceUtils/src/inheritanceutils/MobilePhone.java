/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inheritanceutils;

/**
 *
 * @author lawrencechin
 */
public class MobilePhone extends OldPhone{
    
    private final String[] lastDialledNumbers;
    private int count;
    
    public MobilePhone(String brand){
        super(brand);
        lastDialledNumbers = new String[10];
        count = 0;
    }
    
    @Override
    public void call(String number){
        super.call(number);
        
        if(count == 10){
            count = 0;
        }
        
        lastDialledNumbers[count] = (count+1)+" : #"+number;
        count++;
        
    }
    
    public void ringAlarm(String str){
        System.out.println("\n Ding Ding Ding! Alarm:" +str);
    }
    
    private void playGame(String str){
        System.out.println("\n Player one has entered game: "+str);
    }
    
    public void printLastNumbers(){
        System.out.println("\n The last numbers you dialled are: \n");
        for(int i = 0; i < 10; i++){
            System.out.println(lastDialledNumbers[i]);
        }
    }
}
