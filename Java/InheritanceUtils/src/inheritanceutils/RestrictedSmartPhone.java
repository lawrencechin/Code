/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inheritanceutils;

/**
 *
 * @author lawrencechin
 */
public final class RestrictedSmartPhone extends SmartPhone{
    
    public RestrictedSmartPhone(String brand){
        super(brand);
    }
    
    
    //can't override public method playGame() with private access privileges 
    //as you cannot assign a weaker privilege (public>package>protected>private)
    //alternatively:
    
    @Override 
    public void playGame(String str){
        System.out.println("Restricted. You may not play: "+str);
    }
    
    //we can however run a different method that doesn't 'run the game'
}
