/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inheritanceutils;

/**
 *
 * @author lawrencechin
 */
public class Teacher {
    private String name;
    public Teacher(String name) {
        this.name = name; 
    }
    
    public String getName() { 
        return name;
    }
    
    public void teach(String lessonName) {
        System.out.println("\nTeaching lesson: " + lessonName); 
    }
}
