/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inheritanceutils;

/**
 *
 * @author lawrencechin
 */
public class OldPhone implements Phone{
    
    final private String brand;
    
    public OldPhone(String brand){
        this.brand = brand;
    }
    
    public String getBrand(){
        return brand;
    }
    
    @Override
    public void call(String number){
        System.out.println("Calling "+number+"…");
    }
}
