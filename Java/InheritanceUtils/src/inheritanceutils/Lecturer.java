/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inheritanceutils;

/**
 *
 * @author lawrencechin
 */
public class Lecturer extends Teacher{
    //no constructor, super() doesn't contain the parameter 'name' required to
    //construct the teacher object. So…
    
    public Lecturer(String name){
        //…we pass in the name to the super constructor
        super(name);
    }
    
    public void doResearch(String topic) { 
        System.out.println("\nDoing research on: " + topic);
    }
}
