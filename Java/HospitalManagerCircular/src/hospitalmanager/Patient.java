package hospitalmanager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lawrencechin
 */
public class Patient {
	private String name; 
	private int age;
	private String illness; 
	private Patient nextPatient;
        private int patientNumber;
        private static int patCount = 1;

	public Patient(String name, int age, String illness) { 

		this.name = name;
		this.age = age;
		this.illness = illness;
		this.nextPatient = null;
        this.patientNumber = 1;
	}
        
        public boolean getData(Patient patient){
            if (patient.nextPatient.patientNumber == 1){   
                System.out.println("No." +patient.patientNumber + " | Name: "+ patient.name + " | " + "Age: "+patient.age+ " | "+"Illness: "+patient.illness+"\n");
                return false;
            }else{
                System.out.println("No." +patient.patientNumber + " | Name: "+ patient.name + " | " + "Age: "+patient.age+ " | "+"Illness: "+patient.illness+"\n");

                return this.getData(patient.nextPatient);
            }                  
        }
        

	public void addPatient(Patient newPatient) {
            
            if(this.patientNumber == 1 && newPatient.nextPatient == null){
                
                newPatient.nextPatient = this;
            }

            if(newPatient.patientNumber == 1 && this.patientNumber == patCount) {
                // this means this is the last patient in the list 
                this.nextPatient = newPatient;
                        
                patCount++;
                
                this.patientNumber = patCount-1;
                newPatient.patientNumber = patCount;
 
            } else { 
                this.nextPatient.addPatient(newPatient);
            } 
	}

	public boolean deletePatient(Patient patient) { 
		if (this.nextPatient.patientNumber == 1) {
                    // patient to remove was not found
                    return false;
		} else if (
                    this.nextPatient.name.equals(patient.name)) {
                    // We found it! It is the next one!
                    // Now link this patient to the one after the next 
                    this.nextPatient = nextPatient.nextPatient; 
                    patCount--;
                    this.nextPatient.deincrementPatient(this.nextPatient);
                    return true;
                    
		} else {
                    return this.nextPatient.deletePatient(patient);
		} 
	}
        
        public boolean deincrementPatient(Patient patient){
            if(this.patientNumber == 1){
                return false;
            }else{
                this.patientNumber = this.patientNumber -1;
                return patient.nextPatient.deincrementPatient(patient.nextPatient);
            }
        }
        
        public boolean patientCount(Patient patient){
            if(patient.nextPatient.patientNumber == 1){
                System.out.println("The total number of patients is "+patCount+"\n");
                return false;
            }else{
                return this.patientCount(patient.nextPatient);
            }
        }

}
