/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospitalmanager;

/**
 *
 * @author lawrencechin
 */
public class HospitalManager {

	private Patient patientListStart = null;

	public static void main(String[] args) { 
		HospitalManager hm = new HospitalManager();
                hm.launch();
                
                Patient pat0 = new Patient("Mary", 66, "Meningitis"); 
                hm.patientListStart.addPatient(pat0);
                
                Patient pat1 = new Patient("Ping", 33, "Monster-mash"); 
                hm.patientListStart.addPatient(pat1);
                
                Patient pat2 = new Patient("Pong", 23, "Massive elbow"); 
                hm.patientListStart.addPatient(pat2);
                
                Patient pat3 = new Patient("Penny", 67, "Monkey Wrenched"); 
                hm.patientListStart.addPatient(pat3);
                
                Patient pat4 = new Patient("Pahjay", 17, "Miniscule heart"); 
                hm.patientListStart.addPatient(pat4);
                
                Patient pat5 = new Patient("Paddy", 12, "Model Knees"); 
                hm.patientListStart.addPatient(pat5);
                
                Patient pat6 = new Patient("Padraig", 66, "Masculine Eyebrow"); 
                hm.patientListStart.addPatient(pat6);
                
                Patient pat7 = new Patient("Pelvin", 89, "Mommas Boy"); 
                hm.patientListStart.addPatient(pat7);
                
                Patient pat8 = new Patient("Penelope", 24, "Missive Whisper"); 
                hm.patientListStart.addPatient(pat8);
                
                Patient pat70 = new Patient("PondoRyse", 60, "Magellan");
                
                hm.patientListStart.getData(hm.patientListStart);
                hm.patientListStart.patientCount(hm.patientListStart);
                
                hm.patientListStart.deletePatient(pat4);
                hm.patientListStart.deletePatient(pat7);
                hm.patientListStart.deletePatient(pat8);
                
                hm.patientListStart.getData(hm.patientListStart);
                
                hm.patientListStart.deletePatient(pat70);
                hm.patientListStart.addPatient(pat70);
                hm.patientListStart.deletePatient(pat2);
                hm.patientListStart.getData(hm.patientListStart);
                
                hm.patientListStart.patientCount(hm.patientListStart);
	}
        
        public void launch(){
            // ...inside some method, maybe launch()
            Patient firstPatient = new Patient("John", 33, "Tuberculosis"); 
            this.patientListStart = firstPatient;
	// ... 
        }
}
