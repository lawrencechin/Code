
String s,t = ""
int len, largest
boolean finished = false

println "Please enter a series of line (end by typing 'end')"

while(!finished){
    s = System.console().readLine()
    len = s.length()
    if (s != "end"){
        if (len > largest){
        t = s
        largest = len
        }
    } else {
        finished = true
    }   
}

if (t.length() == 0){
    println "You did not enter anything"
}else{
    println "The largest line you entered was '" + t + "' with a total number of " +largest+ " characters"
}
