String s, var_same = "Same, ", var_up = "Up, ", var_down = "Down, ", 
total = "The results are: "
boolean finished = false, firstInput = true
int x, current = 0

println "Please enter a series of numbers (End with 0)"

while(!finished){
    s = System.console().readLine()
    x = Integer.parseInt(s)
    
    if (firstInput){
        if (x !=0){
            current = x
            total = total + "The first number is " + x + ", "
            firstInput = false
        } else {
            finished = true
        }
        
    } else if (x != 0){
        if (x == current){
            current = x
            total = total + "(" + x + ") " + var_same
        } else if (x > current){
            current = x
            total = total + "(" + x + ") " + var_up
        } else if (x < current){
            current = x
            total = total + "(" + x + ") " + var_down
        }
    } else {
        finished = true
    }
}

if (firstInput){
    println "You did not enter enough numbers, please try again" 
} else {
    println total
}