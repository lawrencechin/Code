int num=0
int total=0
int questions = 7
int wrong=0
String str

boolean finished=false
boolean q1 = true, q2 = false, q3 = false, q4 = false, q5 = false, q6 = false, q7 = false

while(!finished){
    if(q1){
        println "1.What number would be stored as an integer if you typed:"
        println "int x = 1.25 ?"
        str=System.console().readLine()
        num=Integer.parseInt(str)

            if(num==1){
                println "Correct. It is 1!"
                println "Lets move on."
                total=total+1
                q1=false                
                q2=true
                
            }else{
                println "Hint: An integer is a whole number."
                println "Try again."
                wrong=wrong+1
                
            }
}

    else if (q2){
        println "2.What would the result of (10 % 2) be?"
        str=System.console().readLine()
        num=Integer.parseInt(str)
            
            if(num==0){
                println "Correct. There will be no remainder."
                println "Next!"
                total=total+1
                q2=false
                q3=true
                

            }else{
                wrong=wrong+1
                println "The % symbole represents the remainder.."
                println "Try again."
    
            }
}
    
    else if (q3){
        println "3.How would you write a comment line in Groovy?"
        str=System.console().readLine()
        
            
            if(str=="//"){
                println "Correct! // is how it's done!" 
                total=total+1
                q3=false
                q4=true
                
            }else{
                wrong=wrong+1
                println "it's near the shift key.."
            }
        
}    
    
    else if (q4){
        println "4.String str = 'Transendant Plinth'. What word would be created"
        println "if you ran the command: "
        println "String str1 = str.substring(12, 13)+ str.substring(5,11)" 
        str=System.console().readLine()
        
            if(str=="pendant"){
                println "Correct. Str1 will hole the string pendant."
                println "Next!"
                total=total+1
                q4=false
                q5=true
                
            }else{
                wrong=wrong+1
                println "Remember the substring includes 0.."
            }
                        
}
    else if (q5){
        println "5.If you were to write a while loop and the condition you"
        println "are searching for is !boolean. Would the while loop run"
        println "if boolean was initialised as boolean = true? (answer yes or no)"
        str=System.console().readLine()
            
                if(str=="no"){
                    println "Correct! ! means false"
                    println "Next!"
                    total=total+1
                    q5=false
                    q6=true
                    
                }else{
                    wrong=wrong+1
                    println "Think about what the ! means.."
                }
        
}        

    else if (q6){
        println "6.Does 'A' < 'a' ? (yes or no)"
        str=System.console().readLine()

                if(str=="no"){
                    println "Correct! capitals do not come after lower-case"
                    println "Next!"
                    total=total+1
                    q6=false
                    q7=true
            }else{
                wrong=wrong+1
                println "Capitals come before lower-case..."
            }
}

    else if (q7){
        println "7.Is 7 > 'b' ? (yes or no)"
        str=System.console().readLine()

                if(str=="yes"){
                    println "Correct! numbers ome before letters!"
                    println "That's the last question"
                    total=total+1
                    q7=false
                    q1=false
                    finished=true
            }else{
                wrong=wrong+1
                println "Numbers come before letters.."
        
            }
}
  
}


if(wrong==0){
    println "You got " + questions + "/" + questions + " right with no mistakes!"
    println "Perfect score, have an imaginery cookie like the cheap bastard lawrece gives out."
    
}else{
    println "Out of " + questions +" you got " + total + " right, got there in the end ay."
    println "You got " + wrong + " wrong in total." 
}