class Money{
    double borrowed;
    double rate;
    double total;
    int years;
    double perYear;
    
    
    void getValues(){
        println "Enter the amount borrowed:";
        this.borrowed = Double.parseDouble(System.console().readLine());
        println "Enter the mortgage rate:";
        this.rate = Double.parseDouble(System.console().readLine());
        println "Enter number of years to pay back monies:";
        this.years = Integer.parseInt(System.console().readLine());
        
        this.totalToBePaid();
    }
    
    void totalToBePaid(){
        this.total = this.borrowed*(1+(rate/100));
        println "The total amount to be paid back is: £"+total;
        this.moneyPerYear();
    }
    
    void moneyPerYear(){
        this.perYear = this.total/this.years;
        println "The total amount to pay per year is £"+ perYear;
        this.interestByYear();
    }
    
    void interestByYear(){
        double interest = this.total - this.borrowed;
        double accum;
        int amountYears;
        
        for(int i = 0; i <= this.years; i++){
            accum += this.perYear;
            if(accum >= interest){
                amountYears = i+1;
                break;
            }
        }
        
        println "It will take "+amountYears+" years to pay off the interest of "+interest;
    }
}

Money money = new Money();
money.getValues();