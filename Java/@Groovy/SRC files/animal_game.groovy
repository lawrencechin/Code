String animalOne = "Lion", animalTwo = "Whale", animalThree = "Wolf", 
animalFour = "Penguin", animalFive = "Lizard", speciesOne = "Feline", 
speciesTwo = "Mammal", speciesThree = "Canine", speciesFour = "Bird", 
speciesFive = "Reptitle", answer = ""

Boolean finished = false
int total = 0, num = 0

println "Hi there! Welcome to the animal game."
println "I'm going to say the name of an animal and you have to guess the biological family."
println "The options to choose from are: " +speciesFour+ ", " +speciesFive+ ", "+speciesOne+ ", " +speciesThree+ " and " +speciesTwo
println "Good luck"

while (!finished){
    if (num == 0){
        println animalOne + " ?"
        answer = System.console().readLine()
        if (answer == speciesOne){
            num = num + 1
            total = total + 1
            println "That's correct"
        } else {
            println "That's incorrect"
            finished = true
        }
    } else if (num == 1){
        println animalTwo + " ?"
        answer = System.console().readLine()
        if (answer == speciesTwo){
            num = num + 1
            total = total + 1
            println "That's correct"
        } else {
            println "That's incorrect"
            finished = true
        }
    } else if (num == 2){
        println animalThree + " ?"
        answer = System.console().readLine()
        if (answer == speciesThree){
            num = num + 1
            total = total + 1
            println "That's correct"
        } else {
            println "That's incorrect"
            finished = true
        }
    } else if (num == 3){
        println animalFour + " ?"
        answer = System.console().readLine()
        if (answer == speciesFour){
            num = num + 1
            total = total + 1
            println "That's correct"
        } else {
            println "That's incorrect"
            finished = true
        }
    } else if (num == 4){
        println animalFive + " ?"
        answer = System.console().readLine()
        if (answer == speciesFive){
            num = num + 1
            total = total + 1
            println "That's correct"
            finished = true
        } else {
            println "That's incorrect"
            finished = true
        }
    }
}

println "Thanks for playing. Here's your score: "
if (total >= 0 && total <= 2){
    println "You scored " + total + "/5"
    println "A shabby effort" 
} else if (total > 2 && total <= 4){
    println "You scored " + total + "/5"
    println "Almost but not quite!" 
} else {
    println "You scored " + total + "/5"
    println "Perfect! Have a cookie" 
}