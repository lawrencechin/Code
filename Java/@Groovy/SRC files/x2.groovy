String s
int x, total
boolean finished = false, firstInput = true

println "Please enter a series of numbers."
println "End with 0"

while(!finished){
    s = System.console().readLine()
    x = Integer.parseInt(s)
    
    if (firstInput){
        if (x != 0){
           total = x
           firstInput = false
        } else {
            finished = true
        }
    } else if (x != 0){
        if (x > total){
            total = x
        }
    } else {
        finished = true
    }
}

if (firstInput){
    println "You didn't enter a number!"
} else {
    println "The largest number you entered was "+total
}