class Rent{
    double rentCost;
    int yearsTenancy;
    double monthHalf;
    double totalRent;
    double monCost;
    
    void getValues(){
        println "Please enter the rent cost per month:";
        this.rentCost = Double.parseDouble(System.console().readLine());
        println "Please enter the number of years as per your tenancy agreement: ";
        this.yearsTenancy = Integer.parseInt(System.console().readLine());
        this.monthHalf = this.rentCost + (this.rentCost/2);
        
        this.totalCost();
    }
    
    void totalCost(){
        this.totalRent = this.rentCost * 12 * this.yearsTenancy + this.monthHalf;
        println "The total cost of your rent for the whole tenancy is £"+this.totalRent;   
        
        this.monthlyCost();
    }
    
    void monthlyCost(){
        this.monCost = (this.totalRent / this.yearsTenancy)/12;
        println "The total amount one would pay per month is £"+this.monCost;
        
        this.halfYearCost();
    }
    
    void halfYearCost(){
        double accum;
        int count;
        for(int i = 0; i < (yearsTenancy*12); i++){
            accum += monCost;
            
            if(accum >= monthHalf){
                count = i+1;
                break;
            }
        }
        
        println "It will take "+count+" years to pay back your month and a half rent\nIt's always two isn't it?";
    }
}

Rent rent = new Rent();
rent.getValues();