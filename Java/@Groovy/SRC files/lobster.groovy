String str
println "Please enter a number representing the time" 
println "eg. 930 equates to 9:30am. 0 represents midnight."
str = System.console().readLine()
int time = Integer.parseInt(str)
println "Thanks"

if (time < 0){
    println time + "? I'm sorry sir but there is no negative time"
} else if (time >= 2400){
    println time + "? Indeed time is infinite but a clock only goes as far as 23:59 so do try again"
}else{
    int hours = time / 100, minutes = time % 100
    if (minutes >= 60){   
        println hours + ":" + minutes +" Once more good sir, an actual time please"
    } else if (hours >= 21){
        println "The time is " + hours + ":" + minutes + " pm." 
        println "I think you'd better be preparing for bed dear sir."
    } else if (hours >= 18){
        println "The time is " + hours + ":" + minutes + " pm." 
        println "Good evening. Dinner? Quail? Pheasant? Kebab?"
    } else if (hours >= 12){
        println "The time is " + hours + ":" + minutes + " pm." 
        println "Stop that coding at once! Stop looking at the internet. The afternoon is upon us."
    } else if (hours >= 9){
        println "The time is " + hours + ":" + minutes + " am."
        println "Good morning. The weather is bright and breezy with sporadic showers of awesome throughout the day."
        println "Also, get up!" 
    } else if (hours >= 3){
        println "The time is " + hours + ":" + minutes + " am." 
        println "...zzzz...zzzzzz.....zzzzz...Groovy sleeps, so should you"
    } else {
        println "The time is " + hours + ":" + minutes + " am." 
    }
}