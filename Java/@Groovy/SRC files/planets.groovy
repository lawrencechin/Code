println "Hello and welcome to Jack names the planets!"
println "I'm going to throw out some planet names and you're going to tell me the order in which it comes in our solar system (1 - 9)"

String p1 = "Mercury", p2 = "Venus", p3 = "Earth", p4 = "Mars", p5 = "Jupiter",
p6 = "Saturn", p7 = "Uranus", p8 = "Neptune", p9 = "Pluto", ans = ""

Boolean finished = false

int num = 0, answer

while (!finished){
    if (num == 0){
        println p6 + " ?"
        ans = System.console().readLine()
        answer = Integer.parseInt(ans)
        if (answer == 6){
            println "Correct"
            num ++
        } else {
            println "Wrong!"
            finished = true
        }
    } else if (num == 1){
        println p3 + " ?"
        ans = System.console().readLine()
        answer = Integer.parseInt(ans)
        if (answer == 3){
            println "Correct"
            num ++
        } else {
            println "Wrong!"
            finished = true
        }
    } else if (num == 2){
        println p9 + " ?"
        ans = System.console().readLine()
        answer = Integer.parseInt(ans)
        if (answer == 9){
            println "Correct"
            num ++
        } else {
            println "Wrong!"
            finished = true
        }
    } else if (num == 3){
        println p1 + " ?"
        ans = System.console().readLine()
        answer = Integer.parseInt(ans)
        if (answer == 1){
            println "Correct"
            num ++
        } else {
            println "Wrong!"
            finished = true
        }
    } else if (num == 4){
        println p7 + " ?"
        ans = System.console().readLine()
        answer = Integer.parseInt(ans)
        if (answer == 7){
            println "Correct"
            num ++
        } else {
            println "Wrong!"
            finished = true
        }
    } else if (num == 5){
        println p2 + " ?"
        ans = System.console().readLine()
        answer = Integer.parseInt(ans)
        if (answer == 2){
            println "Correct"
            num ++
        } else {
            println "Wrong!"
            finished = true
        }
    } else if (num == 6){
        println p8 + " ?"
        ans = System.console().readLine()
        answer = Integer.parseInt(ans)
        if (answer == 8){
            println "Correct"
            num ++
        } else {
            println "Wrong!"
            finished = true
        }
    } else if (num == 7){
        println p4 + " ?"
        ans = System.console().readLine()
        answer = Integer.parseInt(ans)
        if (answer == 4){
            println "Correct"
            num ++
        } else {
            println "Wrong!"
            finished = true
        }
    } else if (num == 8){
        println p5 + " ?"
        ans = System.console().readLine()
        answer = Integer.parseInt(ans)
        if (answer == 5){
            println "Correct"
            num ++
            finished = true
        } else {
            println "Wrong!"
            finished = true
        }
    }
}

println "Thanks for playing"
println "You scored "+num+"/9"

if(num >= 0 && num <= 3 ){
    println "Try harder"
} else if (num >= 4 && num <= 6){
    println "Not bad"
} else if (num >= 7 && num <= 8){
    println "Almost there..."
} else {
    println "Full marks, well done!"
}