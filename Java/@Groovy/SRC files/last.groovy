String str
int num, total = 0, questions = 7, wrong = 0
boolean finished = false, q1 = true, q2 = false, q3 = false, q4 = false, q5 = false, q6 = false, q7 = false

println "Answer the following series of questions to win a prize!"

while (!finished){
	if (q1){
		println "1.What number would be stored as an integer if you typed:"
		println "int x = 1.25 ?"
		str = System.console().readLine()
		num = Integer.parseInt(str)
		total++

		if (num == 1){
			println "Correct"
			q1 = false
			q2 = true
		} else {
			println "Wrong, try again. What type of number can an interger hold?"
		}

	} else if (q2){
		println "2.What would the result of (10 % 2) be?"
		str = System.console().readLine()
		num = Integer.parseInt(str)
		total++

		if (num == 0){
			println "Correct"
			q2 = false
			q3 = true
		} else {
			println "Wrong, try again. Remember that modulous is not the same as divide!"
		}

	} else if (q3){
		println "3.How would you write a comment line in Groovy?"
		str = System.console().readLine()
		total++

		if (str == "//"){
			println "Correct"
			q3 = false
			q4 = true
		} else {
			println "Wrong, try again. Remember that this is for a single line and not a comment block"
		}

	} else if (q4){
		println "4.String str = 'Transendant Plinth'. What word would be created"
		println "if you ran the command: "
		println "String str1 = str.substring(12, 13)+ str.substring(5,11)" 
		str = System.console().readLine()
		total++

		if (str == "Pendant"){
			println "Correct"
			q4 = false
			q5 = true
		} else {
			println "Wrong. Try counting again?"
		}

	} else if (q5){
		println "5.If you were to write a while loop and the condition you"
		println "are searching for is !boolean. Would the while loop run"
		println "if boolean was initialised as boolean = true? (answer yes or no)"
		str = System.console().readLine()
		total++

		if(str == "no"){
			println "Correct"
			q5 = false
			q6 = true
		} else {
			println "Wrong. There are only two options, why is it 'yes'?"
		}
	} else if (q6){
		println "6.Does 'A' < 'a' ? (yes or no)"
		str = System.console().readLine()
		total++

		if(str == "yes"){
			println "Correct"
			q6 = false
			q7 = true
		} else {
			println "Wrong. Remember that in this case < means 'A' comes before 'a'"
		}
	} else if (q7){
		println "7.Is 7 > 'b' ? (yes or no)"
		str = System.console().readLine()
		total++

		if (str == "no"){
			println "Correct"
			q7 = false
			finished = true
		} else{
			println "Wrong. Consider the order of UNICODE characters."
		}
	}
}
println "Thanks for playing"
wrong = total - questions
if (wrong > 0){
	println "You made " +wrong+ " attempts to answer the questions. Try again."
} else{
	println "Perfect, you're ready for the tomorrow! (that's the prize by the way)"
}


