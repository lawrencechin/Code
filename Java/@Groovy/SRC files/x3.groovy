String s
int x, total
boolean finished = false

println "Please enter a series of numbers."
println "I'll let you know how many times you entered 100. End with 0"

while(!finished){
    s = System.console().readLine()
    x = Integer.parseInt(s)
    
    if (x == 100){
        total++
    } else if (x == 0){
        finished = true
    }
}

println "You entered 100 "+total+ " times"
