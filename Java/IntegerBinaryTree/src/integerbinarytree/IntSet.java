/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integerbinarytree;

/**
 *
 * @author lawrencechin
 */
public interface IntSet {
    /**
     * adds a new int to the set; if it is there already, nothing happens.
     * @param num
     */
    void add(int num);
    /**
     * returns true if the number is in the set, false otherwise.
     * @param num;
     * @return
     */
    boolean contains(int num);
    /**
     * returns the same values as the former method, but for every element that is checked prints
     * its value on screen.
     * @param num
     * @return
     */
    boolean containsVerbose(int num) ;
    /**
     * returns a string with the values of the elements in the set separated by commas.
     * @return
     * 
     */
    @Override
    String toString(); 
}
