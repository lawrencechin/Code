/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integerbinarytree;

/**
 *
 * @author lawrencechin
 */
public class TreeIntSet implements IntSet{
    private IntTreeNode root;
    
    public TreeIntSet(){
        root = null;
    }
    
    public void add(int num){
        IntTreeNode newNode = new IntTreeNode(num);
        if(root == null){
            root = newNode;
        }else if(root.getValue() == num){
            //do nothing
        }else{
            //run the add function of the node
            root.add(newNode);
        }
    }
    
    public boolean contains(int num){
        return root.contains(num);
    }
    
    public boolean containsVerbose(int num){
        return root.containsVerbose(num);
    }
    
    public String toString(){
        root.resetOutput();
        root.toStringInternal(root);
        return root.getOutput();
    }
}
