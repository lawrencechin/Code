/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integerbinarytree;

/**
 *
 * @author lawrencechin
 */
public class IntegerTreeNode {
    private int value;
    private IntegerTreeNode left;
    private IntegerTreeNode right;
    private static String output;
    private static int depthLeft;
    private static int depthRight;
    
    public IntegerTreeNode(int value){
        this.value = value;
        left = null;
        right = null;
        output = "";
        depthLeft = 0;
        depthRight = 0;
    }
    
    public int getValue(){
        return value;
    }
    
    public void setLeftRight(IntegerTreeNode newNode, boolean leftTrue){
        //use one method to set leaves using a boolean (true for left)
        //repeat for get method
        if(leftTrue){
           left = newNode; 
        }else{
            right = newNode;
        }
    }
    
    public IntegerTreeNode getLeftRight(boolean leftTrue){
        return leftTrue ? left : right;
    }
    
    public void insertLeaf(IntegerTreeNode newNode){
        if(newNode.value > this.value){
            if(right == null){
                right = newNode;
            }else{
                right.insertLeaf(newNode);
            }
        }else{
            if(left == null){
                left = newNode;
            }else{
                left.insertLeaf(newNode);
            }
        }
    }
    
    public void remove(int value){
        if(this.left != null && this.left.value == value){
            this.left = this.left.left;
        }else{
            if(this.left == null){
            }else{
                this.left.remove(value);
            }
        }
        
        if(this.right != null && this.right.value == value){
            this.right = this.right.right;
        }else{
            if(this.right == null){
            }else{
                this.right.remove(value);
            }
        }
    }
    
    public boolean contains(int value){
        if(this.value == value){
            return true;
        }else if(value > this.value){
            if(right == null){
                return false;
            }else{
                return right.contains(value);
            }
        }else{
            if(left == null){
                return false;
            }else{
                return left.contains(value);
            }
        }
    }
    
    public int getMax(){
        if(right == null){
            return this.value;
        }else{
            depthRight++;
            return right.getMax();
        }
    }
    
    public int getMin(){
        if(left == null){
            return this.value;
        }else{
            depthLeft++;
            return left.getMin();
        }
    }
    
    
    public void toString(IntegerTreeNode testNode){
        //if node is empty append empty brackets and stop current recursion
        if(testNode == null){
            output+= "[]";
            return;
        }
        //append open bracket, get value then L 
        output+= "["+testNode.value;
        output+= " L";   
        //run again using the left node
        toString(testNode.left);
        //append R and run right node
        output+= " R";
        toString(testNode.right);
        //finally append closing bracket, order of appends are important
        output+= "]";
    }
    
    public void simpleToString(IntegerTreeNode testNode){
        if(testNode == null){
            return;
        }
        output+= " ["+testNode.value;
        simpleToString(testNode.left);
        simpleToString(testNode.right);
        output+= "]";
    }
    
    public void getAllValues(IntegerTreeNode testNode){
        if(testNode == null){
            return;
        }
        output+= testNode.value+",";
        getAllValues(testNode.left);
        getAllValues(testNode.right);
    }
    
    public String getOutput(){
        return output;
    }
    
    public void setOutput(){
        //reset STATIC output String whenever toString is run
        output = "";
    }
        
    public int depth(){
        //reset depths
        depthLeft = 0;
        depthRight = 0;
        //run/reuse min/max to increment depths 
        getMin();
        getMax();
        //return the biggest number
        return depthLeft > depthRight ? depthLeft : depthRight;
    }
    
}
