/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integerbinarytree;

/**
 *
 * @author lawrencechin
 */
public class IntListSet implements IntSet{
    private IntListNode head;
    private String output;
    
    public IntListSet(){
        head = null;
        output = "";
    }
    
    public void add(int num){
        IntListNode newNode = new IntListNode(num);
        if(head == null){
            head = newNode;
        }else{
            addInternal(head, newNode);
        }
    }
    
    private void addInternal(IntListNode start, IntListNode newNode){
        if(start.getValue() == newNode.getValue()){
            return;
        }else if(start.getNext() == null){
            start.setNext(newNode);
        }else{
            addInternal(start.getNext(), newNode);
        }
    }
    
    public boolean contains(int num){
        if(head == null){
            return false;
        }else if(head.getValue() == num){
            return true;
        }else{
            return containsInternal(head.getNext(), num, false);
        }
    }
    
    private boolean containsInternal(IntListNode start, int num, boolean verbose){
        if(start == null){
            return false;
        }else if(start.getValue() == num){
            if(verbose){
                System.out.println(start.getValue());
            }
            return true;
        }else{
            if(verbose){
                System.out.println(start.getValue());
            }
            return containsInternal(start.getNext(), num, verbose);
        }
    }
    
    public boolean containsVerbose(int num){
        if(head == null){
            return false;
        }else if(head.getValue() == num){
            System.out.println(head.getValue());
            return true;
        }else{
            System.out.println(head.getValue());
            return containsInternal(head.getNext(), num, true);
        }
    }
    
    public String toString(){
        output = ""; //reset output before starting
        if(head == null){
            return output;
        }else{
            output += head.getValue() + ", ";
            return toStringInternal(head.getNext());
        }
    }
    
    private String toStringInternal(IntListNode start){
        if(start == null){
            return output;
        }else{
            output += start.getValue() + ", ";
            return toStringInternal(start.getNext());
        }
    }
}
