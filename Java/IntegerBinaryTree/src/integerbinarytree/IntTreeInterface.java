/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integerbinarytree;

/**
 *
 * @author lawrencechin
 */
public interface IntTreeInterface {
    /**
     * adds new node/leave
     * @param value 
     */
    void add(int value);
    /**
     * removes node/leave
     * @param value 
     */
    void remove(int value);
    /**
     * returns true if value is contained in tree
     * @param value
     * @return 
     */
    boolean contains(int value);
    /**
     * returns the largest value held in tree, null if empty
     * @return 
     */
    int getMax();
    /**
     * returns the smallest value held in tree, null if empty
     * @return 
     */
    int getMin();
}
