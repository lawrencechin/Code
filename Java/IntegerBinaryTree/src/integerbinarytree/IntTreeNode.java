/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integerbinarytree;

/**
 *
 * @author lawrencechin
 */
public class IntTreeNode {
    private int value;
    private IntTreeNode left;
    private IntTreeNode right;
    private static String output;
    
    public IntTreeNode(int value){
        this.value = value;
        left = null;
        right = null;
        output = "";
    }
    
    public void setValue(int num){
        value = num;
    }
    
    public int getValue(){
        return value;
    }
    
    public void setLeftRight(IntTreeNode newNode, boolean leftRight){
        if(leftRight){
            left = newNode;
        }else{
            right = newNode;
        }
    }
    
    public IntTreeNode getLeftRight(boolean leftRight){
        return leftRight ? left : right;
    }
    
    public void add(IntTreeNode newNode){
        //if value is the same return void
        if(newNode.value == this.value){
            return;
        }
        //otherwise run through left and right depending on higher or lower
        if(newNode.value > this.value){
            if(this.right == null){
                this.right = newNode;
            }else{
                this.right.add(newNode);
            }
        }else{
            if(this.left == null){
                this.left = newNode;
            }else{
                this.left.add(newNode);
            }
        }
    }
    
    public boolean contains(int num){
        if(this.value == num){
            return true;
        }else if(num > this.value){
            if(this.right == null){
                return false;
            }else{
                return this.right.contains(num);
            }
        }else{
            if(this.left == null){
                return false;
            }else{
                return this.left.contains(num);
            }
        }
    }
    
    //literally just copies the previous method
    //i'd use a boolean to set verbose or not verbose but the interface doesn't 
    //have a boolean parameter
    public boolean containsVerbose(int num){
        if(this.value == num){
            System.out.println(this.value);
            return true;
        }else if(num > this.value){
            if(this.right == null){
                System.out.println(this.value);
                return false;
            }else{
                System.out.println(this.value);
                return this.right.containsVerbose(num);
            }
        }else{
            if(this.left == null){
                System.out.println(this.value);
                return false;
            }else{
                System.out.println(this.value);
                return this.left.containsVerbose(num);
            }
        }
    }
    
    public void resetOutput(){
        output = "";
    }
    
    public String getOutput(){
        return output;
    }
    
    public void toStringInternal(IntTreeNode testNode){
        if(testNode == null){
            return;
        }
        
        output += testNode.value + ",  ";
        toStringInternal(testNode.left);
        toStringInternal(testNode.right);
        
    }
}
