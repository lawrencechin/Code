/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integerbinarytree;

/**
 *
 * @author lawrencechin
 */
public class IntListNode {
    final private int value;
    private IntListNode next;
    
    public IntListNode(int num){
        value = num;
        next = null;
    }
    
    public Integer getValue(){
        return value;
    }
    
    public IntListNode getNext(){
        return next;
    }
    
    public void setNext(IntListNode newNode){
        next = newNode;
    }
}
