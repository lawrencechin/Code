/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integerbinarytree;

import static java.util.Arrays.sort;
import java.util.Random;

/**
 *
 * @author lawrencechin
 */
public class intBinTree implements IntTreeInterface{
    private IntegerTreeNode head;
    
    public intBinTree(){
        head = null;
    }
    
    public void add(int value){
        IntegerTreeNode newNode = new IntegerTreeNode(value);
        if(head == null){
            head = newNode;
        }else if(head.getValue() == newNode.getValue()){
            //if values are equal, set newNode as head and update left/right pointers
            //generate a random boolean to determine which side is updated
            Random randBool = new Random();
            boolean rBool = randBool.nextBoolean();
           
            newNode.setLeftRight(head, rBool);
            newNode.setLeftRight(head.getLeftRight(!rBool), !rBool);
            head.setLeftRight(null, !rBool);
            head = newNode;
            
        }else{
            head.insertLeaf(newNode);
        }
    }
    
    public void remove(int value){
        //check for null tree
        if(head == null){
        //check if head is the node is to be removed    
        }else if(head.getValue() == value){
            //check if the right node is null
            if(head.getLeftRight(false) == null){
                //set head to null if true
                head = null;
            }else{
                //otherwise run getLeftNull to get position to reconnect the branches
                //set head's left branch to correct position on right side
                getLeftNull(head.getLeftRight(false)).setLeftRight(head.getLeftRight(true), true);
                //set head to first/lowest right side node
                head = head.getLeftRight(false);
            } 
        }else{
            //run internal node function to find node for removal
            head.remove(value);
        }
    }
    
    private IntegerTreeNode getLeftNull(IntegerTreeNode head){
        //run from provided node and check if left side is null then return node
        if(head.getLeftRight(true) == null){
            return head;
        }else{
            //keep running until found
            return getLeftNull(head.getLeftRight(true));
        }
    }
    
    public boolean contains(int value){
        return head.contains(value);
    }
    
    public int getMin(){
        return head.getMin();
    }
    
    public int getMax(){
        return head.getMax();
    }
    
    public void rebalance(){
        //reset the output string
        head.setOutput();
        //get all the values from the tree
        head.getAllValues(head);
        //split string into array using commas
        String[] array = head.getOutput().split(","); 
        //create int array, used for easy sorting purposes
        int[] results = new int[array.length];
        //loop through results parsing Int
        for(int i=0; i < array.length; i++){
           results[i] = Integer.parseInt(array[i]); 
        }
        //sort results lowest to highest
        sort(results);
        //get the middle number (or closest to)
        int middleNum = (results.length/2);
        //add middle number back to tree
        //kill the current tree
        head = null;
        add(results[middleNum]);
        
        //loop through array adding all numbers that aren't the middleNum
        //for the sake of neatness we want the lowest number at the bottom
        //rather than the FIRST left node so we run the first loop in reverse order
        //upto the middleNum
        for(int i = middleNum; i >= 0; i--){
            if(i != middleNum){
                add(results[i]);
            }
        }
        
        //then we run the from the middleNum to array length as per usual
        for(int i = middleNum; i < results.length; i++){
            if(i != middleNum){
                add(results[i]);
            }
        }
    }
    
    public String toString(){
        //reset output to blank
        head.setOutput();
        //run node function to populate output
        head.toString(head);
        //return the results
        return head.getOutput();
    }
    
    public String simpleToString(){
        head.setOutput();
        head.simpleToString(head);
        return head.getOutput();
    }
    
    public int depth(){
        return head.depth();
    }
}
