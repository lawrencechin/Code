/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package integerbinarytree;

/**
 *
 * @author lawrencechin
 */
public class IntegerBinaryTree {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //IntegerBinaryTree.runTest();
        //IntegerBinaryTree.treeSet();
        //IntegerBinaryTree.listSet();
        System.out.println("King");
        System.out.print("For a day");
        System.out.println("For a day");
    }
    
    public static void runTest(){
        intBinTree test = new intBinTree();
        test.add(6);
        test.add(9);
        test.add(5);
        test.add(3);
        test.add(8);
        test.add(11);
        test.add(12);
        
        test.add(3);
        test.add(15);
        test.add(6);
        test.add(56);
        test.add(78);
        test.add(1);
        test.add(10);
        test.add(6);
        test.add(11);
        test.add(11);
        test.add(12);
        test.add(40);
        System.out.println(test.depth());
        test.remove(6);
        test.remove(10);
        test.remove(78);
        System.out.println("The number 10 exists? "+test.contains(10));
        System.out.println("The number 100 exists? "+test.contains(100));
        System.out.println("The number 6 exists? "+test.contains(6));
        System.out.println("The number 12 exists? "+test.contains(12));
        System.out.println("The Highest number is: "+test.getMax());
        System.out.println("The Lowest number is: "+test.getMin());
        System.out.println(test.toString());
        System.out.println(test.simpleToString());
        System.out.println(test.depth());
        test.rebalance();
        System.out.println(test.toString());
        System.out.println(test.simpleToString());
        System.out.println(test.depth());
        System.out.println("The Highest number is: "+test.getMax());
        System.out.println("The Lowest number is: "+test.getMin());
    }
    
    public static void treeSet(){
        TreeIntSet monster = new TreeIntSet();
        monster.add(12);
        monster.add(13);
        monster.add(20);
        monster.add(18);
        monster.add(1);
        monster.add(5);
        monster.add(4);
        monster.add(4);
        monster.add(12);
        monster.add(18);
        monster.add(19);
        System.out.println(monster.contains(19));
        System.out.println(monster.containsVerbose(19));
        System.out.println(monster.contains(25));
        System.out.println(monster.containsVerbose(25));
        System.out.println(monster.toString());
    }
    
    public static void listSet(){
        IntListSet notTree = new IntListSet();
        
        notTree.add(3);
        notTree.add(10);
        notTree.add(15);
        notTree.add(2);
        notTree.add(100);
        notTree.add(3);
        notTree.add(15);
        System.out.println(notTree.contains(15)); //true
        System.out.println(notTree.containsVerbose(15));
        System.out.println(notTree.contains(25)); //false
        System.out.println(notTree.containsVerbose(25));
        System.out.println(notTree.toString());
    }
    
}
