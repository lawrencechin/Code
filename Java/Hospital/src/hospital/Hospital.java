/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospital;

/**
 *
 * @author lawrencechin
 */
public class Hospital {
	private Patient head;
        
        public Hospital(){
            head = null;
        }
	
	/**
         * 
         * @param name
         * @param illness 
         */
	public void addPatient(String name, String illness) {
            Patient newPatient = new Patient(name, illness);
            
            if(head == null){
                head = newPatient;
            }else{
                addPatientInternal(head, newPatient);
            }
	}
        
        private void addPatientInternal(Patient head, Patient newPatient){
            if(head.getNext() == null){
                head.setNext(newPatient);
            }else{
                addPatientInternal(head.getNext(), newPatient);
            }
        } 

	/**
	* A method that calls the printList method of patient.
	*/
	public void print(){
            printInternal(head);
	}
        
        private void printInternal(Patient head){
            if(head == null){
                return;
            }else{
                System.out.println(head.getName() +" | "+head.getIllness());
                printInternal(head.getNext());
            }
        }

	/**
	* A method that calls the removePatientFromList method of patient.
        * @param name
	*/
	public void remove(String name){
            if(head == null){
                return;
            }else if(head.getName().equals(name)){
                head = head.getNext();
            }else{
                removeInternal(head, name);
            }
	}
        
        private void removeInternal(Patient head, String name){
            if(head.getNext() == null){
                return;
            }else if(head.getNext().getName().equals(name)){
                head.setNext(head.getNext().getNext());
            }else{
                removeInternal(head.getNext(), name);
            }
        }

	public static void main(String[]args) {
		Hospital aHospital = new Hospital();
                //should be blank
                aHospital.print();
		aHospital.addPatient("Adam" ,"high fever");
                //remove what doesn't exist
                aHospital.remove("King");
                aHospital.print();
		aHospital.addPatient("Brad" ,"Broken leg");
		aHospital.addPatient("Carl" ,"Broken chin");
		aHospital.addPatient("Dean" ,"Bad back");
                //remove 'head'
                aHospital.remove("Adam");
                aHospital.print();
		aHospital.addPatient("Eric" ,"Huge knee");
		aHospital.addPatient("Frank" ,"Massive Elbow");
		aHospital.remove("Eric");
		aHospital.print();
                
                //loopy();
                //whileLoop();
                bodNum();

	}
        
        public static void loopy(){
            int count = 0;
            
            for(int i = 1; i < 5; i++){
                   for(int j = 9; j > 1; j--){
                       for(int k = 1; k < 5; k++){
                           count++;
                           
                           if(i % k == j){
                               return;
                           }
                       }
                   }
            }
        }
        
        public static void whileLoop(){
            int total = 0;
            int v1 = 10;
            int v2 = 20;
            int v3 = 30;
            
            while(v1 != 0){
                v1--;
                while(v2 !=0){
                    v2-=2;
                    while(v3 != 0){
                        v3-=3;
                        total++;
                    }
                    total++;
                }
                total+=10;
            }
        }
        
        public static void bodNum(){
            Bods one = new Bods("Generic Person", "building: 1");
            System.out.println(one.howManyBods());
            Bods two = new Bods("Generic Person", "building: 1");
            System.out.println(two.howManyBods());
            Bods three = new Bods("Generic Person", "building: 1");
            System.out.println(three.howManyBods());
        }
}
