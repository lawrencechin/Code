/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospital;

/**
 *
 * @author lawrencechin
 */
public class Bods {
	private static int BodNum;
	private String person;
	private String building;
	private static final int N = 2;
	
	public Bods(String person, String building){
		if(BodNum < N){
			BodNum++;
			this.person = person;
			this.building = building;
		}else{
			System.out.println("Building limit reached");
		}
		
	} 
	public int howManyBods(){
		return BodNum;
	}
}
