/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospital;

/**
 *
 * @author lawrencechin
 */
public class Patient {
	
	private Patient next;
	final private String name;
	final private String illness;

	/**
	* Constructor for the patient 
	* @param name
        * @param illness
	*/
	public Patient(String name, String illness) {
		this.name = name;
		this.illness = illness;
                next = null;
	} 
	
        public String getName(){
            return name;
        }
        
        public String getIllness(){
            return illness;
        }
        
        public Patient getNext(){
            return next;
        }
        
        public void setNext(Patient newPatient){
            next = newPatient;
        }
}
