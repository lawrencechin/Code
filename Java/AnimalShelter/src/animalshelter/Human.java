/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animalshelter;

/**
 *
 * @author lawrencechin
 */
public class Human extends Animal{
    
    public Human(String type, String name, int age){
        super(type, name, age);
        setLimbs(2);
        setFur(false);
    }
}
