/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animalshelter;

/**
 *
 * @author lawrencechin
 */
public interface AnimalShelterInt {
    /**
     * adds new animal to end of queue
     * @param <T>
     * @param newAnimal 
     */
    <T extends Animal> void add(T newAnimal);
    /**
     * rehouses first animal in queue
     */
    void remove();
    /**
     * returns the first element in the queue
     * @param <T>
     * @return 
     */
    <T extends Animal> T peek();
    /**
     * Checks if the queue is empty
     * @return 
     */
    Boolean isEmpty();
}
