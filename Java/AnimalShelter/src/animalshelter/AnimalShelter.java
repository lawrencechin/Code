/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animalshelter;

/**
 *
 * @author lawrencechin
 */
public class AnimalShelter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AnimalShelterInt pimlico = new Shelter();
        
        System.out.println(pimlico.isEmpty());
        
        pimlico.add(new Dog("Dog", "Lassie", 12));
        System.out.println(pimlico.peek().getType() +" | name:"+pimlico.peek().getName() +" | age:"+ pimlico.peek().getAge() +" | limbs:"+pimlico.peek().getLimbs());
        
        pimlico.add(new Cat("Cat", "Terry", 6));
        System.out.println(pimlico.peek().getType() +" | name:"+pimlico.peek().getName() +" | age:"+ pimlico.peek().getAge() +" | limbs:"+pimlico.peek().getLimbs());
        
        pimlico.add(new Human("Human", "Trevor", 34));
        System.out.println(pimlico.peek().getType() +" | name:"+pimlico.peek().getName() +" | age:"+ pimlico.peek().getAge() +" | limbs:"+pimlico.peek().getLimbs());
        
        System.out.println(pimlico.isEmpty());
        
        pimlico.remove();
        
        System.out.println(pimlico.peek().getType() +" | name:"+pimlico.peek().getName() +" | age:"+ pimlico.peek().getAge() +" | limbs:"+pimlico.peek().getLimbs());
        
        pimlico.remove();
        
        System.out.println(pimlico.peek().getType() +" | name:"+pimlico.peek().getName() +" | age:"+ pimlico.peek().getAge() +" | limbs:"+pimlico.peek().getLimbs());
        
        pimlico.remove();
        pimlico.remove();      
    }
}
