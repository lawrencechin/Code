/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animalshelter;

/**
 *
 * @author lawrencechin
 */
public class Dog extends Animal{
    
    public Dog(String type, String name, int age){
        super(type, name, age);
        setLimbs(4);
        setFur(true);
    }
}
