/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animalshelter;

/**
 *
 * @author lawrencechin
 */
public class Animal {
    
    final private String aType;
    final private String name;
    final private int age;
    private int standingLimbs;
    private Boolean fur;
    private Animal next;
    
    public Animal(String type, String name, int age){
        aType = type;
        this.name = name;
        this.age = age;
        standingLimbs = 0;
        fur = false;
        next = null;
    }
    
    public String getName(){
        return name;
    }
    
    public String getType(){
        return aType;
    }
    
    public int getAge(){
        return age;
    }
    
    public Animal getNext(){
        return next;
    }
    
    public void setNext(Animal newAnimal){
        next = newAnimal;
    }
    
    public int getLimbs(){
        return standingLimbs;
    }
    
    public Boolean getFur(){
        return fur;
    }
    
    public void setLimbs(int num){
        standingLimbs = num;
    }
    
    public void setFur(Boolean fur){
        this.fur = fur;
    }
}
