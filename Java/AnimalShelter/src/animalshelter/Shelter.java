/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package animalshelter;

/**
 *
 * @author lawrencechin
 */
public class Shelter implements AnimalShelterInt{
    
    private Animal head;
    
    public Shelter(){
        head = null;
    }
    
    @Override
    public <T extends Animal> void add(T newAnimal){
        if(isEmpty()){
            head = newAnimal;
        }else{
            addInternal((T)head, newAnimal);
        }
    }
    
    private <T extends Animal> void addInternal(T head, T newAnimal){
        if(head.getNext() == null){
            head.setNext(newAnimal);
        }else{
            addInternal(head.getNext(), newAnimal);
        }
    }
    
    @Override
    public void remove(){
        if(isEmpty()){
            System.out.println("Shelter is empty");
        }else{
            head = head.getNext();
        }
    }
    
    @Override
    public <T extends Animal> T peek(){
        return (T) head;
    }
    
    @Override
    public Boolean isEmpty(){
        return head == null;
    }
}
