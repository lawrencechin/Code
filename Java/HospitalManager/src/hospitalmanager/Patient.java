package hospitalmanager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lawrencechin
 */
public class Patient {
	private String name; 
	private int age;
	private String illness; 
	private Patient nextPatient;
        private static int patCount = 0;

	public Patient(String name, int age, String illness) { 

		this.name = name;
		this.age = age;
		this.illness = illness;
		this.nextPatient = null;
                patCount++;
	}
        
        public Boolean getData(Patient patient){
            if (patient == null){
                return false;
            }else{
                System.out.println("Name: "+ patient.name + "\n" + "Age: "+patient.age+ "\n"+"Illness: "+patient.illness+"\n");
                return this.getData(patient.nextPatient);
            }
        }

	public void addPatient(Patient newPatient) {

		if (this.nextPatient == null) {
			// this means this is the last patient in the list 
			this.nextPatient = newPatient;
		} else { 
			this.nextPatient.addPatient(newPatient);
		} 
	}

	public boolean deletePatient(Patient patient) { 
		if (this.nextPatient == null) {
			// patient to remove was not found
			return false;
		} else if (
			this.nextPatient.name.equals(patient.name)) {
			// We found it! It is the next one!
			// Now link this patient to the one after the next 
			this.nextPatient = nextPatient.nextPatient; 
                        patCount--;
			return true;
		} else {
			return this.nextPatient.deletePatient(patient);
		} 
	} 
        
        public boolean patientCount(Patient patient){
            if(patient == null){
                System.out.println("The total number of patients is "+patCount+"\n");
                return false;
            }else{
                return this.patientCount(patient.nextPatient);
            }
        }

}
