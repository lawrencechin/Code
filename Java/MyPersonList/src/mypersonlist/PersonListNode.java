/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypersonlist;

/**
 *
 * @author lawrencechin
 */
public class PersonListNode {
    private Person thisPerson;
    private PersonListNode nextNode;
    private MyPersonList rootPointer;

    public void setRootP(MyPersonList rootP){
        this.rootPointer = rootP;
    }
    
    public void addPerson(Person p, MyPersonList rootOfList) {
        if (this.rootPointer == rootOfList) {
            this.nextNode = new PersonListNode();
            this.nextNode.setThisPerson(p);
            this.nextNode.rootPointer = rootOfList;
            this.rootPointer = null;
            this.nextNode.nextNode = rootOfList.getPL();
        } else {
            this.nextNode.addPerson(p, rootOfList);
        }
    }
    
    public boolean deletePerson(String person, MyPersonList rootOfList){
        if(this.rootPointer == rootOfList){
            return false;
        }else{
            if(this.nextNode.thisPerson.getName().equals(person)){
                
                if(this.nextNode.rootPointer == rootOfList){
                    this.rootPointer = rootOfList;
                }
                 
               this.nextNode = this.nextNode.nextNode;

               return true;
            }else{
                return this.nextNode.deletePerson(person, rootOfList);
            }
        }
    }

    public Person getThisPerson() {
        return this.thisPerson;
    }

    public void setThisPerson(Person p) {
        this.thisPerson = p;
    }

    public boolean getData(PersonListNode listNode, MyPersonList rootOfList){
        System.out.println("Patient name: "+listNode.thisPerson.getName()+"\n");
        if(listNode.rootPointer == rootOfList){
            return false;
        }else{
            return listNode.getData(listNode.nextNode, rootOfList);
        }
    }
}

