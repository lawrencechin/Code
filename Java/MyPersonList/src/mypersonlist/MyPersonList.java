/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypersonlist;

/**
 *
 * @author lawrencechin
 */
public class MyPersonList {

    private PersonListNode firstNode;
    private MyPersonList rootOfList = this;
    
    //gets the PRIVATE variable firstNode
    public PersonListNode getPL(){
        return this.firstNode;
    }
    
    //sets the PRIVATE variable firstNode
    public void setPL(PersonListNode test){
        this.firstNode = test;
    }
    
    //utilises the method already in the PersonListNode setting the value of
    //the now private field
    public void setPerson(Person p){
        this.firstNode.setThisPerson(p);
    }
    
    //references the PersonListNode method used to set the private field
    public void setRootP(MyPersonList rootP){
        this.firstNode.setRootP(rootP);
    }
    
    public void addPerson(Person p) {
        if (this.firstNode == null) {
            this.firstNode = new PersonListNode();
            setPerson(p);
            setRootP(rootOfList);
        } else {
            this.firstNode.addPerson(p, rootOfList);
        }
    }

     public static void main(String[] args) {

        MyPersonList myPersonList = new MyPersonList();
        myPersonList.addPerson(new Person("Person One"));
        myPersonList.addPerson(new Person("Person Two"));
        myPersonList.addPerson(new Person("Person Three"));
        myPersonList.addPerson(new Person("Person Four"));
        myPersonList.addPerson(new Person("Person Five"));
        myPersonList.addPerson(new Person("Person Six"));
        myPersonList.addPerson(new Person("Person Seven"));
        
        myPersonList.firstNode.getData(myPersonList.firstNode, myPersonList.rootOfList);
        myPersonList.firstNode.deletePerson("Person Five", myPersonList.rootOfList);
        myPersonList.firstNode.getData(myPersonList.firstNode, myPersonList.rootOfList);
        myPersonList.firstNode.deletePerson("Person Eleven", myPersonList.rootOfList);
        myPersonList.firstNode.deletePerson("Person Seven", myPersonList.rootOfList);
        myPersonList.firstNode.getData(myPersonList.firstNode, myPersonList.rootOfList);

    }
}
