/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package personinterface;

/**
 *
 * @author lawrencechin
 */
public class KidPerson implements Person{
    
    private int position;
    final private Brain brain = new Brain();
    
    public void move(int distance){
        crawl(distance);
    }
    
    public void say(String message){
        String finalMsg = getUnderstoodWords(message);
        System.out.println(finalMsg);
    }
    
    private void crawl(int distance){
        for (int i = 0; i < distance; i++){
            position++;
            waitALittle();
        }
    }
    
    private String getUnderstoodWords(String text){
        String result = "";
        String [] words = brain.divideIntoWords(text);
        for(int i = 0; i < words.length; i++){
            if(brain.isKnown(words[i])){
                result = result + words[i] + " ";
            }
        }
        return result;
    }
    
    public void waitALittle(){
        System.out.println("I is waiting");
    }
}
