/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package personinterface;

/**
 *
 * @author lawrencechin
 */
public class Brain {
    
    public String [] divideIntoWords(String text){
        String[] words = text.split("[[ ]*|[,]*|[\\.]*|[:]*|[/]*|[!]*|[?]*|[+]*]+");
        return words;
    }
    
    public boolean isKnown(String test){
        double knownWord = Math.random();
        return Math.round(knownWord) == 1? true : false;
    }

}
