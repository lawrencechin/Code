/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package personinterface;

/**
 *
 * @author lawrencechin
 */
public class AdultPerson implements Person{
    
    private int situation;
    private int energy;
    final private Leg leftLeg = new Leg();
    final private Leg rightLeg = new Leg();
    
    /**
     * Move a distance in a straight line, given in meters
     */
    
    public void move(int distance){
        if(rightLeg.isHealthly() && leftLeg.isHealthly()){
            run(distance);
        }else{
            walk(distance);
        }
    }
    
    /**
     * Say something
     */
    
    public void say(String message){
        System.out.println(message);
    }
    
    private void run(int distance){
        situation = situation + distance;
        energy--;
        getRun();
    }
    
    private void walk(int distance){
        for (int i = 0; i < distance; i++){
            situation++;
        }
        getWalk();
    }
    
    public void getRun(){
        System.out.println("You ran "+situation+" meters using "+energy+" energy");
    }
    
    public void getWalk(){
        System.out.println("You walked "+situation+" meters");
    }
}
