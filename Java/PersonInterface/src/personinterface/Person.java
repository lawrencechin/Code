/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package personinterface;

/**
 *
 * @author lawrencechin
 */
public interface Person {
    /**
     * Move a distance in a straight line, given in meters
     */
    void move(int distance);
    
    /**
     * Say something, printing it on a screen.
     * It may or may not be a perfect transcription.
     */
    
    void say(String message);
    
}
