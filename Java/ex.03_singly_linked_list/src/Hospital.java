/**
* A hospltal that holds a list of patients. 
*Singly linked-list
*Day 7 practice.
*/
public class Hospital {
	private Patient head = null;
	
	/**
	* Adds patient to list but makes sure the head is initialozed first. 
	* @param aNewPateint is passed in.
	*/
	public void addPatient(Patient aNewPatient) {
		if ( head == null ) {
			head = aNewPatient;
		} else {
			head.addPatientToList(aNewPatient);
		}
	}

	/**
	* A method that calls the printList method of patient.
	*/
	public void print() {
		head.printList();
	}

	/**
	* A method that calls the removePatientFromList method of patient.
	*/
	public void remove(String name) {
		head.removePatientFromList(name);
	}

	public static void main(String[]args) {
		Hospital aHospital = new Hospital();
		aHospital.addPatient(new Patient("Adam" ,"high fever"));
		aHospital.addPatient(new Patient("Brad" ,"Broken leg"));
		aHospital.addPatient(new Patient("Carl" ,"Broken chin"));
		aHospital.addPatient(new Patient("Dean" ,"Bad back"));
		aHospital.addPatient(new Patient("Eric" ,"Huge knee"));
		aHospital.addPatient(new Patient("Frank" ,"Massive Elbow"));
		aHospital.remove("Eric");
		aHospital.print();

	}
}