/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hospital;

/**
 *
 * @author lawrencechin
 */
public class Patient {
	
	private Patient next = null;
	private String name; //can be final
	private String illness; //can be final

	/**
	* Constructor for the patient 
	* @param name
        * @param illness
	*/
	public Patient(String name, String illness) {
		this.name = name;
		this.illness = illness;
	} 
	
	/**
	* Adds a patient to the list
	*@param aPatient
	*/
	public void addPatientToList(Patient aPatient) {
		if (this.next == null) {
			this.next = aPatient;
		} else {
			this.next.addPatientToList(aPatient);
		}
	}

	public void removePatientFromList(String name) {
            //null pointer error if list is empty or only has one node
            //should .equals() to comapre patient names
            //how to delete the head?
		if (this.next.getPatientName() == name ) {
			this.next = this.next.next;
                        //don't need this line
			this.next.next = null;
		} else {
			this.next.removePatientFromList(name);
		}
	}

	/**
	* Prints the content of the list
	*/
	public void printList() {
		if (this.next == null) {
			System.out.println(this.getPatientName());
		} else {
			System.out.println(this.getPatientName());
			this.next.printList();
		}
	}

	/**
	* Getter for patient name
	* @return patient name
	*/
	public String getPatientName() {
		return this.name;
	}

	/**
	* Getter for patient illness
	* @return patient illness
	*/
	public String getPatientIllness() {
		return this.illness;
	}
}
