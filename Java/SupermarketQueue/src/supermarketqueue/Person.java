/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package supermarketqueue;

/**
 *
 * @author lawrencechin
 */
public class Person {
    final private int custNum;
    private Person next;
    
    public Person(int custNum){
        this.custNum = custNum;
        next = null;
    }
    
    /**
    * Returns the integer customer number
    * @return
    */
    public int getNum() {
        return custNum;
    }

    /**
     * Set the next node to point to the given node
     * @param node
     */
    public void setNext(Person node) {
        next = node;		
    }

    /**
     * Set the next node to point to the given node
     * @return
     */
    public Person getNext() {
	  return next;		
    }
}
