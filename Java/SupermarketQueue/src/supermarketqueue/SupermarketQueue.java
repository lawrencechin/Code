/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package supermarketqueue;

/**
 *
 * @author lawrencechin
 */
public class SupermarketQueue {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SupermarketQueue.supermarketQueue();
        //SupermarketQueue.supermarketQueueAgain();
        SupermarketQueue.unfairSupermarket();
        SupermarketQueue.prioirtyQueue();
    }
    
    public static void supermarketQueue(){
        Supermarket tesco = new Supermarket();
        
        System.out.println("The queue is empty? "+tesco.isEmpty());
        tesco.insert(1);
        tesco.insert(13);
        tesco.insert(20);
        tesco.insert(6);
        tesco.insert(56);
        tesco.insert(109);
        System.out.println("The number of customers in the queue is "+tesco.getSize());
        System.out.println("The queue is empty? "+tesco.isEmpty());
        tesco.retrieve();
        tesco.retrieve();
        tesco.retrieve();
        tesco.retrieve();
        tesco.retrieve();
        tesco.retrieve();
        tesco.retrieve();
        System.out.println("The number of customers in the queue is "+tesco.getSize());
        System.out.println("The queue is empty? "+tesco.isEmpty());
        tesco.insert(200);
        System.out.println("The number of customers in the queue is "+tesco.getSize());
        System.out.println("The queue is empty? "+tesco.isEmpty());
    } 
    
    public static void supermarketQueueAgain(){
        SupermarketAgain waitrose = new SupermarketAgain();
        
        waitrose.addPerson(new Persons("Ming", 25));
        waitrose.addPerson(new Persons("Ting", 15));
        waitrose.addPerson(new Persons("Bing", 56));
        waitrose.addPerson(new Persons("King", 70));
        waitrose.addPerson(new Persons("Sing", 30));
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.addPerson(new Persons("Sally", 101));
        waitrose.addPerson(new Persons("Wally", 43));
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.addPerson(new Persons("Major", 9));
        waitrose.addPerson(new Persons("Ming, again", 25));
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
    }
    
    public static void unfairSupermarket(){
        UnfairSupermarket waitrose = new UnfairSupermarket();
        System.out.println("Wrong! This simply makes the queue act as a stack, read question wrongly: ");
        waitrose.addPerson(new Persons("Ming", 25));
        waitrose.addPerson(new Persons("Ting", 15));
        waitrose.addPerson(new Persons("Bing", 56));
        waitrose.addPerson(new Persons("King", 70));
        waitrose.addPerson(new Persons("Sing", 30));
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.addPerson(new Persons("Sally", 101));
        waitrose.addPerson(new Persons("Wally", 43));
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.addPerson(new Persons("Major", 9));
        waitrose.addPerson(new Persons("Ming, again", 25));
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
    }
    
    public static void prioirtyQueue(){
        PriorityQueue waitrose = new PriorityQueue();
        
        waitrose.addPerson(new Persons("Ming", 25));
        waitrose.addPerson(new Persons("Ting", 15));
        waitrose.addPerson(new Persons("Bing", 56));
        waitrose.addPerson(new Persons("King", 70));
        waitrose.addPerson(new Persons("Sing", 30));
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.addPerson(new Persons("Sally", 101));
        waitrose.addPerson(new Persons("Wally", 43));
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.addPerson(new Persons("Major", 9));
        waitrose.addPerson(new Persons("Ming, again", 25));
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
        waitrose.servePerson();
    }
}
