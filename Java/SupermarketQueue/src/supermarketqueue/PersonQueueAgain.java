/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package supermarketqueue;

/**
 *
 * @author lawrencechin
 */
public interface PersonQueueAgain {
    /**
    * Adds another person to the queue.
    * @param person
    */
    void insert(Persons person);
    /**
    * Removes a person from the queue. 
    * @return
    */
    Persons retrieve();
}
