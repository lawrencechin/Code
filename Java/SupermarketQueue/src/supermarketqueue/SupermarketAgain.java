/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package supermarketqueue;

/**
 *
 * @author lawrencechin
 */
public class SupermarketAgain implements PersonQueueAgain{
    
    private Persons head;
    
    public SupermarketAgain(){
        head = null;
    }
    
    //set new person to last 
    public void insert(Persons person){
        getLast(head).setNext(person);
    }
    
    //parse list to get last object, return last person
    private Persons getLast(Persons person){
        if(person == null){
            System.out.println("No Customers in queue");
            return null;
        } else if(person.getNext() == null){
            return person;
        }else{
            return getLast(person.getNext());
        }
    }
    
    //if head is null, set otherwise run getLast and insert
    public void addPerson(Persons person){
        if(head == null){
            head = person;
        }else{
            insert(person);
        }
    }
    
    public Persons retrieve(){
        head = head.getNext();
        return head;
    }
    
    public void servePerson(){
        if(head != null){
            System.out.println("Customer: "+head.getVal()+" served!");
            retrieve();
        }
    }
}
