/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package supermarketqueue;

/**
 *
 * @author lawrencechin
 */
public class Supermarket implements PersonQueue{
    
    private Person head;
    static int count;
    
    public Supermarket(){
        head = null;
    }
    
    public void insert(int num){
        if(head != null){
            //increment queue size
            count++;
            //finds the person whose next pointer is null and sets next to 
            //new person
            getLast(head).setNext(new Person(num));
        }else{
            count++;
            head = new Person(num);
        }
    }
    
    private Person getLast(Person person){
        //traverses list until we get to the end
        if(person.getNext() == null){
            return person;
        }else{
           return getLast(person.getNext()); 
        }
    }
    
    public void retrieve(){
        if(head == null){
            //count = 0; - shouldn't need this so have commented it out
        }else{
            //deincrements queue size, sets head to next person
            count --;
            head = head.getNext();
        }  
    }
    
    public int getSize(){
        return count;
    }
    
    public boolean isEmpty(){
        return head == null;
    }
}

