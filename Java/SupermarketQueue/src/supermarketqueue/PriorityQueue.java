/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package supermarketqueue;

/**
 *
 * @author lawrencechin
 */
public class PriorityQueue implements PersonQueueAgain{
    private Persons head;
    
    public PriorityQueue(){
        head = null;
    }
    
    //set new person to last 
    public void insert(Persons person){
        head.getPosition(person);
    }
    
    //if head is null, set otherwise run getLast and insert
    public void addPerson(Persons person){
        if(head == null){
            head = person;
        }else if(head.getAge() < 65 && person.getAge() > 65){
            person.setNext(head);
            head = person;
        }else if(head.getAge() < 18 && person.getAge() > 18){
            person.setNext(head);
            head = person;
        }else{
            insert(person);
        }
    }
    
    public Persons retrieve(){
        head = head.getNext();
        return head;
    }
    
    public void servePerson(){
        if(head != null){
            if(head.getAge() > 65){
                System.out.println("Priority Customer: "+head.getVal()+"("+head.getAge()+") served!");
            }else if(head.getAge() > 18 && head.getAge() < 65){
                System.out.println("Valued Customer: "+head.getVal()+"("+head.getAge()+") served!");
            }else{
                System.out.println("Customer: "+head.getVal()+"("+head.getAge()+") served!");
            }
            
            retrieve();
        }
    }
}
