/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package supermarketqueue;

/**
 *
 * @author lawrencechin
 */
public class Persons{
    private int age;
    private String value;
    private Persons next;
    
    public Persons(String value, int age){
        this.age = age;
        this.value = value;
        next = null;
    }
    
    public int getAge(){
        return age;
    }
    
    public String getVal(){
        return value;
    }
    
    public void setVal(String value){
        this.value = value;
    }
    
    public void setNext(Persons person){
        next = person;
    }
    
    public Persons getNext(){
        return next;
    }

    //parse list to get last object, return last person
    public void getPosition(Persons person){
        
        if(this.next == null){
            this.next = person;
        }else if(this.next.age < 65 && person.age > 65){
            person.next = this.next;
            this.next = person;
        }else if(this.next.age < 18 && person.age > 18){
            person.next = this.next;
            this.next = person;
        }else{
            this.next.getPosition(person);
        }
    }
}
