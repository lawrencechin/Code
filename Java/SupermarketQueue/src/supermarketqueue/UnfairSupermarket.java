/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package supermarketqueue;

/**
 *
 * @author lawrencechin
 */
public class UnfairSupermarket implements PersonQueueAgain{
    private Persons head;
    
    public UnfairSupermarket(){
        head = null;
    }
    
    //set new person to last 
    public void insert(Persons person){
        getLast(head).setNext(person);
    }
    
    //parse list to get last object, return last person
    private Persons getLast(Persons person){
        if(person == null){
            System.out.println("No Customers in queue");
            return null;
        } else if(person.getNext() == null){
            return person;
        }else{
            return getLast(person.getNext());
        }
    }
    
    private Persons getPenultimate(Persons person){
        if(person == null){
            return null;
        }else if(person.getNext() == null){
            return person;
        }else if(person.getNext().getNext() == null){
            return person;
        }else{
            return getPenultimate(person.getNext());
        }
    }
    
    //if head is null, set otherwise run getLast and insert
    public void addPerson(Persons person){
        if(head == null){
            head = person;
        }else{
            insert(person);
        }
    }
    
    public Persons retrieve(){
        Persons penUl = getPenultimate(head);
        if(penUl.getNext() == null){
            head = null;
            return head;
        }else{
            penUl.setNext(penUl.getNext().getNext());
            return penUl;
        }
    }
    
    public void servePerson(){
        Persons last = getLast(head);
        if(last != null){
            System.out.println("Customer: "+last.getVal()+" served!");
            retrieve();
        }
    }
}
