/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package supermarketqueue;

/**
 *
 * @author lawrencechin
 */
public interface PersonQueue {
    /**
     * inserts new person into queue
     * @param num
     */
    void insert(int num);
    /**
     * removes first person from queue
     */
    void retrieve();
    /**
     * gets the size of the queue
     * @return 
     */
    int getSize();
    /**
     * checks if queue is empty
     * @return 
     */
    boolean isEmpty();
}
