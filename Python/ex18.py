# -*- coding: utf-8 -*-
from sys import argv

script, cheesey, crackerys = argv

def cheese_and_crackers(cheese_count, boxes_of_crackers) : 
	print "You have %d cheeses!" % cheese_count
	print "You have %d boxes of crackers!" % boxes_of_crackers
	print "Man that's enough for a party!"
	print "Get a blanket.\n"

def cheese() : 
	return 30

def crackers() : 
	return 10	

#print "We can just give the function numbers directly: "
#cheese_and_crackers(20, 30)

#print "OR, we can use variables from our script: "
amount_of_cheese = 10
amount_of_crackers = 50

#cheese_and_crackers(amount_of_cheese, amount_of_crackers)

#print "We can even do maths inside too: "
#cheese_and_crackers(10+12, 5+6)

#print "And we can combine variables and maths: "
#cheese_and_crackers(amount_of_cheese + 10, amount_of_crackers - 24)	

print "We're now going to run the cheese_and_crackers in 10 different ways, observe!"
#number arguements
cheese_and_crackers(2, 6)
#variables args
cheese_and_crackers(amount_of_cheese, amount_of_crackers)
#maths
cheese_and_crackers(2 + 2, 6 * 2)
#maths + args
cheese_and_crackers(2 + amount_of_cheese, 6 + amount_of_crackers)
#argvs - remember to cast!
cheese_and_crackers(int(cheesey), int(crackerys))
#argvs + vars
cheese_and_crackers(int(cheesey) + amount_of_cheese, int(crackerys))
#run functions
cheese_and_crackers(cheese(), crackers())
#ask for input
cheese_and_crackers(int(raw_input('Cheese!')), int(raw_input('Crackers!')))

#I don't want to write any more of these :(

# ten ways to run function:

# pass in arguments directly
# pass in strings and cast to integers
# ask user for input (raw_input())
# use argv for variables
# pass in variables
# use maths to generate input
# use maths + variables + argv + raw_input
# use another function to pass as an argument
# pass no arguements 
#  