#define variable with four raw placeholders
formatter = "%r %r %r %r"

#print formatter var passing in four numbers as argument
print formatter % (1,2,3,4)
#print formatter passing in str arguments
print formatter % ('one', 'two', 'three', 'four')
#boolean arguments
print formatter % (True, False, False, True)
#pass in formatter itself as the arguments
print formatter % (formatter, formatter, formatter, formatter)
#pass in more strings
print formatter % (
	"I had this thing.",
	"That you could type up right.",
	"But it didn't sing.",
	"So I said goodnight."
	)

#error : line 9 - typo, added an extra 't' to formattttttter

