# -*- coding: utf-8 -*-
from sys import argv
script, random = argv

def testFunction():
	test = "The ming of merciless"
	global random
	try:
		print "Here we take a string and \vconvert it into a list."
		test = test.split()
		while True:
			if len(test) == 4:
				random = random + " %s" %test[2]
				print test
				break
			else:
				print len(test)
				continue
	except:
		print "\aThere was an error"	
	else:
		print "All fine"	
	finally:
		print "We did this in a try/finally block and execute",
		print "the next step in 'finally'."
		print "We will now 'join' the list together using ",
		print "underscores as the join. Enjoy!"
		test = '_'.join(test)
		return "King Power King %s" %test 

print testFunction()				
print random