# -*- coding: utf-8 -*-

""" 
Python keywords to study:
----
Keywords
----
0.class
1.as
2.assert
3.continue
4.del
5.except
6.exec
7.finally
8.global
9.is
10.lambda
11.pass
12.raise
13.try 
14.with
15.yield
----
Data Types
----
16.None (similar to but NOT a NULL pointer)
17.dicts (dictiionary lists, key : value, like associative arrays)
----
String Escapes
----
18.\a - bell
19.\b backspace
20.\f  - Formfeed
21.\r - carriage
22.\v - vertical tab ?
----
String Formats
----								
23.%o octal number
24.%u unsigned decimal
25.%x hexadecimal lowercase
27.%X hexadecimal uppercase
28.%e exponential notation lower 'e'
29.%E exponential notation upper 'e'
30.%g either %f or %e, whichever is shorter
31.%% percent
----
Operators
----
32.** power of
33.// floor division
34.{} dict curly braces
35.@ at(decorators)
36.; semi-colon
37.

