## Animal is-a object (yes, sort of confusing) look at the extra credit
class Animal(object):
    pass

## Dog is-a type of Animal
class Dog(Animal):

    def __init__(self, name):
        ## Dog has-a attribute called name
        self.name = name

## Cat is-a type of Animal
class Cat(Animal):

    def __init__(self, name):
        ## Cat has-a attribute name
        self.name = name

## Person is-a object
class Person(object):

    def __init__(self, name):
        ## Person has-a attribute name
        self.name = name

        ## Person has-a pet of some kind
        self.pet = None

## Employee is-a type of Person
class Employee(Person):

    def __init__(self, name, salary):
        ## employee calls the parent class'(Person) constructor with param name(the super class) - this call is always made and occurs before any other line in the constructor
        super(Employee, self).__init__(name)
        ## employee has-a attribute called salary
        self.salary = salary

## Fish is-a object
class Fish(object):
    pass

## Salmon is-a object
class Salmon(Fish):
    pass

## Halibut is-a object
class Halibut(Fish):
    pass


## rover is-a Dog
rover = Dog("Rover")

## satan is a Cat
satan = Cat("Satan")

## Mary is-a Person
mary = Person("Mary")

## Mary has-a pet satan(Cat obj)
mary.pet = satan

## frank is-a Employee
frank = Employee("Frank", 120000)

## frank has-a pet rover(Dog obj)
frank.pet = rover

## flipper is-a Fish
flipper = Fish()

## crous is-a Salmon
crouse = Salmon()

## harry is-a Halibut
harry = Halibut()