class Parent(object):

    def override(self):
        print "PARENT override()"

    def implicit(self):
        print "PARENT implicit()"

    def altered(self):
        print "PARENT altered()" 

class Child(Parent):

    def override(self):
        print "CHILD override()"

    def altered(self):
        print "CHILD, BEFORE PARENT altered()"
        super(Child, self).altered()
        print "CHILD, AFTER PARENT altered()"

dad = Parent()
son = Child()

dad.implicit()#prints: "PARENT implicit()"
son.implicit()#prints: "PARENT implicit()"

dad.override()#prints: "PARENT override()"
son.override()#prints: "CHILD override()"

dad.altered()#prints: "PARENT altered()" 
son.altered()#prints: **before altered** "PARENT altered()" **after altered**