import hash_map

# create a mapping of state to abbreviation
states = hash_map.new()
hash_map.set(states, 'Oregon', 'OR')
hash_map.set(states, 'Florida', 'FL')
hash_map.set(states, 'California', 'CA')
hash_map.set(states, 'New York', 'NY')
hash_map.set(states, 'Michigan', 'MI')

# create a basic set of states and some cities in them
cities = hash_map.new()
hash_map.set(cities, 'CA', 'San Francisco')
hash_map.set(cities, 'MI', 'Detroit')
hash_map.set(cities, 'FL', 'Jacksonville')

# add some more cities
hash_map.set(cities, 'NY', 'New York')
hash_map.set(cities, 'OR', 'Portland')


# print out some cities
print '-' * 10
print "NY State has: %s" % hash_map.get(cities, 'NY')
print "OR State has: %s" % hash_map.get(cities, 'OR')

# print some states
print '-' * 10
print "Michigan's abbreviation is: %s" % hash_map.get(states, 'Michigan')
print "Florida's abbreviation is: %s" % hash_map.get(states, 'Florida')

# do it by using the state then cities dict
print '-' * 10
print "Michigan has: %s" % hash_map.get(cities, hash_map.get(states, 'Michigan'))
print "Florida has: %s" % hash_map.get(cities, hash_map.get(states, 'Florida'))

# print every state abbreviation
print '-' * 10
hash_map.list(states)

# print every city in state
print '-' * 10
hash_map.list(cities)

print '-' * 10
state = hash_map.get(states, 'Texas')

if not state:
  print "Sorry, no Texas."

# default values using ||= with the nil result
# can you do this on one line?
city = hash_map.get(cities, 'TX', 'Does Not Exist')
print "The city for the state 'TX' is: %s" % city