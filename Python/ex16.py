# -*- coding: utf-8 -*-
from sys import argv
from os.path import exists

script, from_file, to_file = argv
prompt = "> "
print "%sCopying from %s to %s" % (prompt, from_file, to_file)

#we could do these on one line, how?
#in_file = open(from_file)
#indata = in_file.read()
#perhaps?
indata = open(from_file).read()

print "%sThe input file is %d bytes long" % (prompt, len(indata))
print "%sFile exists: %r" % (prompt, exists(to_file))

out_file = open(to_file, 'w')
out_file.write(indata)

print "%sCopied" % prompt

out_file.close()