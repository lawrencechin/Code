# -*- coding: utf-8 -*-

print "Election Special!"
print "We look at the main issues, and solutions presented",
print "by the parties and decide which sounds best to us."
print "We will then tally up the scores and see who wins."
print "En guarde!"

candidates = {
	1 : "Conservatives",
	2 : "Labour",
	3 : "Lib Dems",
	4 : 'UKIP',
	5 : "Green"
}

issues = {
	"Key Priorties" : [2, 5],
	"Economy" : [2, 3],
	"Health and Care" : [5],
	"Immigration" : [3,5],
	"Welfare" : [5],
	"Law and Order" : [3],
	"Education" : [3],
	"Housing" : [3],
	"Foreign and Defence" : [3],
	"Pensions" : [2,3,5],
	"EU" : [1,3],
	"Environment" : [3],
	"Taxation" : [1, 2],
	"Constitution" : [2, 5],
	"Transport" : [2, 3, 5],
	"Local Government" : [1],
	"Rural Affairs" : [2, 3],
	"Science" : [3],
	"Technology" : [3]
}

results = {
	1 : 0,
	2 : 0,
	3 : 0,
	4 : 0,
	5 : 0
}

for iss, winner in issues.items():
	for win in winner:
		results[win] += 1

for win, res in results.items():
	print "%s got %d votes" %(candidates[win], res)		