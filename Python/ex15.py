# -*- coding: utf-8 -*-
from sys import argv

script, filename = argv

print "We're going to erase %r." % filename
print "If you don't want that hit CTRL-C (^C)."
print "If you do want that, hit RETURN."

raw_input("?")
#open the file, specified in argv, in write mode
print "Opening file…"
target = open(filename, 'w')
#erase the file
print "Truncating the file. Goodbye!"

print "Now I'm going to ask you for three lines(not cocaine)."

line1 = raw_input("Line 1: ")
line2 = raw_input("Line 2: (little coke wouldn't go amiss…) ")
line3 = raw_input("Line 3: (i'm desperate man) ")

print "I'm going to write these to the file."
test = "%r\n%r\n%r\n" % (line1, line2, line3)
target.write(test) 

print "And finally, we close it. But why not read it to check it worked huh?"
print "Aah! We were tricked. Here's the contents of your file in it's full glory: "
target = open(filename)
print target.read()
target.close()