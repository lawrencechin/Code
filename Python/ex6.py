# -*- coding: utf-8 -*-
#define variable x with string containing a number variable
x = 'There are %d types of people.' % 10
#string variable
binary = "binary"
#string variable
do_not = "don't"
#string variable containing two string placeholders
y = "Those who know %s and those who %s." % (binary, do_not)

#print out variables x and y respectively
print x
print y

#print the raw variable using %r
print "I said: %r." % x
#print string containing string variable
print "I also said: '%s'." % y

#boolean variable 
hilarious = False
#string with raw variable so boolean is converted to text
joke_evaluation = "Isn't that joke so funny?! %r"

print joke_evaluation % hilarious

w = "This is the left side of …"
e = "a string with a right side."

print w + e

