#import the sys module and accept argument variables
from sys import argv
#we will accept 2 variables from argv, one being the running script 
script, filename = argv
#the second argv is the file name which we then open
txt = open(filename)
#print out the contents of the file using the read function
print "Here's your file %r: " % filename
print txt.read()
txt.close()
#repeat the above but instead of using argv we instead use raw_input
print "Type the filename again: "
file_again = raw_input("> ")

txt_again = open(file_again)
print txt_again.read()
txt_again.close()