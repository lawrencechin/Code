#add a tab spacing to string
tabby_cat = "\tI'm tabbed in."
#split string into two lines
persian_cat = "I'm split\non a line."
#escape a backslash character
backslash_cat = "I'm \\ a \\ cat"
#write a list with tabs and the final list item created with a \n
fat_cat = """
I'll do a list:
\t* Cat Food
\t* Fishies
\t* Catnip\n\t* Grass
"""

masterful = """
Let's test some stuff.
Here is an ASCII bell: \a.
"""

print tabby_cat
print persian_cat
print backslash_cat
print fat_cat
print masterful

while True:
	for i in ["/", "-", "|", "\\", "|"]:
		print "%s\r" % i,
