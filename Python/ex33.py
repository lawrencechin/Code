# -*- coding: utf-8 -*-
ten_things = "Apples Oranges Crows Telephone Light Sugar"
stuff = ten_things.split(' ')
more_stuff = ["Day", "Night", "Song", "Frisbee", "Corn", "Banana", "Girl", "Boy"]

while len(stuff) != 10:
	next_one = more_stuff.pop()
	print "Adding: ", next_one
	stuff.append(next_one)
	print "There are now %d items." % len(stuff)

print "There we go: ", stuff

print "Let's do some things with stuff."

print stuff[1]
print stuff[-1] #negative index starts from last entry in list
print stuff.pop()#will remove item from list
print ' '.join(stuff)#prints list as string with spaces inbetween words
print '#'.join(stuff[3:5])#same as above but presumably with a range of item 3 to 5 and also using a hash symbol to join	