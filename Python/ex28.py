# -*- coding: utf-8 -*-
def testLoop(incrementNum, endLoop):
	i = 0
	list = []
	while i < endLoop:
		print "At the top i is %d" % i
		list.append(i)

		i = i + incrementNum
		print "Numbers now: ", list
		print "At the bottom i is %d" % i

	return list

def rangeLoopTest(incrNum, endLoop):
	list = []
	for num in range(0, endLoop, incrNum):
		print "At the top i is %d" % num
		list.append(num)
		print "Numbers now: ", list
		print "At the bottom i is %d" % num

	return list	

#varOne = testLoop(1, 3)
varThree = rangeLoopTest(15, 100)


