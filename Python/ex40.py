class Song(object):
	def __init__(self, lyrics):
		self.lyrics = lyrics

	def sing_me_a_song(self):
		for line in self.lyrics:
			print line

	def melody(self, beat):
		self.score = range(1, beat)
		for i in range(1, beat):
			if i % 4 == 0:
				self.score[i-1] = "Clap"
			else:
				self.score[i-1] = "Beat"				

happy_bday = Song(["happy birthday to you",
	"I don't want to get sued",
	"So i'll stop right there"])

bulls_on_parade = Song(["They rally around the family", "With pockets full of shells"])

happy_bday.sing_me_a_song()
bulls_on_parade.sing_me_a_song()					

fireworks_lyrics = ["What's your name?", "How 'ya doing?", "What's ya mood?", "What's that song?"]

animal_collective = Song(fireworks_lyrics)
animal_collective.sing_me_a_song()

animal_collective.melody(8)
for beat in animal_collective.score:
	print "%s" % beat