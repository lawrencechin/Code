# Code

![image](image.jpg)

Collection of code samples/working files for various languages including answers to [Exercism problems](https://exercism.io/profiles/lorenzochang).

## Websites

![Java Problems](java_problems.jpg) 

Javascript answers to some programming questions posed on a **Java** course illustrated on a [website](https://lawrencechin.gitlab.io/Code/)

![Java Quiz](java_quiz.jpg)

A minor [Java Quiz](https://lawrencechin.gitlab.io/Code/java_quiz) made to help someone revise.
