// Edited Version to make it cleaner, a bit more modern
// Should we filter the links array first and run another pass over it?
// Convert into bookmarklet using: https://mrcoles.com/bookmarklet/
// Original Version at the bottom for reference
(function(){
    function checkImgMime(u){
        const t = u.split('.'),e = t[t.length-1].toLowerCase();
        return {
            gif:1,jpg:1,jpeg:1,png:1,mng:1
        }[e];
    }

    const z = open().document;
    const q = [ ...document.links ];
    z.write("<style>img{max-width:100%;}</style>");
    z.write(`<p>Images linked to by ${ encodeURI(location.href)}:</p><hr>`);
    q.forEach( link => {
        const h = link.href;
        if(h && checkImgMime(h))
            z.write(`<img src="${ encodeURI( h )}"><br>`);
    });
    z.close();
})();

// Original bookmarlet
javascript:(function(){function%20I(u){var%20t=u.split('.'),e=t[t.length-1].toLowerCase();return%20{gif:1,jpg:1,jpeg:1,png:1,mng:1}[e]}function%20hE(s){return%20s.replace(/&/g,'&amp;').replace(/>/g,'&gt;').replace(/</g,'&lt;').replace(/"/g,'&quot;');}var%20q,h,i,z=open().document;z.write('<p>Images%20linked%20to%20by%20'+hE(location.href)+':</p><hr>');for(i=0;q=document.links[i];++i){h=q.href;if(h&&I(h))z.write('<p>'+q.innerHTML+'%20('+hE(h)+')<br><img%20src="'+hE(h)+'">');}z.close();})()
