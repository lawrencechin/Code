// use this link to encode the final code : https://mrcoles.com/bookmarklet/
(function(){
    const videoLinks = [...document.querySelectorAll("video")];
    const newTab = open().document;
    newTab.write("<style>html{background-color:#222;color:white;font-family:-apple-system;font-size:0.8em;}video{max-width:100%;}</style>");
    newTab.write("<h1>Video Links</h1><hr>");
    videoLinks.forEach(link => {
        const l = link.children;
        let src = l[0] && l[0].tagName === "SOURCE"
            ? encodeURI(l[0].src)
            : encodeURI(link.src);
        newTab.write(`<h3>Link to video: ${src}</h3>`);
        newTab.write(`<video src="${src}" controls=true></video>`);
    });
    newTab.close();
})();
