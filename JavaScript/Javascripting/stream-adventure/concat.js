/* Take input from stdin and reverse the text before piping 
it back to stdout */

var concat = require('concat-stream');

process.stdin.pipe(concat(function(text){
	process.stdout.write(text.toString().split('').reverse().join(''));
}));