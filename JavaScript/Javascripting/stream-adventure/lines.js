/* Transform the even numbered lines from stdin into uppercase
and return all data(transformed and otherwise) to stdout */

var split = require('split');
var through = require('through2');
var count = 0;
process.stdin.pipe(split()).pipe(through(function(buffer, encoding, next){
	var line = buffer.toString();
	this.push(count % 2 === 0 ? line.toLowerCase()+'\n' : line.toUpperCase()+'\n');
	count++;
	next();
}), function(done){
	done();
}).pipe(process.stdout);