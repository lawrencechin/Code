/* Convert data from stdin to uppercase and output
to stdout using through2 module */

//Through module takes two callback functions as paras:
//a write function that 'pushes' data and the end function
//that concludes operation - both functions are optional

var through = require('through2');
process.stdin.pipe(through(function(buffer, encoding, next){
	this.push(buffer.toString().toUpperCase());
	next();
}, function(done){
	done();
})).pipe(process.stdout);