var spawn = require('child_process').spawn;
var duplexer = require('duplexer2');

module.exports = function(cmd, args){
	var cmdSpawn = spawn(cmd, args);
	return duplexer(cmdSpawn.stdin,cmdSpawn.stdout);
}