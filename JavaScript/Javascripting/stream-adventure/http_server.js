var http = require('http');
var through = require('through2');


http.createServer(function(req, res){
	if(req.method == 'POST'){ //yay!
		req.pipe(through(function(buffer, encodings, next){
			this.push(buffer.toString().toUpperCase());
			next();
		}, function(done){
			done();
		})).pipe(res);
	}else{
		res.end('We would like a POST request please.');
	}
}).listen(process.argv[2]);