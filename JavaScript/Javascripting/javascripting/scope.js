var a = 1, b = 2, c = 3;

(function firstFunction(){
	var b = 5, c = 6; //scoped to firstFunction
	(function secondFunction(){
		var b = 8; //scoped to secondFunction
		console.log("a: " +a+", b: " +b+", c: "+c);
		(function thirdFunction(){
			var a = 7, c = 9; //scoped to thirdFunction
			(function fourthFunction(){
				var a = 1; c = 8; //scoped to fourthFunction
			})(); //self invoking
		})();
	})();
})();