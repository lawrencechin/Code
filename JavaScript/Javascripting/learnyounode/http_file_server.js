/* Create a http file server that serves a text file
for each request made */

var http = require('http');
var fs = require('fs');

http.createServer(function(req, res){
	res.writeHeader(200, {'content-type' : 'text/plain'});
	fs.createReadStream(process.argv[3]).pipe(res);
}).listen(process.argv[2]);