/* GET three streams and output the string data
in order */

var http = require('http');
var bl = require('bl');
var resData = [];
var resCount = 0;

function httpGet(index){
	http.get(process.argv[2 + index], function(res){
		res.pipe(bl(function(err, data){
			if(err) return console.error(err);
			resData[index] = data.toString();
			resCount++;
			if(resCount === process.argv.length - 2){
				console.log(resData.join('\n'));
			}
		}));
	});
}

for(var i = 0; i < 3; i++){
	httpGet(i)
}


