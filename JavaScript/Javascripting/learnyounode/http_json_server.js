var http = require('http');
var url = require('url');

http.createServer(function(req, res){
	if(req.method !== 'GET'){
		return res.end('Please send me a GET request, thanks\n');
	}
	var reqUrl = url.parse(req.url, true);
	var date = new Date(reqUrl.query.iso);
	if(reqUrl.pathname == '/api/parsetime'){
		var dateObj = {
			hour : date.getHours(),
			minute : date.getMinutes(),
			second : date.getSeconds()
		}
		res.writeHead(200, {'content-type' : 'application/json'});
		res.end(JSON.stringify(dateObj));
	}else if(reqUrl.pathname == '/api/unixtime'){
		res.writeHead(200, {'content-type' : 'application/json'});
		var unixDate = {unixtime : date.getTime()};
		res.end(JSON.stringify(unixDate));
	}else{
		res.writeHeader(404);
		res.end();
	}
}).listen(process.argv[2]);