/* Collects multiple streams from a http GET request
then writes the data to the console */

var http = require('http');
var bl = require('bl'); //used to join multiple stream data

http.get(process.argv[2], function(res){
	res.pipe(bl(function(err, data){
		if(err) throw err;
		/* official solution : 
		if(err) return console.error(err);
		*/
		data = data.toString();
		console.log(data.length);
		console.log(data);
	}));
});





