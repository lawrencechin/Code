/* Write a TCP time server using net module 
in the format : "YYYY-MM-DD hh:mm" (zero filled if needed eg. 01)
*/

var net = require('net');
net.createServer(function(socket){
	var date = new Date();
	var year = date.getFullYear();
	//add zero to the date and grab/slice the last two digits eg.
	// 01 = 01
	// 011 = 11
	var mon = ('0'+(date.getMonth()+1)).slice(-2);
	var day = ('0'+date.getDate()).slice(-2);
	var hour = ('0'+date.getHours()).slice(-2);
	var min = ('0'+date.getMinutes()).slice(-2);
	var dateStr = year + '-' + mon + '-' + day + ' ' + hour + ':' + min + '\n';
	socket.end(dateStr); //same as socket.write(dateStr) + socket.end()
}).listen(process.argv[2]);

/* Offical Solution

var net = require('net')  
 function zeroFill(i) {  
   return (i < 10 ? '0' : '') + i  
 }  
 function now () {  
   var d = new Date()  
   return d.getFullYear() + '-'  
     + zeroFill(d.getMonth() + 1) + '-'  
     + zeroFill(d.getDate()) + ' '  
     + zeroFill(d.getHours()) + ':'  
     + zeroFill(d.getMinutes())  
 }  
   
 var server = net.createServer(function (socket) {  
   socket.end(now() + '\n')  
 })  
   
 server.listen(Number(process.argv[2]))  */