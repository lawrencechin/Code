var http = require('http');
var map = require('through2-map'); 

http.createServer(function(req, res){
	/*Offical Solution uses error handling */
	if(req.method != 'POST'){
		return res.end('send me a POST request please\n');
	}
	/* ----------------------- */
	req.pipe(map(function(chunk){
		return chunk.toString().toUpperCase();
	})).pipe(res);
}).listen(process.argv[2]);