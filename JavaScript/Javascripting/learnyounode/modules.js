/* Scan a directory, using a module, and log the data */
var scan = require('./scan');

scan(process.argv[2], process.argv[3], function(err, data){
	if(err){
		console.log('There was an error: ' + err);
	}

	data.forEach(function(item){
		console.log(item);
	});
});