console.log("Welcome to Javascript : the good parts!");
//Extend the basic types in javascript by writing a 'method' method attached to the Function prototype
//After this we can simply extend the prototype by writing Object.newMethod with the applicable params
Function.prototype.method = function(name, func){
	//other libraries or plugins might attempt to write 
	//their own methods to the prototype
	//check if method exists before 
	if(!this.prototype[name]){
		this.prototype[name] = func;
		return this;
	}
}

var Grammar = {
	init : function(){
		console.log('This section details notes about grammar');
		console.log("Is 100 equal to 1e2?", 100 === 1e2);
		console.log("Is NaN equal to itself?", NaN === NaN);
		console.log("\\", "You cannot use the escape backslash slash on it's own as it will escape the last quotation mark! This backslash itself is escaped with a backslash");
		console.log('You can enter unicode characters using \\u(hexadecimal digits), e.g. \u0041 (\\u0041)');
		console.log('Switch, while, for and do statements have optional labels, e.g. loopy : for(...; ...; ...;)');
		console.log('Let\'s run a for loop for 10 iterations and print out the prime numbers: ');
		loopy : for(var i = 0; i <= 10; i++){
			if(i === 2 || i === 3){
				console.log(i);
			}else if(i % 2 !== 0 && i % 3 !== 0){
				console.log(i);
			}
		}
		console.log("Falsy values: false, null, undefined, empty string '', number 0, number NaN. The string 'false' is true.");
		console.log(!false + ' : !false', !'false' + ' : !false');
		function returnTest(){
			return;
		}
		var test = returnTest();
		console.log('A blank return from a function will return the falsy value undefined', test);
		console.log('Break clauses break out of loops and switch statements. They can reference specific loops by using the loops label. Observe: ')

		firstLoop : for(var i = 0; i < 10; i++){
			console.log('This is the first loop with a label of firstLoop');
			secondLoop : for(var j = 0; j < 10; j++){
				console.log('This is the second loop with the imaginative label of secondLoop');
				lastLoop : for(var k = 0; k < 10; k++){
					console.log('This is the last loop. We wish to only print one instance of these messages. How do we do that?');
					break secondLoop;
				}
			}
			console.log('We simple type break secondLoop;! After we type this message under the other, nested, loops and type break; No need to use the label.');
			break;
		}
		console.log('Prefix operators : ');
		console.log('To number (+ "10")', + "10");
		console.log('Negate (- "10")', - "10");
		console.log('Type (typeof "10")', typeof "10");
		console.log('Logical Not (!"10")', ! "10");
		console.log('Infix operators : ')
		console.log('&& produces the first value if the first operand is falsy otherwise the second', false && 11);
		console.log('|| produces the first value if the first operand is truthy otherwise the second', true || 11);
	}
};

var Objects = {
	init : function(){
		console.log('This chapter deals with Objects! Woo!');
		console.log('Object values are undefined if the object property is nonexistant. One can use the || operator to set another value in this case. var test = Object.prop || something_else');
		console.log('Objects are passed arounf by reference and never copied');
		var stooge = {};
		var x = stooge;
		x.nickname = 'Curly';
		var nick = stooge.nickname;
		console.log(nick);
		if(typeof Object.create !== 'function'){
			Object.create = function(o){
				var F = function(o){};
				F.prototype = o;
				return new F();
			}
		}
		var another_stooge = Object.create(stooge);
		another_stooge.nickname = 'Moe';
		console.log(another_stooge.nickname);
		delete another_stooge.nickname;
		console.log(another_stooge.nickname);
	}
};

var FunFunction = {
	init : function(){
		console.log('This chapter deals with functions');
		var add = function(a,b){
			console.log(this); //window
			console.log(Function.name); //Function
			return a + b;
		}
		console.log(add(10, 15));

		var myObject = {
			value : 0,
			increment : function(inc){
				//function is part of an object called an method
				//this is scoped to the object that the method
				//belongs to
				this.value += typeof inc === 'number' ? inc : 1;
			},
			double : function(){
				var that = this; //workaround

				var helper = function(){
					//this function does not have access to the parent object via 'this'. To allieviate 'this' we can set this to that as a variable and access it in a function invocation pattern
					that.value = add(that.value, that.value);
				};

				helper();
			}
		};
		myObject.increment(5);
		myObject.increment();
		document.writeln(myObject.value);
		myObject.double();
		document.writeln(myObject.value);
		console.log('Constructor invocation');
		console.log('The constructor invocation pattern instantiates a function object using the new keyword. Objects of this type will, by convention, start with a capital letter.')
		var Quo = function(string){
			this.status = string;
		}

		Quo.prototype.get_status = function(){
			return this.status;
		}
		var myQuo = new Quo('confused');
		document.writeln(myQuo.get_status());
		console.log(myQuo);

		var array = [3, 4];
		var sum = add.apply(null, array);
		console.log(sum);

		var statusObject = {
			status : 'A-OK'
		};

		var status = Quo.prototype.get_status.apply(statusObject);
		console.log(status);

		var sum = function(){
			var i, sum = 0;
			for(i = 0; i < arguments.length; i+=1){
				sum += arguments[i];
			}
			return sum;
		}

		document.writeln(sum(4,8,15,16,23, 42));

		var add = function(a, b){
			if(typeof a !== 'number' || typeof b !== 'number'){
				throw{
					name : 'TypeError',
					message : 'add needs numbers'
				};
			}
			return a + b;
		}

		var try_it = function(){
			try{
				add('10', 'Sandwich');
			}catch(e){
				document.writeln(e.name + ' : ' +e.message);
			}
		}

		try_it();

		Number.method('integer', function(){
			//return an integer 
			return Math[this < 0 ? 'ceil' : 'floor'](this);
		});

		document.writeln((-10 / 3).integer());

		String.method('trim', function(){
			//trim leading and trailing whitespace
			return this.replace(/^\s+|\s+$/g, '');
		});
		document.writeln('"' + "   neat   ".trim() + '"');
		var hanoi = function(disc, src, aux, dst){
			if(disc > 0){
				hanoi(disc - 1, src, dst, aux);
				console.log("Disc: " + disc + " | Src : " + src + " | Aux : " + aux + " | Dst : " + dst);
				hanoi(disc-1, aux, src, dst);
			}
		};
		//important! Follow the arguments to work out where everything goes
		hanoi(3, 'Src', 'Aux', 'Dst');

		var walk_the_DOM = function walk(node, func){
			func(node); //run function on each node
			node = node.firstChild; //set node to first child
			while(node){ //while node exists
				walk(node, func); //recursive call to main func
				node = node.nextSibling;
			}
		};
		var getElementsByAttribute = function(att, value){
			var results = []; //results array
			walk_the_DOM(document.body, function(node){
				//check if node is an element type and that attribute matches passed param
				var actual = node.nodeType === 1 && node.getAttribute(att);
				if(typeof actual === 'string' && (actual === value || typeof value !== 'string')){
					results.push(node);
				} 
			});

			return results;
		}

		var factorial = function factorial(i, a){
			a = a || 1;
			if(i < 2){
				return a;
			}
			console.log(i, a);
			return factorial(i - 1, a * i);
		};

		document.writeln(factorial(4));

		var myObject = (function(){
			var value = 0;

			return{
				increment : function(inc){
					//increment by number or, if not number, by 1 
					value += typeof inc === 'number' ? inc : 1;
					return typeof inc === 'number'? 'Incrementing number by ' + inc : 'Incrementing number by 1'; 
				},

				getValue : function(){
					return value;
				}
			};
		}());

		console.log(myObject.getValue(), myObject.increment(50), myObject.getValue(), myObject.increment("sandwich"));

		var quo = function(status){
			return{
				get_status : function(){
					return status;
				}
			};
		};

		var myQuo = quo('amazed');
		document.writeln(myQuo.get_status());

		var fade = function(node){
			var level = 1;
			var step = function(){
				var hex = level.toString(16);
				node.style.backgroundColor = '#FFFF' + hex + hex;
				if(level < 15){
					level += 1;
					setTimeout(step, 100);
				}
			};
			setTimeout(step, 100);
		};

		fade(document.body);
		//Doesn't work!
		var add_the_handle = function(nodes){
			var i;
			for(i = 0; i < nodes.length; i += 1){
				nodes[i].onclick = function(e){
					alert[i];
				};
			}
		};
		//Does work - don't define functions in loops
		var add_the_handlers = function(nodes){
			var helper = function(i){
				return function(e){
					alert(i);
				};
			};
			var i;
			for(i = 0; i < nodes.length; i += 1){
				nodes[i].onclick = helper(i);
			}
		};

		add_the_handlers(document.getElementsByClassName('test_elem'));

		//Module pattern
		String.method('deentityify', function(){
			//only this method has access to entity making it private
			var entity = {
				quot : '"',
				lt : '<',
				gt : '>'
			};

			return function(){
				return this.replace(/&([^&;]+);/g, function(a,b){
					//b is the capture group that checks for html entity's between & and ;
					var r = entity[b];
					//checks b against entity object
					return typeof r === 'string' ? r : a;
					//returns r if r is a string and thus was found in the entity object otherwise returns original string
				});
			};
		}( ));

		document.writeln('&lt;&quot;&gt;&jim;'.deentityify());

		var serial_maker = function(){
			var prefix = '';
			var seq = 0;
			//serial maker is merely a set of functions
			//the functions can only interact with the variables as noted below 
			return {
				set_prefix : function(p){
					prefix = String(p);
				},
				set_seq : function(s){
					seq = s;
				},
				gensym : function(){
					var result = prefix + seq;
					seq += 1;
					return result;
				}
			};
		};
		//the seqer variable can not change or access the prefix or seq in any other way than using the provided functions
		var seqer = serial_maker();
		seqer.set_prefix('Q');
		seqer.set_seq(1000);
		var unique = seqer.gensym();
		console.log(unique, seqer);

		Function.method('curry', function(){
			var slice = Array.prototype.slice, 
			args = slice.apply(arguments), 
			that = this;
			return function(){
				return that.apply(null, args.concat(slice.apply(arguments)));
			};
		});

		var add1 = add.curry(1);
		document.writeln(add1(6));

		//inefficient fibonacci function
		var fibonacci = function(n){
			return n < 2 ? n : fibonacci(n - 1) + fibonacci(n - 2);
		};

		for(var i = 0; i <= 10; i += 1){
			document.writeln('//' + i + ': ' + fibonacci(i));
		}

		//better fibonacci function using hidden array in closure and memoization
		var fibonacci = (function(){
			//store fib sequence in array, only accessible by fib function
			var memo = [0, 1];
			//closure
			var fib = function(n){
				var result = memo[n]; //check if n is already stored in memo array
				if(typeof result !== 'number'){
					//run fib function and store result in memo array
					result = fib(n - 1) + fib(n - 2);
					memo[n] = result;
				}
				return result;
			};
			return fib;
		}());

		for(var i = 0; i <= 10; i += 1){
			document.writeln('//' + i + ': ' + fibonacci(i));
		}
		//memoizer helper function
		var memoizer = function(memo, formular){
			var recur = function(n){
				var result = memo[n];
				if(typeof result !== 'number'){
					result = formular(recur, n);
					memo[n] = result;
				}
				return result;
			};
			return recur;
		}

		var fibonacci = memoizer([0,1], function(recur, n){
			return recur(n - 1) + recur(n - 2);
		});

		var factorial = memoizer([1, 1], function(recur, n){
			return n * recur(n - 1);
		});
	}
};

var Inheritance = {
	init : function(){
		Function.method('new', function(){
			//create a new object that inherits from the constructor's prototype
			var that = Object.beget(this.prototype);
			//Invoke the constructor, binding -this- to the new object
			var other = this.apply(that, arguments);
			//If its return value isn't an object, substitute the new object
			return(typeof other === 'object' && other) || that;
		});

		Function.method('inherits', function(Parent){
			this.prototype = new Parent();
			console.log('has now inherited prototype from '+ Parent);
			return this;
		});

		var Mammal = function(name){
			this.name = name;
		}

		Mammal.prototype.get_name = function(){
			return this.name;
		};

		Mammal.prototype.says = function(){
			return this.saying || '';
		};

		var myMammal = new Mammal('Herb the Mammal');
		console.log(myMammal.get_name());

		var Cat = function(name){
			this.name = name;
			this.saying = 'meow';
		}.inherits(Mammal).method('purr', function(n){
			var i, s = '';
			for(i = 0; i < n; i += 1){
				if(s){
					s += '-';
				}
				s += 'r';
			}
			return s;
		}).method('get_name', function(){
			return this.says() + ' ' + this.name + ' ' + this.says();
		});

		var myMammal = {
			name : 'Herb the Mammal',
			get_name : function(){
				return this.name;
			},
			says : function(){
				return this.saying || '';
			}
		};

		var myCat = Object.create(myMammal);
		myCat.name = 'Henrietta';
		myCat.saying = 'meow';
		myCat.purr = function(n){
			var i, s = '';
			for(i = 0; i < n; i += 1){
				if(s){
					s += '-';
				}

				s += 'r';
			}
			return s;
		};

		myCat.get_name = function(){
			return this.says() + ' ' + this.name + ' ' + this.says();
		};
		console.log(myCat.saying, myCat.get_name(), myCat.purr(5));

		var block = function(){
			var oldScope = scope;
			scope = Object.create(scope);
			advance('{');
			parse(scope);
			advance('}');
			scope = oldScope;
		}

		var mammal = function(spec){
			var that = {};

			that.get_name = function(){
				return spec.name;
			};
			that.says = function(){
				return spec.saying || '';
			};
			return that;
		};

		var myMammal = mammal({name : 'Herb'});

		var cat = function(spec){
			spec.saying = spec.saying || 'meow';
			var that = mammal(spec);
			that.purr = function(n){
				var i, s = '';
				for(i = 0; i < n; i += 1){
					if(s){
						s += '-';
					}
					s += 'r';
				}
				return s;
			};
			that.get_name = function(){
				return that.says() + ' ' + spec.name + ' ' + that.says();
			};
			return that;
		};

		var myCat = cat({name : 'King'});
		console.log(myCat.get_name(), myCat.purr(5), myCat.says());

		Object.method('superior', function(name){
			var that = this,
			method = that[name];
			return function(){
				return method.apply(that, arguments);
			};
		});

		var coolcat = function(spec){
			var that = cat(spec),
			super_get_name = that.superior('get_name');
			that.get_name = function(n){
				return 'like ' + super_get_name() + ' baby';
			};
			return that;
		};

		var myCoolCat = coolcat({name : 'Bix'});
		console.log(myCoolCat.get_name());

		var eventuality = function(that){
			var registry = {};
			that.fire = function(event){
				var array, func, handler, i, type = typeof event === 'string' ? event : event.type;
				if(registry.hasOwnProperty(type)){
					array = registry[type];
					for(i = 0; i < array.length; i += 1){
						handler = array[i];
						func = handler.method;
						if(typeof func === 'string'){
							func = this[func];
						}

						func.apply(this, hander.parameters || [event]);
					}		
				}
				return this;
			};
			that.on = function(type, method, parameters){
				var handler = {
					method : method,
					parameters : parameters
				};
				if(registry.hasOwnProperty(type)){
					registry[type].push(handler);
				}else{
					registry[type] = [handler];
				}
				return this;
			};
			return that;
		};
	}
};

var Arrays = function(spec){
	var that = {};
	that.introduce = function(){
		return "Welcome to Chapter " + spec.chapter + ". This section deals with " + spec.chapter_name + '. Hopefully we shall understand this one better than the last two!';
	}
	that.init = function(){
		console.log(that.introduce());
		var is_array = function(value){
			//return value && typeof value === 'object' && value.constructor === Array;
			return Object.prototype.toString.apply(value) === '[object Array]';
		};

		var test = [1, 34, 67, 2, 7, 596];
		console.log(is_array(test));
	};

	return that;
};
//Grammar.init();
//Objects.init();
//FunFunction.init();
//Inheritance.init();
var arr = Arrays({chapter : 6, chapter_name : 'Arrays'});
arr.init();