// Explanation: https://medium.com/@hazemu/finding-the-median-of-2-sorted-arrays-in-logarithmic-time-1d3f2ecbeb46
/**
There are two sorted arrays nums1 and nums2 of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

You may assume nums1 and nums2 cannot be both empty.

Example 1:

nums1 = [1, 3]
nums2 = [2]

The median is 2.0
Example 2:

nums1 = [1, 2]
nums2 = [3, 4]

The median is (2 + 3)/2 = 2.5
 **/

/**
const findMediaSortedArrays = ( nums1, nums2 ) => {
    const sortedMergedArr = [ ...nums1, ...nums2 ].sort(
        ( a, b ) => a - b
    );
    const dividedLen = sortedMergedArr.length / 2;

    const median = dividedLen % 1 === 0
        ? ( sortedMergedArr[ dividedLen ] + sortedMergedArr[ dividedLen - 1 ]) / 2
        : sortedMergedArr[ Math.floor( dividedLen )];

    return median.toFixed( 1 );
};
 **/

// O( n/2 ) 
const mergeSortedArrays = ( nums1, nums2, middle, i = 0, j = 0, xs = []) => {
    if( xs.length === middle + 1 ) return xs;
    if( nums1[ i ] < nums2[ j ] || nums2[ j ] === undefined ){
        xs.push( nums1[ i ]);
        return mergeSortedArrays( nums1, nums2, middle, i + 1, j, xs );
    }

    xs.push( nums2[ j ]);
    return mergeSortedArrays( nums1, nums2, middle, i, j + 1, xs );
};

const findMediaSortedArrays = ( nums1, nums2 ) => {
    const m = ( nums1.length + nums2.length ) / 2;
    const mFloored = Math.floor( m );
    const mergeSortedMiddle = mergeSortedArrays( nums1, nums2, mFloored );

    return m % 1 === 0
        ? (( mergeSortedMiddle[ mFloored - 1 ] + mergeSortedMiddle[ mFloored ] ) / 2 ).toFixed( 1 )
        : mergeSortedMiddle[ mFloored ].toFixed( 1 );
};

module.exports = findMediaSortedArrays;
