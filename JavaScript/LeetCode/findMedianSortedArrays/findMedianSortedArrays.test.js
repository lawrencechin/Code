const fmsa = require( "./findMedianSortedArrays.js" );

test( "Median 2.0", () => {
    expect( fmsa([ 1, 3 ], [ 2 ])).toBe( "2.0" );
});

test( "Median 2.5", () => {
    expect( fmsa([ 1, 2 ], [ 3, 4 ])).toBe( "2.5" );
});

test( "Median 6.0", () => {
    expect( fmsa([ 2, 4, 6, 8 ], [ 1, 5, 9, 11, 59 ])).toBe( "6.0" );
});

test( "Median 12.5", () => {
    expect( fmsa([ 5, 10, 15, 20, 21 ], [ 6 ])).toBe( "12.5" );
});

test( "Median 26.0", () => {
    expect( fmsa([ -100, -57, -12, 79, 102 ], [ -46, -45, -5, 26, 36, 46, 100, 110 ])).toBe( "26.0" );
});

test( "Unsorted array?", () => {
    expect( fmsa([ 9, 3, 7 ], [ 10, 2, 8, 4 ])).toBe( "10.0" );
});
