const longestCommonPrefix = strArr => {
    let i = 0, end = 0, len = strArr.length;
    if( len === 0 ) return "";
    if( len === 1 ) return strArr[ 0 ];

    for( ; ; i++ ){
        let j = 0;
        const char = strArr[ j++ ][ i ]; 

        for( ; j < len; j++ ){
            const c = strArr[ j ][ i ];
            if( !c || c !== char ) break;
        }

        if( j === len ) end++;
        else break;
    }

    return end > 0 ? strArr[ 0 ].substring( 0, end ) : "";
};

longestCommonPrefix([]); // ""
longestCommonPrefix([
    "flow",
    "flight",
    "fluid"
]); // "fl"
