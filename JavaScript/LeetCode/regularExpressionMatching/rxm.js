isMatch = ( s, p ) => {
    const starArr = [], sLen = s.length, pLen = p.length;
    let i = 0, j = 0, lastChar = "";

    while( i !== -1 ){
        if( i === sLen && j === pLen ) break; 
        const sChar = s[ i ], jChar = p[ j ], jStar = p[ j + 1 ];

        if( jStar && jStar == "*" ){
            starArr.push( jChar );
            j = j + 2;
            continue;
        }

        if( sChar === jChar || sChar && jChar === "." ){
            if( starArr[ 0 ]){
                while( starArr[ 0 ]){
                    const starChar = starArr[ 0 ];
                    if( starChar === sChar || starChar === "." ){
                        i = i + 1;
                        j = j + 1;
                        if( j === pLen )
                            lastChar = p[ j - 1 ];
                        break;
                    }
                    starArr.shift();
                }
            } else {
                i = i + 1;
                j = j + 1;
            }
            
        } else if( starArr[ 0 ]){
            if( i + 1 === sLen && j === pLen && lastChar.length ){
                if( sChar === lastChar || lastChar === "." ){
                    i = i + 1;
                    break;
                } else { 
                    i = i - 1;
                    break;
                }
            }
            while( starArr[ 0 ]){
                const starChar = starArr[ 0 ];
                if( starChar === sChar || sChar && starChar === "." ){
                    i = i + 1;
                    break;
                }

                starArr.shift();
            }
        } else i = -1;
        console.log( i, j, sChar, jChar, starArr );
    }

    return i === sLen && j === pLen;
};


isMatch( "", "" );
isMatch( "a", "" );
isMatch( "", "a" );
isMatch( "aaa", "aaa" );
isMatch( "aaa", "a*a" );
isMatch( "aaa", "aaaa" );
isMatch( "aaa", "a*b*" );
isMatch( "aaa", "a.*a" );
isMatch( "aaa", "a*b*c*d*e*f*" );
isMatch( "aa", "a" );
isMatch( "aa", "a*" );
isMatch( "ab", ".*" );
isMatch( "aab", "c*a*b" );
isMatch( "mississippi", "mis*is*p*." );
isMatch( "a", "ab*a" );
isMatch( "ab", ".*.." );
isMatch( "aasdfasdfasdfasdfas", "aasdf.*asdf.*asdf.*asdf.*s" );
isMatch( "mississippi", "mis*is*ip*." );
isMatch( "bbba", ".*b" );
isMatch( "acaabbaccbbacaabbbb", "a*.*b*.*a*aa*a*" );
