/**
 Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*'.

'.' Matches any single character.
'*' Matches zero or more of the preceding element.
The matching should cover the entire input string (not partial).

Note:

s could be empty and contains only lowercase letters a-z.
p could be empty and contains only lowercase letters a-z, and characters like . or *.
Example 1:

"aaa", "aaa"
"aaa", "a*a"
"aaa", "aaaa"
"aaa", "a*b*"
"aaa", "a.*a"
"aaa", "a*b*c*d*e*f*"
Input:
s = "aa"
p = "a"
Output: false
Explanation: "a" does not match the entire string "aa".
Example 2:

Input:
s = "aa"
p = "a*"
Output: true
Explanation: '*' means zero or more of the precedeng element, 'a'. Therefore, by repeating 'a' once, it becomes "aa".
Example 3:

Input:
s = "ab"
p = ".*"
Output: true
Explanation: ".*" means "zero or more (*) of any character (.)".
Example 4:

Input:
s = "aab"
p = "c*a*b"
Output: true
Explanation: c can be repeated 0 times, a can be repeated 1 time. Therefore it matches "aab".
Example 5:

Input:
s = "mississippi"
p = "mis*is*p*."
Output: false
 **/

isMatch = ( s, p ) => {
    const sLen = s.length, pLen = p.length;
    let i = 0, j = 0, star = false, dot = false, starCount = 0;

    while( i !== -1 ){
        if( i === sLen && j === pLen ) break;
        dot = p[ j ] === ".";
        star = p[ j + 1 ] && p[ j + 1 ] === "*";

        if( s[ i ] === p[ j ] || dot && i !== sLen ){
            starCount = star ? starCount + 1 : starCount;
            i = i + 1;
            j = star ? j : j + 1;
        } else if( star ) j = j + 2;
        else if( i === sLen && j !== pLen ){
            if( starCount > 0 ){
                starCount = starCount - 1;
                i = i - 1;
            } else if( star ) j = j + 2;
            else i = -1;
        } else i = -1;
    }

    return i === sLen && j == pLen;
};


isMatch( "", "" );
isMatch( "a", "" );
isMatch( "", "a" );
isMatch( "aaa", "aaa" );
isMatch( "aaa", "a*a" );
isMatch( "aaa", "aaaa" );
isMatch( "aaa", "a*b*" );
isMatch( "aaa", "a.*a" );
isMatch( "aaa", "a*b*c*d*e*f*" );
isMatch( "aa", "a" );
isMatch( "aa", "a*" );
isMatch( "ab", ".*" );
isMatch( "aab", "c*a*b" );
isMatch( "mississippi", "mis*is*p*." );
isMatch( "a", "ab*a" );
isMatch( "ab", ".*.." );
isMatch( "aasdfasdfasdfasdfas", "aasdf.*asdf.*asdf.*asdf.*s" );
isMatch( "mississippi", "mis*is*ip*." );
isMatch( "bbba", ".*b" );
isMatch( "acaabbaccbbacaabbbb", "a*.*b*.*a*aa*a*" );
