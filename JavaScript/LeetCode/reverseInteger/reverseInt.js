/**
Given a 32-bit signed integer, reverse digits of an integer.

Example 1:

Input: 123
Output: 321
Example 2:

Input: -123
Output: -321
Example 3:

Input: 120
Output: 21
Note:
Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.

The upper bound of a signed integer is not 232 - 1, but 231 - 1, since the first bit is the sign bit.

Use abs to ignore negative numbers
Math.ceil( Math.log10( num + 1 )) - to get the length number

Use modulus in conjunction with the base to generate number e.g.

123 ->  123 % 10 = 3 * 100 = 300
        120 % 100 = 20 = 20
        100 % 1000 = 100 / 100 = 1
        = 312

1234 -> 1234 % 10 = 4 * 1000 = 4000
        1230 % 100 = 30 * 10 = 300
        1200 % 1000 = 200 / 10 = 20
        1000 % 10000 = 1000 / 1000 = 1
        = 4321

12345 -> 12345 % 10 = 5 * 10000 = 50000
         12340 % 100 = 40 * 100 = 4000
         12300 % 1000 = 300 
         12000 % 10000 = 2000 / 100 = 20
         10000 % 100000 = 10000 / 10000 = 1
         = 54321
 **/

const reverse = n => {
    const negative = n < 0 ? true : false;
    let num = Math.abs( n ), result = 0;

    while( num !== 0 ){
        result = result * 10 + ( num % 10 );
        num = Math.floor( num / 10 );
    }

    return result > Math.pow( 2, 31 ) - 1 
        ? 0
        : negative 
            ? -result 
            : result;
};

