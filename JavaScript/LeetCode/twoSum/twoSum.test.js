const twoSum = require( './twoSum.js' );

test( "target 9, [ 2, 7, 11, 15 ]", () => {
    expect( twoSum([ 2, 7, 11, 15 ], 9 )).toEqual([ 0, 1 ]);
});

test( "target 10, [ 1, 20, 10, 8, 5, 7 ], no match", () => {
    expect( twoSum([ 1, 20, 10, 8, 5, 7 ], 10 )).toBeNull();
});

test( "target 25, [ 5, 26, 25, 500, 7000, -5, 10, 24, 8, 9, 17, 1 ]", () => {
    expect( twoSum([ 5, 26, 25, 500, 7000, -5, 10, 24, 8, 9, 17, 1 ], 25 )).toEqual([ 7, 11 ]);
});

test( "target 3, [ -1, -5, -7, -20, 4, 9 ]", () => {
    expect( twoSum([ -1, -5, -7, -20, 4, 9 ], 3 )).toEqual([ 0, 4 ]);
});

test( "target -500, [ -1, -1000, -60, 700, -495, -17, -52, 100.7, -5 ]", () => {
    expect( twoSum([ -1, -1000, -60, 700, -495, -17, -52, -5 ], -500 )).toEqual([ 4, 7 ]);
});

test( "target 6, [ 1, 3, 4, 2 ]", () => {
    expect( twoSum([ 1, 3, 4, 2 ], 6 )).toEqual([ 2, 3 ]);
});
