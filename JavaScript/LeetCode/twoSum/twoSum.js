// Given a target and an array of numbers
// find out which two numbers equal the target
//
// There will be exactly one match
// Must be unique numbers (no repeats)
// Return the index of the each selected number as an array

// Example:
// target 9, arr [ 2, 7, 11, 15 ]
// return [ 0, 1 ]

/**
const compareFirstToRestInArr = ( arr, cur, nxt, comparison ) => {
    const l = arr.length;
    if( cur === l - 1 ) return null;
    if( nxt === l ) return compareFirstToRestInArr( arr, cur + 1, cur + 2, comparison );

    const n1 = arr[ cur ];
    const n2 = arr[ nxt ];
    if( comparison( n1, n2 )) return [ n1, n2 ];

    return compareFirstToRestInArr( arr, cur, nxt + 1, comparison );
};

const twoSum = ( numArr, target ) => {
    const res = compareFirstToRestInArr( 
        numArr,
        0,
        1,
        ( n1, n2 ) => n1 + n2 === target
    );

    if( res )
        return [ 
        numArr.indexOf( res[ 0 ]),
        numArr.indexOf( res[ 1 ])
    ];

    return res;
};
const twoSum = ( numArr, target ) => {
    let i = 0; const len = numArr.length;
    for( i; i < len; i++ ){
        for( let j = i + 1; j < len; j++ ){
            if( numArr[ i ] + numArr[ j ] === target )
                return [ i, j ];
        }
    }
};
*/

const twoSum = ( numArr, target ) => {
    const hMap = new Map( numArr.map(( n, i ) => [ n, i ]));

    for( let i = 0; i < numArr.length; i++ ){
        const n = target - numArr[ i ];
        const hn = hMap.get( n );
        if( hn && i !== n ) return [ i, hn ];
    }
};

module.exports = twoSum;
