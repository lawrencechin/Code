const { 
    addTwoNumbers,
    createList
} = require( "./addTwoNumbers.js" );

test( "add two numbers together", () => {
    expect( addTwoNumbers( createList([ 2, 4, 3 ]), createList([ 5, 6, 4 ]))).toEqual( createList([ 7, 0, 8 ]));
});

test( "add two more numbers together", () => {
    expect( addTwoNumbers( createList([ 7, 0, 8 ]), createList([ 3, 9, 1 ]))).toEqual( createList([ 0, 0, 0, 1 ]));
});

test( "add two numbers, one of which is 0", () => {
    expect( addTwoNumbers( createList([ 0 ]), createList([6, 4, 3, 9, 6 ]))).toEqual( createList([ 6, 4, 3, 9, 6 ]));
});

test( "failed test case", () => {
    expect( addTwoNumbers(
        createList([ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 ]),
        createList([ 5, 6, 4 ]))
    ).toEqual( createList([ 6, 6, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 ]));
});
