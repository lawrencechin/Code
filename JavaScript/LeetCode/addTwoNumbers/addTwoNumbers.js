/**
You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 **/

function ListNode(val) {
    this.val = val;
    this.next = null;
}

const createList = ( numArr, i = 0, list = null, currentNode = null ) => {
    if( i === numArr.length ) return list;

    const node = new ListNode( numArr[ i ]);
    if( !list ) list = node;
    else if( !currentNode ) list.next = node;
    else currentNode.next = node;

    return createList( numArr, i + 1, list, node );
};

const processTwoNodes = ( n1, n2, remainder = 0, list = null, current = null ) => {
    if( !n1 && !n2 ){
        if( remainder === 1 ) current.next = new ListNode( 1 );
        return list;
    }
    let num1 = 0, num2 = 0, node1 = null, node2 = null;
    if( n1 ){ num1 = n1.val; node1 = n1.next; }
    if( n2 ){ num2 = n2.val; node2 = n2.next; }

    let val = num1 + num2 + remainder;

    if( val > 9 ){
        val = val % 10;
        remainder = 1;
    } else remainder = 0;

    const node = new ListNode( val );
    if( !list ) list = node;
    else if( !current ) list.next = node;
    else current.next = node;

    return processTwoNodes( node1, node2, remainder, list, node );
};


const addTwoNumbers = ( l1, l2 ) => {
    return processTwoNodes( l1, l2 );
};

module.exports = {
    addTwoNumbers,
    createList
};
