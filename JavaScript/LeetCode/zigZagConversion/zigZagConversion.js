/**
The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R

5 rows 4 columns
00    04    08    12
01 03 05 07 09 11 13
02    06    10

And then read line by line: "PAHNAPLSIIGYIR"

Write the code that will take a string and make this conversion given a number of rows:

string convert(string s, int numRows);
Example 1:

Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"
Example 2:

Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"
Explanation:

P     I    N
A   L S  I G
Y A   H R
P     I
**/

const convert = ( s, numRows ) => {
    if( numRows === 0 || s === "" ) return "";
    if( numRows === 1 ) return s;

    const len = s.length;
    const finalStrArr = [];
    const columnGap = numRows + numRows - 2;

    for( let i = 0; i < numRows; i++ ){
        finalStrArr.push( s[ i ]);
        let j = columnGap;
        while( j ){
            let c1, c2;
            if( i !== 0 && i !== numRows - 1 ) 
                c1 = i + j - ( i * 2 );
            c2 = i + j; 

            if( c1 && c1 < len ) finalStrArr.push( s[ c1 ]);
            if( c2 < len ){
                finalStrArr.push( s[ c2 ]);
                j = j + columnGap;
            } else {
                j = false;
            }
        }
    }

    return finalStrArr.join( "" );
};
