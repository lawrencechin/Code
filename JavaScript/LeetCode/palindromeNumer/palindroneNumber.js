/**
Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.

Example 1:

Input: 121
Output: true
Example 2:

Input: -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
Example 3:

Input: 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
Follow up:

Could you solve it without converting the integer to a string?
 **/

// any negative number cannot be a palindrome
// check first and outer most number if they equal each other
// using modulus 
// until the two numbers converge in the middle
const isPalindrome = x => {
    if( x === 0 || x === 1 ) return true;
    if( x < 0 || x % 10 === 0 ) return false;

    let isPal = false,
        numLen = Math.ceil( Math.log10( x )) - 1, 
        base = Math.pow( 10, numLen );

    while( x > 0 ){
        const firstNum = Math.floor( x / base );
        const lastNum = x % 10;

        if( firstNum !== lastNum ) break;

        x = ( x - ( firstNum * base ) - lastNum ) / 10;
        base = base / 100;
        if( x <= 0 ) isPal = true;
    }

    return isPal;
};
