/**
Given a string, find the length of the longest substring without repeating characters.

Example 1:

Input: "abcabcbb"
Output: 3 
Explanation: The answer is "abc", with the length of 3. 
Example 2:

Input: "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3. 
             Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 **/

const charNotInArr = ( s, arr ) => arr.every( c => c !== s );
const charsOnlyInStrLower = s => s.replace( /(\W|\d)/g, "" ).toLowerCase();
const strAllOneChar = s => s.split( "" ).every( c => c === s[ 0 ] );
const findLongestSubString = ( s, i = 0, j = 0, curr = [], longest = []) => {
    if( i === s.length ) return longest;
    if( j === s.length ) return findLongestSubString(
        s,
        i + 1,
        i + 1,
        curr,
        longest
    );

    if( charNotInArr( s[ j ], curr )){
        curr.push( s[ j ]);
        return findLongestSubString( s, i, j + 1, curr, longest );
    }

    if( curr.length > longest.length ){
        longest = curr;
    }

    return findLongestSubString( s, i + 1, i + 1, [ s[ j ]], longest );
};

const lengthOfLongestSubString = s => {
    if( s.length === 0 ) return 0;
    const adjustedStr = charsOnlyInStrLower( s );
    if( strAllOneChar( adjustedStr )) return 1;

    return findLongestSubString( adjustedStr ).length;
};

module.exports = lengthOfLongestSubString;
