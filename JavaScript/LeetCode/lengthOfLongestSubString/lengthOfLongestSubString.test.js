const lenLongestSubStr = require( "./lengthOfLongestSubstring.js" );

test( "no match", () => {
    expect( lenLongestSubStr( "" )).toBe( 0 );
});

test( "single letter", () => {
    expect( lenLongestSubStr( "a" )).toBe( 1 );
});

test( "single letter repeated", () => {
    expect( lenLongestSubStr( "aaaaaaaa" )).toBe( 1 );
});

test( "3 letter substrings", () => {
    expect( lenLongestSubStr( "bcbcd" )).toBe( 3 );
    expect( lenLongestSubStr( "bbcd" )).toBe( 3 );
    expect( lenLongestSubStr( "abcabcbb" )).toBe( 3 );
    expect( lenLongestSubStr( "pwwkew" )).toBe( 3 );
});

test( "4 letter substrings, sneaky 5", () => {
    expect( lenLongestSubStr( "abcabcaacbfacbfbc" )).toBe( 4 );
    expect( lenLongestSubStr( "ffffffffffffabfffffffffbcfffffabcffff" )).toBe( 4 );
    expect( lenLongestSubStr( "abcfabcfabcfabcfabcfabcfabcefabcfabcf" )).toBe( 5 );
});

test( "2, 3, 4, 5, 6 letter substrings consecutively", () => {
    expect( lenLongestSubStr( "ababcabcdabcdeabcdef" )).toBe( 6 );
});

test( "ignore spaces or non letters", () => {
    expect( lenLongestSubStr( "adbcdefag" )).toBe( 7 );
});
