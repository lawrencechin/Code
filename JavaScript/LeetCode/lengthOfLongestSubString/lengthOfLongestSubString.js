const lengthOfLongestSubstring = s => {
    const len = s.length;
    if( len === 0 ) return 0;
    
    let i = 0, j = 0, longest = 0, hMap = new Map();
    for( ; j < len; j++ ){
        console.log( i, j, longest, s[ j ], hMap );
        const jMapped = hMap.get( s[ j ]);
        if( jMapped )
            i = Math.max( jMapped, i );

        longest = Math.max( longest, j - i );
        hMap.set( s[ j ], j + 1 );
    }

    return longest + 1;
};

module.exports = lengthOfLongestSubstring;
