/** 
Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

Example 1:

Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:

Input: "cbbd"
Output: "bb"
**/

/*
const subStringLengthCheck = ( s, i, j, current ) => {
    const subS = s.substring( i, j + 1 );
    return subS.length > current.length ? subS : current;
};

const palindromeSubStringSearch = ( s, start = 0, i = 1, end = s.length, longest = "" ) => {
    if( start === end - 1 ) return longest;

    const preS = s[ start - i ] || false;
    const postS = s[ start + i ];
    const current = s[ start ];

    if( preS ){
        if( preS === postS )
            return palindromeSubStringSearch( 
                s, 
                start, 
                i + 1, 
                end, 
                subStringLengthCheck( s, start - i, start + i, longest ) 
            );
        else if( preS === current )
            longest = subStringLengthCheck(
                s,
                start - i,
                start,
                longest
            );
    }
            
    if( postS === current )
        longest = subStringLengthCheck(
            s,
            start,
            start + i,
            longest
        );
    return palindromeSubStringSearch( s, start + 1, 1, end, longest );
};
    
const longestPalindrome = s => {
    if( s.length === 0 ) return "";
    if( s.length === 1 ) return s;

    const palSubSearch = palindromeSubStringSearch( s );

    return palSubSearch === ""
        ? s[ 0 ]
        : palSubSearch;
};
*/

const checkAroundCenter = ( s, i, j ) => {
    if( i >= 0 && j < s.length && s[ i ] === s[ j ]) 
        return checkAroundCenter( s, i - 1, j + 1 );
    return j - i - 1;
};

const longestPalindrome = s => {
    const len = s.length;
    if( len === 0 ) return "";
    if( len === 1 ) return s;

    let start = 0, end = 0;

    for( let i = 0; i < len; i++ ){
        const odd = checkAroundCenter( s, i, i );
        const even = checkAroundCenter( s, i, i + 1 );
        const length = Math.max( odd, even );

        if( length > end - start ){
            start = length % 2 === 0
                ? i + 1 - length / 2
                : i - ( length - 1 ) / 2;
            end = start + length;
        }
    }

    return s.substring( start, end );
};

module.exports = longestPalindrome;
