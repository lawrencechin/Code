const lp = require( "./longestPalindrone.js" );

test( "babad", () => {
    expect( lp( "babad" )).toBe( "bab" );
});

test( "cbbd", () => {
    expect( lp( "cbbd" )).toBe( "bb" );
});

test( "z", () => {
    expect( lp( "z" )).toBe( "z" );
});

test( "", () => {
    expect( lp( "" )).toBe( "" );
});

test( "abcd", () => {
    expect( lp( "abcd" )).toBe( "a" );
});

test( "ebcdcbn", () => {
    expect( lp( "ebcdcbn" )).toBe( "bcdcb" );
});

test( "dddcbabcdgh", () => {
    expect( lp( "dddcbabcdgh" )).toBe( "dcbabcd" );
});

test( "abracadabra", () => {
    expect( lp( "abracadabra" )).toBe( "aca" );
});

test( "banana", () => {
    expect( lp( "banana" )).toBe( "anana" );
});

test( "abacdfgdcaba", () => {
    expect( lp( "abacdfgdcaba" )).toBe( "aba" );
});

test( "aaaa", () => {
    expect( lp( "aaaa" )).toBe( "aaaa" );
});

test( "aaaaaa", () => {
    expect( lp( "aaaaaa" )).toBe( "aaaaaa" );
});

test( "xaaaaaax", () => {
    expect( lp( "xaaaaaax" )).toBe( "xaaaaaax" );
});
