/*
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

Example 1:

Input: ["flower","flow","flight"]
Output: "fl"
Example 2:

Input: ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.
Note:

All given inputs are in lowercase letters a-z.
*/

const longestCommonPrefix = function(strs) {
    if( strs.length === 0 ) return "";
    if( strs.length === 1 ) return strs[ 0 ];
    
    let end = 0, i = 0, len = strs.length;
    
};
