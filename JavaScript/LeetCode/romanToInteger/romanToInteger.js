/*

Example 1:

Input: "III"
Output: 3
Example 2:

Input: "IV"
Output: 4
Example 3:

Input: "IX"
Output: 9
Example 4:

Input: "LVIII"
Output: 58
Explanation: L = 50, V= 5, III = 3.
Example 5:

Input: "MCMXCIV"
Output: 1994
Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
*/

/*
romanToInt = str => {
    const romanDict = {
        "I" : 1,
        "IV" : 4,
        "V" : 5,
        "IX" : 9,
        "X" : 10,
        "XL" : 40,
        "L" : 50,
        "XC" : 90,
        "C" : 100,
        "CD" : 400,
        "D" : 500,
        "CM" : 900,
        "M" : 1000
    };

    let total = 0, i = str.length - 1;

    while( i > -1 ){
        const char1 = str[ i ],
            char2 = str[ --i ],
            combined = char2 ? char2 + char1 : undefined;
        if( combined && romanDict[ combined ]){
            total += romanDict[ combined ];
            i -= 1;
        } else
            total += romanDict[ char1 ];
    }

    return total;
};
*/

romanToInt = str => {
    const romanDict = {
        "I" : 1,
        "IV" : 4,
        "V" : 5,
        "IX" : 9,
        "X" : 10,
        "XL" : 40,
        "L" : 50,
        "XC" : 90,
        "C" : 100,
        "CD" : 400,
        "D" : 500,
        "CM" : 900,
        "M" : 1000
    };

    let total = 0, i = str.length - 1;

    while( i > -1 ){
        const char1 = str[ i ],
            char2 = str[ --i ],
            combined = char2 ? char2 + char1 : undefined;
        if( combined && romanDict[ combined ]){
            total += romanDict[ combined ];
            i -= 1;
        } else
            total += romanDict[ char1 ];
    }

    return total;
};

romanToInt( "III" );
romanToInt( "IV" );
romanToInt( "IX" );
romanToInt( "LVIII" );
romanToInt( "MCMXCIV" );
romanToInt( "MCDLXXVI" );
