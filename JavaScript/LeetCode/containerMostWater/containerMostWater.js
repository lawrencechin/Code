/**
Given n non-negative integers a1, a2, ..., an , where each represents a point at coordinate (i, ai). n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0). Find two lines, which together with x-axis forms a container, such that the container contains the most water.

Note: You may not slant the container and n is at least 2.

Example:

Input: [1,8,6,2,5,4,8,3,7]
Output: 49

Explanation:
8 & 8 would yield an area of 64 but that isn't how it works.
In that example, 8 would represent the height of the container
but the width would be 5 yielding an area of 40.
On the other hand, 8 to 7 has a gap of 7 with a maximum area
of 7 * 7 (7 because the container can't slant so the minimum
height must be used rather than 8)

Other Examples:

[ 1, 2, 1 ] = 2 -> 1 * 2
[ 1, 2, 3, 4 ] = 4 -> 2 * 2
[ 1, 5, 1, 1, 1, 5 ] = 20 -> 5 * 4


Notes:

A height of 1 can only be as large as the furthest distance,
e.g. 1 * 8 using the above example. No need to check any more.

Can't reorder the array as the position is important.

If one records the tallest vertical no subsequent vertical that is smaller or equal can be larger in the same sequence.
 **/

/*
maxArea = heightArr => {
    const last = heightArr.length - 1;
    let area = 0, tallest = 0, i = 0, j = last;

    for( ; i < last; i++ ){
        const current = heightArr[ i ];
        if( current <= tallest ) continue;

        tallest = current;
        if( current === 1 ) 
            area = Math.max( area, 1 * ( last - i ));
        else{ 
            while( i !== j ){
                const len = j - i;
                const start = heightArr[ i ], end = heightArr[ j ];
                area = Math.max( area, Math.min( start, end ) * len );
                j--;
            }
        }

        j = last;
    }

    return area;
};

maxArea = heightArr => {
    const last = heightArr.length - 1;
    let area = 0, i = 0, iModified = false;

    for( ; i < last; i++ ){
        const current = heightArr[ i ];

        iModified = false;
        if( current === 1 ) 
            area = Math.max( area, 1 * ( last - i ));
        else{ 
            let j = i + 1;
            const start = heightArr[ i ];
            const iPos = i;
            while( j <= last ){
                const len = j - iPos;
                const end = heightArr[ j ];
                if( end > start & !iModified ){
                    i = j - 1;
                    iModified = true;
                }
                if( j === last && i === iPos & !iModified )
                    i = last;
                area = Math.max( area, Math.min( start, end ) * len );
                j++;
            }
        }
    }

    return area;
};
*/

maxArea = heightArr => {
    let i = 0, j = heightArr.length - 1, maxArea = 0;
    for( ; i !== j; ){
        const start = heightArr[ i ],
            end = heightArr[ j ],
            len = j - i;

        maxArea = Math.max( maxArea, Math.min( start, end ) * len );
        if( start <= end ) i++;
        else j--;
    }

    return maxArea;
};

maxArea([ 1, 2, 1 ]);
maxArea([ 1, 2, 3, 4 ]);
maxArea([ 1, 5, 1, 1, 1, 5 ]);
maxArea([ 1, 8, 6, 2, 5, 4, 8, 3, 7 ]);
maxArea([ 6, 4, 3, 1, 4, 6, 99, 62, 1, 2, 6 ]);
maxArea([ 2, 3, 10, 5, 7, 8, 9 ]);
