// Crear Scoreboard

const scope = "resultados_";
const data = [ "Mejor", "Peor", "Promedio" ];
const categories = [ "Velocidad", "Errores", "Marcador" ];

const createList = ( heading => {
    const ul = document.createElement( 'ul' );
    ul.classList.add( scope + "list" );
    const header = document.createElement( "li" );

    header.innerHTML = "<h2>"+heading+"</h2>";
    ul.appendChild( header );

    data.forEach( d => {
        const li = document.createElement( "li" );
        li.innerHTML = "<p id='"+heading+"_"+d+"'>0</p>";
        ul.appendChild( li );
    });

    return ul;
});

const container = document.createElement( "div" );
container.id = scope + "container";

const h1 = document.createElement( "h1" );
h1.innerHTML = "<em>¡Resultados!</em>";

const results_key = document.createElement( "p" );
results_key.id = scope + "results_key";
results_key.innerHTML = data.reduce(( s, d ) => {
    s += "<span id='rk_"+d+"'>"+d+"</span> ";
    return s;
}, "" );

const rounds = document.createElement( "p" );
rounds.id = scope + "rounds";
rounds.innerHTML = "Rondas: <span id='"+scope+"rondas'>0</span>";

const close_btn = document.createElement( "a" );
close_btn.href = "#";
close_btn.id = scope + "close_btn";
close_btn.innerHTML = "<strong>x</strong>";

const list_container = document.createElement( "div" );
list_container.id = scope + "list_container";

const warning = document.createElement( "div" );
warning.id = scope + "warning";
warning.innerHTML = "<p>¡Require la visita de modo 'Compact Mode' para funciona!";

container.appendChild( h1 );
container.appendChild( results_key );
categories.forEach( heading => {
    list_container.appendChild( createList( heading ));
});
container.appendChild( list_container );
container.appendChild( rounds );
container.appendChild( close_btn );
container.appendChild( warning );

const positioning_container = document.createElement( "div" );
positioning_container.id = scope + "pc";

positioning_container.appendChild( container );

close_btn.addEventListener( "click", e => {
    e.preventDefault();
    positioning_container.remove();
});

document.body.appendChild( positioning_container );
