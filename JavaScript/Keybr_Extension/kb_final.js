// Crear Scoreboard

const scope = "resultados_";
const data = [ "Mejor", "Peor", "Promedio" ];
const categories = [ "Velocidad", "Errores", "Marcador" ];

const createList = ( heading => {
    const ul = document.createElement( 'ul' );
    ul.classList.add( scope + "list" );
    const header = document.createElement( "li" );

    header.innerHTML = "<h2>"+heading+"</h2>";
    ul.appendChild( header );

    data.forEach( d => {
        const li = document.createElement( "li" );
        li.innerHTML = "<p id='"+heading+"_"+d+"'>0</p>";
        ul.appendChild( li );
    });

    return ul;
});

const container = document.createElement( "div" );
container.id = scope + "container";

const h1 = document.createElement( "h1" );
h1.innerHTML = "<em>¡Resultados!</em>";

const results_key = document.createElement( "p" );
results_key.id = scope + "results_key";
results_key.innerHTML = data.reduce(( s, d ) => {
    s += "<span id='rk_"+d+"'>"+d+"</span> ";
    return s;
}, "" );

const rounds = document.createElement( "p" );
rounds.id = scope + "rounds";
rounds.innerHTML = "Rondas: <span id='"+scope+"rondas'>0</span>";

const close_btn = document.createElement( "a" );
close_btn.href = "#";
close_btn.id = scope + "close_btn";
close_btn.innerHTML = "<strong>x</strong>";

const list_container = document.createElement( "div" );
list_container.id = scope + "list_container";

const warning = document.createElement( "div" );
warning.id = scope + "warning";
warning.innerHTML = "<p>¡Require la visita de modo 'Compact Mode' para funciona!";

container.appendChild( h1 );
container.appendChild( results_key );
categories.forEach( heading => {
    list_container.appendChild( createList( heading ));
});
container.appendChild( list_container );
container.appendChild( rounds );
container.appendChild( close_btn );
container.appendChild( warning );

const positioning_container = document.createElement( "div" );
positioning_container.id = scope + "pc";

positioning_container.appendChild( container );

close_btn.addEventListener( "click", e => {
    e.preventDefault();
    positioning_container.remove();
});

// Scoreboard data

const scoreboard = {
    turns : 0,
    velocidad : { mejor : 0.0, peor: false, promedio: 0 },
    errores : { mejor : false, peor: 0, promedio: 0 },
    marcador : { mejor : 0, peor: false, promedio: 0 }
};

// Scoreboard Functions

const updateScoreboard = ( velocidad, error, score ) => {
    const greaterThan = (( a, b ) => a > b ? a : b );
    const lessThan = (( a, b ) => a < b ? a : b );
    scoreboard.turns = scoreboard.turns + 1;

    scoreboard.velocidad.mejor = greaterThan( scoreboard.velocidad.mejor, velocidad );
    scoreboard.velocidad.peor = !scoreboard.velocidad.peor 
        ? velocidad 
        : lessThan( scoreboard.velocidad.peor, velocidad );
    scoreboard.velocidad.promedio = scoreboard.velocidad.promedio + velocidad;

    scoreboard.errores.mejor = !scoreboard.errores.mejor 
        ? error 
        : lessThan( scoreboard.errores.mejor, error );
    scoreboard.errores.peor = greaterThan( scoreboard.errores.peor, error );
    scoreboard.errores.promedio = scoreboard.errores.promedio + error;

    scoreboard.marcador.mejor = greaterThan( scoreboard.marcador.mejor, score );
    scoreboard.marcador.peor = !scoreboard.marcador.peor 
        ? score
        : lessThan( scoreboard.marcador.peor, score );
    scoreboard.marcador.promedio = scoreboard.marcador.promedio + score;
};

// DOM Values

const scores = () => document.querySelector( ".IndicatorRow-gauges" );
const speed = () => document.querySelector( ".Practice-speed .Value" );
const errors = () => document.querySelector( ".Practice-errors .Value" );
const score = () => document.querySelector( ".Practice-score .Value" );

// Mutation Observers

const scoreCb = ( mutation => {
    sc = parseInt( score().innerText );
    err = parseInt( errors().innerText );
    sp = parseFloat( speed().innerText );

    updateScoreboard( sp, err, sc );
    categories.forEach( c => {
        const cat = c.toLowerCase();
        data.forEach( d => {
            const dat = d.toLowerCase();
            if( dat === "promedio" ) {
                let average = scoreboard[ cat ][ dat ] / scoreboard.turns;
                average = average % 1 === 0 ? average : average.toFixed( 2 );
                document.querySelector( "#"+c+"_"+d ).innerText = average;
            } else
                document.querySelector( "#"+c+"_"+d ).innerText = scoreboard[ cat ][ dat ];
        });
    });
    document.querySelector( "#"+ scope + "rondas" ).innerText = scoreboard.turns;
});

let scoreObserver;

const connectObserver = () => {
    // observador muere cuando el elemento observado es eliminado
    // necesite crear un nuevo observador
    scoreObserver = new MutationObserver( scoreCb );
    scoreObserver.observe( scores(), { 
        characterData: true,
        childList: true,
        subtree: true
    });
};

const clsCb = ( mutation => {
    if( mutation[ 0 ].attributeName === "class" 
        && [ ...mutation[ 0 ].target.classList ].some( cls => cls === "Practice--bareLayout" )){
        warning.classList.add( "show" );
        scoreObserver = false;
    } else {
        warning.classList.remove( "show" );
        if( !scoreObserver ) connectObserver(); 
    }
});

const clsObserver = new MutationObserver( clsCb );

const stupidFunction = () => {
    if( !document.querySelector( ".Practice" ))
        setTimeout( stupidFunction, 1000 );
    else {
        document.body.appendChild( positioning_container );
        clsObserver.observe( document.querySelector( ".Practice" ), { attributes: true });
        connectObserver();
    }
};

stupidFunction();
