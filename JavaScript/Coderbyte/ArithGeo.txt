# ArithGeo

Using the JavaScript language, have the function ArithGeo(arr) take the array of numbers stored in arr and return the string "Arithmetic" if the sequence follows an arithmetic pattern or return "Geometric" if it follows a geometric pattern. If the sequence doesn't follow either pattern return -1. 

An arithmetic sequence is one where the difference between each of the numbers is consistent, where as in a geometric sequence, each term after the first is multiplied by some constant or common ratio. 

Arithmetic example: [2, 4, 6, 8] and Geometric example: [2, 6, 18, 54]. Negative numbers may be entered as parameters, 0 will not be entered, and no array will contain all the same elements. 

```javascript
function ArithGeo(arr){
	var difference = arr.reduce(function(cur, item, pos, arr){
		if(arr[pos+1]){
			cur.aMap.push(arr[pos + 1] - item);
			cur.gMap.push(arr[pos + 1] / item);	
		}else{
			cur.aFactor = cur.aMap.every(function(elem, index, arr){
				return arr[index + 1] ? elem === arr[index + 1] : true;
			});
			cur.gFactor = cur.gMap.every(function(elem, index, arr){
				return arr[index + 1] ? 
				elem === arr[index + 1] || arr[index + 1] / elem === elem
				: true;
			});
		}
		return cur;
	}, {aMap : [], gMap : [], aFactor : false, gFactor : false} );

	return difference.aFactor ? "Arithmetic" : difference.gFactor ? "Geometric" : -1; 
}
```