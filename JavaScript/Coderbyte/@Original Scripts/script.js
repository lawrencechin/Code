var Letter_Changes = {
	// Using the JavaScript language, have the function LetterChanges(str) take the str parameter being
	// passed and modify it using the following algorithm. Replace every letter in the string with the
	// letter following it in the alphabet (ie. c becomes d, z becomes a). 
	// Then capitalize every vowel in this new string (a, e, i, o, u) and finally return this modified string. 

	//using a built in alphabet
	LetterChanges : function(str){
		var alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z'];
		var newStr = '';
		var temp = null;
		//analyse each character of the str
		for(var i = 0; i < str.length; i++){
			//compare letters to alpha array
			for(var j = 0; j < alpha.length; j++){
				if(str[i].toLowerCase() === alpha[j]){
					//check if letter is Z
					if(j === alpha.length - 1){
						temp = alpha[0];
					}else{
						temp = alpha[j+1];
					}
					//capitalize vowels
					if(temp === 'a' || temp === 'e' || temp === 'i' || temp === 'o' || temp === 'u'){
						temp = temp.toUpperCase();
					}

					if(temp !== null){ break; }//stop checking if lettter has been found
					
				}
			}

			if(temp !== null){
				newStr += temp;
				temp = null; //reset temp after adding to newStr
			}else{
				//not an a-z character, leave as is
				newStr += str[i];
			}
		}
		str = newStr;
		return str;
	},

	//using ASCI functions
	LetterChange : function(str){
		var lowerUpperObj = {
			// a : A
			97 : 65,
			101 : 69,
			105 : 73,
			111 : 79,
			117 : 85
		};

		var newStr = '';
		//loop over str and test the ASCI number for each char
		for(var i = 0; i < str.length; i++){
			//lowercase a-z in range of 97 - 122 || uppercase A-Z in range of 65 - 90
			var charCode = str[i].charCodeAt(0);
			if(charCode >= 97 && charCode <= 122 || charCode >= 65 && charCode <= 90){
				if(charCode === 122 || charCode === 90){
					//change z to A || Z to A
					charCode = 65;
				}else{
					charCode++;
					if(lowerUpperObj.hasOwnProperty(charCode)){
						//note - there's a diff of 32 between all lower and uppercase vowels
						charCode = lowerUpperObj[charCode];
					}
				}
			}

			newStr += String.fromCharCode(charCode);
		}
		str = newStr;
		return str;
	},
	//i'm not great at reg ex patterns which would prove useful - here's another users solution using regex(i've added the comments to the function myself)
	OtherUserBetterSolution : function(str) { 
		return str.replace(/[a-z]/ig,function(c){ //replace all instances(g flag) of letters a-z(i flag - case insenitive) with the data from the callback function
		//get the next charcode 
		var next = String.fromCharCode(c.charCodeAt(0) + 1);
		//check if 'next' is a lowercase vowel and convert to uppercase
		if(/[aeiou]/g.test(next)){
		  next = next.toUpperCase(); 
		}
		//return A if character is either Z or z otherwise return next
		return c == 'z' ? 'A' : c == 'Z' ? 'A' : next;
		});    
		//this is a far superior solution to the problem - both the tools used and the pattern make a lot more sense. Bare this in mind when aproaching other problems and learn some darn regex
	},

	LetterChangeAgain : function(str){
		return str.replace(/[a-zA-Z]/g, function(c){
			var next = String.fromCharCode(c.charAt(0) + 1); //get next character
			next = /[aeiou]/i.test(next) ? next.toUpperCase() : next; //check if next is a vowel and convert to uppercase
			return c.test(/zZ/) ? 'A' : next;
		});
	}
};

var First_Factorial = {
	// Using the JavaScript language, have the function FirstFactorial(num) take the num parameter 
	// being passed and return the factorial of it (ie. if num = 4, return (4 * 3 * 2 * 1)).
	// For the test cases, the range will be between 1 and 18.

	FirstFactorial : function(num){
		var total = num;
		for(var i = num; i > 0; i--){
			if(i !== 1){
				total = total * (i - 1);
			}
			
		}
		num = total;
		return num;
	},
	//using recursion - clean and elegant
	OtherUserFirstFactorial : function(num) { 
		return num === 1 ? 1 : num * First_Factorial.OtherUserFirstFactorial(num-1);
	}
};

var Longest_Word = {
	// Using the JavaScript language, have the function LongestWord(sen) take the sen parameter
	// being passed and return the largest word in the string.
	// If there are two or more words that are the same length, 
	// return the first word from the string with that length. Ignore punctuation and assume 
	// sen will not be empty. 
	LongestWord : function(sen){
		sen.replace(/[^a-z \s]/gi, '').split(' ').reduce(function(str, cur){
			str = cur.length > str.length ? cur : str;
			return str;
		}, "" );
	}
	LongestWord : function(sen){
		//use a regex pattern to get words between spaces 
		//keep a tally of the longest work and test each subsequent word against that
		//store longest words in variable(if same length then ignore)
		//return longest word
		var longestWord;
		sen.match(/\b\S+\b/g).forEach(function(elem, index){
			if(longestWord === undefined){
				//nb.probably should check for punctuation to make sure the word is a proper word
				longestWord = elem;
			}else{
				if(elem.replace(/[^a-zA-Z]/g, '').length > longestWord.length){
					longestWord = elem;
				}
			}
		});
		sen = longestWord;
		return sen;
	}
};

var Simple_Adding = {
	// Using the JavaScript language, have the function SimpleAdding(num) add 
	// up all the numbers from 1 to num. For the test cases, the parameter num 
	// will be any number from 1 to 1000.

	SimpleAdding : function(num){
		return num === 0 ? num : num + Simple_Adding.SimpleAdding(num-1);
	}
};

var Letter_Capitalize = {
	// Using the JavaScript language, have the function LetterCapitalize(str) 
	// take the str parameter being passed and capitalize the first letter 
	// of each word. Words will be separated by only one space.

	LetterCapitalize : function(str){
		//grab the first character after a word boundry and then capitalize it
		return str.replace(/\b[a-z]/g, function(c){
			return c.toUpperCase();
		});
	} 
};

var Simple_Symbols = {
	// Using the JavaScript language, have the function SimpleSymbols(str) take 
	// the str parameter being passed and determine if it is an acceptable 
	// sequence by either returning the string true or false. 
	// The str parameter will be composed of + and = symbols with several 
	// letters between them (ie. ++d+===+c++==a) and for the string to be 
	// true each letter must be surrounded by a + symbol. 
	// So the string to the left would be false. The string will not be 
	// empty and will have at least one letter. 

	SimpleSymbols : function(str){
		//store character length
		var count = str.match(/[a-zA-Z]/g).length;
		//store correct matches
		var matches = 0;
		//search for characters preceded and succeded by + symbols
		str.match(/\+[a-zA-Z]\+/g).forEach(function(){
			//increment matches
			matches++;
		});
		//check that there were valid matches and that the number corresponds to the total count
		return count !== 0 && count === matches ? true : false;
	},

	alt : function(str) { 
	  // matches any letter that does NOT have a plus symbol before or after
	  return ('='+str+'=').match(/([^\+][a-z])|([a-z][^\+])/gi)===null; 
	}
};

var Check_Nums = {
	// Using the JavaScript language, have the function CheckNums(num1,num2) 
	// take both parameters being passed and return the string true if num2 
	// is greater than num1, otherwise return the string false. 
	// If the parameter values are equal to each other then return the string -1.

	CheckNums : function(num1, num2){
		return num2 > num1 ? "true" : num2 === num1 ? "-1" : "false";
	}
};

var Time_Convert = {
	// Using the JavaScript language, have the function TimeConvert(num) take 
	// the num parameter being passed and return the number of hours and 
	// minutes the parameter converts to (ie. if num = 63 then the output should be 1:3). 
	// Separate the number of hours and minutes with a colon. 

	TimeConvert : function(num){
		//assumes number input is valid for question asked otherwise input would be validated
		return ((num - (num % 60)) / 60) + ":" + (num % 60);
	}
};
