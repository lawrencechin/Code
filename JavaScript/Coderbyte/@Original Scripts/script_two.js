function AlphabetSoup(str){
	// Using the JavaScript language, have the function AlphabetSoup(str) take 
	// the str string parameter being passed and return the string with the 
	// letters in alphabetical order (ie. hello becomes ehllo). 
	// Assume numbers and punctuation symbols will not be included in the string. 

	//split string into array, sort by charcode number, rejoin into str
	return str.split('').sort(function(a,b){
		//note that uppercase comes before all lowercase characters so they will not sort alphabetically without fidlin'
		return a.toLowerCase().charCodeAt(0) - b.toLowerCase().charCodeAt(0);
	}).join('');
}

function ABCheck(str){
	// Using the JavaScript language, have the function ABCheck(str) take 
	// the str parameter being passed and return the string true if the 
	// characters a and b are separated by exactly 3 places anywhere in 
	// the string at least once (ie. "lane borrowed" would result in true 
	// because there is exactly three characters between a and b). 
	// Otherwise return the string false. 

	return /(a...b|b...a)/gi.test(str);
}

function VowelCount(str){
	// Using the JavaScript language, have the function VowelCount(str) 
	// take the str string parameter being passed and return the number 
	// of vowels the string contains (ie. "All cows eat grass" would return 5). 
	// Do not count y as a vowel for this challenge. 
	var vCount = str.match(/[aeiou]/gi);
	return vCount ? vCount.length : 0; //can we do this in one line?
}

function WordCount(str){
	// Using the JavaScript language, have the function WordCount(str) 
	// take the str string parameter being passed and return the number 
	// of words the string contains (ie. "Never eat shredded wheat" would 
	// return 4). Words will be separated by single spaces. 

	var wordCount = str.match(/\b\w+\b/gi);
	return wordCount ? wordCount.length : 0; //again, one line? Can't run length on a null object and don't want to repeat the regex match in event there are matches eg. return str.match(/.../) ? str.match(/../)/length : 0;
}

function ExOh(str){
	// Using the JavaScript language, have the function ExOh(str) take the 
	// str parameter being passed and return the string true if there is 
	// an equal number of x's and o's, otherwise return the string false. 
	// Only these two letters will be entered in the string, no punctuation 
	// or numbers. For example: if str is "xooxxxxooxo" then the output 
	// should return false because there are 6 x's and 5 o's. 

	var ex = str.match(/x/ig);
	var oh = str.match(/o/ig);
	return ex && oh && ex.length === oh.length ? true : false;
	//return str.split(/x/i).length === str.split(/o/i).length; - one liner!
}

function Palindrome(str){
	// Using the JavaScript language, have the function Palindrome(str) take 
	// the str parameter being passed and return the string true if the parameter 
	// is a palindrome, (the string is the same forward as it is backward) 
	// otherwise return the string false. For example: "racecar" is also "racecar" 
	// backwards. Punctuation and numbers will not be part of the string. 

	//examples 'never odd or even' | 'eye' | 'racecar' - palindromes
	return str.replace(/[^a-zA-Z]/g, '').toLowerCase().split('').every(function(elem, index, arr){
		return elem === arr[arr.length - 1 - index];
	});
}

function ArithGeo(arr){
	// Using the JavaScript language, have the function ArithGeo(arr) take the 
	// array of numbers stored in arr and return the string "Arithmetic" if the 
	// sequence follows an arithmetic pattern or return "Geometric" if it follows 
	// a geometric pattern. If the sequence doesn't follow either pattern return -1. 
	// An arithmetic sequence is one where the difference between each of the 
	// numbers is consistent, where as in a geometric sequence, each term after 
	// the first is multiplied by some constant or common ratio. 
	// Arithmetic example: [2, 4, 6, 8] and Geometric example: [2, 6, 18, 54]. 
	// Negative numbers may be entered as parameters, 0 will not be entered, 
	// and no array will contain all the same elements. 

	var difference = arr.reduce(function(cur, item, pos, arr){
		if(arr[pos+1]){
			cur.aMap.push(arr[pos + 1] - item);
			cur.gMap.push(arr[pos + 1] / item);	
		}else{
			cur.aFactor = cur.aMap.every(function(elem, index, arr){
				return arr[index + 1] ? elem === arr[index + 1] : true;
			});
			cur.gFactor = cur.gMap.every(function(elem, index, arr){
				return arr[index + 1] ? 
				elem === arr[index + 1] || arr[index + 1] / elem === elem
				: true;
			});
		}
		return cur;
	}, {aMap : [], gMap : [], aFactor : false, gFactor : false} );

	return difference.aFactor ? "Arithmetic" : difference.gFactor ? "Geometric" : -1; 
}

function ArrayAddition(arr){
	// Using the JavaScript language, have the function ArrayAdditionI(arr) 
	// take the array of numbers stored in arr and return the string true if 
	// any combination of numbers in the array can be added up to equal the 
	// largest number in the array, otherwise return the string false. 
	// For example: if arr contains [4, 6, 23, 10, 1, 3] the output should 
	// return true because 4 + 6 + 10 + 3 = 23. The array will not be empty, 
	// will not contain all the same elements, and may contain negative numbers. 
	//Input = 5,7,16,1,2Output = "false"
	//Input = 3,5,-1,8,12Output = "true"
	var arr = [4,6,23,10,1,3];
	//1,3,4,6,10
	var highestVal = arr.sort(function(a,b){
		return a - b;
	}).pop();
}

function LetterCountI(str){
	// Using the JavaScript language, have the function LetterCountI(str) 
	// take the str parameter being passed and return the first word with 
	// the greatest number of repeated letters. 
	// For example: "Today, is the greatest day ever!" should return 
	// greatest because it has 2 e's (and 2 t's) and it comes before ever 
	// which also has 2 e's. If there are no words with repeating letters return -1. 
	// Words will be separated by spaces. 
	var results = {};
	str.match(/\b[a-zA-Z]+\b/g).forEach(function(elem){
		elem.toLowerCase().split('').forEach(function(e,i,a){
			var tempCount = 0;
			a.forEach(function(el){
				tempCount = el === e ? tempCount + 1 : tempCount;
			});
			if(tempCount > 1 && tempCount > (results.count | 0)){
				results.count = tempCount;
				results.char = e;
				results.word = elem;
			}
		});
	});
	return results.word ? results.word : -1;
}


function SecondGreatLow(arr){
	// Using the JavaScript language, have the function SecondGreatLow(arr) 
	// take the array of numbers stored in arr and return the second lowest 
	// and second greatest numbers, respectively, separated by a space. 
	// For example: if arr contains [7, 7, 12, 98, 106] the output should be 12 98. 
	// The array will not be empty and will contain at least 2 numbers. 
	// It can get tricky if there's just two numbers! 
	//Examples : 
	/*	
		[7, 7, 12, 98, 106] the output should be 12 98
		Input = 1, 42, 42, 180 Output = "42 42"
		Input = 4, 90 Output = "90 4"
	*/
	var results = {secondLowest : 0, secondHighest : 0};
	arr.sort(function(a,b){return a - b;});
	results.lowest = arr.shift();
	results.highest = arr.pop();
	arr.forEach(function(elem){
		results.secondLowest = results.secondLowest === 0 && elem > results.lowest ? elem : results.lowest < elem && elem < results.secondLowest ? elem : results.secondLowest;
		results.secondHighest = results.secondHighest === 0 && elem < results.highest ? elem : results.highest > elem && elem > results.secondHighest ? elem : results.secondHighest;
	});

	results.secondLowest = results.secondLowest === 0 ? results.lowest : results.secondLowest;
	results.secondHighest = results.secondHighest === 0 ? results.highest : results.secondHighest; 

	return results.secondLowest + " " + results.secondHighest; 
}

function SecondGreatLow_OTHERUSER(arr) { 
  var unique = arr.join(' ').match(/(\b\d+\b)(?!.+\b\1\b)/g); 
  unique.sort(function(a,b){return a-b});
  if(unique.length == 1){
    return unique[0] + ' ' + unique[0]; 
  }
  return unique[1] + ' ' + unique[unique.length -2];
}

function DivisionStringified(num1, num2){
	// Using the JavaScript language, have the function DivisionStringified(num1,num2) 
	// take both parameters being passed, divide num1 by num2, and return the result 
	// as a string with properly formatted commas. If an answer is only 3 digits long, 
	// return the number with no commas (ie. 2 / 3 should output "1"). For example: 
	// if num1 is 123456789 and num2 is 10000 the output should be "12,345". 

	//num1 is 123456789 and num2 is 10000 the output should be "12,345".
	//Input = 5 & num2 = 2 Output = "3"
	//Input = 6874 & num2 = 67 Output = "103"
	//Commas in numbers occurs every three letters from the decimal point to the start eg. 123456.90 > 123,456.90
	//NO COMMAS AFTER THE DECIMAL POINT!!!!
	//100,000,000,00 0 
	//012 345 678 91011
	//4567
	//1234
	//modulus of three works but only in context of the total length = (str.length - index + 1) % 3
	// str.length - index - 1 === 0 then DO NOT add a comma
	var commaStr = '';
	Math.round(num1 / num2).toString().split('').forEach(function(elem, index, arr){
		commaStr += (arr.length - (index + 1)) % 3 === 0 && arr.length - (index + 1) !== 0 ? elem + ',' : elem;
	});

	return commaStr;
}

function CountingMinutesI(str){
	// Using the JavaScript language, have the function CountingMinutesI(str) 
	// take the str parameter being passed which will be two times 
	// (each properly formatted with a colon and am or pm) separated by a hyphen 
	// and return the total number of minutes between the two times. 
	// The time will be in a 12 hour clock format. For example: if str is 
	// 9:00am-10:00am then the output should be 60. 
	// If str is 1:00pm-11:00am the output should be 1320. 

	return str.split('-').map(function(time, index, arr){
		var hour = time.match(/([01][0-2]|[1-9])/);
		var minutes = time.match(/:([0-5][0-9])/);
		var timeofDay = time.match(/(am|pm)/);
		//validate correct times(hours between 1 and 12, minutes between 0 and 59, am/pm)
		if(hour && minutes && timeofDay){
			arr[index] = timeofDay[0] == 'am' ?  Number(hour[0]) * 60 + Number(minutes[1]) : (Number(hour[0]) + 12) * 60 + Number(minutes[1]); 
		}else{
			arr[index] = false;
		}

		if(index === 1 && arr[index - 1] && arr[index]){
			return arr[1] < arr[0] ? (arr[1] + (24 * 60)) - arr[0] : arr[1] - arr[0];
		}else{
			return "You have entered incorrect times. Please try again";
		}
	})[1];
}

function MeanMode(arr){
	// Using the JavaScript language, have the function MeanMode(arr) 
	// take the array of numbers stored in arr and return 1 if the 
	// mode equals the mean, 0 if they don't equal each other 
	// (ie. [5, 3, 3, 3, 1] should return 1 because the mode (3) 
	// equals the mean (3)). The array will not be empty, will 
	// only contain positive integers, and will not contain more than one mode. 
	var mean = arr.reduce(function(a, b){ return a + b; }) / arr.length;
	var mode = arr.reduce(function(cur, elem){
		var test = cur.numMap[elem] = (cur.numMap[elem] || 0) + 1;
		if(test > cur.greatestFreq){
			cur.greatestFreq = test;
			cur.mode = elem;
		}
		return cur;
	},{mode : null, greatestFreq : 0, numMap : {}});
	return mean === mode.mode ? 1 : 0;
}

function DashInsert(str){
	// Using the JavaScript language, have the function DashInsert(str) 
	// insert dashes ('-') between each two odd numbers in str. 
	// For example: if str is 454793 the output should be 4547-9-3. 
	// Don't count zero as an odd number. 

	return str.replace(/[0-9]/g, function(d, index, arr){
		return d % 2 === 0 ? d : arr[index + 1] && arr[index + 1] % 2 !== 0 ? d + '-' : d;
	});
}

function SwapCase(str){
	// Using the JavaScript language, have the function SwapCase(str) 
	// take the str parameter and swap the case of each character. 
	// For example: if str is "Hello World" the output should be hELLO wORLD. 
	// Let numbers and symbols stay the way they are. 

	return str.replace(/[a-zA-Z]/g, function(c){
		return /[a-z]/.test(c) ? c.toUpperCase() : /[A-Z]/.test(c) ? c.toLowerCase() : c;
	});
}

function NumberAdditional(str){
	// Using the JavaScript language, have the function NumberSearch(str) 
	// take the str parameter, search for all the numbers in the string, 
	// add them together, then return that final number. 
	// For example: if str is "88Hello 3World!" the output should be 91. 
	// You will have to differentiate between single digit numbers and 
	// multiple digit numbers like in the example above. 
	// So "55Hello" and "5Hello 5" should return two different answers. 
	// Each string will contain at least one letter or symbol. 

	var total = 0;
	var nums = str.match(/[0-9]+/g);
	if(nums){
		nums.forEach(function(d){
		total+= Number(d);
		});
	}
	return total;
}

function ThirdGreatest(strArr){
	// Using the JavaScript language, have the function ThirdGreatest(strArr) 
	// take the array of strings stored in strArr and return the third largest 
	// word within in. So for example: if strArr is 
	// ["hello", "world", "before", "all"] your output should be world 
	// because "before" is 6 letters long, and "hello" and "world" are both 5, 
	// but the output should be world because it appeared as the last 5 
	// letter word in the array. If strArr was ["hello", "world", "after", "all"] 
	// the output should be after because the first three words are all 5 letters 
	// long, so return the last one. The array will have at least three strings 
	// and each string will only contain letters. 

	strArr.sort(function(a,b){
		return b.length - a.length; //sort by word length
	});

	var results = {
		first : strArr.shift(),
		second : strArr.shift(),//remove the first and second entries from the array
		third : ''
	};
	
	strArr.forEach(function(elem){
		results.third = elem.length === results.second.length || elem.length >= results.third.length ? elem : results.third;  
	});

	return results.third;
}

function PowersofTwo(num){
	// Using the JavaScript language, have the function PowersofTwo(num) 
	// take the num parameter being passed which will be an integer and 
	// return the string true if it's a power of two. If it's not return 
	// the string false. For example if the input is 16 then your program 
	// should return the string true but if the input is 22 then the output 
	// should be the string false. 

	 return num !== 0 && !(num & (num - 1));
}

function AdditivePersistence(num){
	// Using the JavaScript language, have the function AdditivePersistence(num) 
	// take the num parameter being passed which will always be a positive 
	// integer and return its additive persistence which is the number of 
	// times you must add the digits in num until you reach a single digit. 
	// For example: if num is 2718 then your program should return 2 
	// because 2 + 7 + 1 + 8 = 18 and 1 + 8 = 9 and you stop at 9. 
	var total = 0;
	while(num > 9){
		num = parseInt(num.toString().split('').reduce(function(prev, cur){
			return parseInt(prev) + parseInt(cur);
		}));

		total++;
	}
	
	return total;
}

function MultiplicativePersistence(num){
	// Using the JavaScript language, have the function MultiplicativePersistence(num) 
	// take the num parameter being passed which will always be a positive integer and 
	// return its multiplicative persistence which is the number of times you must 
	// multiply the digits in num until you reach a single digit. 
	// For example: if num is 39 then your program should return 3 because 3 * 9 = 27 
	// then 2 * 7 = 14 and finally 1 * 4 = 4 and you stop at 4. 
	// 4 = 0
	// 25 = 2

	var total = 0;
	while(num > 9){
		num = parseInt(num.toString().split('').reduce(function(prev, cur){
			return parseInt(prev) * parseInt(cur);
		}));

		total++;
	}
	
	return total;
}

function OffLineMinimum(strArr){
	// Using the JavaScript language, have the function OffLineMinimum(strArr) 
	// take the strArr parameter being passed which will be an array of integers 
	// ranging from 1...n and the letter "E" and return the correct subset based 
	// on the following rules. The input will be in the following 
	// format: ["I","I","E","I",...,"E",...,"I"] where the I's stand for integers 
	// and the E means take out the smallest integer currently in the whole set. 
	// When finished, your program should return that new set with integers 
	// separated by commas. For example: if strArr is 
	// ["5","4","6","E","1","7","E","E","3","2"] then your program 
	// should return 4,1,5. 
	var tempArr = [];
	var finalStr = '';
	for(var i = 0; i < strArr.length; i++){
		if(/[0-9]+/.test(strArr[i])){
			tempArr.push(strArr[i]);
			tempArr.sort(function(a,b){ return a - b;});
		}else if(/E/.test(strArr[i])){
			finalStr += tempArr[0] ? tempArr[0] + ',' : '';
			tempArr.shift();
		}
	}
	return finalStr.slice(0, -1);

	return strArr.reduce(function(cur, elem){
		if(/[0-9]+/.test(elem)){
			cur.numArr.push(elem);
		}else if(/E/.test(elem)){
			cur.finalStr = numArr.sort(function(a, b){ return a - b; }).shift();
		}
	},{ numArr : [], finalStr : [] }).finalStr.join(',');
}

/*End of easy questions! Now here's the medium difficulty level, hmm…*/

function PrimeTime(num){
	/*Using the JavaScript language, have the function PrimeTime(num) 
	take the num parameter being passed and return the string true 
	if the parameter is a prime number, otherwise return the string 
	false. The range will be between 1 and 2^16. 
	Link: http://www.stoimen.com/blog/2012/05/08/computer-algorithms-determine-if-a-number-is-prime/*/
	if(num === 1){
		return 'false'
	}else if(num == 2 || num == 3){
		return 'true';
	}
	var i = 2;
	var sqrt = Math.sqrt(num);
	while(i <= sqrt){ //square root represents the highest divisor for a number, no need to check beyond 
		if(num % i === 0){
			return 'false';
		}
		i = i === 2 ? i + 1 : i + 2; //if not divisible by 2 then no need to check 4,6,8,10 etc
	}
	return 'true';
}

function RunLength(str){
	// Using the JavaScript language, have the function RunLength(str) 
	// take the str parameter being passed and return a compressed version 
	// of the string using the Run-length encoding algorithm. This algorithm 
	// works by taking the occurrence of each repeating character and 
	// outputting that number along with a single character of the 
	// repeating sequence. For example: "wwwggopp" would return 3w2g1o2p. 
	// The string will not contain any numbers, punctuation, or symbols. 

	return str.match(/(\w)(\1){0,}/g).reduce(function(compressed, elem){
		//grab all characters and group repetitions 
		compressed += elem.length + elem[0];
		return compressed;
	}, "");
}

function PrimeMover(num){
	/*Using the JavaScript language, have the function PrimeMover(num) 
	return the nth prime number. The range will be from 1 to 10^4. 
	For example: if num is 16 the output should be 53 as 53 is 
	the 16th prime number. */

}

function PalindromeTwo(str){

	/*Using the JavaScript language, have the function PalindromeTwo(str) 
	take the str parameter being passed and return the string true if 
	the parameter is a palindrome, (the string is the same forward as 
	it is backward) otherwise return the string false. The parameter 
	entered may have punctuation and symbols but they should not affect 
	whether the string is in fact a palindrome. 
	For example: "Anne, I vote more cars race Rome-to-Vienna" should return true. */
	str = str.toLowerCase().replace(/[^a-z]/g, ''); //remove all non-characters 
	var length = Math.ceil(str.length / 2); //center point of str
	var startPos = str.length % 2 === 0 ? 0 : 1; //for substr'ing odd numbered strs
	var str_2 = str.substr(length - startPos).split('').reverse().join(''); //reverse second half of str
	return str.substr(0, length) === str_2 ? "true" : "false"; //compare
}

function Division(num1, num2){
	/*Using the JavaScript language, have the function Division(num1,num2)
	take both parameters being passed and return the Greatest Common Factor. 
	That is, return the greatest number that evenly goes into both numbers 
	with no remainder. For example: 12 and 16 both are divisible by 1, 2, 
	and 4 so the output should be 4. The range for both parameters will be from 1 to 10^3. */

	var dividend = num1 > num2? num1 : num2; //greatest number
	var divisor = num1 < num2? num1 : num2; //lowest number
	var quotient = Math.floor(dividend/divisor); //highest number of divisor in dividend
	var remainder = dividend % (divisor * quotient); //left over

	while(remainder !== 0){
		dividend = divisor;
		divisor = remainder;
		quotient = Math.floor(dividend/divisor);
		remainder = dividend % (divisor * quotient);
	}

	return divisor;
}

function StringScramble(str1, str2){
	/*Using the JavaScript language, have the function StringScramble(str1,str2) 
	take both parameters being passed and return the string true if a portion 
	of str1 characters can be rearranged to match str2, otherwise return the 
	string false. For example: if str1 is "rkqodlw" and str2 is "world" the 
	output should return true. Punctuation and symbols will not be entered 
	with the parameters. */
	return str2.match(/([a-z])(\1+)?(\1)?/gi).every(function(item){
		var pattern = new RegExp(item, "i"); //need to use 'new RegExp' to use variable in pattern
		if(item.length > 1){
			var patStr = "(" + item[0] + ")";
			for(var i = 1; i < item.length; i++){
				patStr += "(.*"+item[0]+")";
			}
			pattern = new RegExp(patStr, "ig");
		}
		return pattern.test(str1); //will return false if character not found in str1
	});
}

function ArithGeoII(arr){
	/*Using the JavaScript language, have the function ArithGeoII(arr) 
	take the array of numbers stored in arr and return the string 
	"Arithmetic" if the sequence follows an arithmetic pattern or 
	return "Geometric" if it follows a geometric pattern. If the 
	sequence doesn't follow either pattern return -1. An arithmetic 
	sequence is one where the difference between each of the 
	numbers is consistent, where as in a geometric sequence, 
	each term after the first is multiplied by some constant 
	or common ratio. Arithmetic example: [2, 4, 6, 8] and 
	Geometric example: [2, 6, 18, 54]. Negative numbers may be 
	entered as parameters, 0 will not be entered, and no array 
	will contain all the same elements. */

	//seems to be the same as before - try that code?
}

function BinaryConverter(str){
	/*Using the JavaScript language, have the function 
	BinaryConverter(str) return the decimal form of the 
	binary value. For example: if 101 is passed return 5, 
	or if 1000 is passed return 8. */
	return parseInt(str, 2); //base 2 radix

	str.split('').reverse().reduce(function(numObj, elem){
		numObj.total = (elem * numObj.base) + numObj.total;
		numObj.base *= 2;
		return numObj;
	}, { base : 1, total : 0 });

}

function LetterCount(str){
	/*Using the JavaScript language, have the function LetterCount(str) 
	take the str parameter being passed and return the first word 
	with the greatest number of repeated letters. For example: 
	"Today, is the greatest day ever!" should return greatest because 
	it has 2 e's (and 2 t's) and it comes before ever which also has 2 e's. 
	If there are no words with repeating letters return -1. 
	Words will be separated by spaces. */

	//remove punctuation and split words into array
	return str.replace(/[^a-z \s]/gi, '').split(' ').reduce(function(obj, elem){
		//split each word into letters and count how many times each one crops up
		var word = elem.toLowerCase().split('').reduce(function(obj_interior, item){
			var test = obj_interior.map[item] = (obj_interior.map[item] || 0) + 1;
			obj_interior.count = test > obj_interior.count ? test : obj_interior.count;
			return obj_interior;
		}, {map : {}, count : 0});
		//check if there is a repeating character greater than 1 and greater than count
		if(word.count > obj.count){
			obj.word = elem;
			obj.count = word.count;
		}
		return obj;
	}, {word : -1, count : 1})['word'];
}

function CaesarCipher(str, num){
	/*Using the JavaScript language, have the function CaesarCipher(str,num) 
	take the str parameter and perform a Caesar Cipher shift on it using 
	the num parameter as the shifting number. A Caesar Cipher works by 
	shifting each letter in the string N places down in the alphabet 
	(in this case N will be num). Punctuation, spaces, and capitalization 
	should remain intact. For example if the string is "Caesar Cipher" 
	and num is 2 the output should be "Ecguct Ekrjgt". */
	var lowerCaseBounds = ["a".charCodeAt(0) - 1, "z".charCodeAt(0)];
	var upperCaseBounds = ["A".charCodeAt(0) - 1, "Z".charCodeAt(0)];

	return str.replace(/[a-z]/gi, function(c){
		var charCode = c.charCodeAt(0) + num;
		if(/[a-z]/.test(c) && charCode > lowerCaseBounds[1]){
			charCode = (charCode - lowerCaseBounds[1]) + (lowerCaseBounds[0]);
		}else if(/[A-Z]/.test(c) && charCode > upperCaseBounds[1]){
			charCode = (charCode - upperCaseBounds[1]) + (upperCaseBounds[0]);
		}
		return String.fromCharCode(charCode);
	});
}

function SimpleMode(arr){
	/*Using the JavaScript language, have the function SimpleMode(arr)
	take the array of numbers stored in arr and return the number 
	that appears most frequently (the mode). For example: if arr 
	contains [10, 4, 5, 2, 4] the output should be 4. If there is 
	more than one mode return the one that appeared in the array 
	first (ie. [5, 10, 10, 6, 5] should return 5 because it 
	appeared first). If there is no mode return -1. 
	The array will not be empty. */
	var mode = arr.reduce(function(obj, cur){
		//set tally for each number in test & numMap
		var test = obj.numMap[cur] = (obj.numMap[cur] || 0) + 1;
		if(test > obj.greatestFreq){
			obj.greatestFreq = test;
			obj.mode = cur;
		}else if(test === obj.greatestFreq){
			var posCur = -1;
			var posMode = -1;
			for(var i = 0; i < arr.length; i++){
				if(posCur !== -1 && posMode !== -1){
					break;
				}else{
					posCur = posCur !== -1 ? posCur : arr[i] === cur ? i : posCur;
					posMode = posMode !== -1 ? posMode : arr[i] === obj.mode ? i : posMode;
				}
			}
			obj.mode = posCur < posMode ? cur : obj.mode;
		}
		return obj;
	}, { greatestFreq : 0, mode : 0, numMap : {} });
	return mode.greatestFreq > 1 ? mode.mode : '-1';
}

function Consecutive(arr){

	/*Using the JavaScript language, have the function Consecutive(arr) take
	the array of integers stored in arr and return the minimum number of integers
	needed to make the contents of arr consecutive from the lowest number to the
	highest number. For example: If arr contains [4, 8, 6] then the output should
	be 2 because two numbers need to be added to the array (5 and 7) to make it a
	consecutive array of numbers from 4 to 8. Negative numbers may be entered as
	parameters and no array will have less than 2 elements.  */
	var tally = 0;
	arr.sort(function(a,b){
		return a - b;
	}).reduce(function(prev, cur){
		tally += (cur - prev) - 1;
		return cur;
	});
	return tally;
}


function FormattedDivision(num1, num2){ /*NOT SUBMITTED*/

	/*Using the JavaScript language, have the function
	FormattedDivision(num1,num2) take both parameters being passed, divide
	num1 by num2, and return the result as a string with properly formatted
	commas and 4 significant digits after the decimal place. For example: if
	num1 is 123456789 and num2 is 10000 the output should be "12,345.6789".
	The output must contain a number in the one's place even if it is a zero.*/
	var div = (num1/num2).toFixed(4);
	div = div.split('.');
	div[0] = div[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
	return div.join('.');
}

function CountingMinutes(str){
	/*Using the JavaScript language, have the function CountingMinutes(str) take
		the str parameter being passed which will be two times (each properly
		formatted with a colon and am or pm) separated by a hyphen and return the
		total number of minutes between the two times. The time will be in a 12
		hour clock format. For example: if str is 9:00am-10:00am then the output
		should be 60. If str is 1:00pm-11:00am the output should be 1320.*/	
	//convert str into array and then convert values into minutes
	var time = str.split('-').reduce(function(arr, elem){
		arr.push(elem.replace(/([01]?[0-9])(?::)?([0-9]{2})([a-z]{2})/gi, function($1,$2,$3,$4){
			var minutes = ($2 * 60) + parseInt($3);
			minutes = $4 === 'pm' && $2 != '12' ? minutes + (12 * 60) : $4 === 'am' && $2 == '12' ?
			minutes - (12 * 60) : minutes;
			return minutes;
		}));
		return arr;
	}, []);
	//if second time value is smaller than the first, add on 24 hours
	return time[1] < time[0] ? (parseInt(time[1]) + 24 * 60) - parseInt(time[0]) : 
	time[1] - time[0]; 
}

function PermutationStep(num){

	/*  Using the JavaScript language, have the function PermutationStep(num) take
	the num parameter being passed and return the next number greater than num
	using the same digits. For example: if num is 123 return 132, if it's 12453
	return 12534. If a number has no greater permutations, return -1 (ie. 999).
	*/   

	var revNumArr = num.toString().split('').reverse();
	var firstSwitchPos = null;
	//swap over larger number from right to left, only one swap is made
	for(var i = 0; i < revNumArr.length; i++){
		if(revNumArr[i+1]){
			if(revNumArr[i] > revNumArr[i+1]){
				var temp = revNumArr[i+1];
				revNumArr[i+1] = revNumArr[i];
				revNumArr[i] = temp;
				firstSwitchPos = i;
				break;
			}
		}
	}
	//if a swap has occured we then loop through again to see if a smaller
	//number can be made upto the original swap position in array
	if(firstSwitchPos && firstSwitchPos > 0){
		var newArr = revNumArr.slice(0, firstSwitchPos+1).sort(function(a,b){
			return a - b;
		});
		revNumArr = newArr.reverse().concat(revNumArr.slice(firstSwitchPos+1));
	}
	var final = parseInt(revNumArr.reverse().join(''))
	return final === num ? -1 : final;
}

function PrimeChecker(num){

	/*  Using the JavaScript language, have the function PrimeChecker(num) take
	num and return 1 if any arrangement of num comes out to be a prime number,
	otherwise return 0. For example: if num is 910, the output should be 1 because
	910 can be arranged into 109 or 019, both of which are primes.  */   

}

function DashInsertII(num){
	/*  Using the JavaScript language, have the function DashInsertII(str) insert
	dashes ('-') between each two odd numbers and insert asterisks ('*') between
	each two even numbers in str. For example: if str is 4546793 the output should
	be 454*67-9-3. Don't count zero as an odd or even number.  */
	return num.toString().split('').reduce(function(str, elem, pos, arr){
		if(elem == 0){
			return str += elem;
		}else if(arr[pos+1] && arr[pos+1] != 0){
			if(elem % 2 === 0 &&  arr[pos+1] % 2 === 0){
				return str += elem + '*';
			}else if(elem % 2 !== 0 && arr[pos+1] % 2 !== 0){
				return str += elem + '-';
			}
		}
		return str += elem;;
	}, "");
}

function SwapII(str){

	/*  Using the JavaScript language, have the function SwapII(str) take the str
	parameter and swap the case of each character. Then, if a letter is between
	two numbers (without separation), switch the places of the two numbers. For
	example: if str is "6Hello4 -8World, 7 yes3" the output should be 4hELLO6
	-8wORLD, 7 YES3.  */ 
	str = str.replace(/[a-z]/gi, function(c){
		return c.charCodeAt(0) > "a".charCodeAt(0) - 1 ? c.toUpperCase() : c.toLowerCase(); 
	});

	return str.replace(/([0-9])([a-z]+)([0-9])/gi, function($1, $2, $3, $4){
		return $4 + $3 + $2;
	});

}

function NumberSearch(str){
	/*  Using the JavaScript language, have the function NumberSearch(str) take
	the str parameter, search for all the numbers in the string, add them
	together, then return that final number divided by the total amount of letters
	in the string. For example: if str is "Hello6 9World 2, Nic8e D7ay!" the
	output should be 2. First if you add up all the numbers, 6 + 9 + 2 + 8 + 7 you
	get 32. Then there are 17 letters in the string. 32 / 17 = 1.882, and the
	final answer should be rounded to the nearest whole number, so the answer is
	2. Only single digit numbers separated by spaces will be used throughout the
	whole string (So this won't ever be the case: hello44444 world). Each string
	will also have at least one letter.  */   
	var numbers = str.replace(/[^0-9]/g, '').split('').reduce(function(total, num){
		return total += parseInt(num);
	}, 0);

	var length = str.replace(/[^a-z]/gi, '').length;

	return Math.round(numbers/length);
}

function TripleDouble(num1, num2){

	/*  Using the JavaScript language, have the function TripleDouble(num1,num2)
	take both parameters being passed, and return 1 if there is a straight triple
	of a number at any place in num1 and also a straight double of the same number
	in num2. For example: if num1 equals 451999277 and num2 equals 41177722899,
	then return 1 because in the first parameter you have the straight triple 999
	and you have a straight double, 99, of the same number in the second
	parameter. If this isn't the case, return 0.  */
	var trebles = num1.toString().match(/([0-9])\1{2}/g);
	var doubles = num2.toString().match(/([0-9])\1{1}/g);
	var match = 0;
	if(trebles && doubles){
		trebles.forEach(function(elem){
			doubles.forEach(function(item){
				if(elem[0] === item[0]){
					match = 1;
				}
			});
		});
	}
	return match;
}

function BracketMatcher(str){ /*NOT SUBMITTED*/

	/*  Using the JavaScript language, have the function BracketMatcher(str) take
	the str parameter being passed and return 1 if the brackets are correctly
	matched and each one is accounted for. Otherwise return 0. For example: if str
	is "(hello (world))", then the output should be 1, but if str is "((hello
	(world))" the the output should be 0 because the brackets do not correctly
	match up. Only "(" and ")" will be used as brackets. If str contains no
	brackets return 1.  */

	//Ouch! Brackets actually need to match correctly so )( doesn't work, boo
	var openingParen = str.match(/\(/g);
	var closingParen = str.match(/\)/g);

	return !openingParen && !closingParen ? 1 : 
	openingParen && closingParen && openingParen.length === closingParen.length ? 
	1 : 0;
}

function StringReduction(str){ /*NOT SUBMITTED*/

	/*  Using the JavaScript language, have the function StringReduction(str) take
	the str parameter being passed and return the smallest number you can get
	through the following reduction method. The method is: Only the letters a, b,
	and c will be given in str and you must take two different adjacent characters
	and replace it with the third. For example "ac" can be replaced with "b" but
	"aa" cannot be replaced with anything. This method is done repeatedly until
	the string cannot be further reduced, and the length of the resulting string
	is to be outputted. For example: if str is "cab", "ca" can be reduced to "b"
	and you get "bb" (you can also reduce it to "cc"). The reduction is done so
	the output should be 2. If str is "bcab", "bc" reduces to "a", so you have
	"aab", then "ab" reduces to "c", and the final string "ac" is reduced to "b"
	so the output should be 1.  
	*/
	function reducer(str){
		return str.replace(/([a-c])(?!\1)(.)/g, function($1, $2, $3){
			if($2 === 'a'){
				return $3 === 'b' ? 'c' : 'b';
			}else if($2 === 'b'){
				return $3 === 'a' ? 'c' : 'a';
			}else{
				return $3 === 'b' ? 'a' : 'b';
			}
		});
	}

	var str_1 = reducer(str);
	//str will be equal to str_1 when no further changes are possible
	while(str !== str_1){
		str = str_1;
		str_1 = reducer(str_1);
	}

	return str_1.length;

}

function ThreeFiveMultiples(num){

/*  Using the JavaScript language, have the function ThreeFiveMultiples(num)
return the sum of all the multiples of 3 and 5 that are below num. For
example: if num is 10, the multiples of 3 and 5 that are below 10 are 3, 5, 6,
and 9, and adding them up you get 23, so your program should return 23. The
integer being passed will be between 1 and 100.  */

}

function CoinDeterminer(num){

	/*  Using the JavaScript language, have the function CoinDeterminer(num) take
	the input, which will be an integer ranging from 1 to 250, and return an
	integer output that will specify the least number of coins, that when added,
	equal the input integer. Coins are based on a system as follows: there are
	coins representing the integers 1, 5, 7, 9, and 11. So for example: if num is
	16, then the output should be 2 because you can achieve the number 16 with the
	coins 9 and 7. If num is 25, then the output should be 3 because you can
	achieve 25 with either 11, 9, and 5 coins or with 9, 9, and 7 coins.  */
	function inArray(num){
		return [11,9,7,5,1].some(function(item){
			return item === num; //check if num is in array
		});
	}

	function elevens(num){
		return {divisor : (Math.floor(num / 11)), remainder : num % 11, num : num};
	}

	function nonElevens(numObj){
		numObj.num = numObj.num - ((numObj.divisor - 1) * 11) - 9;
		numObj.remainder = numObj.num;
		return remainder(numObj);
	}

	function remainder(numObj){
		if(numObj.remainder === 0){
			return 0;
		}else if(inArray(numObj.remainder)){
			return 1; //single coin
		}else if(numObj.remainder === 3 && numObj.divisor === 0){
			return 3 //only made up of 3 1s
		}else if(numObj.remainder === 4 && numObj.divisor === 0){
			return 4; //same as above but 4 1s
		}else if(numObj.remainder !== 3 && numObj.num !== 4){
			return 2; //all other numbers made up of 2 nums in array
		}else{
			return nonElevens(numObj); //can make num with smaller number of coins
		}
	}

	var number = elevens(num);
	var rem = remainder(number);
	return number.divisor + rem;

}

function FibonacciChecker(num){

	/*  Using the JavaScript language, have the function FibonacciChecker(num)
	return the string yes if the number given is part of the Fibonacci sequence.
	This sequence is defined by: Fn = Fn-1 + Fn-2, which means to find Fn you add
	the previous two numbers up. The first two numbers are 0 and 1, then comes 1,
	2, 3, 5 etc. If num is not in the Fibonacci sequence, return the string no.
	*/
	return (Math.sqrt(5*(num*num)+4)) % 1 === 0 | (Math.sqrt(5*(num*num)-4)) % 1 === 0 ? "yes" : "no";
}

function MultipleBrackets(str){

	/*  Using the JavaScript language, have the function MultipleBrackets(str)
	take the str parameter being passed and return 1 #ofBrackets if the brackets
	are correctly matched and each one is accounted for. Otherwise return 0. For
	example: if str is "(hello [world])(!)", then the output should be 1 3 because
	all the brackets are matched and there are 3 pairs of brackets, but if str is
	"((hello [world])" the the output should be 0 because the brackets do not
	correctly match up. Only "(", ")", "[", and "]" will be used as brackets. If
	str contains no brackets return 1.  */
	var openPar = str.match(/\(/g);
	var closingPar = str.match(/\)/g);
	var openBracket = str.match(/\[/g);
	var closingBracket = str.match(/\]/g);

	
}

function MostFreeTime(strArr){

	/*  Using the JavaScript language, have the function MostFreeTime(strArr) read
	the strArr parameter being passed which will represent a full day and will be
	filled with events that span from time X to time Y in the day. The format of
	each event will be hh:mmAM/PM-hh:mmAM/PM. For example, strArr may be
	["10:00AM-12:30PM","02:00PM-02:45PM","09:10AM-09:50AM"]. Your program will
	have to output the longest amount of free time available between the start of
	your first event and the end of your last event in the format: hh:mm. The
	start event should be the earliest event in the day and the latest event
	should be the latest event in the day. The output for the previous input would
	therefore be 01:30 (with the earliest event in the day starting at 09:10AM and
	the latest event ending at 02:45PM). The input will contain at least 3 events
	and the events may be out of order.  */

	//convert times into minutes, order ASC, work out differences, output the 
	//highest value
	//map - sort - reduce
	strArr.map(function(item){
		var pattern = /([0-9]{2,2})(?::)?([0-9]{2,2})([A-Z]{2,2})/g;
		var str = "10:00AM-12:30PM";
		var match = pattern.exec(str);
		var minutes_one = null;
		var minutes_two = null;

		while(match !== null){
			var minutes = (match[1] * 60) + 
				parseInt(match[2]);
			if(match[3] == 'PM'){
				minutes = minutes + (12 * 60);
			}
			if(minutes_one === null){
				minutes_one = minutes; 
			}else{
				minutes_two = minutes;
			}
			match = pattern.exec(str);
		}

		console.log(minutes_one, minutes_two);
		item.match(/([0-9]{2,2})(?::)?([0-9]{2,2})([a-z]{2,2})/gi)
	}).sort(function(a,b){
		return a - b;
	}).reduce(function(stuff){

	})

}

function OverlappingRectangles(strarr){

	/*  Using the JavaScript language, have the function
	OverlappingRectangles(strArr) read the strArr parameter being passed which
	will represent two rectangles on a Cartesian coordinate plane and will contain
	8 coordinates with the first 4 making up rectangle 1 and the last 4 making up
	rectange 2. It will be in the following format:
	["(0,0),(2,2),(2,0),(0,2),(1,0),(1,2),(6,0),(6,2)"] Your program should
	determine the area of the space where the two rectangles overlap, and then
	output the number of times this overlapping region can fit into the first
	rectangle. For the above example, the overlapping region makes up a rectanglec
	of area 2, and the first rectangle (the first 4 coordinates) makes up a
	rectangle of area 4, so your program should output 2. The coordinates will all
	be integers. If there's no overlap between the two rectangles return 0.  */
	//Input = "(0,0),(0,-2),(3,0),(3,-2),(2,-1),(3,-1),(2,3),(3,3)"Output = 6
	//Input = "(0,0),(5,0),(0,2),(5,2),(2,1),(5,1),(2,-1),(5,-1)"Output = 3

}

function LookSaySequence(num){

	/*  Using the JavaScript language, have the function LookSaySequence(num) take
	the num parameter being passed and return the next number in the sequence
	according to the following rule: to generate the next number in a sequence
	read off the digits of the given number, counting the number of digits in
	groups of the same digit. For example, the sequence beginning with 1 would be:
	1, 11, 21, 1211, ... The 11 comes from there being "one 1" before it and the
	21 comes from there being "two 1's" before it. So your program should return
	the next number in the sequence given num.  */

	//convert number into string, run regular expression to determine if the
	//is repeated, add to string and then parseInt the result to return a number

	return num.toString().replace(/([0-9])(\1){0,}/g, function(d){
		return d.length + d[0];
	});
}

function DistinctList(arr){

	/*  Using the JavaScript language, have the function DistinctList(arr) take
	the array of numbers stored in arr and determine the total number of duplicate
	entries. For example if the input is [1, 2, 2, 2, 3] then your program should
	output 2 because there are two duplicates of one of the elements.  */
	arr.sort(function(a,b){ return a-b; });
	var results = [];
	for (var i = 0; i < arr.length - 1; i++) {
	    if (arr[i + 1] == arr[i]) {
	        results.push(arr[i]);
	    }
	}  
	return results.length;
}

function NumberEncoding(str){

	/*  Using the JavaScript language, have the function NumberEncoding(str) take
	the str parameter and encode the message according to the following rule:
	encode every letter into its corresponding numbered position in the alphabet.
	Symbols and spaces will also be used in the input. For example: if str is
	"af5c a#!" then your program should return 1653 1#!.  */
	return str.replace(/[a-z]/gi, function(c){
		return (c.charCodeAt(0) - 96);
	})
}

