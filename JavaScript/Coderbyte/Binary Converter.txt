# Binary Converter

Using the JavaScript language, have the function BinaryConverter(str) return the decimal form of the binary value. For example: if 101 is passed return 5, or if 1000 is passed return 8.

```javascript
function BinaryConverter(str){
	return parseInt(str, 2); //base 2 radix

	str.split('').reverse().reduce(function(numObj, elem){
		numObj.total = (elem * numObj.base) + numObj.total;
		numObj.base *= 2;
		return numObj;
	}, { base : 1, total : 0 });

}
```
