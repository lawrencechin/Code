# Word Count

Using the JavaScript language, have the function WordCount(str) take the str string parameter being passed and return the number of words the string contains (ie. "Never eat shredded wheat" would return 4). Words will be separated by single spaces. 

``` javascript
function WordCount(str){
	var wordCount = str.match(/\b\w+\b/gi);
	return wordCount ? wordCount.length : 0; //again, one line? Can't run length on a null object and don't want to repeat the regex match in event there are matches eg. return str.match(/.../) ? str.match(/../)/length : 0;
}
```