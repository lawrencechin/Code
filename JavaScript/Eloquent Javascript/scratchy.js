function parseExpression( program ){
    // remove whitespace up to the first non-whitespace character
    program = skipSpace( program );
    let match, expr;
    // check if program starts with double quotes
    if( match = /^"([^"]*)"/.exec( program ))
        // expr is a value type
        expr = { type : "value", value : match[ 1 ] };
    // check if program is a number
    else if( match = /^\d+\b/.exec( program ))
        expr = { type : "value", value : Number( match[ 0 ])};
    // check if program starts with anything that isn't a space, parenthesis, commas or quotation marks
    else if( match = /^[^\s(),"]+/.exec( program ))
        expr = { type : "word", name : match[ 0 ] };
    else 
        // received invalid program 
        throw new SyntaxError( "Unexpected syntax: " + program );
    // return expr and rest of program after the match
    return parseApply( expr, program.slice( match[ 0 ].length ));
}

function skipSpace( string ){
    // search for the first instance of non-whitespace character
    let skippable = string.match( /^(\s|#.*)*/ );
    // if no matches then not a valid expression
    return string.slice( skippable[ 0 ].length );
    // else if( comment != -1 ) return string.slice( comment );
    // return the string up to the first valid character without whitespace
    return string.slice( first );
}

function parseApply( expr, program ){
    // remove white-space until first character
    program = skipSpace( program );
    // if the program does not have an opening parenthesis then simply return the expr and the rest (should be empty if valid expression )
    if( program[ 0 ] != "(" )
        return { expr : expr, rest : program };

    // check whitespace again after the opening parenthesis
    program = skipSpace( program.slice( 1 ));
    // type if application so set expr accordingly
    expr = { type : "apply", operator : expr, args : [] };
    // loop over program while the first/last character isn't closing parenthesis
    while( program[ 0 ] != ")" ){
        let arg = parseExpression( program );
        expr.args.push( arg.expr );
        program = skipSpace( arg.rest );
        if( program[ 0 ] == "," )
            program = skipSpace( program.slice( 1 ));
        else if( program[ 0 ] != ")" )
            throw new SyntaxError( "Expected ',' or ')'" );
    }
    return parseApply( expr, program.slice( 1 ));
}

function parse( program ){
    // run the expression through parseExpression
    let result = parseExpression( program );
    // if anything is left over in the 'rest' property then we have an error
    if( skipSpace( result.rest ).length > 0 )
        throw new SyntaxError( "Unexpected text after program" );
    // great! return the object
    return result.expr;
}

console.log( parse( "+( a, 10 )" ));

function evaluate( expr, env ){
    // check what type of expression was passed
    switch( expr.type ){
        case "value" :
            // simply return the value
            return expr.value;
        case "word" :
            // return the env variable if found in env
            if( expr.name in env )
                return env[ expr.name ];
            else
                throw new ReferenceError( "Undefined variable : " + expr.name );
        case "apply" :
            // return special forms if the operator type is word and in specialForms
            if( expr.operator.type == "word" &&
                    expr.operator.name in specialForms )
                return specialForms[ expr.operator.name ]( expr.args, env );
            // recursively call evaluate with the operator of the apply
            let op = evaluate( expr.operator, env );
            // throw error if not a function
            if( typeof op != "function" )
                throw new TypeError( "Applying a non-function" );
            return op.apply( null, expr.args.map( arg => {
                return evaluate( arg, env );
            }));
    }
}

let specialForms = Object.create( null );

specialForms[ "if" ] = function( args, env ){
    // 'if' takes exactly three arguments, more or less results in
    // a syntax error
    if( args.length != 3 )
        throw new SyntaxError( "Bad number of args to if" );
    if( evaluate( args[ 0 ], env ) !== false )
        return evaluate( args[ 1 ], env );
    else
        return evaluate( args[ 2 ], env );
};

specialForms[ "while" ] = function( args, env ){
    // while loops require exactly two arguments
    if( args.length != 2 )
        throw new SyntaxError( "Bad number of args to while" );
    while( evaluate( args[ 0 ], env ) !== false )
        evaluate( args[ 1 ], env );
    // Since undefined does not exist in Egg, we return false,
    // for lack of a meaningful result.
    return false;
};

specialForms[ "do" ] = function( args, env ){
    // do forms simply execute expression from top to bottom
    // returning the value
    let value = false;
    args.forEach( arg => {
        value = evaluate( arg, env );
    });
    return value;
};

specialForms[ "define" ] = function( args, env ){
    // define allows you to set environment variables 
    if( args.length != 2 || args[ 0 ].type != "word" )
        throw new SyntaxError( "Bad use of define" );
    let value = evaluate( args[ 1 ], env );
    env[ args[ 0 ].name ] = value;
    // define is an expression so must return a value
    return value;
};

specialForms[ "set" ] = function(args, env) {
    if( args.length != 2 || args[ 0 ].type != 'word' )
        throw new SyntaxError( "Bad use of set" );
    let value = evaluate( args[ 1 ], env );
    let varName = args[ 0 ].name;

    for( let scope = env; scope; scope = Object.getPrototypeOf( scope )){
        if( Object.prototype.hasOwnProperty.call( scope, varName )){
            scope[ varName ] = value;
            return value;
        }
        throw new ReferenceError( "Setting undefined variable " + varName );
    }
};

let topEnv = Object.create( null );
topEnv[ "true" ] = true;
topEnv[ "false" ] = false;
topEnv[ "array" ] = function(){
    return Array.prototype.slice.call( arguments, 0 );
};
topEnv[ "length" ] = function( array ){
    if( !array instanceof Array ) 
        throw new TypeError( "No array provided" );
    return array.length;
};
topEnv[ "element" ] = function( array, n ){
    if( !array instanceof Array )
        throw new TypeError( "Invalid array supplied" );
    let pos = Number( n );
    if( isNaN( pos ) ) 
        throw new TypeError( "Invalid number supplied for array index" );
    return array[ pos ]; 
};

let prog = parse( "if( true, false, true )" );
console.log( evaluate( prog, topEnv ));

[ "+", "-", "*", "/", "==", "<", ">" ].forEach( op => {
    topEnv[ op ] = new Function( "a, b", "return a " + op + " b;" );
});

topEnv[ "print" ] = function( value ){
    console.log( value );
    // expression!
    return value;
};

function run(){
    // create fresh environment using the topEnv as a base
    let env = Object.create( topEnv );
    // use the Array slice to get arguments as a proper array structure and convert into a string
    let program = Array.prototype.slice.call( arguments, 0 ).join( "\n" );
    // return the results of evaluating the parsed string/program
    return evaluate( parse( program ), env );
}

run( "do( define( total, 0 ), ",
        " define ( count, 1 ), ",
        " while( < ( count, 11 ), ",
        "   do( define( total, +( total, count )), ",
        "       define( count, +( count, 1 )))), ",
        " print( total )) ");
// define total as 0
// define count as 1
// while count is < 11
// define total as total + count
// define count as count + 1
// print the total
// THE SUM OF 1-10 : 55!

specialForms[ 'fun' ] = function( args, env ){
    if( !args.length )
        throw new SyntaxError( "Functions need a body" );
    // all arguments must be named
    function name( expr ){
        if( expr.type != "word" )
            throw new SyntaxError( "Arg names must be words" );
        return expr.name;
    }
    // the final argument is the function body 
    let argNames = args.slice( 0, args.length - 1 ).map( name );
    let body = args[ args.length -1 ];

    return function(){
        if( arguments.length != argNames.length)
            throw new TypeError( "Wrong number of arguments" );
        // setup a local environment with access to the global scope (but not vice versa! )
        let localEnv = Object.create( env );
        // push the arguments into the local scope and evaluate the function body
        for( let i = 0; i < arguments.length; i++ ){
            localEnv[ argNames[ i ]] = arguments[ i ];
        }
            return evaluate( body, localEnv );

    };
};

run( "do( define( plusOne, fun( a, +( a, 1 ))), ",
        " print( plusOne( 10 ))) " );
// define plusOne which is a function that takes in one argument
// print plusOne( 10 )
// hopefully 11…

run( "do( define( pow, fun( base, exp, ",
        "   if( == (exp, 0 ), ",
        "       1, ",
        "       *( base, pow( base, -( exp, 1 )))))), ",
        " print( pow( 2, 10 ))) " );
// define pow function that receives base and exp as arguments
// if exp == 0 return 1 
// else multiply base with recursive call to self with exp - 1 as second argument
// print results…hopefully 1024

// Closure! How does this work? Answer is 9 by the way
run( "do( define( f, fun( a, fun( b, + ( a, b )))), ",
        " print( f( 4 )( 5 ))) " );

console.log(parse("# hello\nx"));
// → {type: "word", name: "x"}

console.log(parse("a # one\n   # two\n()"));

run("do(define(sum, fun(array,",
    "     do(define(i, 0),",
    "        define(sum, 0),",
    "        while(<(i, length(array)),",
    "          do(define(sum, +(sum, element(array, i))),",
    "             define(i, +(i, 1)))),",
    "        sum))),",
    "   print(sum(array(1, 2, 3))))");


run("do(define(x, 4),",
    "   define(setx, fun(val, set(x, val))),",
    "   setx(50),",
    "   print(x))");
// → 50
run("set(quux, true)");
// → Some kind of ReferenceError
