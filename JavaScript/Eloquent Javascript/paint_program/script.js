// element creation helper function
function elt( name, attributes ){
    let node = document.createElement( name );
    if( attributes ){
        for( let attr in attributes )
            if( attributes.hasOwnProperty( attr ))
                node.setAttribute( attr, attributes[ attr ]);
    }
    for( let i = 2; i < arguments.length; i++ ){
        let child = arguments[ i ];
        if( typeof child == "string" )
            child = document.createTextNode( child );
        node.appendChild( child );
    }
    return node;
}

function loadImageURL( cx, url ){
    let image = document.createElement( "img" );
    image.addEventListener( "load", () => {
        let color = cx.fillStyle, size = cx.lineWidth;
        cx.canvas.width = image.width;
        cx.canvas.height = image.height;
        cx.drawImage( image, 0, 0 );
        cx.fillStyle = color;
        cx.strokeStyle = color;
        cx.lineWidth = size;
    });
    image.src = url;
}

let controls = Object.create( null );

function createPaint( parent ){
    let canvas = elt( "canvas", { width : 500, height : 300 });
    let cx = canvas.getContext( "2d" );
    let toolbar = elt( "div", { class : "toolbar" });
    for( let name in controls )
        toolbar.appendChild( controls[ name ]( cx ));
    let panel = elt( "div", { class : "picturepanel" }, canvas );
    parent.appendChild( elt( "div", null, panel, toolbar ));
}

let tools = Object.create( null );
controls.tool = function( cx ){
    let select = elt( "select" );
    for( let name in tools )
        select.appendChild( elt( "option", null, name ));

    cx.canvas.addEventListener( "mousedown", event => {
        if( event.which == 1 ){
            tools[ select.value ]( event, cx );
            event.preventDefault();
        }
    });

    return elt( "span", null, "Tool: ", select );
};

controls.color = function( cx ){
    let input = elt( "input", { type : "color" });
    input.addEventListener( "change", () => {
        cx.fillStyle = input.value;
        cx.strokeStyle = input.value;
    });
    return elt( "span", null, "Color: ", input );
};

controls.brushSize = function( cx ){
    let select = elt( "select" );
    let sizes = [ 1, 2, 3, 5, 8, 12, 25, 35, 50, 75, 100 ];
    sizes.forEach( size => {
        select.appendChild( elt( "option", { value : size }, size + " pixels" ));
    });
    select.addEventListener( "change", () => {
        cx.lineWidth = select.value;
    });
    return elt( "span", null, "Brush size: ", select );
};

controls.save = function( cx ){
    let link = elt( "a", { href : "/" }, "Save" );
    function update(){
        try{
            link.href = cx.canvas.toDataURL();
        } catch( e ){
            if( e instanceof SecurityError )
                link.href = "javascript:alert(" + 
                    JSON.stringify( "Can't save : " +
                            e.toString()) + ")";
            else 
                throw e;
        }
    }
    link.addEventListener( "mouseover", update );
    link.addEventListener( "focus", update );
    return link;
};

controls.openFile = function( cx ){
    let input = elt( "input", { type : "file" });
    input.addEventListener( "change", () => {
        if( input.files.length == 0 ) return;
        let reader = new FileReader();
        reader.addEventListener( "load", () => {
            loadImageURL( cx, reader.result );
        });
        reader.readAsDataURL( input.files[ 0 ]);
    });
    return elt( "div", null, "Open file: ", input );
};

controls.openURL = function( cx ){
    let input = elt( "input", { type : "text" });
    let form = elt( "form", null, "Open URL: ", input, elt( "button", { type : "submit" }, "load" ));
    form.addEventListener( "submit", event => {
        event.preventDefault();
        loadImageURL( cx, input.value );
    });
    return form;
};

function relativePos( event, element ){
    let rect = element.getBoundingClientRect();
    return { x : Math.floor( event.clientX - rect.left ),
        y : Math.floor( event.clientY - rect.top )};
}

function trackDrag( onMove, onEnd ){
    function end( event ){
        removeEventListener( "mousemove", onMove );
        removeEventListener( "mouseup", end );
        if( onEnd )
            onEnd( event );
    }
    addEventListener( "mousemove", onMove );
    addEventListener( "mouseup", end );
}

tools.Line = function( event, cx, onEnd ){
    cx.lineCap = "round";
    let pos = relativePos( event, cx.canvas );
    trackDrag( event => {
        cx.beginPath();
        cx.moveTo( pos.x, pos.y );
        pos = relativePos( event, cx.canvas );
        cx.lineTo( pos.x, pos.y );
        cx.stroke();
    }, onEnd );
};


tools[ "Pick color" ] = function( event, cx ){
    let pos = relativePos( event, cx.canvas );
    try{
        let color = cx.getImageData( pos.x, pos.y, 1, 1 );
        let r = color.data[ 0 ].toString( 16 );
        let g = color.data[ 1 ].toString( 16 );
        let b = color.data[ 2 ].toString( 16 );
        let hex = "#" + r + g + b;

        document.getElementsByTagName( "input" )[ 0 ].value = hex;
        cx.strokeStyle = hex;
        cx.fillStyle = hex;
    } catch( e ){
        /* if( e instanceof SecurityError )
            link.href = "javascript:alert(" +
                JSON.stringify( "Can't get image data from external image : " + e.toString()) + ")";
        else
            throw e; */
        throw e;
    }
};

function rectangleFrom( a, b ){
    return { 
        left : Math.min( a.x, b.x ),
        top : Math.min( a.y, b.y ),
        width : Math.abs( a.x - b.x ),
        height : Math.abs( a.y - b.y )
    };
}
function drawingGuide(){
    let rect = document.createElement( "div" );
    rect.style.position = "absolute";
    rect.style.border = "1px dashed cyan";
    rect.style.background = "rgba( 0, 252, 255, 0.2 )";
    return rect;
}

function guideResize( shape, coords ){
    shape.style.left = coords.left + "px";
    shape.style.top = coords.top + "px";
    shape.style.width = coords.width + "px";
    shape.style.height = coords.height + "px";
}

tools.Rectangle = function( event, cx ){
    let relStart = relativePos( event, cx.canvas );
    let pageStart = { x : event.pageX, y : event.pageY };
    let rect = drawingGuide(); 
    document.body.appendChild( rect );

    trackDrag( event => {
        let coord = rectangleFrom( pageStart, {
            x : event.pageX, y : event.pageY });
        guideResize( rect, coord );
    }, event => {
        let finalRect = rectangleFrom( relStart, relativePos( event, cx.canvas ));
        cx.fillRect( finalRect.left, finalRect.top, finalRect.width, finalRect.height );
        document.body.removeChild( rect );
    });
};

tools.Oval = function( event, cx ){
    let relStart = relativePos( event, cx.canvas );
    let pageStart = { x : event.pageX, y : event.pageY };
    let ov = drawingGuide();
    ov.style[ "border-radius" ] = "50%";
    document.body.append( ov );

    trackDrag( event => {
        let coord = rectangleFrom( pageStart, {
            x : event.pageX, y : event.pageY });
        guideResize( ov, coord );
    }, event => {
        let finalOv = rectangleFrom( relStart, relativePos( event, cx.canvas ));
        let cxp = finalOv.left + ( finalOv.width / 2 ); 
        let cyp = finalOv.top + ( finalOv.height / 2 );
        cx.beginPath();
        cx.ellipse( 
                cxp, cyp,
                finalOv.width / 2,
                finalOv.height / 2,
                0, Math.PI * 2,
                false );
        cx.closePath();
        cx.fill();
        document.body.removeChild( ov );
    });
};

tools.Erase = function( event, cx ){
    cx.globalCompositeOperation = "destination-out";
    tools.Line( event, cx, function() {
        cx.globalCompositeOperation = "source-over";
    });
};

tools.Text = function( event, cx ){
    let text = prompt( "Text:", "" );
    if( text ){
        let pos = relativePos( event, cx.canvas );
        cx.font = Math.max( 7, cx.lineWidth ) + "px sans-serif";
        cx.fillText( text, pos.x, pos.y );
    }
};

function forAllNeighbours( point, fn ){
    fn({ x : point.x, y : point.y + 1 });
    fn({ x : point.x, y : point.y - 1 });
    fn({ x : point.x + 1, y : point.y });
    fn({ x : point.x - 1, y : point.y });
}

function isSameColor( data, pos1, pos2 ){
    let offset1 = ( pos1.x + pos1.y * data.width ) * 4;
    let offset2 = ( pos2.x + pos2.y * data.width ) * 4;
    for( let i = 0; i < 4; i++ ){
        if( data.data[ offset1 + i ] != data.data[ offset2 + i ])
            return false;
    }
    return true;
}

tools[ "Flood Fill" ] = function( event, cx ){
    let startPos = relativePos( event, cx.canvas );
    let data = cx.getImageData( 0, 0, cx.canvas.width, cx.canvas.height );
    let alreadyFilled = new Array( data.width * data.height );

    let workList = [ startPos ];
    while( workList.length ){
        let pos = workList.pop();
        let offset = pos.x + data.width * pos.y;
        if( alreadyFilled[ offset ]) continue;

        cx.fillRect( pos.x, pos.y, 1, 1 );
        alreadyFilled[ offset ] = true;

        forAllNeighbours( pos, neighbour => {
            if( neighbour.x >= 0 && neighbour.x < data.width && neighbour.y >= 0 && neighbour.y < data.height && isSameColor( data, startPos, neighbour ))
                workList.push( neighbour );
        });
    }
};

tools.Dot = function( event, cx ){
    let startPos = relativePos( event, cx.canvas );
    cx.fillRect( startPos.x - ( cx.lineWidth / 2 ), startPos.y - ( cx.lineWidth / 2 ), cx.lineWidth, cx.lineWidth );
};


tools.Spray = function( event, cx ){
    let radius = cx.lineWidth / 2;
    let area = radius * radius * Math.PI;
    let dotsPerTick = Math.ceil( area / 30 );
    let currentPos = relativePos( event, cx.canvas );
    let spray = setInterval( () => {
        for( let i = 0; i < dotsPerTick; i++ ){
            let offset = randomPointInRadius( radius );
            cx.fillRect( currentPos.x + offset.x, currentPos.y + offset.y, 1, 1 );
        }
    }, 25 );
    trackDrag( event => {
        currentPos = relativePos( event, cx.canvas );
    }, () => {
        clearInterval( spray );
    });
};

function randomPointInRadius( radius ){
    for( ;; ){
        let x = Math.random() * 2 - 1;
        let y = Math.random() * 2 - 1;
        if( x * x + y * y <= 1 )
            return { x : x * radius, y : y * radius };
    }
}
