function request( options, callback ){
    let req = new XMLHttpRequest();
    req.open( options.method || "GET", options.pathname, true );
    req.addEventListener( "load", () => {
        if( req.status < 400 )
            callback( null, req.responseText );
        else
            callback( new Error( "Request failed : " + req.statusText ));
    });
    req.addEventListener( "error", () => {
        callback( new Error( "Network error" ));
    });
    req.send( options.body || null );
}

let lastServerTime = 0;

request({ pathname : "talks" }, ( error, res ) => {
    if( error ){
        reportError( error );
    } else {
        res = JSON.parse( res );
        displayTalks( res.talks );
        lastServerTime = res.serverTime;
        waitForChanges();
    }
});

function reportError( error ){
    if( error )
        alert( error.toString());
}

let talkDiv = document.querySelector( "#talks" );
let shownTalks = Object.create( null );

function saveComment( talk, commentField ){
    let comment = commentField.value;
    if( comment.length > 0 )
        localStorage.setItem( talk, comment );
}

function restoreComment( talk, commentField ){
    if( localStorage.getItem( talk )){
        commentField.value = localStorage.getItem( talk );
    }
}

function removeSavedComment( talk ){
    localStorage.removeItem( talk );
}

function displayTalks( talks ){
    talks.forEach( talk => {
        let shown = shownTalks[ talk.title ];
        if( talk.deleted ){
            if( shown ){
                talkDiv.removeChild( shown );
                delete shownTalks[ talk.title ];
            }
        } else {
            let node = drawTalk( talk );
            if( shown )
                talkDiv.replaceChild( node, shown );
            else
                talkDiv.appendChild( node );
            shownTalks[ talk.title ] = node;
        }
    });
}

function instantiateTemplate( name, values ){
    function instantiateText( text ){
        return text.replace( /\{\{(\w+)\}\}/g, ( _, name ) => {
            return values[ name ];
        });
    }

    function attr( node, attrName ){
        return node.nodeType == document.ELEMENT_NODE && node.getAttribute( attrName );
    }
    
    function instantiate( node, values ){
        if( node.nodeType == document.ELEMENT_NODE ){
            let copy = node.cloneNode();
            for( let i = 0; i < node.childNodes.length; i++ ){
                let child = node.childNodes[ i ];
                let when = attr( child, "template-when" ), unless = attr( child, "template-unless" );
                if( when && !values[ when ] || unless && values[ unless ])
                    continue;

                let repeat = attr( child, "template-repeat" );
                if( repeat )
                    ( values[ repeat ] || [] ).forEach( element => {
                        copy.appendChild( instantiate( child, element ));
                    });
                else
                    copy.appendChild( instantiate( child, values ));
            }
            return copy;
        } else if( node.nodeType == document.TEXT_NODE ){
            return document.createTextNode( instantiateText( node.nodeValue ));
        } else {
            return node;
        }
    }
    let template = document.querySelector( "#template ." + name );
    return instantiate( template, values );
}

function drawTalk( talk ){
    let node = instantiateTemplate( "talk", talk );
    let comments = node.querySelector( ".comments" );
    let comments_input = node.querySelector( "input" );

    restoreComment( talk.title, comments_input );

    node.querySelector( "button.del" ).addEventListener( "click", deleteTalk.bind( null, talk.title ));
    comments_input.addEventListener( "keydown", saveComment.bind( null, talk.title, comments_input ));

    let form = node.querySelector( "form" );
    form.addEventListener( "submit", event => {
        event.preventDefault();
        addComment( talk.title, form.elements.comment.value );
        removeSavedComment( talk.title );
        form.reset();
    });
    return node;
}

function talkURL( title ){
    return "talks/" + encodeURIComponent( title );
}

function deleteTalk( title ){
    request({ pathname : talkURL( title ), method : "DELETE" }, reportError );
}

let nameField = document.querySelector( "#name" );
nameField.value = localStorage.getItem( "name" ) || "";
nameField.addEventListener( "change", () => {
    localStorage.setItem( "name", nameField.value );
    console.log( "changed name" );
});

function addComment( title, comment ){
    let com = { 
        author : nameField.value,
        message : comment };
    request({
        pathname : talkURL( title ) + "/comments",
        body : JSON.stringify( com ),
        method : "POST" },
        reportError );
}

let talkForm = document.querySelector( "#newtalk" );

talkForm.addEventListener( "submit", event => {
    event.preventDefault();
    request({
        pathname : talkURL( talkForm.elements.title.value ),
        method : "PUT",
        body : JSON.stringify({
            presenter : nameField.value,
            summary : talkForm.elements.summary.value })}, reportError );
    talkForm.reset();
});

function waitForChanges(){
    request({
        pathname : "talks?changesSince=" + lastServerTime },
        ( error, res ) => {
            if( error ){
                setTimeout( waitForChanges, 2500 );
                console.error( error.stack );
            } else {
                res = JSON.parse( res );
                displayTalks( res.talks );
                lastServerTime = res.serverTime;
                waitForChanges();
            }
        });
}


