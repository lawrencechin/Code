let Router = module.exports = function(){
    this.routes = [];
};

Router.prototype.add = function( method, url, handler ){
    this.routes.push({ method, url, handler });
};

Router.prototype.resolve = function( req, res ){
    let path = require( "url" ).parse( req.url ).pathname;

    return this.routes.some( route => {
        let match = route.url.exec( path );
        if( !match || route.method != req.method )
            return false;

        let urlParts = match.slice( 1 ).map( decodeURIComponent );
        route.handler.apply( null, [ req, res ].concat( urlParts ));
        return true;
    });
};
