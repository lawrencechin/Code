let http = require( "http" );
let Router = require( "./router" );
let ecstatic = require( "ecstatic" );
let fs = require( "fs" );

let fileServer = ecstatic({ root : "./public" });
let router = new Router();

http.createServer(( req, res ) => {
    if( !router.resolve( req, res ))
        fileServer( req, res );
}).listen( 8000 );

function respond( res, status, data, type ){
    res.writeHeader( status, {
        "Content-Type" : type || "text/plain"
    });
    res.end( data );
}

function respondJSON( res, status, data ){
    respond( res, status, JSON.stringify( data ),
            "application/json" );
}

let talks = loadTalks();

function loadTalks(){
    let result = Object.create( null ), json;
    try {
        json = JSON.parse( fs.readFileSync( "talks/talks.json", "utf8" ));
    } catch( e ){
        json = {};
    }
    for( let title in json )
        result[ title ] = json[ title ];
    return result;
}


router.add( "GET", /^\/talks\/([^\/]+)$/,
        ( req, res, title ) => {
            if( title in talks )
                respondJSON( res, 200, talks[ title ]);
            else
                respond( res, 404, "No talk '" + title + "' found" );
        });

router.add( "DELETE", /^\/talks\/([^\/]+)$/,
        ( req, res, title ) => {
            if( title in talks ){
                delete talks[ title ];
                registerChange( title );
            }
            respond( res, 204, null );
        });

function readStreamAsJSON( stream, callback ){
    let data = "";
    stream.on( "data", chunk => {
        data += chunk;
    });
    stream.on( "end", () => {
        let result, error;
        try { result = JSON.parse( data ); }
        catch( e ){ error = e; }
        callback( error, result );
    });
    stream.on( "error", error => {
        callback( error );
    });
}

router.add( "PUT", /^\/talks\/([^\/]+)$/,
        ( req, res, title ) => {
            readStreamAsJSON( req, ( error, talk ) => {
                if( error ){
                    respond( res, 400, error.toString());
                } else if ( !talk ||
                        typeof talk.presenter != "string" ||
                        typeof talk.summary != "string" ){
                    respond( res, 400, "Bad talk data" );
                } else {
                    talks[ title ] = {
                        title : title,
                        presenter : talk.presenter,
                        summary : talk.summary,
                        comments : [] };
                    registerChange( title );
                    respond( res, 204, null );
                }
            });
        });
router.add( "POST", /^\/talks\/([^\/]+)\/comments$/,
        ( req, res, title ) => {
            readStreamAsJSON( req, ( error, comment ) => {
                if( error ){
                    respond( res, 400, error.toString());
                } else if ( !comment || 
                        typeof comment.author != "string" ||
                        typeof comment.message != "string" ){
                    respond( res, 400, "Bad comment data" );
                } else if( title in talks ){
                    talks[ title ].comments.push( comment );
                    registerChange( title );
                    respond( res, 204, null );
                } else {
                    respond( res, 404, "No talk '" + title + "' found" );
                }
            });
        });

function sendTalks( talks, res ){
    respondJSON( res, 200, {
        serverTime : Date.now(),
        talks : talks
    });
}

router.add( "GET", /^\/talks$/, ( req, res ) => {
    let query = require( "url" ).parse( req.url, true ).query;
    if( query.changesSince == null ){
        let list = [];
        for( let title in talks )
            list.push( talks[ title ]);
        sendTalks( list, res );
    } else {
        let since = Number( query.changesSince );
        if( isNaN( since )){
            respond( res, 400, "Invalid Parameter" );
        } else {
            let changed = getChangedTalks( since );
            if( changed.length > 0 )
                sendTalks( changed, res );
            else
                waitForChanges( since, res );
        }
    }
});

let waiting = [];

function waitForChanges( since, res ){
    let waiter = { since : since, response : res };
    waiting.push( waiter );
    setTimeout( () => {
        let found = waiting.indexOf( waiter );
        if( found > -1 ){
            waiting.splice( found, 1 );
            sendTalks( [], res );
        }
    }, 90 * 1000 );
}

let changes = [];

function registerChange( title ){
    changes.push({ title : title, time : Date.now()});

    waiting.forEach( waiter => {
        sendTalks( getChangedTalks( waiter.since ), waiter.response );
    });
    waiting = [];

    fs.writeFile( "talks/talks.json", JSON.stringify( talks ), err => {
        if ( err ) console.log( err );
        console.log( "Data saved" );
    });

}

function getChangedTalks( since ){
    let found = [];
    function alreadySeen( title ){
        return found.some( f => {
            return f.title == title; 
        });
    }
    for( let i = changes.length - 1; i >= 0; i-- ){
        let change = changes[ i ];
        if( change.time <= since )
            break;
        else if( alreadySeen( change.title ))
            continue;
        else if( change.title in talks )
            found.push( talks[ change.title ]);
        else
            found.push({ title : change.title, deleted : true });
    }
    return found;
}
