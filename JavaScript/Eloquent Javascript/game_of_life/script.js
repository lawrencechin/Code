/* Grid */
function Grid( x, y ){
    if( !( this instanceof Grid ))
        return new Grid( x, y );
    this.x = x;
    this.y = y;
    this.grid = []; 
}

Grid.prototype = Object.create( null ); 

Grid.prototype.generateGrid = function(){
    for( let i = 0; i < this.y; i++ ){
        let arrX = [];
        for( let j = 0; j < this.x; j++ ){
            if( Math.ceil( Math.random() * 2 ) == 1 )
                arrX.push( 1 );
            else 
                arrX.push( 0 );
        }
        this.grid.push( arrX );
    }
};

Grid.prototype.checkSurroundingCells = function( x, y ){
    let checkedArr = [];
    for( let i = y - 1; i <= y + 1; i++ ){
        if( i >= 0 && i < this.grid.length ){
            for( let j = x - 1; j <= x + 1; j++ ){
                if( j >= 0 && j < this.grid[ i ].length ){
                    if( this.grid[ i ][ j ] == 1 )
                        checkedArr.push( 1 );
                }
            }
        }
    }
    return this.grid[ y ][ x ] == 1 ? checkedArr.slice( 0, checkedArr.length - 1 ) : checkedArr;
};

Grid.prototype.cellLives = function( x, y ){
    let surrCells = this.checkSurroundingCells( x, y );
    if( this.grid[ y ][ x ] == 1 )
        return surrCells.length == 2 || surrCells.length == 3;
    return surrCells.length == 3;
};

Grid.prototype.updateCellStatus = function( x, y ){
    if( this.grid[ y ][ x ] == 1 )
        this.grid[ y ][ x ] = 0;
    else
        this.grid[ y ][ x ] = 1;
};

Grid.prototype.checkCellStatus = function(){
    let cellsToUpdate = [];
    let rowCount = 0;
    let cellCount = 0;
    this.grid.forEach( ( row, index ) => {
        rowCount++;
        row.forEach( ( cell, i ) => {
            cellCount++;
            if( cell == 1 && !this.cellLives( i, index ))
                cellsToUpdate.push( [ i, index ] );
            else if( cell == 0 && this.cellLives( i, index ))
                cellsToUpdate.push( [ i, index ] );
        });
    });
    return cellsToUpdate;
};

Grid.prototype.changePendingCells = function( arr ){
    arr.forEach( vector => {
        this.updateCellStatus( vector[ 0 ], vector[ 1 ] );
    });
};

Grid.prototype.runGeneration = function(){
    this.changePendingCells( this.checkCellStatus() );
};

/* Display */

function Display( x, y ){
    this.grid = new Grid( x, y );
    
    this.grid.generateGrid();
}

Display.prototype = Object.create( null );

Display.prototype.generateTable = function(){
    let table = document.createElement( "table" );
    this.grid.grid.forEach( ( row, index ) => {
        let tr = document.createElement( "tr" );
        row.forEach( ( cell, i ) => {
            let tc = document.createElement( "td" );
            let input = document.createElement( "input" );
            input.type = "checkbox";
            input.setAttribute( "data-vector", i + "," + index );
            if( cell == 1 )
                input.checked = true;
            tc.appendChild( input );
            tr.appendChild( tc );
        });
        table.appendChild( tr );
    });
    return table;
};

Display.prototype.nextGen = function( btn, container ){
    document.getElementById( btn ).addEventListener( "click", () => {
        this.clearGrid( container );
        this.grid.runGeneration();
        this.appendTable( this.generateTable(), container );
    });
};

Display.prototype.checkBoxToggle = function( container ){
    document.getElementById( container ).addEventListener( "click", () => {
        if( event.target.nodeName == "INPUT" ){
            let vector = event.target.getAttribute( "data-vector" ).split( "," );
            this.grid.updateCellStatus( vector[ 0 ], vector[ 1 ] );
        }
    });
};

Display.prototype.clearGrid = function( container ){
    document.getElementById( container ).innerText = "";
};

Display.prototype.appendTable = function( table, container ){
    document.getElementById( container ).appendChild( table );
};

Display.prototype.init = function( btn, container ){
    this.appendTable( this.generateTable(), container );
    this.nextGen( btn, container );
    this.checkBoxToggle( container );
};
