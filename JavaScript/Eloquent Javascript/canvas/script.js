function newCanvas( func, width, height ){
    let can = document.createElement( "canvas" );
    if( width )
        can.width = width;
    if( height )
        can.height = height;
    let canCx = can.getContext( "2d" );
    func( canCx );
    document.body.appendChild( can );
}

let circle = document.querySelector( "circle" );
circle.setAttribute( "fill", "cyan" );

let canvas = document.querySelector( "canvas" );
let context = canvas.getContext( "2d" );
context.fillStyle = "red";
context.fillRect( 10, 10, 100, 50 );

newCanvas( cx => {
    cx.strokeStyle = "blue";
    cx.strokeRect( 5, 5, 50, 50 );
    cx.lineWidth = 5;
    cx.strokeRect( 135, 5, 50, 50 );
});

newCanvas( cx => {
    cx.beginPath();
    for( let y = 10; y < 100; y +=10 ){
        cx.moveTo( 10, y );
        cx.lineTo( 90, y );
    }
    cx.stroke();
});

newCanvas( cx => {
    cx.beginPath();
    cx.moveTo( 50, 10 );
    cx.lineTo( 10, 70 );
    cx.lineTo( 90, 70 );
    cx.fill();
});

newCanvas( cx => {
    cx.beginPath();
    cx.moveTo( 10, 90 );
    // control( 60, 10 ) goal=( 90, 90 )
    cx.quadraticCurveTo( 60, 10, 90, 90 );
    cx.lineTo( 60, 10 );
    cx.closePath();
    cx.stroke();
});

newCanvas( cx => {
    cx.beginPath();
    cx.moveTo( 10, 90 );
    // control1=( 10, 10 ) control2=( 90, 10 ) goal=( 50,90 )
    cx.bezierCurveTo( 10, 10, 90, 10, 50, 90 );
    cx.lineTo( 90, 10 );
    cx.lineTo( 10, 10 );
    cx.closePath();
    cx.stroke();
});

newCanvas( cx => {
    cx.beginPath();
    cx.moveTo( 10, 10 );
    cx.arcTo( 90, 10, 90, 90, 20 );
    cx.moveTo( 10, 10 );
    cx.arcTo( 90, 10, 90, 90, 80 );
    cx.lineTo( 90, 90 );
    cx.stroke();
});

newCanvas( cx => {
    cx.beginPath();
    cx.arc( 50, 50, 40, 0, 7 );
    cx.moveTo( 150, 50 );
    cx.arc( 150, 50, 40, 0, 0.5 * Math.PI );
    cx.stroke();
});

newCanvas( cx => {
    const results = [
        { name : "Satisfied", count : 1043, color : "lightblue" },
        { name : "Neutral", count : 563, color : "lightgreen" },
        { name : "Unsatisfied", count : 510, color : "pink" },
        { name : "No comment", count : 175, color : "silver" }
    ];
    let total = results.reduce( ( total, choice ) => {
        return total += choice.count;
    }, 0 );
    // Start at the top
    let currentAngle = -0.5 * Math.PI;
    results.forEach( result => {
        let sliceAngle = ( result.count / total ) * 2 * Math.PI;
        cx.beginPath();
        cx.arc( 100, 100, 100, currentAngle, currentAngle + sliceAngle );
        currentAngle += sliceAngle;
        cx.lineTo( 100, 100 );
        cx.fillStyle = result.color;
        cx.fill();
    });
}, 200, 200 );

newCanvas( cx => {
    cx.font = "28px Georgia";
    cx.fillStyle = "fushsia";
    cx.fillText( "I can draw text, too!", 10 , 50 );
});

newCanvas( cx => {
    let img = document.createElement( "img" );
    img.src = "hat.png";
    img.addEventListener( "load", () => {
        for( let x  = 10; x < 200; x += 30 )
            cx.drawImage( img, x, 10 );
    });
});

/* newCanvas( cx => {
    let img = document.createElement( "img" );
    img.src = "player.png";
    let spriteW = 24, spriteH = 30;
    img.addEventListener( "load", () => {
        let cycle = 0;
        setInterval( () => {
            cx.clearRect( 0, 0, spriteW, spriteH );
            cx.drawImage( img,
                                // source rect
                                cycle * spriteW, 0, spriteW, spriteH,
                                // dest rect
                                0, 0, spriteW, spriteH );
            cycle = ( cycle + 1 ) % 8;
        }, 120 );
    });
}); */

newCanvas( canvas => {
    canvas.scale( 3, .5 );
    canvas.beginPath();
    canvas.arc( 50, 50, 40, 0, 7 );
    canvas.lineWidth = 3;
    canvas.stroke();
});

function flipHorizontally( context, around ){
    context.translate( around, 0 );
    context.scale( -1, 1 );
    context.translate( -around, 0 );
};

newCanvas( canvas => {
    let img = document.createElement( "img" );
    img.src = "player.png";
    let spriteW = 24, spriteH = 30;
    img.addEventListener( "load", () => {
        flipHorizontally( canvas, 100 + spriteW / 2 );
        canvas.drawImage( img, 0, 0, spriteW, spriteH,
            100, 0, spriteW, spriteH );
    });
});

newCanvas( cx => {
    function branch( length, angle, scale ){
        cx.fillRect( 0, 0, 1, length );
        if( length < 8 ) return;
        cx.save();
        cx.translate( 0, length );
        cx.rotate( -angle );
        branch( length * scale, angle, scale );
        cx.rotate( 2 * angle );
        branch( length * scale, angle, scale );
        cx.restore();
    }
    cx.translate( 300, 0 );
    branch( 60, 0.5, 0.8 );
}, 600, 300 );

const shapesCanvas = document.getElementById( "shapes" );
const shapesCx = shapesCanvas.getContext( "2d" );
// canvas width 600 height 200

function trapezoid( xpos, ypos ){
    shapesCx.beginPath();
    shapesCx.moveTo( xpos, ypos );
    shapesCx.lineTo( xpos + 50, ypos );
    shapesCx.lineTo( xpos + 40, ypos - 30 );
    shapesCx.lineTo( xpos + 10, ypos - 30 );
    shapesCx.closePath();
    shapesCx.stroke();
}

trapezoid( 0, 60 );

function diamond( xpos, ypos ){
    shapesCx.translate( xpos + 15, ypos + 15 );
    shapesCx.rotate( Math.PI / 4 );
    shapesCx.fillStyle = "red";
    shapesCx.fillRect( -15, -15, 30, 30 );
    shapesCx.setTransform( 1, 0, 0, 1, 0, 0 );
}

diamond( 60, 25 );

function zigzag( xpos, ypos, lines ){
    shapesCx.beginPath();
    shapesCx.moveTo( xpos, ypos );
    for( let i = 0; i <= lines; i++ ){
        shapesCx.lineTo( xpos + 50, ypos + ( i * lines ) + ( lines / 2 ));
        shapesCx.lineTo( xpos, ypos + ( i * lines ) + lines );
    }
    shapesCx.stroke();
}

zigzag( 100, 25, 8 );

function spiral( xpos, ypos ){
    shapesCx.beginPath();
    let radius = 50; xCenter = xpos + radius; yCenter = ypos + radius;
    shapesCx.moveTo( xCenter, yCenter );
    for( let i = 0; i < 300; i++ ){
        let angle = i * Math.PI / 30;
        let distance = radius * i / 300;
        shapesCx.lineTo( xCenter + Math.cos( angle ) * distance, yCenter + Math.sin( angle ) * distance );
    }
    shapesCx.stroke();
}

spiral( 160, 25 );

function star( xpos, ypos, points ){
    let radius = 50; xCen = xpos + radius; yCen = ypos + radius;
    shapesCx.beginPath();
    shapesCx.moveTo( xCen + radius, yCen );
    for( let i = 1; i <= points; i++ ){
        let angle = i * Math.PI / ( points / 2 );
        shapesCx.quadraticCurveTo( xCen, yCen, xCen + Math.cos( angle ) * radius, yCen + Math.sin( angle ) * radius );
    }
    shapesCx.closePath();
    shapesCx.fillStyle = "gold";
    shapesCx.fill();
}

star( 270, 25, 20 );


