// Project : A Platform Game

// a simple level 

let simplePlan = [
  "                      ",
  "                      ",
  "  x              = x  ",
  "  x         o o    x  ",
  "  x @      xxxxx   x  ",
  "  xxxxx            x  ",
  "      x!!!!!!!!!!!!x  ",
  "      xxxxxxxxxxxxxx  ",
  "                      "
];

// The 'x' represents walls, the space characters for empty space and the '!' stand in for our non-moving lava tiles.
// '@' is the player (the starting position) while the 'o' characters are the gold coins to collect. Lastly the '=' stands for a moving lava tile that travels horizontally.

// Level Constructor - takes in a string-based plan

function Level( plan ){
    this.width = plan[ 0 ].length;
    this.height = plan.length;
    this.grid = [];
    this.actors = [];

    for( let y = 0; y < this.height; y++ ){
        let line = plan[ y ], gridLine = [];
        for( let x = 0; x < this.width; x++ ){
            let ch = line[ x ], fieldType = null;
            let Actor = ACTORCHARS[ ch ];
            if( Actor )
                this.actors.push( new Actor( new Vector( x, y ), ch ));
            else if( ch == "x" )
                fieldType = "wall";
            else if( ch == "!" )
                fieldType = "lava";
            gridLine.push( fieldType );
        }
        this.grid.push( gridLine );
    }
    this.player = this.actors.filter( actor => {
        return actor.type == "player";
    })[0];
    this.status = this.finishDelay = null;
}

Level.prototype.isFinished = function(){
    return this.status != null && this.finishDelay < 0;
};

Level.prototype.obstacleAt = function( pos, size ){
    let xStart = Math.floor( pos.x );
    let xEnd = Math.ceil( pos.x + size.x );
    let yStart = Math.floor( pos.y );
    let yEnd = Math.ceil( pos.y + size.y );

    if( xStart < 0 || xEnd > this.width || yStart < 0 )
        return "wall";
    if( yEnd > this.height )
        return "lava";
    for( let y = yStart; y < yEnd; y++ ){
        for( let x = xStart; x < xEnd; x++ ){
            let fieldType = this.grid[ y ][ x ];
            if( fieldType ) return fieldType;
        }
    }
};

Level.prototype.actorAt = function( actor ){
    for( let i = 0; i < this.actors.length; i++ ){
        let other = this.actors[ i ];
        if( other != actor &&
            actor.pos.x + actor.size.x > other.pos.x &&
            actor.pos.x < other.pos.x + other.size.x &&
            actor.pos.y + actor.size.y > other.pos.y &&
            actor.pos.y < other.pos.y + other.size.y )
            return other;
    }
};

const maxStep = 0.05;

Level.prototype.animate = function( step, keys ){
    if( this.status != null )
        this.finishDelay -= step;
    while( step > 0 ){
        let thisStep = Math.min( step, maxStep );
        this.actors.forEach( actor => {
            // this should be the correct one using an
            // arrow function. If something breaks check
            // on this function
            actor.act( thisStep, this, keys );
        });
        step -= thisStep;
    }
};

Level.prototype.playerTouched = function( type, actor ){
    if( type == "lava" && this.status == null ){
        this.status = "lost";
        this.finishDelay = 1;
    } else if( type == "coin" ){
        this.actors = this.actors.filter( other => {
            return other != actor;
        });
        if( !this.actors.some( actor => {
            return actor.type == "coin";
        })){
            this.status = "won";
            this.finishDelay = 1;
        }
    }
};
        

// Trusty Vector Constructor, same as it ever was

function Vector( x, y ){
    this.x = x; this.y = y;
}

Vector.prototype.plus = function( other ){
    return new Vector( this.x + other.x, this.y + other.y );
};

Vector.prototype.times = function( factor ){
    return new Vector( this.x * factor, this.y * factor );
};

// ACTORCHARS object containing key for symbols

const ACTORCHARS = {
    "@" :   Player,
    "o" :   Coin,
    "=" :   Lava,
    "|" :   Lava,
    "v" :   Lava
};

// Player constructor 

function Player( pos ){
    this.pos = pos.plus( new Vector( 0, -0.5 ));
    this.size = new Vector( 0.8, 1.5 );
    this.speed = new Vector( 0, 0 );
}
Player.prototype.type = "player";

const playerXSpeed = 7;

Player.prototype.moveX = function( step, level, keys ){
    this.speed.x = 0;
    if( keys.left ) this.speed.x -= playerXSpeed;
    if( keys.right ) this.speed.x += playerXSpeed;
    
    let motion = new Vector( this.speed.x * step, 0 );
    let newPos = this.pos.plus( motion );
    let obstacle = level.obstacleAt( newPos, this.size );
    if( obstacle )
        level.playerTouched( obstacle );
    else
        this.pos = newPos;
};

const gravity = 30, jumpSpeed = 17;

Player.prototype.moveY = function( step, level, keys ){
    this.speed.y += step * gravity;
    let motion = new Vector( 0, this.speed.y * step );
    let newPos = this.pos.plus( motion );
    let obstacle = level.obstacleAt( newPos, this.size );
    if( obstacle ){
        level.playerTouched( obstacle );
        if( keys.up && this.speed.y > 0 )
            this.speed.y -= jumpSpeed;
        else
            this.speed.y = 0;
    } else {
        this.pos = newPos;
    }
};

Player.prototype.act = function( step, level, keys ){
    this.moveX( step, level, keys );
    this.moveY( step, level, keys );

    let otherActor = level.actorAt( this );
    if( otherActor )
        level.playerTouched( otherActor.type, otherActor );

    // Losing animation
    if( level.status == "lost" ){
        this.pos.y += step;
        this.size.y -= step;
    }
};

// Lava constructor

function Lava( pos, ch ){
    this.pos = pos;
    this.size = new Vector( 1, 1 );
    if( ch == "=" ){
        this.speed = new Vector( 2, 0 );
    } else if( ch == "|" ){
        this.speed = new Vector( 0, 2 );
    } else if( ch == "v" ){
        this.speed = new Vector( 0, 3 );
        this.repeatPos = pos;
    }
}
Lava.prototype.type = "lava";

Lava.prototype.act = function( step, level ){
    let newPos = this.pos.plus( this.speed.times( step ));
    if( !level.obstacleAt( newPos, this.size ))
        this.pos = newPos;
    else if( this.repeatPos )
        this.pos = this.repeatPos;
    else
        this.speed = this.speed.times( -1 );
};

// Coin constructor

function Coin( pos ){
    this.basePos = this.pos = pos.plus( new Vector( 0.2, 0.1 ));
    this.size = new Vector( 0.6, 0.6 );
    this.wobble = Math.random() * Math.PI * 2;
}
Coin.prototype.type = "coin";
const wobbleSpeed = 8, wobbleDist = 0.07;
Coin.prototype.act = function( step ){
    this.wobble += step * wobbleSpeed;
    let wobblePos = Math.sin( this.wobble ) * wobbleDist;
    this.pos = this.basePos.plus( new Vector( 0, wobblePos ));
};

// Drawing section - uses encapsulation (interfaces)

function elt( name, className ){
    let elt  = document.createElement( name );
    if( className ) elt.className = className;
    return elt;
}

function DOMDisplay( parent, level ){
    this.wrap = parent.appendChild( elt( "div", "game" ));
    this.level = level;
    this.wrap.appendChild( this.drawBackground() );
    this.actorLayer = null;
    this.drawFrame();
}

let scale = 20;

DOMDisplay.prototype.drawBackground = function(){
    let table = elt( "table", "background" );
    table.style.width = this.level.width * scale + "px";
    this.level.grid.forEach( row => {
        let rowElt = table.appendChild( elt( "tr" ));
        rowElt.style.height = scale + "px";
        row.forEach( type => {
            rowElt.appendChild( elt( "td", type ));
        });
    });
    return table;
};

DOMDisplay.prototype.drawActors = function(){
    let wrap = elt( "div" );
    this.level.actors.forEach( actor => {
        let rect  = wrap.appendChild( elt( "div", "actor " + actor.type ));
        rect.style.width = actor.size.x * scale + "px";
        rect.style.height = actor.size.y * scale + "px";
        rect.style.left = actor.pos.x * scale + "px";
        rect.style.top = actor.pos.y * scale + "px";
    });
    return wrap;
};

DOMDisplay.prototype.drawFrame = function(){
    if( this.actorLayer )
        this.wrap.removeChild( this.actorLayer );
    this.actorLayer = this.wrap.appendChild( this.drawActors());
    this.wrap.className = "game " + ( this.level.status || "" );
    this.scrollPlayerIntoView();
};

DOMDisplay.prototype.scrollPlayerIntoView = function(){
    let width = this.wrap.clientWidth;
    let height = this.wrap.clientHeight;
    let margin = width / 3;

    // The viewport
    let left = this.wrap.scrollLeft, right = left + width;
    let top = this.wrap.scrollTop, bottom = top + height;

    let player = this.level.player;
    let center = player.pos.plus( player.size.times( 0.5 )).times( scale );

    if( center.x < left + margin )
        this.wrap.scrollLeft = center.x - margin;
    else if( center.x > right - margin )
        this.wrap.scrollLeft = center.x + margin - width;
    if( center.y < top + margin )
        this.wrap.scrollTop = center.y - margin;
    else if( center.y > bottom - margin )
        this.wrap.scrollTop = center.y + margin - height;
};

DOMDisplay.prototype.clear = function(){
    this.wrap.parentNode.removeChild( this.wrap );
};

function CanvasDisplay( parent, level ){
    this.canvas = document.createElement( "canvas" );
    this.canvas.width = Math.min( 600, level.width * scale );
    this.canvas.height = Math.min( 450, level.height * scale );
    parent.appendChild( this.canvas );
    this.cx = this.canvas.getContext( "2d" );

    this.level = level;
    this.animationTime = 0;
    this.flipPlayer = false;

    this.viewport = {
        left : 0,
        top : 0,
        width : this.canvas.width / scale,
        height : this.canvas.height / scale
    };

    this.drawFrame( 0 );
}

CanvasDisplay.prototype.clear = function(){
    this.canvas.parentNode.removeChild( this.canvas );
};

CanvasDisplay.prototype.drawFrame = function( step ){
    this.animationTime += step;
    this.updateViewport();
    this.clearDisplay();
    this.drawBackground();
    this.drawActors();
};

CanvasDisplay.prototype.updateViewport = function(){
    let view = this.viewport, margin = view.width / 3;
    let player = this.level.player;
    let center = player.pos.plus( player.size.times( 0.5 ));

    if( center.x < view.left + margin )
        view.left = Math.max( center.x - margin, 0 );
    else if( center.x > view.left + view.width - margin )
        view.left = Math.min( center.x + margin - view.width,
                this.level.width - view.width );
    if( center.y < view.top + margin )
        view.top = Math.max( center.y - margin, 0 );
    else if( center.y > view.top + view.height - margin )
        view.top = Math.min( center.y + margin - view.height,
                this.level.height - view.height );
};

CanvasDisplay.prototype.clearDisplay = function(){
    if( this.level.status == "won" )
        this.cx.fillStyle = "rgb( 68, 191, 255 )";
    else if( this.level.status == "lost" )
        this.cx.fillStyle = "rgb( 44, 136, 214 )";
    else 
        this.cx.fillStyle = "rgb( 52, 166, 251 )";
    this.cx.fillRect( 0, 0, this.canvas.width, this.canvas.height );
};

let otherSprites = document.createElement( "img" );
otherSprites.src = "sprites.png";

CanvasDisplay.prototype.drawBackground = function(){
    let view = this.viewport;
    let xStart = Math.floor( view.left );
    let xEnd = Math.ceil( view.left + view.width );
    let yStart = Math.floor( view.top );
    let yEnd = Math.ceil( view.top + view.height );

    for( let y = yStart; y < yEnd; y++ ){
        for( let x = xStart; x < xEnd; x++ ){
            let tile = this.level.grid[ y ][ x ];
            if( tile == null ) continue;
            let screenX = ( x - view.left ) * scale;
            let screenY = ( y = view.top ) * scale;
            let tileX = tile == "lava" ? scale : 0;
            this.cx.drawImage( otherSprites, tileX, 0, 
                    scale, scale, screenX, screenY, scale,
                    scale );
        }
    }
};

let playerSprites = document.createElement( "img" );
playerSprites.src = "player.png";
let playerXOverlap = 4;

CanvasDisplay.prototype.drawPlayer = function( x, y, width, height ){
    let sprite = 8, player = this.level.player;
    width += playerXOverlap * 2;
    x -= playerXOverlap;
    if( player.speed.x != 0 )
        this.flipPlayer = player.speed.x < 0;

    if( player.speed.y != 0 )
        sprite = 9;
    else if( player.speed.x != 0 )
        sprite = Math.floor( this.animationTime * 12 ) % 8;

    this.cx.save();
    if( this.flipPlayer )
        flipHorizontally( this.cx, x + width / 2 );

    this.cx.drawImage( playerSprites, sprite * width, 0, 
            width, height, x, y, width, height );
    this.cx.restore();
};

CanvasDisplay.prototype.drawActors = function(){
    this.level.actors.forEach( actor => {
        let width = actor.size.x * scale;
        let height = actor.size.y * scale;
        let x = ( actor.pos.x - this.viewport.left ) * scale;
        let y = ( actor.pos.y - this.viewport.top ) * scale;
        if( actor.type == "player" ){
            this.draw( x, y, width, height );
        } else {
            let tileX = ( actor.type == "coin" ? 2 : 1 ) * scale;
            this.cx.drawImage( otherSprites, tileX, 0, width,
                    height, x, y, width, height );
        }
    });
};


// Key tracking - pressed obj will track if the key/direction is currently being pressed (true) or not (false ). The player will keep moving while one of the directions are true

const arrowCodes = { 37 : "left", 38 : "up", 39 : "right" };

function trackKeys( codes, register ){
    let pressed = Object.create( null );
    function handler( event ){
        if( codes.hasOwnProperty( event.keyCode )){
            let down = event.type == "keydown";
            pressed[ codes[ event.keyCode ]] = down;
            event.preventDefault();
        }
    }
    addEventListener( "keydown", handler );
    addEventListener( "keyup", handler );

    pressed.unregister = function(){
        removeEventListener( "keydown", handler );
        removeEventListener( "keyup", handler );
    };

    return pressed;
}

// Animating the game 
// Helper function to ease the use of requestAnimationFrame

function runAnimation( frameFunc ){
    let lastTime = null;
    function frame( time ){
        let stop = false;
        if( lastTime != null ){
            let timeStep = Math.min( time - lastTime, 100 ) / 1000;
            stop = frameFunc( timeStep ) === false;
        }
        lastTime = time;
        if( !stop )
            requestAnimationFrame( frame );
    }
    requestAnimationFrame( frame );
}


function runLevel( level, Display, andThen ){
    let display = new Display( document.body, level );
    let paused = false;
    let arrows = trackKeys( arrowCodes );

    function steps( step ){
        if( paused )
            return false;
        level.animate( step, arrows );
        display.drawFrame( step );
        if( level.isFinished() ){
            display.clear();
            removeEventListener( "keydown", pause );
            arrows.unregister();
            if( andThen )
                andThen( level.status );
            return false;
        }
    }

    function pause( event ){
        if( event.keyCode == 80 && !paused ){
            paused = true;
            pauseDisplay( paused );
            runAnimation( steps );
        }else if( event.keyCode == 80 && paused ){
            paused = false;
            pauseDisplay( paused );
            runAnimation( steps );
        }
    }

    function pauseDisplay( pause ){
        if( paused ){
            let div = document.createElement( "div" );
            let h1 = document.createElement( "h1" );
            h1.textContent = "Game Paused (press p to resume)";
            div.appendChild( h1 );
            div.style.width = "600px";
            div.style.height = "450px";
            div.style.position = "absolute";
            div.style.left = "0px";
            div.style.top = "0px";
            div.id = "paused";
            div.style.background = "rgba( 255, 255, 255, 0.7 )";
            document.body.appendChild( div );
        } else {
            document.body.removeChild( document.getElementById( "paused" ));
        }
    }

    addEventListener( "keydown", pause );
    runAnimation( steps );
}

function runGame( plans, Display ){
    function startLevel( n, playerLives ){
        runLevel( new Level( plans[ n ]), Display, status => {
            if( playerLives < 1 )
                startLevel( 0, 3 );
            else if( status == "lost" )
                startLevel( n, playerLives - 1 );
            else if( n < plans.length - 1 )
                startLevel( n + 1, playerLives );
            else
                console.log( "You win!" );
        });
    }
    startLevel( 0, 3 );
}


function flipHorizontally( context, around ){
    context.translate( around, 0 );
    context.scale( -1, 1 );
    context.translate( -around, 0 );
};

const GAME_LEVELS = [
  ["                                                                                ",
   "                                                                                ",
   "                                                                                ",
   "                                                                                ",
   "                                                                                ",
   "                                                                                ",
   "                                                                  xxx           ",
   "                                                   xx      xx    xx!xx          ",
   "                                    o o      xx                  x!!!x          ",
   "                                                                 xx!xx          ",
   "                                   xxxxx                          xvx           ",
   "                                                                            xx  ",
   "  xx                                      o o                                x  ",
   "  x                     o                                                    x  ",
   "  x                                      xxxxx                             o x  ",
   "  x          xxxx       o                                                    x  ",
   "  x  @       x  x                                                xxxxx       x  ",
   "  xxxxxxxxxxxx  xxxxxxxxxxxxxxx   xxxxxxxxxxxxxxxxxxxx     xxxxxxx   xxxxxxxxx  ",
   "                              x   x                  x     x                    ",
   "                              x!!!x                  x!!!!!x                    ",
   "                              x!!!x                  x!!!!!x                    ",
   "                              xxxxx                  xxxxxxx                    ",
   "                                                                                ",
   "                                                                                "],
  ["                                      x!!x                        xxxxxxx                                    x!x  ",
   "                                      x!!x                     xxxx     xxxx                                 x!x  ",
   "                                      x!!xxxxxxxxxx           xx           xx                                x!x  ",
   "                                      xx!!!!!!!!!!xx         xx             xx                               x!x  ",
   "                                       xxxxxxxxxx!!x         x                                    o   o   o  x!x  ",
   "                                                xx!x         x     o   o                                    xx!x  ",
   "                                                 x!x         x                                xxxxxxxxxxxxxxx!!x  ",
   "                                                 xvx         x     x   x                        !!!!!!!!!!!!!!xx  ",
   "                                                             xx  |   |   |  xx            xxxxxxxxxxxxxxxxxxxxx   ",
   "                                                              xx!!!!!!!!!!!xx            v                        ",
   "                                                               xxxx!!!!!xxxx                                      ",
   "                                               x     x            xxxxxxx        xxx         xxx                  ",
   "                                               x     x                           x x         x x                  ",
   "                                               x     x                             x         x                    ",
   "                                               x     x                             xx        x                    ",
   "                                               xx    x                             x         x                    ",
   "                                               x     x      o  o     x   x         x         x                    ",
   "               xxxxxxx        xxx   xxx        x     x               x   x         x         x                    ",
   "              xx     xx         x   x          x     x     xxxxxx    x   x   xxxxxxxxx       x                    ",
   "             xx       xx        x o x          x    xx               x   x   x               x                    ",
   "     @       x         x        x   x          x     x               x   x   x               x                    ",
   "    xxx      x         x        x   x          x     x               x   xxxxx   xxxxxx      x                    ",
   "    x x      x         x       xx o xx         x     x               x     o     x x         x                    ",
   "!!!!x x!!!!!!x         x!!!!!!xx     xx!!!!!!!!xx    x!!!!!!!!!!     x     =     x x         x                    ",
   "!!!!x x!!!!!!x         x!!!!!xx       xxxxxxxxxx     x!!!!!!!xx!     xxxxxxxxxxxxx xx  o o  xx                    ",
   "!!!!x x!!!!!!x         x!!!!!x    o                 xx!!!!!!xx !                    xx     xx                     ",
   "!!!!x x!!!!!!x         x!!!!!x                     xx!!!!!!xx  !                     xxxxxxx                      ",
   "!!!!x x!!!!!!x         x!!!!!xx       xxxxxxxxxxxxxx!!!!!!xx   !                                                  ",
   "!!!!x x!!!!!!x         x!!!!!!xxxxxxxxx!!!!!!!!!!!!!!!!!!xx    !                                                  ",
   "!!!!x x!!!!!!x         x!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!xx     !                                                  "],
  ["                                                                                                              ",
   "                                                                                                              ",
   "                                                                                                              ",
   "                                                                                                              ",
   "                                                                                                              ",
   "                                        o                                                                     ",
   "                                                                                                              ",
   "                                        x                                                                     ",
   "                                        x                                                                     ",
   "                                        x                                                                     ",
   "                                        x                                                                     ",
   "                                       xxx                                                                    ",
   "                                       x x                 !!!        !!!  xxx                                ",
   "                                       x x                 !x!        !x!                                     ",
   "                                     xxx xxx                x          x                                      ",
   "                                      x   x                 x   oooo   x       xxx                            ",
   "                                      x   x                 x          x      x!!!x                           ",
   "                                      x   x                 xxxxxxxxxxxx       xxx                            ",
   "                                     xx   xx      x   x      x                                                ",
   "                                      x   xxxxxxxxx   xxxxxxxx              x x                               ",
   "                                      x   x           x                    x!!!x                              ",
   "                                      x   x           x                     xxx                               ",
   "                                     xx   xx          x                                                       ",
   "                                      x   x= = = =    x            xxx                                        ",
   "                                      x   x           x           x!!!x                                       ",
   "                                      x   x    = = = =x     o      xxx       xxx                              ",
   "                                     xx   xx          x                     x!!!x                             ",
   "                              o   o   x   x           x     x                xxv        xxx                   ",
   "                                      x   x           x              x                 x!!!x                  ",
   "                             xxx xxx xxx xxx     o o  x!!!!!!!!!!!!!!x                   vx                   ",
   "                             x xxx x x xxx x          x!!!!!!!!!!!!!!x                                        ",
   "                             x             x   xxxxxxxxxxxxxxxxxxxxxxx                                        ",
   "                             xx           xx                                         xxx                      ",
   "  xxx                         x     x     x                                         x!!!x                xxx  ",
   "  x x                         x    xxx    x                                          xxx                 x x  ",
   "  x                           x    xxx    xxxxxxx                        xxxxx                             x  ",
   "  x                           x           x                              x   x                             x  ",
   "  x                           xx          x                              x x x                             x  ",
   "  x                                       x       |xxxx|    |xxxx|     xxx xxx                             x  ",
   "  x                xxx             o o    x                              x         xxx                     x  ",
   "  x               xxxxx       xx          x                             xxx       x!!!x          x         x  ",
   "  x               oxxxo       x    xxx    x                             x x        xxx          xxx        x  ",
   "  x                xxx        xxxxxxxxxxxxx  x oo x    x oo x    x oo  xx xx                    xxx        x  ",
   "  x      @          x         x           x!!x    x!!!!x    x!!!!x    xx   xx                    x         x  ",
   "  xxxxxxxxxxxxxxxxxxxxxxxxxxxxx           xxxxxxxxxxxxxxxxxxxxxxxxxxxxx     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ",
   "                                                                                                              ",
   "                                                                                                              "],
  ["                                                                                                  xxx x       ",
   "                                                                                                      x       ",
   "                                                                                                  xxxxx       ",
   "                                                                                                  x           ",
   "                                                                                                  x xxx       ",
   "                          o                                                                       x x x       ",
   "                                                                                             o o oxxx x       ",
   "                   xxx                                                                                x       ",
   "       !  o  !                                                xxxxx xxxxx xxxxx xxxxx xxxxx xxxxx xxxxx       ",
   "       x     x                                                x   x x   x x   x x   x x   x x   x x           ",
   "       x= o  x            x                                   xxx x xxx x xxx x xxx x xxx x xxx x xxxxx       ",
   "       x     x                                                  x x   x x   x x   x x   x x   x x     x       ",
   "       !  o  !            o                                  xxxx xxxxx xxxxx xxxxx xxxxx xxxxx xxxxxxx       ",
   "                                                                                                              ",
   "          o              xxx                              xx                                                  ",
   "                                                                                                              ",
   "                                                                                                              ",
   "                                                      xx                                                      ",
   "                   xxx         xxx                                                                            ",
   "                                                                                                              ",
   "                          o                                                     x      x                      ",
   "                                                          xx     xx                                           ",
   "             xxx         xxx         xxx                                 x                  x                 ",
   "                                                                                                              ",
   "                                                                 ||                                           ",
   "  xxxxxxxxxxx                                                                                                 ",
   "  x         x o xxxxxxxxx o xxxxxxxxx o xx                                                x                   ",
   "  x         x   x       x   x       x   x                 ||                  x     x                         ",
   "  x  @      xxxxx   o   xxxxx   o   xxxxx                                                                     ",
   "  xxxxxxx                                     xxxxx       xx     xx     xxx                                   ",
   "        x=                  =                =x   x                     xxx                                   ",
   "        xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   x!!!!!!!!!!!!!!!!!!!!!xxx!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",
   "                                                  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
   "                                                                                                              "]
];
