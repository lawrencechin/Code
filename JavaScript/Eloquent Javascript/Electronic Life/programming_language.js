function parseExpression( program ){
    // remove whitespace up to a non white space char
    program = skipSpace( program );
    let match, expr;
    // match a term encased in double quotes
    if( match = /^"([^"]*)"/.exec( program ))
        expr = { type : "value", value : match[ 1 ] };
    // match a number
    else if( match = /^\d+\b/.exec( program ))
        expr = { type : "value", value : Number( match[ 0 ] )};
    // match a word not in quotes and not whitespace, commas or parenthesis 
    else if( match = /^[^\s()",]+/.exec( program ))
        expr = { type : "word", name : match[ 0 ] };
    // no matches, throw an error
    else
        throw new SyntaxError( "Unexpected syntax: " + program );

    // pass the expr object and the rest of the program to another function
    return parseApply( expr, program.slice( match[ 0 ].length ));
}

function skipSpace( string ){
    // find whitespace or a hash followed by anything, 
    // this represents comments, and use the length
    // property to skip either
    let skippable = string.match( /^(\s|#.*)*/ );
    // return the string minus the trailing whitespace
    return string.slice( skippable[ 0 ].length );
}

function parseApply( expr, program ){
    program = skipSpace( program );
    // if first char is not opening parenthesis then return the expr as it is
    if( program[ 0 ] != "(" )
        return { expr : expr, rest : program };

    // remove the "(" and then whitespace
    program = skipSpace( program.slice( 1 ));
    expr = { type : "apply", operator : expr, args : [] };
    // loop until we hit the closing parenthesis
    while( program[ 0 ] != ")" ){
        // run rest of program back through parseExpression
        let arg = parseExpression( program );
        expr.args.push( arg.expr );
        program = skipSpace( arg.rest );
        // remove comma and process other args
        if( program[ 0 ] == "," )
            program = skipSpace( program.slice( 1 ));
        // no closing parenthesis, throw error
        else if( program[ 0 ] != ")" )
            throw new SyntaxError( "Expected ',' or ')'" );
    }
    // application expression can itself be applied ( func(1)(2) so we must call parseApply again
    return parseApply( expr, program.slice( 1 ));
}

function parse( program ){
    let result = parseExpression( program );
    // if there are any chars left after processing the program then we have an error
    if( skipSpace( result.rest ).length > 0 )
        throw new SyntaxError( "Unexpected text after program" );
    return result.expr;
}

// at this stage the following command will not run the expression but will parse each char and statement then return the objects created. Check everything is as it should be.
console.log( parse( "+( a, 10 )" ));


function evaluate( expr, env ){
    switch( expr.type ){
        case "value" :
        return expr.value;

        case "word" :
            // check if variable name is in the environment and return if so otherwise throw an error
            if( expr.name in env )
                return env[ expr.name ];
            else
                throw new ReferenceError( "Unidentified variable: " + expr.name );

        case "apply" :
            // if the operator type is a word and in specialForms then run the function with expressions arguments
            if( expr.operator.type == "word" &&
                expr.operator.name in specialForms )
                return specialForms[ expr.operator.name ](expr.args, env );
            let op = evaluate( expr.operator, env );
            if( typeof op != "function" )
                throw new TypeError( "Applying a non-function." );
            return op.apply( null, expr.args.map( arg => {
                return evaluate( arg, env );
            }));
    }
}

let specialForms = Object.create( null );

specialForms[ "if" ] = function( args, env ){
    if( args.length != 3 )
        throw new SyntaxError( "Bad number of args to if" );
    if( evaluate( args[ 0 ], env ) != false )
        return evaluate( args[ 1 ], env );
    else 
        return evaluate( args[ 2 ], env );
};

specialForms[ "while" ] = function( args, env ){
    if( args.length != 2 )
        throw new SyntaxError( "Bad number of args to while" );
    while( evaluate( args[ 0 ], env ) != false )
        evaluate( args[ 1 ], env );
    // Since undefined does not exist in this language,
    // we return false for lack of otherwise meaningful
    // result
    return false;
};

specialForms[ "do" ] = function( args, env ){
    let value = false;
    args.forEach( arg => {
        value = evaluate( arg, env );
    });
    return value;
};

specialForms[ "define" ] = function( args, env ){
    if( args.length != 2 || args[ 0 ].type != "word" )
        throw new SyntaxError( "Bad use of define" );
    let value = evaluate( args[ 1 ], env );
    env[ args[ 0 ].name ] = value;
    return value;
};

specialForms[ "set" ] = function( args, env ){
    if( args.length != 2 || args[ 0 ].type != "word" )
        throw new SyntaxError( "Bad use of set" );
    let name  = args[ 0 ].name;
    let value = evaluate( args[ 1 ], env );

    for( let scope = env; scope; scope = Object.getPrototypeOf( scope ) ){
        if( Object.prototype.hasOwnProperty.call (scope, name )) {
            scope[ name ] = value;
            return value;
        }
    }
    throw new ReferenceError( "Undefined variable : " + name );
};

specialForms[ "fun" ] = function( args, env ){
    if( !args.length )
        throw new SyntaxError( "Functions need a body" );
    function name( expr ){
        if( expr.type != "word" )
            throw new SyntaxError( "Arg names must be words" );
        return expr.name;
    }
    let argNames = args.slice( 0, args.length - 1 ).map( name );
    let body = args[ args.length - 1 ];

    return function(){
        if( arguments.length != argNames.length )
            throw new TypeError( "Wrong number of arguments" );
        let localEnv = Object.create( env );
        for( let i = 0; i < arguments.length; i++ )
            localEnv[ argNames[ i ]] = arguments[ i ];
        return evaluate( body, localEnv );
    };
};

let topEnv = Object.create( null );
topEnv[ "true" ] = true;
topEnv[ "false" ] = false;

// second test evaluation
// uses an 'if' statement and boolean expressions held in the global environment
let prog = parse( "if( true, false, true )" );
console.log( evaluate( prog, topEnv ) );

// shorthand for specifying multiple operators rather than writing each one out individually
[ "+", "-", "/", "*", "==", "<", ">" ].forEach( op => {
    topEnv[ op ] = new Function( "a, b", "return a " + op + "b;" );
});

topEnv[ "print" ] = function( value ){
    console.log( value );
    return value;
};

topEnv[ "array" ] = function(){
    return Array.prototype.slice.call( arguments, 0 ); 
};

topEnv[ "length" ] = function( arr ){
    if( !( arr instanceof Array ))
        throw new TypeError( "No array supplied to length" );
    return arr.length;
};

topEnv[ "element" ] = function( arr, n ){
    // check we have an array and a numerical index
    if( !( arr instanceof Array ) && isNaN( n ))
        throw new TypeError( "Incorrect arguments supplied to element" );
    // return the array at index or false since undefined doesn't have any meaning in this language
    return arr[ n ] || false;
};

function run() {
    let env = Object.create( topEnv );
    let program = Array.prototype.slice
    .call( arguments, 0 ).join( "\n" );
    return evaluate( parse( program ), env );
}

run( "do( define( total, 0 ),",
    "   define( count, 1 ),",
    "   while( < (count, 11 ),",
    "       do( define( total, +( total, count )),",
    "           define( count, +( count, 1 )))),",
    "   print( total ))");

run( "do( define( plusOne, fun( a, +( a, 1 ))),",
    "   print( plusOne( 10 )))" );

run( "do( define( pow, fun( base, exp, ",
    "       if( ==( exp, 0 ), ",
    "           1, ",
    "           *( base, pow( base, -( exp, 1 )))))), ",
    "   print( pow( 2, 10 ))) " );

run("do(define(sum, fun(array,",
    "     do(define(i, 0),",
    "        define(sum, 0),",
    "        while(<(i, length(array)),",
    "          do(define(sum, +(sum, element(array, i))),",
    "             define(i, +(i, 1)))),",
    "        sum))),",
    "   print(sum(array(1, 2, 3))))");

run("do(define(x, 4),",
    "   define(setx, fun(val, set(x, val))),",
    "   setx(50),",
    "   print(x))");
// → 50
run("set(quux, true)");
// → Some kind of ReferenceError
