// Note - we can't run this stuff through node so we type it out here just for sanity purposes! Everything written will be yanked and put into a web browser console, en garde!

// A recursive function to scan a HTML tree for text nodes containing a given text

function talksAbout( node, string ){
    if( node.nodeType == document.ELEMENT_NODE ){
        for( let i = 0; i < node.children.length; i++ ){
            if( talksAbout( node.childNodes[ i ], string ))
                return true;
        }
        return false;
    } else if( node.nodeType == document.TEXT_NODE ){
        return node.nodeValue.indexOf( string ) > -1;
    }
}

console.log( talksAbout( document.body, "book" ));

// replace imgs in document with their alt text

function replaceImages(){
    // grab images by tag name
    let images = document.body.getElementsByTagName( "img" );
    // work the list backwards presumably because the list is 'live' and while 'i' increments forward, the list of images decreases each time we replace one with text
    for( let i = images.length - 1; i >= 0; i-- ){
        let image = images[ i ];
        if( image.alt ){
            let text = document.createTextNode( image.alt );
            image.parentNode.replaceChild( text, image );
        }
    }
}

// instead of a 'live' list of nodes one can do this instead
let arrayish = { 0 : "one", 1 : "two", length : 2 };
let real = Array.prototype.slice.call( arrayish, 0 );
real.forEach( elt => {
    console.log( elt );
});

// a simple syntax highlighter for <pre> content

function highlightCode( node, keywords ){
    let text = node.textContent;
    node.txtContent = ""; // Clear node contents

    let match, pos = 0;
    while( match = keywords.exec( text )){
        let before = text.slice( pos, match.index );
        node.appendChild( document.createTextNode( before ));
        let strong = document.createElement( "strong" );
        strong.appendChild( document.createTextNode( match[ 0 ]));
        node.appendChild( strong );
        pos = keywords.lastIndex;
    }
    let after = text.slice( pos );
    node.appendChild( document.creatTextNode( after ));
}

let languages = {
    javascript : /\b(function|return|var)\b/g
};

function highlightAllCode(){
    let pres = document.body.getElementsByTagName( "pre" );
    for( let i = 0; i < pres.length; i ++ ){
        let pre = pres[ i ];
        let lang = pre.getAttribute( "data-language" );
        if( languages.hasOwnProperty( lang ))
            highlightCode( pre, languages[ lang ] );
    }
}

// moving an image around using position, top and left
// picture is centered on page and has a position of relative
let cat = document.querySelector( "img" );
let angle = 0, lastTime = null;
function animate( time ){
    if( lastTime != null )
        angle += ( time - lastTime ) * 0.001;
    lastTime = time;
    cat.style.top = ( Math.sin( angle ) * 20 ) + "px";
    cat.style.left = ( Math.cos( angle ) * 200 ) + "px";
    // recursive call to continue the animation
    requestAnimationFrame( animate );
}
// schedules the animation to run when the browser is ready to repaint the screen
requestAnimationFrame( animate );

// build the same table we previously made in JavaScript but this time in HTML
// MOUNTAINS is an array of objects with name, height and country properties
// name, height and country should be headers <tr><th>Name</th></tr>
// Data should be beneath the correct headers as <td>

function buildTable( data ){
    let table = document.createElement( "table" );
    let tableHeader = document.createElement( "tr" );
    let keys = Object.keys( data[ 0 ] );
    keys.forEach( header => {
        let textNode = document.createTextNode( header );
        let columnHeader = document.createElement( "th" );
        columnHeader.appendChild( textNode );
        tableHeader.appendChild( columnHeader );
    });
    table.appendChild( tableHeader );

    data.forEach( obj => {
        let tableRow = document.createElement( "tr" );
        keys.forEach( header => {
            let textNode = document.createTextNode( obj[ header ] );
            let column = document.createElement( "td" );
            if( !isNaN( obj[ header ] )) column.style.textAlign = "right";
            tableRow.appendChild( column );
        });
        table.appendChild( tableRow );
    });
    return table;
}

// elements by tag name
// let's make out own version!
// Idea! We need to recursively loop through all children elements that themselves might have children and return an array structure full of lovely elements providing they have the same tag names as supplied.
function byTagName( node, tagName ){
    let arr = [];
    let tN = tag.toUpperCase();
    function children( node ){
        for( let i = 0; i < node.children.length; i++ ){
            if( node.children[ i ].tagName = tN )
                arr.push( node.children[ i ] );
            if( node.children[ i ].children.length > 0 )
                children( node.children[ i ] );
        }
        return arr;
    }
    return children( node );
}

// Write a JavaScript enabled tabbed interface

function asTabs(node) {
    // grab children and construct array  
}

asTabs(document.querySelector("#wrapper"));
