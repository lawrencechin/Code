// Utility Functions
const createElem = ( name, props = {}) => {
    const e = document.createElement( name );
    for( p in props ){
        if( p === "class" )
            e.classList.add( props[ p ]);
        else
            e[ p ] = props[ p ];
    }
    return e;
};

const generateId = () => ( Date.now() + Math.random()).toFixed();

// Local Storage Functions
const parseList = ([ ...xs ]) => {
    const objStor = {};
    xs.forEach( elem => {
        if( elem.tagName === "LI" && elem.innerText !== "" )
            objStor[ elem.id ] = elem.innerText;
    });

    return objStor;
};

const addToLocalStorage = ( objStor, key ) => {
    localStorage.setItem( key, JSON.stringify( objStor ));
};

const removeLocalStorage = ( predicate, ...list ) => {
    function _removeLocalStorage( event ){
        const id = event.target.id;
        if( id === "both" )
            localStorage.clear();
        else
            localStorage.removeItem( id );

        list.forEach( xs => {
            [ ...xs.children ].forEach( elem => {
                if( predicate( elem ))
                    xs.removeChild( elem );
            });
        });
    };

    return _removeLocalStorage;
};

const populateList = ( ul, key ) => {
    const data = JSON.parse( localStorage.getItem( key ));

    if( data )
        Object.keys( data ).forEach( item => {
            ul.appendChild( createElem( "li", {
                id : item,
                innerText : data[ item ],
                draggable : true
            }));
        });
};

// Create new list item functions

function addNewListItem( event ){
    const input = event.target;
    const innerText = input.value;

    if( innerText.length < 1 ) return false;

    const parentUl = input.parentElement.parentElement;
    const id = `${ generateId() }_list`;

    parentUl.appendChild( createElem( "li", {
        id,
        innerText,
        draggable : true
    }));

    input.value = "";

    addToLocalStorage( parseList( parentUl.children ), "to_do" );
}

// Drag and Drop Functions

const onDragStart = ( dropzone, style ) => {
    function _onDragStart( event ){
        event.dataTransfer.setData( "text/plain", event.target.id );
        dropzone.classList.add( style );
    }
    return _onDragStart;
};

function onDragOver( event ){
    event.preventDefault();
}

const onDrop = ( list, key ) => {
    function _onDrop( event ){
        const id = event.dataTransfer.getData( "text" );
        const draggableElement = document.getElementById( id );
        const dropzone = event.target.tagName === "UL" 
            ? event.target
            : event.target.parentElement;

        dropzone.appendChild( draggableElement );
        dropzone.classList.remove( "dragOver" );

        event.dataTransfer.clearData();

        addToLocalStorage( parseList( dropzone.children ), "done" );
        addToLocalStorage( parseList( list.children ), key );
    }

    return _onDrop;
};

const onDragEnd = ( list, style ) => {
    function _onDragEnd( event ){
        list.classList.remove( style );
    };

    return _onDragEnd;
};

// Page Init

const input = document.getElementById( "new_list_item" );
const to_do = document.querySelector( ".to_do" );
const done = document.querySelector( ".done" );
const clear_to_do = document.querySelector( "#to_do" );
const clear_done = document.querySelector( "#done" );
const clear_both = document.querySelector( "#both" );

const clearPredicate = id => elem => elem.tagName === "LI" && elem.id !== id;

const keepInputElem = clearPredicate( "input_item" );

input.addEventListener( "change", addNewListItem );
to_do.addEventListener( "dragstart", onDragStart( done, "dragOver" ));
to_do.addEventListener( "dragend", onDragEnd( done, "dragOver" ));
done.addEventListener( "dragover", onDragOver );
done.addEventListener( "drop", onDrop( to_do, "to_do" ));
clear_to_do.addEventListener( "click", removeLocalStorage( keepInputElem, to_do ));
clear_done.addEventListener( "click", removeLocalStorage( keepInputElem, done ));
clear_both.addEventListener( "click", removeLocalStorage( keepInputElem, to_do, done ));

populateList( to_do, "to_do" );
populateList( done, "done" );
