const createElem = ( name, props = {}) => {
    const e = document.createElement( name );
    for( p in props ){
        if( p === "class" )
            e.classList.add( props[ p ]);
        else
            e[ p ] = props[ p ];
    }
    return e;
};

function onDragStart( event ){
    event.dataTransfer.setData( "text/plain", event.target.id );
    event.currentTarget.style.backgroundColor = "#CCDD88";
}

function onDragOver( event ){
    event.preventDefault();
}

function onDrop( event ){
    const id = event.dataTransfer.getData( "text" );
    const draggableElement = document.getElementById( id );
    const dropzone = event.target;

    dropzone.appendChild( draggableElement );

    event.dataTransfer.clearData();
}


const parentDiv = createElem( "div", { class : "parent" });
const draggableSpan = createElem( "span", {
    id : "draggableSpan",
    draggable : true,
    innerText : "draggable"
});
const dropSpan = createElem( "span", {
    innerText : "dropzone"
});

dropSpan.addEventListener( "dragover", onDragOver );
dropSpan.addEventListener( "drop", onDrop );
draggableSpan.addEventListener( "dragstart", onDragStart );
parentDiv.appendChild( draggableSpan );
parentDiv.appendChild( dropSpan );
document.body.appendChild( parentDiv );
