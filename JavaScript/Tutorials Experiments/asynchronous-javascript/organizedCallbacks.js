const request = require( "request" );

const calculateAverageScore = ( reviews, movie, director, toBeChecked, movies ) => {
    toBeChecked.count -= 1;
    let aggregatedScore = 0;
    let count = 0;
    
    reviews.forEach( review => {
        aggregatedScore += review.rating;
        count += 1;
    });

    movie.averageRating = aggregatedScore / count;

    if( toBeChecked.count === 0 ){
        movies.sort(( m1, m2 ) => m2.averageRating - m1.averageRating );
        console.log( `The best move by ${ director } is… ${ movies[ 0 ].title }! The movie with the lowest score is ${ movies[ movies.length - 1 ].title }…` );
    }
};

const getReviews = ( movies, director ) => {
    let toBeChecked = { count : movies.length };
    movies.forEach( movie => {
        request(
            `https://maciejtreder.github.io/asynchronous-javascript/movies/${ movie.id }/reviews`,
            { json : true },
            ( err, res, reviews ) => calculateAverageScore( 
                reviews,
                movie,
                director,
                toBeChecked,
                movies
            )
        );
    });
};

const findDirector = ( directors, name ) => {
    const directorId = directors.find( 
        director => director.name === name ).id;
    request( 
        `https://maciejtreder.github.io/asynchronous-javascript/directors/${ directorId }/movies`,
        { json : true },
        ( err, res, movies ) => getReviews(
            movies,
            name
        )
    );
};

const findBestAndWorstFilms = director_name => {
    request(
        `https://maciejtreder.github.io/asynchronous-javascript/directors`,
        { json : true },
        ( err, res, directors ) => findDirector(
            directors,
            director_name
        )
    );
};

findBestAndWorstFilms( "Quentin Tarantino" );
findBestAndWorstFilms( "Stanley Kubrick" );
findBestAndWorstFilms( "James Cameron" );
findBestAndWorstFilms( "Wes Anderson" );
