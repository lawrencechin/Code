const fetch = require( "node-fetch" );

const lat = 51.75;
const lon = -0.33;

const sevenTimerTemp = 
    fetch( `http://www.7timer.info/bin/api.pl?lon=${ lon }&lat=${ lat }&product=astro&output=json` )
    .then( res => res.json())
    .then( res => res.dataseries[ 0 ].temp2m );

const fccWeatherTemp = 
    fetch( `https://fcc-weather-api.glitch.me/api/current?lat=${ lat }&lon=${ lon }`)
    .then( res => res.json())
    .then( res => res.main.temp );

Promise.all([
    sevenTimerTemp.catch(() => -100 ), 
    fccWeatherTemp.catch(() => -100 ) 
    ])
    .then( res => {
        let sum = 0;
        let count = 0;
        res.forEach( r => {
            sum += r;
            count += 1;
        });

        return sum / count;
    })
    .then( average => console.log( `Average of reported temperatures is ${ average } C°` ));
