const somethingWasSuccessful = -10;

const someAsyncFunc = () => {
    return new Promise(( resolve, reject ) => {
        if( somethingWasSuccessful > 0 )
            resolve( somethingWasSuccessful );
        else
            reject( "rejected" );
    });
};

someAsyncFunc()
    .then( successVal => { 
        console.log( `The resolved value is ${ successVal }` );
    })
    .catch( rejectMsg => {
        console.log( `The reject message: ${ rejectMsg }` );
    });
