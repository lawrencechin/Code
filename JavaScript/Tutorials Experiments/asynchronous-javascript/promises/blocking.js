const wait = ( ms ) => {
    let start = Date.now(),
        now = start;

    while( now - start < ms )
        now = Date.now();
};

const promiseOne = new Promise( resolve => {
    console.log( "Inside the promiseOne executor code" );
    setTimeout(() => resolve( "Value from the promiseOne resolve method" ), 2000 );
});

console.log( "After the promiseOne constructor" );
console.log( promiseOne );
promiseOne.then( resolveValue => console.log( resolveValue ));
console.log( "After promiseOne.then is called and fulfilled" );
console.log( promiseOne );
