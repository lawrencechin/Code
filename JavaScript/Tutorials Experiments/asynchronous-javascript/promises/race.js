const fetch = require( "node-fetch" );

const lat = 51.75;
const lon = -0.33;

const sevenTimerTemp = 
    fetch( `http://www.7timer.info/bin/api.pl?lon=${ lon }&lat=${ lat }&product=astro&output=json` )
    .then( res => res.json())
    .then( res => res.dataseries[ 0 ].temp2m );

const fccWeatherTemp = 
    fetch( `https://fcc-weather-api.glitch.me/api/current?lat=${ lat }&lon=${ lon }`)
    .then( res => res.json())
    .then( res => res.main.temp );

const getWatchdog = timeout => {
    return new Promise(( res, rej ) => setTimeout(() => rej( "Timeout!" ), timeout * 1000 ));
};

Promise.race([ 
    sevenTimerTemp.catch(() => new Promise(( resolve ) => {/* noop */})),
    fccWeatherTemp.catch(() => new Promie(( resolve ) => {/* noop */})),
    getWatchdog( 5 )
])
    .then( temp => console.log( `Temperature reported by the fastest station is: ${ temp } C°` ));
