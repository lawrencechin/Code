const fetch = require( "node-fetch" );

const lat = 37;
const lon = 122;

fetch( `http://www.7timer.info/bin/api.pl?lon=${ lon }&lat=${ lat }&product=astro&output=json` )
    .then( res => res.json())
    .then( res => res.dataseries[ 0 ].temp2m )
    .then( temp => console.log( `The temperature reported by the 7timer is: ${ temp } C°` ));
