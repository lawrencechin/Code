const ora = require( "ora" );
const spinner = ora( "Loading promise two" ).start();
const promiseTwo = new Promise(( res, rej ) => {
    setTimeout(() => {
        if( Math.random() >= 0.5 )
            res( "Promise resolved" );
        else
            rej( "Promise rejected" );
    }, 2000 );
});

promiseTwo
    .finally(() => spinner.stop())
    .then( console.log )
    .catch( console.error );

promiseTwo.catch(() => { /* noop */ }).finally(
    () => console.log( "This will always be executed" )
);

