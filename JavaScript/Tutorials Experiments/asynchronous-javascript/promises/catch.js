class Glass {
    constructor( glassId, level ){
        this.GlassId = glassId;
        this.fillLevel = level;
    }
}

const fillGlass = ( pourtime ) => {
    return new Promise(( res, rej ) => {
        setTimeout(() => {
            let level = Math.random();
            if( level >= 0.7 )
                res( Math.round( level * 100 ));
            else
                rej( new Error( "Missed the glass!" ));
        }, pourtime );
    });
};

const serveGlass = ( curGlass, result ) => {
    console.log( `That's a good pour! Glass ${ curGlass.GlassId } is ${ result }% full. Drink up.` );
};

const returnGlass = ( curGlass, errorMsg ) => {
    console.log( `That's a bad pour. Glass ${ curGlass.GlassId } is ${ errorMsg }. Try again.` );
};

const pour = ( ordered, pourtime ) => {
    let attempted = 1;
    while( attempted <= ordered ){
        let curGlass = new Glass( attempted );
        fillGlass( pourtime )
            .then( res => serveGlass( curGlass, res ))
            .catch( errorMsg => returnGlass( curGlass, errorMsg ));
        attempted += 1;
    }
};

pour( 10, 500 );

