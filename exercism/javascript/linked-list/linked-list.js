const Node = function( value ){
    this.value = value;
    this.prevNode = null;
    this.nextNode = null;
};

const LinkedList = function(){
    this.node = null;
};

LinkedList.prototype.pop = function(){
    let ln = traverseList( this.node, node => node.nextNode == null );
    if( ln ){
        const value = ln.value;
        if( ln.prevNode ) ln.prevNode.nextNode = null;
        else this.node = null;
        return value;
    }
};

LinkedList.prototype.push = function( value ){
    const newNode = new Node( value );
    const ln = traverseList( this.node, node => node.nextNode == null );
    if( !ln ){
        this.node = newNode;
        return this.node;
    }
    newNode.prevNode = ln;
    ln.nextNode = newNode;
};

LinkedList.prototype.unshift = function( value ){
    const newNode = new Node( value );
    if( this.node != null ){ 
        this.node.prevNode = newNode;
        newNode.nextNode = this.node;
    }
    this.node = newNode;
};

LinkedList.prototype.shift = function(){
    if( this.node ){
        const value = this.node.value;
        if( this.node.nextNode )
            this.node = this.node.nextNode;
        else 
            this.node = null;
        return value;
    }
};

LinkedList.prototype.count = function(){
    let count = 0;
    let node = this.node;
    while( node != null ){
        count += 1;
        node = node.nextNode;
    }
    return count;
};

LinkedList.prototype.delete = function( value ){
    const node = traverseList( this.node, node => node.value == value );
    if( node ){
        if( !node.prevNode ){
            this.node = null; 
        } else { 
            const prevNode = node.prevNode;
            prevNode.nextNode = node.nextNode;
            node.nextNode.prevNode = prevNode;
        }
    }
};

function traverseList( startNode, predicateFunction ){
    if( startNode == null ) return startNode;
    if( predicateFunction( startNode )) return startNode;
    return traverseList( startNode.nextNode, predicateFunction );
}

module.exports = LinkedList;
