const Squares = function( limit ){
    this.sequence = Array.from( new Array( limit )).map(( _, i ) => {
        return i + 1;
    });

    this.sumOfSquares = sumOfSquares( this.sequence );
    this.squareOfSums = squareOfSums( this.sequence );
    this.difference = this.squareOfSums - this.sumOfSquares;
};

function sumOfSquares( sequence ){
    return sequence.reduce(( total, num ) => {
        return total += num * num;
    }, 0 );
};

function squareOfSums( sequence ){
    const sum = sequence.reduce(( total, num ) => {
        return total += num;
    }, 0 );
    return sum * sum;
};

module.exports = Squares;
