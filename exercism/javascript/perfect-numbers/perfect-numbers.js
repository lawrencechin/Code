const PerfectNumbers = function(){};

PerfectNumbers.prototype.classify = function( number ){
    if( sanitizeNumber( number )) return sanitizeNumber( number );

    const aliquot = factors( number, 2, [ 1 ]).reduce(( total, num ) => {
        return total += num;
    }, 0 );

    if( aliquot == number ) return "perfect";
    if( aliquot > number ) return "abundant";
    if( aliquot < number ) return "deficient";
};

function sanitizeNumber( number ){
    if( number < 1 ) return "Classification is only possible for natural numbers.";
    if( number == 1 ) return "deficient";
    return false;
}

function factors( number, divisor, resultArray ){
    if( divisor > Math.sqrt( number )) return resultArray;
    if( number % divisor == 0 ){
        resultArray.push( divisor );
        if( number / divisor != divisor ) resultArray.push( number / divisor );
    }
    return factors( number, divisor + 1, resultArray );
}

module.exports = PerfectNumbers;
