// slow when you get to around the millionth prime
// could do with a spot of optimisation but the spec 
// doesn't test for larger numbers

const Prime = function(){};

Prime.prototype.nth = function( n ){
    if( n < 1 ) throw new Error( "Prime is not possible" );
    if( n == 1 ) return 2;
    const length = n * ( Math.log( n ) + Math.log( Math.log( n ))) + 6;
    return generateSieve( Math.ceil( length ))[ n - 1 ];
};

function generateSieve( length ){
    const primes = [ 2 ];
    const sieve = Array.from( new Array( length )).fill( true );
    for( let i = 2; i < length; i += 1 ){
        if( !sieve[ i ] ) continue;
        for( let j = i * i; j < length; j += i ){
            sieve[ j ] = false;
        }
    }
    for( let i = 3; i < length; i += 2 ){
        if( sieve[ i ] ) primes.push( i );
    }
    return primes;
}

module.exports = new Prime();
