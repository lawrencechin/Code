export default class List {
    constructor( xs ){
        this.values = Array.isArray( xs ) 
            ? xs
            : [];
    }

    // what's the difference between append and concat?
    append( ls ){
        this.values = [ ...this.values, ...ls.values ];
        return this;
    }

    concat( ls ){
        return this.append( ls );
    }

    _loopValues( fn, acc, reverse = false ){
        const count = this.values.length;
        const loopValues = i => {
            if( i === count ) return;
            if( reverse )
                fn( this.values[ count - 1 - i ], acc );
            else 
                fn( this.values[ i ], acc );
            return loopValues( i += 1 );
        };
        loopValues( 0 );
        return acc;
    }

    filter( predicate ){
        this.values = this._loopValues(( v, arr ) => {
            if( predicate( v )) arr.push( v );
        }, []);
        return this;
    }

    length(){
        return this.values.length;
    }

    map( fn ){
        this.values = this._loopValues(( v, arr ) => arr.push( fn( v )), []);
        return this;
    }

    foldl( reducer, acc, reverse = false ){
        let total = acc;
        this._loopValues( v  => {
            total = reducer( total, v );
        }, total, reverse );
        return total;
    }  

    foldr( reducer, acc ){
        return this.foldl( reducer, acc, true );
    }

    reverse(){
        const $reverse = ( arr, newArr ) => {
            if( arr.length < 1 ) return newArr;
            newArr.push( arr.pop());
            return $reverse( arr, newArr );
        };
        this.values = $reverse( this.values, []);
        return this;
    }
}
