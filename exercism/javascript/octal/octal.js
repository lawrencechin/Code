const Octal = function( decimal ){
    this.toDecimal = function(){
        if( validStringNumber( decimal ))
            return convertDecimalToOctal( decimal, 0, 0 );

        return 0;
    };
};

function validStringNumber( string ){
    // number string with spaces isn't valid, consider trimming
    return typeof string == "string"
        && string.length > 0
        && string.match( /[^0-7]/ ) == null;
}

function convertDecimalToOctal( string, result, increment ){
    if( string.length == 0 ) return result;
    return convertDecimalToOctal( 
        string.slice( 0, string.length - 1 ),
        result + ( string.slice( string.length - 1 )
            * Math.pow( 8, increment )),
        increment + 1
    );
}

module.exports = Octal;
