const Triangle = function( x, y, z ){
    this.dimensions = [ x, y, z ];
};

Triangle.prototype.kind = function(){
    const sortedDimensions = this.dimensions.sort(( a, b ) => a > b ? 1 : -1 );
    if( !this.validTriangle( sortedDimensions ))
        throw new Error( "Not a valid triangle" );

    if( this.equilateral( sortedDimensions, sortedDimensions[ 0 ] ))
        return "equilateral";
    else if( this.isosceles( sortedDimensions ))
        return "isosceles";
    else if( !this.isosceles( sortedDimensions ))
        return "scalene";

};

Triangle.prototype.validTriangle = function( arr ){
    return arr[ 0 ] + arr[ 1 ] >= arr[ 2 ] && !this.equilateral( arr, 0 );
};

Triangle.prototype.equilateral = function( arr, value ){
    return arr.every( side => side == value );
};

Triangle.prototype.isosceles = function( arr ){
    return arr[ 0 ] == arr[ 1 ] || arr[ 1 ] == arr[ 2 ];
};

module.exports = Triangle;

