const CryptoSquare = function( string ){
    const originalStr = string; // private variable
    
    return {
        normalizePlaintext(){
            return originalStr.toLowerCase().replace( /[^a-z0-9]/g, "" );
        },
        size(){
            return Math.ceil( Math.sqrt( 
                this.normalizePlaintext().length 
            ));
        },
        plaintextSegments(){
            const regex = new RegExp( "(\\w{1," + this.size() + "})", "g" );
            return this.normalizePlaintext().match( regex );
        },
        ciphertext(){
            return this.plaintextSegments()
                .reduce(( resultArr, segment ) => {
                    segment.split( "" ).forEach(( char, i ) => {
                            if( resultArr[ i ]) 
                                resultArr[ i ].push( char );
                            else resultArr[ i ] = [ char ];
                    });
                    return resultArr;
                }, [] )
                .reduce(( str, arr ) => {
                    return str += arr.join( "" );
                }, "" );
        }
    };
};

module.exports = CryptoSquare;
