const Luhn = function( number ){
    this.valid = processNumber( number );
};

function processNumber( number ){
    if( !validNumber( number )) return false;

    return number.replace( / /g, "" )
        .split( "" )
        .reverse()
        .map(( num, i ) => {
            if( i % 2 != 0 ){
                const double = num * 2;
                return double > 9 ? double - 9 : double;
            }
            return num;
        })
        .reduce(( sum, num ) => sum += +num, 0 ) % 10 == 0;
}

function validNumber( number ){
    return typeof number == "string"
        && number.length > 1
        && number.match( /[^0-9 ]/ ) == null;
}

module.exports = Luhn;
