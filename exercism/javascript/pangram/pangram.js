var Pangram = function( sentence ){
    this.sentence = sentence;
};

Pangram.prototype.isPangram = function(){
    var alpha = this.generateAlphaArr( "a", "z" );
    var sent = this.sentence.toLowerCase().split( "" );

    return alpha.every( function( char ){
        return sent.indexOf( char ) > -1;
    });
};

Pangram.prototype.generateAlphaArr = function( startChar, endChar){
    var arr = [], 
    length = endChar.charCodeAt() - startChar.charCodeAt(),
    start = startChar.charCodeAt();
    for( var i = 0; i <= length; i++ ){
        arr.push( String.fromCharCode( start ));
        start += 1;
    }
    return arr;
};

module.exports = Pangram;
