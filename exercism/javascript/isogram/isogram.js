var Isogram = function( word ){
    this.word = word;
};

Isogram.prototype.isIsogram = function(){
    return !this.word
        .toLowerCase()
        .replace( /[^a-z\u00E0-\u00FF]/g, "")
        .split( "" )
        .some( function( char, pos, arr ){
            return arr.indexOf( char, pos + 1 ) > pos;
        });
};
module.exports = Isogram;
