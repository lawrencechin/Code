var Words = function(){};

Words.prototype.count = function( str ){
    var wordCount = {};
    var string = str.replace( /^(\s+)/, "" ).replace( /(\s+)$/, "" );
    // ordinarily I would remove the prototype of wordCount
    // using wordCount.prototype = Object.create( null )
    // but doing this fails the jasmine tests
    string.split( /\s+/ ).forEach( function( word ){
        var wd = word.toLowerCase();
        if( typeof wordCount[ wd ] != "number" )
            wordCount[ wd ] = 1;
        else
            wordCount[ wd ] = wordCount[ wd ] + 1;
    });

    return wordCount;
};

module.exports = Words;
