const PigLatin = function(){};

PigLatin.prototype.translate = function( phrase ){
    return phrase.split( " " ).map( word => {
        let index = word.search( /[aeiou]/ );
        // seems a tad specific, would likely break with further tests
        if( word[ index - 1 ] == "q" ) index++;
        return word.slice( index ) + word.slice( 0, index ) + "ay";
    }).join( " " );
};

module.exports = new PigLatin();
