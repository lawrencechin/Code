export default class NucleotideCounts {
    static parse( str ){
        if( str.length < 1 ) return "0 0 0 0";
        if( /[^ACGT]/.test( str )) 
            throw new Error( "Invalid nucleotide in strand" );

        const count = str.split( "" ).reduce(( nC, n ) => {
            nC[ n ] = nC[ n ] + 1;
            return nC;
        },
            {
                "A" : 0,
                "C" : 0,
                "G" : 0,
                "T" : 0
            }   
        );

        return `${ count[ "A" ]} ${ count[ "C" ]} ${ count[ "G" ]} ${ count[ "T" ]}`; 
    }
}
