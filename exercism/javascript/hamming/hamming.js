var Hamming = function(){
};

Hamming.prototype.compute = function( strandA, strandB ){
    if( strandA.length != strandB.length )
        throw new Error( "DNA strands must be of equal length." );
    var distance = 0; // considered setting this as a property of the constructor but would need to be reset each time compute is called
    for( var i = 0; i < strandA.length; i++ ){
        distance = strandA[ i ] != strandB[ i ] ? distance + 1 : distance;
    }
    return distance;
};

module.exports = Hamming;
