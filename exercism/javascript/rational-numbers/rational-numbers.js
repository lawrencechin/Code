const gcd = ( a, b ) => {
    const lesser = Math.min( a, b );
    const greater = Math.max( a, b );

    const $gcd = ( x, y, divisor ) => {
        if( divisor === 1 ) return divisor;
        if( x % divisor === 0 && y % divisor === 0 ) return divisor;

        return $gcd( x, y, divisor - 1 );
    };

    return $gcd( greater, lesser, lesser );
};

const reduce = ( a, b ) => {
    if( a === 0 ) return new Rational( 0, 1 );
    const _gcd = gcd( Math.abs( a ), Math.abs( b ));
    let x = a / _gcd;
    let y = b / _gcd;

    return y < 0 
        ? new Rational( -x, -y )
        : new Rational( x, y );
};

function Rational( a, b ){
    if( b === 0 ) throw new Error( "Not a rational number" );
    this.a = a;
    this.b = b;
};

Rational.prototype.add = function( R ){
    return reduce( this.a * R.b + R.a * this.b, this.b * R.b );
};
Rational.prototype.sub = function( R ){
    return reduce( this.a * R.b - R.a * this.b , this.b * R.b );
};
Rational.prototype.mul = function( R ){
    return reduce( this.a * R.a, this.b * R.b );
};
Rational.prototype.div = function( R ){
    return reduce( this.a * R.b, R.a * this.b );
};
Rational.prototype.abs = function(){
    return new Rational( Math.abs( this.a ), Math.abs( this.b ));
};
Rational.prototype.exprational = function( num ){
    return num > 0
        ? reduce( this.a ** num, this.b ** num )
        : reduce( Math.abs( this.a ) ** num, Math.abs( this.b ) ** num );
};
Rational.prototype.expreal = function( num ){
    // cheated here, didn't understand it. Must look it up.
    return Math.pow( 
        10.0, 
        Math.log10( Math.pow( num, this.a)) / this.b );
};
Rational.prototype.reduce = function(){
    return reduce( this.a, this.b );
};

export { Rational };
