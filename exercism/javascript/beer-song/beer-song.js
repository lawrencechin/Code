let BeerSong = function(){};

BeerSong.prototype.lyrics = {
    firstPart : "{{number}} bottle{{plural}} of beer on the wall, {{number}} bottle{{plural}} of beer.",
    secondPart : "{{sentence}}, {{--number}} bottle{{plural_second}} of beer on the wall.",
    secondPartSentence : "Take {{one}} down and pass it around",
    altSecondPartSentence : "Go to the store and buy some more"
};

BeerSong.prototype.sing = function( start, end ){
    let string = "";
    const fin = end ? end : 0;
    for( let i = start; i >= fin; i-- )
        string += i == start ? this.verse( i ) : "\n" + this.verse( i );
    return string;
};

BeerSong.prototype.verse = function( lineNumber ){
    const replace = this.replacements( lineNumber );
    return this.parseTemplate( this.lyrics.firstPart, replace ) + "\n" + this.parseTemplate( this.lyrics.secondPart, replace ) + "\n";
};

BeerSong.prototype.parseTemplate = function( str, replacements ){
    return str.replace( /{{(\S+)}}/g, ( _replace, term, pos ) => {
        // horrible hack to convert "No more" into "no more"
        return typeof replacements[ term ] == "string" && term == "number" && pos > 0 ? replacements[ term ].toLowerCase() : replacements[ term ];
    });
};

BeerSong.prototype.replacements = function( lineNumber ){
    let obj = {
        number : lineNumber,
        plural : "s",
        plural_second : "s",
        sentence : this.lyrics.secondPartSentence,
        "--number" : --lineNumber,
        one : "one"
    };
    
    if( obj[ "--number" ] == 1 ){
        obj.plural_second = "";
    } else if( obj.number == 1 ){
        obj.plural = "";
        obj.plural_second = "s";
        obj.one = "it";
        obj[ "--number" ] = "no more";
    } else if( obj.number == 0 ){
        obj.number = "No more";
        obj.sentence = this.lyrics.altSecondPartSentence;
        obj[ "--number" ] = 99;
    }
    obj.sentence = this.parseTemplate( obj.sentence, obj );
    return obj;
};


module.exports = BeerSong;
