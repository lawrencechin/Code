const SpaceAge = function( seconds ){
    this.seconds = seconds;
};

function orbitalPeriods(){
    if( !SpaceAge.prototype.orbitalPeriod ){
        const earth = 31557600;
        let periods = {};
        periods.Earth = earth;
        periods.Mercury = earth * 0.2408467;
        periods.Venus = earth * 0.61519726;
        periods.Mars = earth * 1.8808158;
        periods.Jupiter = earth * 11.862615;
        periods.Saturn = earth * 29.447498;
        periods.Uranus = earth * 84.016846;
        periods.Neptune = earth * 164.79132;
    
        SpaceAge.prototype.orbitalPeriod = periods;

        Object.keys( periods ).forEach( key => {
            const funcName = "on" + key;
            if( !SpaceAge.prototype[ funcName ] ){
                SpaceAge.prototype[ funcName ] = function(){
                    return this.onPlanet( key );
                };
            }
        });
    }
};

orbitalPeriods();

SpaceAge.prototype.onPlanet = function( planet ){
    return +( this.seconds / this.orbitalPeriod[ planet ]).toFixed( 2 );
};

module.exports = SpaceAge;
