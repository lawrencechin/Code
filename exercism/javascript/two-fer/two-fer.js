const $twofer = name => `One for ${ name }, one for me.`;
const nameCheck = name => name ? name : "you";

export default function twofer( name ){
    return $twofer( nameCheck( name ));
};
