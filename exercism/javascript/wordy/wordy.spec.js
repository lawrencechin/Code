var WordProblem   = require('./wordy').WordProblem;
var ArgumentError = require('./wordy').ArgumentError;

describe('Word Problem', function() {

  it('add 1', function() {
    var question = 'What is 1 plus 1?';
    expect(new WordProblem(question).answer()).toEqual(2);
  });

  it('add 2', function() {
    var question = 'What is 53 plus 2?';
    expect(new WordProblem(question).answer()).toEqual(55);
  });

  it('add negative numbers', function() {
    var question = 'What is -1 plus -10?';
    expect(new WordProblem(question).answer()).toEqual(-11);
  });

  it('add more digits', function() {
    var question = 'What is 123 plus 45678?';
    expect(new WordProblem(question).answer()).toEqual(45801);
  });

  it('subtract', function() {
    var question = 'What is 4 minus -12?';
    expect(new WordProblem(question).answer()).toEqual(16);
  });

  it('multiply', function() {
    var question = 'What is -3 multiplied by 25?';
    expect(new WordProblem(question).answer()).toEqual(-75);
  });

  it('divide', function() {
    var question = 'What is 33 divided by -3?';
    expect(new WordProblem(question).answer()).toEqual(-11);
  });

  it('add twice', function() {
    var question = 'What is 1 plus 1 plus 1?';
    expect(new WordProblem(question).answer()).toEqual(3);
  });

  it('add then subtract', function() {
    var question = 'What is 1 plus 5 minus -2?';
    expect(new WordProblem(question).answer()).toEqual(8);
  });

  it('subtract twice', function() {
    var question = 'What is 20 minus 4 minus 13?';
    expect(new WordProblem(question).answer()).toEqual(3);
  });

  it('subtract then add', function() {
    var question = 'What is 17 minus 6 plus 3?';
    expect(new WordProblem(question).answer()).toEqual(14);
  });

  it('multiply twice', function() {
    var question = 'What is 2 multiplied by -2 multiplied by 3?';
    expect(new WordProblem(question).answer()).toEqual(-12);
  });

  it('add then multiply', function() {
    var question = 'What is -3 plus 7 multiplied by -2?';
    expect(new WordProblem(question).answer()).toEqual(-8);
  });

  it('divide twice', function() {
    var question = 'What is -12 divided by 2 divided by -3?';
    expect(new WordProblem(question).answer()).toEqual(2);
  });

    // extended tests for squares and cubes and exponents
    it( "Exponents", function(){
        const question = "What is 5 raised to 5?";
        expect( new WordProblem( question ).answer()).toEqual( 3125 );
    });

    it( "Square a given number", function(){
        const question = "What is 5 squared?";
        expect( new WordProblem( question ).answer()).toEqual( 25 );
    });

    it( "Cube a given number", function(){
        const question = "What is 5 cubed?";
        expect( new WordProblem( question ).answer()).toEqual( 125 );
    });

    it( "Chained squares", function(){
        const question = "What is 5 squared plus 5 squared minus 5?";
        expect( new WordProblem( question ).answer()).toEqual( 45 );
    });

    it( "A sum to a power", function(){
        const question = "What is 5 plus 5 divided by 2 raised by 5?";
        expect( new WordProblem( question ).answer()).toEqual( 3125 );
    });

    // not too advanced!
  xit('too advanced', function() {
    var question = 'What is 53 cubed?';
    var problem  = new WordProblem(question);

    expect(problem.answer.bind(problem)).toThrow(new ArgumentError());
  });

  it('irrelevant', function() {
    var question = 'Who is the president of the United States?';
    var problem  = new WordProblem(question);

    expect(problem.answer.bind(problem)).toThrow(new ArgumentError());
  });

});
