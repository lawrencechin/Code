const ArgumentError = function( message ){
    this.message = message || "Invalid expression given";
    this.stack = ( new Error()).stack;
};

ArgumentError.prototype = Object.create( Error.prototype );
ArgumentError.prototype.name = "ArgumentError";

const Operators = function(){};

const WordProblem = function( question ){
    this.question = parseQuestion( question );
};

function parseQuestion( question ){
    const operators = Object.keys( Operators );
    return question.match( /(-?\d+|\w+)/g ).filter( item => {
        return !isNaN( item ) || operators.indexOf( item ) > -1;
    });
}


Operators.plus = function( a, b ){
    return a + b;
};

Operators.minus = function( a, b ){
    return a - b;
};

Operators.multiplied = function( a, b ){
    return a * b;
};

Operators.divided = function( a, b ){
    return a / b;
};

Operators.cubed = function( a ){
    return a * a * a;
};

Operators.squared = function( a ){
    return a * a;
};

Operators.raised = function( a, power ){
    function exponential( base, power, total ){
        if( power == 0 ) return total;
        return exponential( base, power - 1, total * base );
    }
    return exponential( a, power, 1 );
};

WordProblem.prototype.answer = function(){
    if( !verifyQuestion( this.question ))
        throw new ArgumentError();

    let arr = this.question;
    if( arr.indexOf( "squared" ) > -1 )
        arr = singleNumberOperations( arr, "squared" );
    if( arr.indexOf( "cubed" ) > -1 )
        arr = singleNumberOperations( arr, "cubed" );
        
    return arr.reduce(( result, item, i ) => {
        if( i == 0 ) return { total : +item, operator : null };
        if( isNaN( item )) return { total : result.total, operator : item };

        return { 
            total : Operators[ result.operator ]( result.total, +item ),
            operator : result.operator
        };
    }, {}).total;
};

function verifyQuestion( questionArray ){

    const num = questionArray.filter( item => !isNaN( item )).length;
    const str = questionArray.filter( item => isNaN( item )).length;

    if( num + str < 2 ) return false;
    if( str < 1 || num < 1 ) return false;
    if( str != num
        && str + 1 != num
        && str - 1 != num ) return false;

    return true;
}

function singleNumberOperations( questionArr, operator ){
    while( questionArr.indexOf( operator ) > -1 ){
        const pos = questionArr.indexOf( operator );
        questionArr[ pos - 1 ] = Operators[ operator ]( questionArr[ pos - 1 ]);
        questionArr.splice( pos, 1 );
    }
    return questionArr;
}

module.exports = {
    WordProblem, ArgumentError
};
