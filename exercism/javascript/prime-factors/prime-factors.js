const PrimeFactors = function(){};
PrimeFactors.prototype.for = function( num ){
    const factors = [];
    for( let i = 2; i <= num / i; ){
        while( num % i == 0 ){
            factors.push( i );
            num /= i;
        }
        i += i == 2 ? 1 : 2; // only test odd numbers after 2
    }
    if( num > 1 ) factors.push( num );
    return factors;
};

module.exports = new PrimeFactors();
