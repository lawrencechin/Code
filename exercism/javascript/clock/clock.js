const Clock = Object.create( null );

Clock.at = function(){
    return function( hour, minutes ){
        let time = { h : hour, m : minutes };

        time.equals = function( atObj ){
            return this.toString() == atObj.toString();
        };

        time.plus = function( mins ){
            this.m = this.m + mins;
            return this;
        };

        time.minus = function( mins ){
            this.m = this.m - mins;
            return this;
        };

        time.toString = function(){
            const stringTime = generate( this.h, this.m );
            return padNumber( stringTime[ 0 ])
                + ":"
                + padNumber( stringTime[ 1 ]);
        };
        
        return time;
    };
}();

function generate( hours, minutes ){
    const genTime = minutesToHours( minutes );
    const h = negativeTime(( genTime.hrs + hours ) % 24, true );
    const m = negativeTime( genTime.mins, false );
    return [ h, m ];
}

function minutesToHours( minutes = 0 ){
    const time = { hrs : 0, mins : minutes };
    if( minutes >= 60 || minutes <= -60 ){
        time.hrs = Math.floor( minutes / 60 );
        time.mins = minutes - ( 60 * time.hrs );
    } else if( minutes < 0 && minutes > -60 ){
        time.hrs = -1;
        time.mins = minutes;
    }

    return time;
}

function negativeTime( time, hour ){
    if( time < 0 )
        return hour ? 24 + time : 60 + time;
    return time;
}

function padNumber( number ){
    if( number < 10 ) return "0" + number;
    return number;
}

module.exports = Clock;
