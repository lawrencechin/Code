const Triplet = function( a, b, c ){
    this.a = a;
    this.b = b;
    this.c = c;
};

Triplet.prototype.sum = function(){
    return this.a + this.b + this.c;
};

Triplet.prototype.product = function(){
    return this.a * this.b * this.c;
};

Triplet.prototype.isPythagorean = function(){
    return this.a * this.a 
        + this.b * this.b 
        == this.c * this.c && this.c % 1 == 0;
};

Triplet.where = function( options ){
    options = options || {};
    options.minFactor = options.minFactor || 2;
    options.maxFactor = options.maxFactor || 10;
    options.sum = options.sum || null;
    const resultsArray = [];
    const factors = [];

    for( let i = options.minFactor; i < options.maxFactor; i++ ){
        for( let j = options.minFactor + 1; j < options.maxFactor; j++ ){
            if( i == j ) continue;
            const c = Math.sqrt( i * i + j * j );
            if( factors.indexOf( c ) > -1 ) continue;
            const triplet = new Triplet( i, j, c );
            if( isTriplet( triplet, options )){
                factors.push( triplet.c );
                resultsArray.push( triplet );
            }
        }
    }
            
    return resultsArray;
};

function isTriplet( triplet, options ){
    if( !triplet.isPythagorean()) return false;
    if( !( triplet.c <= options.maxFactor )) return false;
    if( options.sum )
        if( !( triplet.sum() == options.sum )) return false;
    return true;
}

module.exports = Triplet;
