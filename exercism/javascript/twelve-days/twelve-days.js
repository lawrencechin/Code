const verses = {
    1 : [ "first", "a Partridge in a Pear Tree.\n" ],
    2 : [ "second", "two Turtle Doves, and " ],
    3 : [ "third", "three French Hens, " ],
    4 : [ "fourth", "four Calling Birds, " ],
    5 : [ "fifth", "five Gold Rings, " ],
    6 : [ "sixth", "six Geese-a-Laying, " ],
    7 : [ "seventh", "seven Swans-a-Swimming, " ],
    8 : [ "eighth", "eight Maids-a-Milking, " ],
    9 : [ "ninth", "nine Ladies Dancing, " ],
    10 : [ "tenth", "ten Lords-a-Leaping, " ],
    11 : [ "eleventh", "eleven Pipers Piping, " ],
    12 : [ "twelfth", "twelve Drummers Drumming, " ]
};

const constructVerse = ( num ) => {
    const intro = `On the ${ verses[ num ][ 0 ] } day of Christmas my true love gave to me, `;

    const verse = ( pos, str ) => {
        if( pos < 1 ) return str;
        str += verses[ pos ][ 1 ];
        return verse( pos - 1, str );
    };

    return verse( num, intro );
};

const constructVerses = ( start, end, str = "" ) => {
    if( start > end ) return str;
    str += start !== end 
        ? constructVerse( start ) + "\n"
        : constructVerse( start );
    return constructVerses( start + 1, end, str );
};

const TwelveDays = function(){};
TwelveDays.prototype.verse = function( start, end = start ){
    return constructVerses( start, end );
};

TwelveDays.prototype.sing = function(){
    return this.verse( 1, 12 );
};

export default TwelveDays;
