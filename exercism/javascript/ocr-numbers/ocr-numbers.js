const OCR = function(){
};

OCR.prototype.convert = function( str ){
    let returnStr =  splitStringIntoDigits( str ).reduce(( numberStr, arr ) => {
        let comma = arr.length == 5 ? arr.pop() : "";
        const key = convertToDigits( arr );
        numberStr += this.digitLookup[ key ] || "?";
        return numberStr + comma;
    }, "" );

    if( returnStr[ returnStr.length - 1 ] == "," )
        return returnStr.slice( 0, returnStr.length - 1 );
    return returnStr;
};

OCR.prototype.digitLookup = {
    "000001001000" : "1",
    "010011110000" : "2",
    "010011011000" : "3",
    "000111001000" : "4",
    "010110011000" : "5",
    "010110111000" : "6",
    "010001001000" : "7",
    "010111111000" : "8",
    "010111011000" : "9",
    "010101111000" : "0"
};

function splitStringIntoDigits( str ){
    const arr = [];
    const newLineSplit = str.split( "\n" );
    const newLineLength = newLineSplit.length;
    let iterator = 0;

    for( let i = 0; i < newLineSplit.length; i++ ){
        if( i != 0 && i % 4 == 0 ) iterator += 3;
        newLineSplit[ i ].match( /.{3}/g ).forEach(( item, j ) => {
            const pos = j + iterator;
            if( !arr[ pos ] ) arr[ pos ] = [ item ];
            else arr[ pos ].push( item );

            if( newLineLength > 4 )
                if(( i + 1 ) % 4 == 0 && j == 2 )
                    arr[ pos ].push( "," );
        });
    }

    return arr;
}

function convertToDigits( lines ){
    return lines.reduce(( str, line ) => {
        str += line[ 0 ] == "|" ? "1" : "0";
        str += line[ 1 ] == "_" ? "1" : "0";
        str += line[ 2 ] == "|" ? "1" : "0";

        return str;
    }, "" );
};

module.exports = new OCR();
