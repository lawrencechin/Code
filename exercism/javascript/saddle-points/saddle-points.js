const Matrix = function( matrixString ){
    this.rows = matrixString.split( "\n" ).reduce(( arr, row ) => {
        arr.push( row.split( " " ).map( num => parseInt( num )));
        return arr;
    }, []);
    this.columns = generateColsFromRows( this.rows );
    this.saddlePoints = generateSaddlePoints( this.rows, this.columns );
};

function generateColsFromRows( rows ){
    const arr = [];
    for( let i = 0; i < rows.length; i++ ){
        const col = [];
        for( let j = 0; j < rows.length; j++ ){
            col.push( rows[ j ][ i ]);
        }
        arr.push( col );
    }
    return arr;
}

function generateSaddlePoints( rows, cols ){
    const arr = [];
    for( let i = 0; i < rows.length; i++ ){
        let sp;
        for( let j = 0; j < rows.length; j++ ){
            if( rows[ i ].every( elem => {
                return rows[ i ][ j ] >= elem;
            }) && cols[ j ].every( elem => {
                return rows[ i ][ j ] <= elem;
            }))
                sp = [ i, j ];
        }
        if( sp ) arr.push( sp );
    }
    return arr;
}

module.exports = Matrix;

