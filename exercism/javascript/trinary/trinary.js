const BreakException = {};
const Trinary = function( num ){
    this.trinaryNum = num;
};

Trinary.prototype.toDecimal = function(){
    let returnNum = 0;
    try { 
        returnNum = this.trinaryNum.split( "" )
            .reverse()
            .reduce(( returnNum, currentNum, i ) => {
                if( isNaN( currentNum )) throw BreakException; 
                return returnNum += currentNum * Math.pow( 3, i );
            }, 0 );
    } catch( e ){
        // we still want other exceptions to be thrown
        if( e != BreakException ) throw e;
    }
    return returnNum;
};

module.exports = Trinary;
