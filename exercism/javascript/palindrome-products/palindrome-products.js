const Palindrome = function( factors ){
    this.minFactor = factors.minFactor ? factors.minFactor : 0;
    this.maxFactor = factors.maxFactor ? factors.maxFactor : 0;
};

Palindrome.prototype.generate = function(){
    const palindromes = palindromeProduct( this.minFactor, this.maxFactor );
    const keys = Object.keys( palindromes ).sort(( a, b ) => {
        return a - b;
    });
    const keyLength = keys.length;
    const smallest = { 
        value : parseInt( keys[ 0 ]),
        factors : removeArrayDuplicates( palindromes[ keys[ 0 ]]) 
    };
    const largest = {
        value : parseInt( keys[ keyLength -1 ]),
        factors : removeArrayDuplicates( palindromes[ keys[ keyLength - 1 ]])
    };

    this.smallest = function(){ return smallest; };
    this.largest = function(){ return largest; };
};

function removeArrayDuplicates( arr ){
    const uniqueArr = [];
    arr.map( array => {
        return array.sort(( a, b ) => a - b ).join( "," );
    }).forEach( string => {
        if( uniqueArr.indexOf( string ) < 0 ) uniqueArr.push( string );
    });
    return uniqueArr.map( string => {
        return string.split( "," ).map( char  => parseInt( char ));
    });
}

function palindromeProduct( min, max ){
    // slow! This is the brute force method with only one
    // concession to efficiency : any multiple of a number that 
    // is divisible by 10 can never be a palindromic number.
    // Except 0.
    const obj = {};
    for( let i = min; i <= max; i++ ){
        if( i % 10 == 0 ) continue;
        for( let j = min; j <= max; j++ ){
            if( isPalindrome( i * j )){
                if( obj[ i * j ] ) obj[ i * j ].push([ i, j ]);
                else obj[ i * j ] = [[ i, j ]];
            }
        }
    }
    return obj;
}

function isPalindrome( number ){
    return number == number.toString().split( "" ).reverse().join( "" );
}

module.exports = Palindrome;
