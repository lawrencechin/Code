const Raindrops = function(){};
Raindrops.prototype.convert = function( num ){
    let output = "";
    const conversionArray = [
        { 3 : "Pling" },
        { 5 : "Plang" },
        { 7 : "Plong" }
    ];

    conversionArray.forEach( obj => {
        const key = Object.keys( obj )[ 0 ];
        if( isNum( num, key)) output += obj[ key ];
    });

    if( output ) return output;
    return num.toString();
};

function isNum( num, factor ){
    return num % factor == 0;
}

module.exports = Raindrops;
