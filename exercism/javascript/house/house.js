const lyrics = [
    [ "house that Jack built.", "lay in" ],
    [ "malt", "ate" ],
    [ "rat", "killed" ],
    [ "cat", "worried" ],
    [ "dog", "tossed" ],
    [ "cow with the crumpled horn", "milked" ],
    [ "maiden all forlorn", "kissed" ],
    [ "man all tattered and torn", "married" ],
    [ "priest all shaven and shorn", "woke" ],
    [ "rooster that crowed in the morn", "kept" ],
    [ "farmer sowing his corn", "belonged to" ],
    [ "horse and the hound and the horn" ]
];

export default class House {
    static verse( num ){
        let iter = num - 1;
        const lyricsArr = [ `This is the ${ lyrics[ iter ][ 0 ]}` ];
        
        const lyricsLoop = n => {
            if( n < 0 ) return;
            lyricsArr.push( `that ${ lyrics[ n ][ 1 ]} the ${ lyrics[ n ][ 0 ]}` );
            return lyricsLoop( n - 1 );
        };

        lyricsLoop( iter - 1 );

        return lyricsArr;
    }

    static verses( start, end ){
        let lyricsArr = [];
        const lyricLoop = ( s, e ) => {
            if( s > e ) return;
            lyricsArr = lyricsArr.concat( this.verse( s ));
            lyricsArr.push( "" );
            return lyricLoop( s + 1, e );
        };
        lyricLoop( start, end );
        lyricsArr.pop();
        return lyricsArr;
    }
}
