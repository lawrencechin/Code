const Allergies = function( score ){ 
    this.score = score; 
};

Allergies.prototype.allergy = [ 
    "eggs",
    "peanuts",
    "shellfish",
    "strawberries",
    "tomatoes",
    "chocolate",
    "pollen",
    "cats"
];

Allergies.prototype.list = function(){
    return this.allergy.filter(( _,i ) => this.score & ( 1 << i ));
};

Allergies.prototype.allergicTo = function( allergy ){
    return this.list().indexOf( allergy ) > -1;
};

module.exports = Allergies;
