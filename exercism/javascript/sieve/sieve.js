const Sieve = function( range ){
    this.primes = generateSieve( range );
};

function generateSieve( range ){
    const primesArray = [ 2 ];
    const sieve = Array.from( new Array( range + 1 )).map( item => true );
    for( let i = 2; i < Math.sqrt( range ); i++ ){
        if( sieve[ i ]){
            for( let j = i * i; j < sieve.length; j += i ){
                sieve[ j ] = false;
            }
        }
    }
    for( let i = 3; i <= sieve.length; i += 2 ){
        // no need to check even numbers after 2
        if( sieve[ i ]) primesArray.push( i );
    }

    return primesArray;
};

module.exports = Sieve;
