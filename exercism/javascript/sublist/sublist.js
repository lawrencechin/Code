export default class List {
    constructor( xs ){
        this.values = Array.isArray( xs )
            ? xs
            : [];
    }

    _isEmpty( xs ){
        if( xs.length < 1 ) return true;
    }

    _longerList( xs ){
        return this.values.length > xs.length;
    }

    _subsetCheck( xs1, xs2 ){
        let idx = xs2.indexOf( xs1[ 0 ]);
        
        const compareValues = ( ind, pos ) => {
            if( xs1.length === pos ) return true;
            if( xs1[ pos ] === xs2[ ind ]) 
                return compareValues( ind + 1, pos + 1 );
            return false;
        };

        const indices = ind => {
            if( ind === -1 ) return false;
            const $compareValues = compareValues( ind + 1, 1 );
            if( $compareValues ) return true;
            return indices( xs2.indexOf( xs1[ 0 ], ind + 1 ));
        };

        if( idx === -1 ) return false;
        return indices( idx );

    }

    _emptyListCompare( xs ){
        if( this._isEmpty( this.values )){
            if( this._isEmpty( xs )) return "EQUAL";
            return "SUBLIST";
        }
        if( this._isEmpty( xs )) return "SUPERLIST";

        return false;
    }

    compare( list ){
        /** Shortcut, string compare edition **/
        return this.compareStr( list );
        /** *** **/
        const emptyLists = this._emptyListCompare( list.values );
        if( emptyLists ) return emptyLists;

        const superList = this._longerList( list.values );
        const equalLength = this.values.length === list.values.length;
        const subChk = superList
            ? this._subsetCheck( list.values, this.values )
            : this._subsetCheck( this.values, list.values )
        ;

        return equalLength && subChk
            ? "EQUAL"
            : superList
                ? subChk
                    ? "SUPERLIST"
                    : "UNEQUAL"
                : subChk
                    ? "SUBLIST"
                    : "UNEQUAL"
        ;
    }

    compareStr( list ){
        const longerStr = this._longerList( list.values );
        const equalLength = this.values.length === list.values.length;

        const str1 = longerStr ? this.values.join( "," ) : list.values.join( "," );
        const str2 = longerStr ? list.values.join( "," ) : this.values.join( "," );
        const match = str1.match( str2 );

        return match
            ? equalLength
                ? "EQUAL"
                : longerStr
                    ? "SUPERLIST"
                    : "SUBLIST"
            : "UNEQUAL"
        ;
    }   
}
