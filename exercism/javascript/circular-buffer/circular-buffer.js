/** Exceptions **/
function BufferEmptyException( message ){
      this.message = message || "Cannot read from empty buffer";
      this.stack = ( new Error()).stack;
}
BufferEmptyException.prototype = Object.create(Error.prototype);
BufferEmptyException.prototype.name = "BufferEmptyException";

function BufferFullException( message ){
    this.message = message || "Cannot write to full buffer";
    this.stack = ( new Error()).stack;
}

BufferFullException.prototype = Object.create( Error.prototype );
BufferFullException.prototype.name = "BufferFullException";

/** Node in circular linked-list **/
const BufferNode = function( value, prevNode, nextNode ){
    this.value = value;
    this.prevNode = prevNode;
    this.nextNode = nextNode;
};

const Buffer = function( length ){
    this.written = 0;
    this.length = length;
    this.bufferList = createBuffers( length, new BufferNode( null, null, null ));
    this.read = function(){
        let r = null; 
        try { 
            r = read( this.bufferList );
        } catch( e ){
            throw e.message;
        }
        // move top most node pointer to previous node
        this.bufferList = this.bufferList.prevNode;
        this.written -= 1;
        return r;
    };

    this.write = function( value ){
        let writeValue;
        try { 
            writeValue = write( value,
                this.bufferList,
                this.written,
                this.length
            );
        } catch( e ){
            throw e;
        }
        if( writeValue ) this.written += 1;
    };

    this.forceWrite = function( value ){
        // attempt a regular write first
        // then attempt a forced write
        let exception;
        try { 
            this.write( value );
        } catch( e ){
            exception = e;
        }
        if( exception ){
            this.bufferList.value = value;
            // overwrite oldest value but treat as new value
            this.bufferList = this.bufferList.prevNode;
        }
    };

    this.clear = function(){ 
        clear( this.bufferList );
        this.written = 0;
    };
};

function createBuffers( length, buffer ){
    // not a zero-based index
    if( length == 1 ) return buffer;
    const newNode = new BufferNode( 
        null,
        buffer,
        buffer.nextNode || buffer
    );
    newNode.nextNode.prevNode = newNode;
    buffer.nextNode = newNode;
    
    return createBuffers( length - 1, newNode );
}

function read( buffer ){
    const value = buffer.value;
    buffer.value = null;
    if( value == null )
        throw new BufferEmptyException();
    return value;
}

function write( value, buffer, written, length ){
    if( value == null || value == undefined ) return false;
    if( written == length ) throw new BufferFullException();
    if( buffer.value != null ) return write( value, buffer.prevNode, written, length );
    buffer.value = value;
    return value;
}

function clear( buffer ){
    let exception;
    try { 
        read( buffer );
    } catch( e ){
        exception = e;
    }
    if( exception ) return;
    clear( buffer.prevNode );
}

module.exports = {
    circularBuffer( length ){
        return buffer = new Buffer( length );
    },
    bufferEmptyException(){
        new BufferEmptyException(); 
    },
    bufferFullException(){
        new BufferFullException();
    }
};
