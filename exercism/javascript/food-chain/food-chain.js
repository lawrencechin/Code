let FoodChain = function(){};

FoodChain.prototype.lyrics = [
    { eaten : "fly", lyric : "I don't know why she swallowed the fly. Perhaps she'll die." },
    { eaten : "spider", lyric : "It wriggled and jiggled and tickled inside her." },
    { eaten : "bird", lyric : "How absurd to swallow a bird!" },
    { eaten : "cat", lyric : "Imagine that, to swallow a cat!" },
    { eaten : "dog", lyric : "What a hog, to swallow a dog!" },
    { eaten : "goat", lyric : "Just opened her throat and swallowed a goat!" },
    { eaten : "cow", lyric : "I don't know how she swallowed a cow!" },
    { eaten : "horse", lyric : "She's dead, of course!" }
];

FoodChain.prototype.verses = function( from, to ){
    if( from > to ) return "";
    return this.verse( from ) + "\n" + this.verses( from + 1, to );
};

FoodChain.prototype.verse = function( line ){
    let firstLine = `I know an old lady who swallowed a ${ this.lyrics[ line - 1 ].eaten }.\n`;

    if( line == 1 )
        return firstLine += `${ this.lyrics[ 0 ].lyric }\n`;
    else if( line == 8 )
        return firstLine += `${this.lyrics[ 7 ].lyric }\n`;
    else
        return firstLine
            + this.lyrics[ line - 1 ].lyric
            + "\n"
            + this.constructVerse( line - 1 )
            + this.lyrics[ 0 ].lyric
            + "\n";
};

FoodChain.prototype.constructVerse = function( line ){
    if( line == 0 ) return "";

    // bird catching spider has extra text compared to other verses
    let birdSpider = ".";
    if( line == 2 ) birdSpider = " that "
        + this.lyrics[ line - 1 ]
        .lyric.split( " " )
        .slice( 1 )
        .join( " " );

    return "She swallowed the "
    + this.lyrics[ line ].eaten
    + " to catch the "
    + this.lyrics[ line - 1 ].eaten
    + birdSpider
    + "\n"
    + this.constructVerse( line - 1 ); 
};

module.exports = FoodChain;
