const RomanNumerals = function() {
    this.numerals = {
        1 : "I",
        4 : "IV",
        5 : "V",
        9 : "IX",
        10 : "X",
        40 : "XL",
        50 : "L",
        90 : "XC",
        100 : "C",
        400 : "CD",
        500 : "D",
        900 : "CM",
        1000 : "M"
    };
    return function( number ){
        return splitNumber( number ).reduce(( string, arr ) => {
            return string = parseNum( 
                arr[ 0 ], 
                this.numerals, 
                arr[ 1 ]
            ) + string;
        }, "" );
    };
}();

module.exports = RomanNumerals;

function splitNumber( num ){
    const arr = [ 0, 0, 0, 0 ];
    let reduceNum = num;
    let decimal = 1;
    return arr.map( _n => {
        const mod = reduceNum % ( decimal * 10 );
        const interiorArr = [ mod, decimal ];
        reduceNum = reduceNum - mod;
        decimal *= 10;
        return interiorArr;
    });
}

function parseNum( num, numerals, decimal ){
    if( num == 0 ) return "";
    const reducedNum = parseInt( num.toString().replace( /0/g, "" ));
    return numerals[ reducedNum * decimal ] || function(){
        return ( reducedNum > 5 ?
            numerals[ decimal * 5 ]
            : "" )
            + Array(( reducedNum % 5 ) + 1 )
            .join( numerals[ decimal ]);
    }();
}
