const SecretHandshake = function( number ){
    if( !isNumber( number )) throw new Error( "Handshake must be a number" );
    this.commands = function(){
        return convertNumberToHandshake( number );
    };
};

const key = [
    "wink",
    "double blink",
    "close your eyes",
    "jump"
];

function isNumber( number ){
    return number % 1 == 0 ? true : false;
}

function convertNumberToHandshake( number ){
    const arr = key.reduce(( returnArr, handshake, i ) => {
        if( number & Math.pow( 2, i )) returnArr.push( handshake );
        return returnArr;
    }, []);
    if( !( number & 16 )) return arr;
    return arr.reverse();
}

module.exports = SecretHandshake;
