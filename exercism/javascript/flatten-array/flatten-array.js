const FlattenArray = function(){};
FlattenArray.prototype.flatten = function( arr ){
    return flattenArr( arr, [] );
};

function flattenArr( arr, result ){
    if( arr.length < 1 ) return result;
    const value = arr.shift();
    if( value instanceof Array ) flattenArr( value, result );
    else if( value != null || value != undefined ) result.push( value );
    return flattenArr( arr, result );
}

module.exports = FlattenArray;
