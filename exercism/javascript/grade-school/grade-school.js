/* Non protected version
 *
const School = function(){
    this.grades = {};
};


School.prototype.roster = function(){
    // return full roster
    let sortedRoster = {};
    for( let grade in this.grades )
        sortedRoster[ grade ] = this.grade( grade );
    return sortedRoster;
};

School.prototype.add = function( student, grade ){
    // add a student to a particular grade
    if( this.grades[ grade ] )
        return this.grades[ grade ].push( student );
    else
        return this.grades[ grade ] = [ student ];
};

School.prototype.grade = function( grade ){
    // return all the students in specified grade sorted alphabetically
    return this.grades[ grade ] ? this.grades[ grade ].sort(( a, b ) => {
        return a > b;
    }) : [];
};
*/

/* Not an amazing pattern to use as the methods have to be recreated every time a new object is initialised. */

const School = function(){
    let grades = {};
    let that = this;

    function setGrades( student, grade ){
        if( grades[ grade ])
            grades[ grade ].push( student );
        else
            grades[ grade ] = [ student ];
    }
    
    this.add = function( student, grade ){
        setGrades( student, grade );
    };

    this.roster = function(){
        let sortedRoster = {};
        for( let num in grades )
            sortedRoster[ num ] = that.grade( num );
        return sortedRoster;
    };

    this.grade = function( num ){
        return grades[ num ] ? grades[ num ].sort() : [];
    };

    this.remove = function( grade ){
        try {
            that.grades[ grade ] = null;
        } catch( e ){
            return "Cannot edit properties of School";
        }
    };
};

module.exports = School;
