const moves = ( x, y, goal ) => {
    let steps = 0;

    const $moves = ( a, b ) => {
        if( a === goal ) return [ b, steps ];

        steps += 1;

        if( a === 0 ) return $moves( x, b );
        if( b === y ) return $moves( a, 0 );

        const c = a + b;
        if( c > y ) return $moves( c - y, y );

        return $moves( 0, c );
    };

    return $moves( 0, 0 );
};

const TwoBucket = function( b1, b2, goal, sB ){
    this.bucketOne = b1;
    this.bucketTwo = b2;
    this.goalBucket = sB;
    this.otherBucket = null;
    this.goal = goal;
};

TwoBucket.prototype.moves = function(){
    const results = this.goalBucket === "one"
        ? moves( this.bucketOne, this.bucketTwo, this.goal )
        : moves( this.bucketTwo, this.bucketOne, this.goal )
    ;

    this.otherBucket = results[ 0 ];
    return results[ 1 ];
};

export default TwoBucket;
