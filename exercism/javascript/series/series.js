const Series = function( numString ){
    this.digits = numString.split( "" ).map( num => +num );
};

Series.prototype.slices = function( numberOfSlices ){
    if( this.digits.length < numberOfSlices ) 
        throw new Error( "Slice size is too big." );
    return sliceNumber( this.digits, numberOfSlices, []);
};

function sliceNumber( digits, sliceLength, resultsArray ){
    if( digits.length < sliceLength ) return resultsArray;
    resultsArray.push( 
        digits.filter(( _, i ) => i < sliceLength )
    );
    return sliceNumber( digits.slice( 1 ), sliceLength, resultsArray );
}

module.exports = Series;
