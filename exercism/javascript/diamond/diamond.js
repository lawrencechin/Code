const A = "A".charCodeAt();

const constructLine = ( length, dist, char, str = "" ) => {
    const count = str.length;
    if( count === length ) return str;
    str += count === dist
        ? char
        : count === length - dist - 1
            ? char
            : " ";
    return constructLine( 
        length, 
        dist,
        char,
        str
    );
};

const constructTopHalf = ( length, dist, char, str = "" ) => {
    if( dist < 0 ) return str;
    str += constructLine( length, dist, char ) + "\n";
    return constructTopHalf(
        length,
        dist - 1,
        String.fromCharCode( char.charCodeAt() + 1 ),
        str
    );
};

const constructBottomHalf = str => {
    const strArr = str.split( "\n" ).reverse().slice( 1, ).reverse();
    const bottom = [ ...strArr ].reverse().slice( 1, );
    return strArr.concat( bottom ).join( "\n" ) + "\n";
};

const makeDiamond = char => {
    const charToALength = char.charCodeAt() - A;
    const lineLength = charToALength * 2 + 1;
    const topHalf = constructTopHalf( lineLength, charToALength, "A" );
    return constructBottomHalf( topHalf );
};

const Diamond = function(){};

Diamond.prototype.makeDiamond = function( char ){
    return makeDiamond( char );
};

export default Diamond;
