const removeHyphens = str => str.replace( /-/g, "" );

const checkValidChars = ( str, length ) => {
    const re = new RegExp( "^[0-9]{" + ( length - 1 ) + "}" );
    return re.test( str ) && /[0-9X]/.test( str[ str.length - 1 ])
        ? str
        : false;
};

const validISBN = ( str, length, target ) => {
    if( !str ) return false;
    return str.split( "" ).reduce(( total, num, i ) => {
        const intWeight = length - i;
        total += num === "X"
            ? intWeight * 10
            : intWeight * Number( num );
        return total;
    }, 0 ) % target === 0;
};

const validate = ( str, length, modulo ) => {
    return validISBN(
        checkValidChars(
            removeHyphens( str ),
            length
        ),
        length,
        modulo
    );
};

const generateISBN13From10 = str => {
    // add 978 and calc the check digit
    if( validate( str, 10, 11 ))
        return new ISBN( str, 13, 10 );

    throw new Error( "Invalid ISBN10" );
};

const ISBN = function( str, length = 10, modulo = 11 ){
    this.value = str;
    this.isValid = function(){ return validate( str, length, modulo ); };
};

export default ISBN;
