const curry = ( fn, arity = fn.length ) => {
    const $curry = oldArgs => ( ...newArgs ) => {
        const allArgs = oldArgs.concat( newArgs );
        const length = allArgs.length;
        return length < arity 
            ? $curry( allArgs )
            : fn( ...allArgs );
    };
    return $curry([]);
};

const compose = ( ...fns ) => x => fns.reduceRight(( t, f ) => f( t ), x ); 

const directions = {
    "upLeft" : [ -1, -1 ],
    "up" : [ -1, 0 ],
    "upRight" : [ -1, 1 ],
    "left" : [ 0, -1 ],
    "right" : [ 0, 1 ],
    "down" : [ 1, 0 ],
    "downLeft" : [ 1, -1 ],
    "downRight" : [ 1, 1 ]
};

const withinBounds = ([ y, x ], rowLen, colLen ) => {
    return y >= 0
        && y <= rowLen
        && x >= 0
        && x <= colLen;
};

const searchSingleDirection = curry(( grid, rowLen, colLen, [ coords, searchTerm, directions ]) => {
    if( coords.length < 1 ) return null;
    if( searchTerm.length === 0 ) return coords[ 0 ];

    const [ y, x ] = [
        coords[ 0 ][ 0 ] + directions[ 0 ][ 0 ],
        coords[ 0 ][ 1 ] + directions[ 0 ][ 1 ]
    ];

    if( withinBounds([ y, x ], rowLen, colLen )
        && grid[ y ][ x ] === searchTerm[ 0 ])
        return searchSingleDirection( 
            grid, 
            rowLen, 
            colLen, 
            [   [[ y, x ]], 
                searchTerm.slice( 1 ), 
                directions
            ]
        );

    return searchSingleDirection( 
        grid, 
        rowLen, 
        colLen, 
        [   coords.slice( 1 ),
            searchTerm,
            directions.slice( 1 )
        ]
    );
});

const checkSurroundingChars = curry(( grid, rowLen, colLen, coords, searchTerm ) => {
    const match = [];
    const dirs = [];

    for( let direction in directions ){
        const dir = directions[ direction ];
        const [ y, x ] = [
            coords[ 0 ] + dir[ 0 ],
            coords[ 1 ] + dir[ 1 ]
        ];

        if( withinBounds([ y, x ], rowLen, colLen ) 
            && grid[ y ][ x ] === searchTerm[ 0 ]){

            dirs.push( dir );
            match.push([ y, x ]);
        }
    }

    return [ match, searchTerm.slice( 1 ), dirs ];
});

const find = ( grid, list ) => {
    const results = {};
    const rowLen = grid.length - 1;
    const colLen = grid[ 0 ].length - 1;
    
    const searchGrid = ( y, x, searchTerm ) => {
        if( y > rowLen ) return null;
        const position = grid[ y ].indexOf( searchTerm[ 0 ], x );

        if( position === -1 ) return searchGrid( y + 1, 0, searchTerm );
        
        const endCoords = compose(
            searchSingleDirection( grid, rowLen, colLen ),
            checkSurroundingChars( 
                grid, 
                rowLen, 
                colLen,
                [ y, position ]
            )
        )( searchTerm.slice( 1 ));

        if( endCoords ) return [[ y, position ], [ ...endCoords ]];

        return searchGrid( y, position + 1, searchTerm );
    };

    list.forEach( searchTerm => {
        const match = searchGrid( 0, 0, searchTerm );
        results[ searchTerm ] = match
            ? { start : [ match[ 0 ][ 0 ] + 1, match[ 0 ][ 1 ] + 1 ], 
                end : [ match[ 1 ][ 0 ] + 1, match[ 1 ][ 1 ] + 1 ]}
            : undefined;
    });

    return results;
};

function WordSearch( grid ){
    this.grid = grid;
    this.find = function( input ){
        return find( this.grid, input );
    };
};

export default WordSearch;
