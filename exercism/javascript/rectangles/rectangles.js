const validRectArr = arr => {
    if( arr.length === 0 ) return false;
    if( !arr.some( str => /\+/.test( str ))) return false;
    return true;
};

const rectsFromGivenPoint = ( origX, origY, arr ) => {
    let total = 0;

    const up = ( x, y ) => {
        if( y < 0 || y < origY ) return;
        const char = arr[ y ][ x ];
        if( /[^|+]/.test( char )) return;

        if( char === "+" && y === origY ){
            total += 1;
            return;
        }
        return up( x, y - 1, arr );
    };

    const left = ( x, y ) => {
        if( x < 0 || x < origX ) return;
        const char = arr[ y ][ x ];
        if( /[^-+]/.test( char )) return;

        if( char === "+" && x === origX ) return up( x, y - 1 );
        return left( x - 1, y );
    };

    const down = ( x, y ) => {
        if( y === arr.length ) return;
        const char = arr[ y ][ x ];
        if( /[^|+]/.test( char )) return;

        if( char === "+" ) left( x - 1, y );
        return down( x, y + 1 );
    };

    const right = ( x, y ) => {
        if( x === arr[ y ].length ) return;
        const char = arr[ y ][ x ];
        if( /[^-+]/.test( char )) return;

        if( char === "+" ) down( x, y + 1 );
        return right( x + 1, y );
    };

    right( origX + 1, origY );
    return total;
};

const count = arr => {
    if( !validRectArr( arr )) return 0;

    return arr.reduce(( total, str, y, arr ) => {
        let t = 0;
        str.split( "" ).forEach(( char, x )  => {
            if( char === "+" ) t += rectsFromGivenPoint( x, y, arr );
        });
        return total + t;
    }, 0 );
};

const Rectangles = {};
Rectangles.count = function( arr ){ return count( arr ); };

export default Rectangles;
