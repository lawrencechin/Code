const BinarySearch = function( array ){
    this.array = checkIfSorted( array ) ? array : undefined;
};

function checkIfSorted( array ){
    if( array.length < 2 ) return true;
    if( array[ 0 ] > array[ 1 ]) return false;
    return checkIfSorted( array.slice( 1 ));
}

BinarySearch.prototype.indexOf = function( value ){
    return binSearch( this.array, value, 1, this.array.length );
};

function binSearch( array, value, low, high ){
    if( low == high ) return -1;
    const midPos = ( low + high ) >>> 1;
    if( array[ midPos ] == value ) return midPos;
    if( array[ midPos ] > value ) return binSearch( array, value, low - 1, high );
    return binSearch( array, value, low + 1, high );
}

module.exports = BinarySearch;
