const transpose = arr => {
    return arr.reduce(( output, chars, col ) => {
        chars.split( "" ).forEach(( c, row ) => {
            if( output[ row ]){
                const len = output[ row ].length;
                output[ row ] = len < col
                    ? output[ row ] + Array( col - len ).fill( " " ).join( "" ) + c
                    : output[ row ] + c;
            } else {
                output[ row ] = col > 0
                    ? Array( col ).fill( " " ).join( "" ) + c
                    : c;
            }
        });
        return output;
    }, []);
};

export default transpose;
