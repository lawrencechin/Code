//
// This is only a SKELETON file for the "Bob" exercise. It's been provided as a
// convenience to get you started writing code faster.
//

var Bob = function() {};

Bob.prototype.hey = function(input) {
    if( !this.silence( input )){
        return "Fine. Be that way!";
    }else if( this.yell( input )){
        return "Whoa, chill out!";
    }else if( this.question( input )){
        return "Sure.";
    }
    
    return "Whatever.";
    
};

Bob.prototype.silence = function( str ){
    return str.replace( /\s/g, "" ).length;
};

Bob.prototype.question = function( str ){
    return str.match( /\?$/ ) != null;
};

Bob.prototype.yell = function( str ){
    var onlyChars = str.replace( /[^a-zA-Z\u00C0-\u024F]/g, "" );
    var onlyCaps = onlyChars.replace( /[^A-Z\u00C0-\u00DD]/g, "" );
    return onlyChars.length == onlyCaps.length 
        && onlyChars.length > 1;
};

module.exports = Bob;
