const Acronym = function(){};

/* Acronym.parse = function( text ){
    return text.split( " " ).reduce(( string, term ) => {
        let initial = term[ 0 ];
        if( /[A-Z][a-z]{1,}[A-Z]/.exec( term ))
            initial = term.match( /[A-Z]/g ).join( "" );
        else if( /\w+-\w+/.exec( term ))
            initial = term[ 0 ] + term.match( /-(\w)/ )[ 1 ];

        return string += initial.toUpperCase();
    }, "" );
}; */

Acronym.parse = function( text ){
    return text.replace( /(\w)([a-z]+(?=[A-Z])|\w+)/g, "$1" )
    .replace( /\W/g, "" )
    .toUpperCase();
};

module.exports = Acronym;

