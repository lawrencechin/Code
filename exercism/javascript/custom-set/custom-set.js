const CustomSet = function( data ){
    if( data ) this.data = removeDuplicates( data, 0 );
};

function removeDuplicates( arr, pos ){
    if( pos == arr.length ) return arr;
    const dupePos = arr.indexOf( arr[ pos ], pos + 1 );
    if( dupePos > -1 ){ 
        arr.splice( dupePos, 1 );
        return removeDuplicates( arr, pos );
    }
    return removeDuplicates( arr, pos + 1 );
}

CustomSet.prototype.empty = function(){
    if( !this.data || this.data.length < 1 ) return true;
    return false;
};

CustomSet.prototype.contains = function( needle ){
    if( this.empty()) return false;
    return this.data.indexOf( needle ) > -1;
};

CustomSet.prototype.subset = function( customSet ){
    let data_a = sortAndFlatten( this.data );
    let data_b = sortAndFlatten( customSet.data );

    return data_a.slice( 0, data_b.length ) == data_b;
};

CustomSet.prototype.disjoint = function( customSet ){
    if( !this.data || !customSet.data ) return true;
    return customSet.data.every( item => {
        return this.data.indexOf( item ) < 0;
    });
};

CustomSet.prototype.eql = function( customSet ){
    return sortAndFlatten( this.data ) == sortAndFlatten( customSet.data );
};

function sortAndFlatten( data ){
    if( !data ) return "";
    return data.sort(( a, b ) => a > b ).join( "" );
}

CustomSet.prototype.add = function( item ){
    if( this.contains( item )) return this;
    if( !this.data ) this.data = [ item ];
    else this.data.push( item );

    return this;
};

CustomSet.prototype.intersection = function( customSet ){
    if( !this.data || !customSet.data ){
        this.data = undefined;
        return this;
    }
    const arr = [];
    customSet.data.forEach( item => {
        if( this.contains( item )) arr.push( item );
    });
    this.data = arr;

    return this;
};

CustomSet.prototype.difference = function( customSet ){
    if( !this.data || !customSet.data ) return this;
    const arr = [];
    this.data.forEach( item => {
        if( !customSet.contains( item ))
            arr.push( item );
    });
    this.data = arr;

    return this;
};

CustomSet.prototype.union = function( customSet ){
    if( !this.data ){
        this.data = customSet.data;
        return this;
    }else if( !customSet.data ) return this;

    customSet.data.forEach( item => {
        if( !this.contains( item )) this.data.push( item );
    });

    return this;
};

CustomSet.prototype.clear = function(){
    this.data = undefined;
    return this;
};

CustomSet.prototype.size = function(){
    if( this.data ) return this.data.length;
    return 0;
};

CustomSet.prototype.toList = function(){
    if( !this.data ) return [];
    return this.data;
};

module.exports = CustomSet;
