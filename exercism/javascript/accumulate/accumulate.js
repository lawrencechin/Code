const Accumulate = function(){};
Accumulate.prototype.acc = function( arr, func ){
    const resultArr = [];
    for( let i = 0; i < arr.length; i++ ){
        resultArr[ resultArr.length ] = func( arr[ i ] );
    }
    return resultArr;
};

module.exports = new Accumulate().acc;
