const ETL = function(){};

ETL.prototype.transform = function( oldStyle ){
    let newStyle = {};
    Object.keys( oldStyle ).forEach( key => {
        oldStyle[ key ].forEach( char => {
            newStyle[ char.toLowerCase() ] = parseInt( key );
        });
    });

    return newStyle;
};

module.exports = ETL;
