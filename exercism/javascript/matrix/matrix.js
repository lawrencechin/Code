const Matrix = function( matrixStr ){
    this.rows = parseMatrixStr( matrixStr );
    this.columns = parseMatrixRowsIntoColumns( this.rows ); 
};

function parseMatrixStr( matrixStr ){
    return matrixStr.split( "\n" ).reduce(( resultArr, row ) => {
        resultArr.push( row.split( " " ).map( num => parseInt( num )));
        return resultArr;
    }, []);
}

function parseMatrixRowsIntoColumns( rows ){
    let columns = [];
    for( let i = 0; i < rows.length; i++ ){
        let column = [];
        for( let j = 0; j < rows.length; j++ ){
            column.push( rows[ j ][ i ]);
        }
        columns.push( column );
    }
    return columns;
}

module.exports = Matrix;
