const Hexadecimal = function( hexString ){
    this.hex = hexString;
};

Hexadecimal.prototype.toDecimal = function(){
    if( parseHex( this.hex )) return 0;
    return convertHexToDec( this.hex.toLowerCase().split( "" ), 0 );
};

function parseHex( hex ){
   return hex.replace( /[0-9a-f]/gi, "" ).length > 0;
}

function convertHexToDec( hexArr, total ){
    if( hexArr.length < 1 ) return total;
    total += 
        convertHexCharToNum( hexArr[ 0 ])
        * Math.pow( 16, hexArr.length - 1 );

    return convertHexToDec( hexArr.slice( 1 ), total );
}

function convertHexCharToNum( char ){
    if( isNaN( char )) 
        return char.charCodeAt() - "a".charCodeAt() + 10;
    return char;
}

module.exports = Hexadecimal;
