const Say = function(){};

Say.prototype.wordList = {
    0 : "zero", 1 : "one",
    2 : "two", 3 : "three",
    4 : "four", 5 : "five",
    6 : "six", 7 : "seven",
    8 : "eight", 9 : "nine",
    10 : "ten", 11 : "eleven",
    12 : "twelve", 13 : "thirteen",
    14 : "fourteen", 15 : "fifteen",
    16 : "sixteen", 17 : "seventeen",
    18 : "eighteen", 19 : "nineteen",
    20 : "twenty", 30 : "thirty",
    40 : "forty", 50 : "fifty",
    60 : "sixty", 70 : "seventy",
    80 : "eighty", 90 : "ninety",
    100 : "hundred", 1000 : "thousand",
    1000000 : "million", 1000000000 : "billion"
};

Say.prototype.inEnglish = function( number ){
    if( number < 0 || number > 999999999999 )
        throw new Error( "Number must be between 0 and 999,999,999,999.");
    if( number < 10 )
        return this.wordList[ number ];
    return parseNumArray(
        splitIntoThrees( number ),
        "",
        number.toString().length,
        this.wordList
    ).trim().replace( /\s{2,}/g, " " );
};

function parseNumArray( numArr, string, unit, wordList ){
    if( numArr.length < 1 ) return string;
    unit = unit - numArr[ 0 ].length;
    return parseNumArray( 
        numArr,
        string += numberToText( 
            numArr.shift(),
            Math.pow( 10, unit ), 
            wordList
        ) + " ",
        unit,
        wordList
    );
}

function splitIntoThrees( number ){
    return number.toString().split( "" ).reverse().reduce(( arr, num, i ) => {
        if( i != 0 && i % 3 == 0 ){
            arr.push([ +num ]);
            return arr;
        }
        arr[ arr.length - 1 ].push( +num );
        return arr;
    }, [[]] ).reverse();
}

function numberToText( numArr, unit, wordList ){
    const [ one, ten, hundred ] = numArr;
    let string = "";
    if( ten == 1 )
        string = wordList[ 10 + one ];
    else if( ten ){
        tenStr = wordList[ ten * 10 ];
        if( one ) string = tenStr + "-" + wordList[ one ];
    }else if( !ten && one )
        string = wordList[ one ];

    if( one && !ten ) string = wordList[ one ];

    if( hundred )
        string = `${wordList[ hundred ]} ${wordList[ 100 ]} ${string}`;
    if( unit > 100 && one + ten + hundred != 0 )
        string += ` ${wordList[ unit ]}`;

    return string;
}

module.exports = new Say();
