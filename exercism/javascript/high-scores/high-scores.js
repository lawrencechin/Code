function HighScores( input ){
    this.scores = input || [];
    this.sorted = this.scores.map( x => x ).sort(( a, b ) => b - a );
    this.latest = this.scores.slice( -1 )[ 0 ];
    this.highest = this.sorted[ 0 ];
    this.top = this.sorted.slice( 0, 3 );
    this.report = function( latest, highest ){
        const latestScore = scr => `Your latest score was ${ scr }.`;
        const newPB = "That's your personal best!";
        const short = diff => `That's ${ diff } short of your personal best!`;
        const diff = highest - latest;
        return diff === 0
            ? `${ latestScore( latest )} ${ newPB }`
            : `${ latestScore( latest )} ${ short( diff )}`;
    }( this.latest, this.highest );
};

module.exports = { HighScores };
