const steps = num => {
    if( num < 1 ) throw new Error( "Only positive numbers are allowed" );    
    
    let stepCount = 0;
    const _steps = num => {
        if( num === 1 ) return;
        
        stepCount += 1; 

        if( num % 2 == 0 ) return _steps( num / 2 );
        return _steps( num * 3 + 1 );
    };

    _steps( num );
    return stepCount;
};

export { steps };

