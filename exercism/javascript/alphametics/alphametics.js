const parsePuzzleString = str => {
    const arr = str.split( " + " );
    const last = arr.pop();
    return arr.concat( last.split( " == " ));
};

const verifyPuzzleString = arr => {
    if( arr.length === 2 ) return arr[ 0 ] === arr[ 1 ];
    const sum = arr[ arr.length - 1 ];
    return arr.every( str => str.length <= sum );
};

const solve = puzzle => {
    const arr = parsePuzzleString( puzzle );
    if( !verifyPuzzleString( arr )) return null;
};
export { solve };
