const Cipher = function( key ){
    this.key = typeof key == "string" ? parseKey( key ) : generateKey();
};

function parseKey( key ){
    if( key.length < 1
        || key.match( /[^a-z]+/ ) != null )
        throw new Error( "Bad key" );
    return key;
}

function generateKey(){
    const lowerBound = "a".charCodeAt();
    const upperBound = "z".charCodeAt() - lowerBound + 1;
    let newKey = "";
    for( let i = 0; i < 100; i++ )
        newKey += String.fromCharCode( Math.floor( Math.random() * upperBound + lowerBound ));

    return newKey;
}

Cipher.prototype.encode = function( string ){
    return encodeDecode( string, this.key, true );
};

Cipher.prototype.decode = function( string ){
    return encodeDecode( string, this.key, false );
};

function encodeDecode( string, key, encode ){
    const bounds = [ "a".charCodeAt(), "z".charCodeAt()]; 
    
    if( string.length > key.length )
        key = fillKey( key, string.length - key.length );

    return string.split( "" ).map(( char, i ) => {
        return generateChar( 
            char.charCodeAt(),
            key[ i ].charCodeAt() - bounds[ 0 ],
            bounds,
            encode
        );
    }).join( "" );
}

function fillKey( key, length ){
    return key += new Array( length ).fill( "a" ).join( "" );
}

function generateChar( start, amount, bounds, increment ){
    if( !increment ) amount = -amount;
    let newCharCode = start + amount;
    
    if( newCharCode < bounds[ 0 ] )
        newCharCode = bounds[ 1 ] - ( bounds[ 0 ] - newCharCode );
    else if( newCharCode > bounds[ 1 ] )
        newCharCode = bounds[ 0 ] + ( newCharCode - bounds[ 1 ]);

    return String.fromCharCode( newCharCode );
}

module.exports = Cipher;

