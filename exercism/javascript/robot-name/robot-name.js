const Robot = function(){
    let codeName = "";
    let that = this;

    function setName(){
        const name = that.generateName();
        if( that.checkNameExists( name ))
            return setName();
        else{
            that.usedNames.push( name );
            return name;
        }
    }

    function getName(){
        return codeName;
    }

    codeName = setName();
    this.name = getName();
};

Robot.prototype.usedNames = [];

Robot.prototype.generateName = function(){
    let finalName = this.stringGen() + this.stringGen();
    for( let i = 0; i < 3; i++ )
        finalName += Math.floor( Math.random() * 10 );        
    return finalName;
};

Robot.prototype.stringGen = function(){
    return String.fromCharCode( 65 + Math.floor( Math.random() * 26 ));
};

Robot.prototype.checkNameExists = function( name ){
    return this.usedNames.includes( name );
};

Robot.prototype.reset = function(){
    // this method of resetting leaves the old, unused name in the usedNames array
    // should we remove said name and allow it to be assigned to another robot?
    this.constructor();
};

module.exports = Robot;
