const isPrime = num => {
    if( num < 2 ) return false;
    if( num === 2 ) return true;
    if( num % 2 === 0 ) return false;

    const length = Math.sqrt( num );

    const _isPrime = ( factor, sqrt, num ) => 
        factor > sqrt
            ? true
            : num % factor === 0
                ? false
                : _isPrime( factor + 2, sqrt, num );

    return _isPrime( 3, length, num );
};

const generatePublicKey = ( a, g, p ) => g ** a % p;
const calcSecretKey = ( a, b, p ) => b ** a % p;

// oops! Misread the test -> this function showcases the flaws in the 
// diffie hellman method, requires much larger primes
const getPrivateKeyFromPublic = ( a, g, p, pub ) => {
    if( a === p ) return null;
    if( generatePublicKey( a, g, p ) === pub ) return a;
    return getPrivateKeyFromPublic( a + 1, g, p, pub );
};

const getPublicFromPrivateKey = ( p, g, num ) => {
    if( num < 2 || num >= p ) throw new Error( "Please supply a valid private key" );

    return generatePublicKey( num, g, p );
};

const DiffieHellman = function( p, g ){
    this.p = isPrime( p ) ? p : null;
    this.g = isPrime( g ) ? g : null;

    if( !this.p || !this.g ) throw new Error();
};

DiffieHellman.prototype.getPublicKeyFromPrivateKey = function( num ){
    return getPublicFromPrivateKey( this.p, this.g, num );
};

DiffieHellman.prototype.getSharedSecret = function( A, b ){
    return calcSecretKey( A, b, this.p );
};

export default DiffieHellman;
