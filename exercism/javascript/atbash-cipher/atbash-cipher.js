const AtBash = function(){};

AtBash.prototype.encode = function( string ){
    return string.toLowerCase().replace( /[^a-z0-9]/g, "" )
        .split( "" )
        .map( charFlip )
        .reduce(( str, char, i ) => {
            return str += i && i % 5 == 0 ? " " + char : char;
        }, "" );
};

function charFlip( char ){
    const m = "m".charCodeAt();
    const charCode = char.charCodeAt();
    
    if( charCode < 97 ) return char; // don't parse numbers

    let newChar = -(( charCode - m ) * 2 - 1 );
    return String.fromCharCode( charCode + newChar );
}

module.exports = new AtBash();
