const PascalsTriangle = function( rows ){
    this.rows = generateRows( rows, [[ 1 ]]);
    this.lastRow = this.rows[ this.rows.length - 1 ];
};

function generateRows( rows, arr ){
    let i = 1;
    while( i < rows ){
        arr.push( generateSingleRow([ 1 ], 1, i, 1 ));
        i += 1;
    }
    return arr;
}

function generateSingleRow( arr, currentVal, nominator, denominator ){
    if( nominator < 1 ) return arr;
    const value = Math.round( currentVal * ( nominator / denominator ));
    arr.push( value );
    return generateSingleRow( arr, value, nominator - 1, denominator + 1 );
}

module.exports = PascalsTriangle;
