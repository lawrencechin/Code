let Garden = function( diagram, students ){
    this.students = students ? students.sort() : [ 
        "Alice", "Bob", "Charlie", 
        "David", "Eve", "Fred", 
        "Ginny", "Harriet", "Ileana", 
        "Joseph", "Kincaid", "Larry"
    ];
    this.flora = { 
        "G" : "grass", 
        "C" : "clover", 
        "R" : "radishes", 
        "V" : "violets" 
    };
    this.diagram = function( garden ){
        if( !verifyDiagram( diagram, garden.flora, garden.students ))
            throw new Error( "Invalid diagram" );
        return processDiagram( diagram );
    }( this );

    const dsf = diagramToStudentFlora( this.diagram, this.flora, this.students );
    Object.keys( dsf ).forEach( key => {
        this[ key ] = dsf[ key ];
    });
};

function verifyDiagram( diagram, flora, students ){
    const diagramArr = diagram.split( "\n" );
    const floraCodes = new RegExp( "[" + Object.keys( flora ).join( "" ) + "]", "gi" );

    if( !diagramArr[ 0 ] || !diagramArr[ 1 ] || diagramArr[ 0 ].length != diagramArr[ 1 ].length ) return false;
    if( diagram.replace( floraCodes, "" ).length != 1 ) return false;
    if( diagramArr[ 0 ].length % 2 != 0 ) return false;
    if( diagramArr[ 0 ].length / 2 > students.length ) return false;

    return true;
}

function processDiagram( diagram ){
    const arr = diagram.toUpperCase().split( "\n" );
    arr[ 0 ] = arr[ 0 ].match( /\w{2}/g );
    arr[ 1 ] = arr[ 1 ].match( /\w{2}/g );
    
    return arr[ 0 ].reduce(( finalArr, floraCode, i ) => {
        const code = floraCode + arr[ 1 ][ i ];
        finalArr.push( code.split( "" ));
        return finalArr;
    }, []);
}

function diagramToStudentFlora( diagram, flora, students ){
    return diagram.reduce(( obj, floraArr, i ) => {
        const studentFlora = floraArr.reduce(( arr, code ) => {
            arr.push( flora[ code ] );
            return arr;
        }, []);
        obj[ students[ i ].toLowerCase()] = studentFlora;
        return obj;
    }, Object.create( null ));
}

module.exports = Garden;
