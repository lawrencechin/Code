const Strain = function(){};

Strain.prototype.keep = function( array, f ){
    return keepDiscard( array, f, true );
};

Strain.prototype.discard = function( array, f ){
    return keepDiscard( array, f, false );
};

function keepDiscard( array, func, keep ){
    // no array functions used
    const resultsArr = [];
    for( let i = 0; i < array.length; i++ ){
        if( keep && func( array[ i ]) 
            || !keep && !func( array[ i ])) 
            resultsArr[ resultsArr.length ] = array[ i ];
    }
    return resultsArr;
}

module.exports = new Strain();
