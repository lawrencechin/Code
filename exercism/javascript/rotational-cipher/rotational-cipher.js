const RotationalCipher = {
    rotate( str, key ){
        const lowerBoundLowerCase = "a".charCodeAt();
        const upperBoundLowerCase = "z".charCodeAt();
        const lowerBoundUpperCase = "A".charCodeAt();
        const upperBoundUpperCase = "Z".charCodeAt();
        const strArr = str.split( "" );

        return strArr.map( c => {
            if( !/[a-zA-Z]/.test( c )) return c;
            let returnChar;
            let charCode = c.charCodeAt();
            const upper = 
                charCode >= lowerBoundUpperCase && charCode <= upperBoundUpperCase ? true : false;
            charCode = upper ? c.toLowerCase().charCodeAt() : charCode;
            const charCodeSum = charCode + key;
            returnChar = charCodeSum > upperBoundLowerCase
                ? String.fromCharCode( lowerBoundLowerCase + ( charCodeSum - upperBoundLowerCase ) - 1 )
                : String.fromCharCode( charCodeSum );

            return upper ? returnChar.toUpperCase() : returnChar;
        }).join( "" );
    }
};

module.exports = RotationalCipher;
