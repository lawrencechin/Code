const Series = function( numberString ){
    this.numStr = numberString;
};

Series.prototype.largestProduct = function( slice ){
    if( !parseSeries( this.numStr, slice )) return 1;
    return processSeries( this.numStr, slice, 0 );
};

function parseSeries( numStr, slice ){
    if( numStr.match( /[^0-9]/g ) || slice < 0 ) 
        throw new Error( "Invalid input." );
    if( slice > numStr.length ) throw new Error( "Slice size is too big." );

    if( numStr.length < 1 || slice == 0 ) return false;
    return true;
}

function processSeries( numStr, slice, largestProduct ){
    if( numStr.length < 1 || numStr.length < slice ) return largestProduct;
    const product = productOfSlice( numStr.slice( 0, slice ));
    largestProduct = product > largestProduct ? product : largestProduct;
    return processSeries( numStr.slice( 1 ), slice, largestProduct );
}

function productOfSlice( numStr ){
    if( numStr.length < 1 ) return 1;
    return numStr[ 0 ] * productOfSlice( numStr.slice( 1 ));
}

module.exports = Series;
