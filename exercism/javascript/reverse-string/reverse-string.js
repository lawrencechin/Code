const reverseString = str => str.split( "" ).reverse().join( "" );

const reverseStringR = ( str, newStr ) => {
    if( str.length === 0 ) return newStr;
    return reverseStringR( str.slice( 1, ), str.slice( 0, 1 ) + newStr );
};

const reverseStringL = str => {
    let newStr = "";
    for( let i = str.length - 1; i >= 0; i -= 1 )
        newStr = newStr + str[ i ];

    return newStr;
};

export default function( str ){ return reverseStringR( str, "" ); };
