var Gigasecond = function( date ){
    this.startDate = date;
    this.gigasecond = 1E9;
};

Gigasecond.prototype.date = function(){
    return new Date( this.startDate.getTime() + this.gigasecond * 1000 );
};

module.exports = Gigasecond;
