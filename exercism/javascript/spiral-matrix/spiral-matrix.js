const fillArrSpiral = num => {
    let total = num ** 2;
    let counter = 1;
    let size = num;
    let arr = Array( num ).fill( num ).map( i => Array( i ).fill( 0 ));

    const fillArr = ( direction, row, column, length ) => {
        if( counter > total ) return arr;
        arr[ row ][ column ] = counter;

        counter += 1;

        if( direction === "U" || direction === "D" ){
            if( length === 0 )
                return direction === "U"
                    ? fillArr( "R", row, column + 1, size )
                    : fillArr( "L", row, column - 1, size );

            return direction === "U" 
                ? fillArr( "U", row - 1, column, length - 1 )
                : fillArr( "D", row + 1, column, length - 1 );
        }

        if( length === 0 )
            return direction === "L"
                ? fillArr( "U", row - 1, column, size -= 1 )
                : fillArr( "D", row + 1, column, size -= 1 );

        return direction === "L"
            ? fillArr( "L", row, column - 1, length - 1 )
            : fillArr( "R", row, column + 1, length - 1 );
    };

    return fillArr( "R", 0, 0, size -= 1 );
};

const SpiralMatrix = function(){};

SpiralMatrix.ofSize = function( num ){
    return fillArrSpiral( num );
};

export default SpiralMatrix;
