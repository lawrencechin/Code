const BST = function( data ){
    this.data = data;
    this.left = null;
    this.right = null;
};

BST.prototype.insert = function( data ){
    insertNode( this, data );
};

function insertNode( root, value ){
    if( root.data >= value ){
        if( root.left != null ) return insertNode( root.left, value );
        return root.left = new BST( value );
    }else{
        if( root.right != null ) return insertNode( root.right, value );
        return root.right = new BST( value );
    }
}

BST.prototype.each = function( func ){
    parseTree( this, func );
};

function parseTree( root, func ){
    if( !root ) return;
    parseTree( root.left, func );
    func( root.data );
    parseTree( root.right, func );
}

module.exports = BST;

