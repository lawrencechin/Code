const SumOfMultiples = function( multiplesArr ){
    this.to = function( limit ){
        return sumOfObjectKeys( 
            processMultiplesArray( 
                multiplesArr,
                limit,
                {}
            )
        );
    };
};

function sumOfObjectKeys( obj ){
    return Object.keys( obj ).reduce(( total, key ) => {
        return total += +key;
    }, 0 );
}

function processMultiplesArray( multiArr, limit, obj ){
    if( multiArr.length < 1 ) return obj;
    const currentNum = multiArr.shift();
    multiplesUpToLimit( currentNum, currentNum, limit, obj );
    return processMultiplesArray( multiArr, limit, obj );
}

function multiplesUpToLimit( multiplier, counter, limit, obj ){
    if( counter >= limit ) return obj;
    obj[ counter ] = true;
    return multiplesUpToLimit( 
        multiplier, 
        counter + multiplier, 
        limit, 
        obj 
    );
}

module.exports = function( multiArr ){
    return new SumOfMultiples( multiArr );
};
