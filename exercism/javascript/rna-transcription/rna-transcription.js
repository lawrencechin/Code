var DnaTranscriber = function(){};

DnaTranscriber.prototype.toRna = function( strand ){
    // better as an internal function of toRna
    // or a prototype function?
    function rnaConverter( char ){
        return char == "C" ?
            "G" : char == "G" ?
            "C" : char == "A" ?
            "U" : char == "T" ?
            "A" : char; // if the correct char isn't found then it probably shouldn't return the original char as the DNA sequence is malformed
    }

    // second iteration with switch and error handling
    // new test written in spec to verify error handling
    function rnaConvert( char ){
        switch( char ){
            case "C" : 
                return "G";
            case "G" :
                return "C";
            case "A" : 
                return "U";
            case "T" : 
                return "A";
            default :
                throw new Error( "Malformed DNA sequence entered.");
        }
    }
    return strand.split( '' ).map( rnaConvert ).join( '' );
};
module.exports = DnaTranscriber;
