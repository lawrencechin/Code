export default class ComplexNumber {

    constructor( real, imaginary ){
        this.real = real;
        this.imag = imaginary;
    }

    get abs(){
        return Math.sqrt( Math.pow( this.real, 2 ) + Math.pow( this.imag, 2 ));
    }

    get conj(){ 
        return new ComplexNumber( this.real, this.imag === 0 ? 0 : - this.imag );
    }

    get exp(){
        return new ComplexNumber(
            Math.exp( this.real ) * Math.cos( this.imag ),
            Math.exp( this.real ) * Math.sin( this.imag )
        );
    }

    add( cN ){
        return new ComplexNumber( 
            this.real + cN.real,
            this.imag + cN.imag
        );
    }

    sub( cN ){
        return new ComplexNumber(
            this.real - cN.real,
            this.imag - cN.imag
        );
    }

    mul( cN ){
        return new ComplexNumber(
            ( this.real * cN.real ) - ( this.imag * cN.imag ),
            ( this.real * cN.imag ) + ( this.imag * cN.real )
        );
    }
    
    div( cN ){
        return new ComplexNumber(
            ( this.real * cN.real + this.imag * cN.imag ) / ( cN.real ** 2 + cN.imag ** 2 ),
            ( this.imag * cN.real - this.real * cN.imag ) / ( cN.real ** 2 + cN.imag ** 2 )
        );
    }
};
