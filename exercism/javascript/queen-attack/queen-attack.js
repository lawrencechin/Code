const Queens = function( queensPosition ){
    if( queensPosition 
        && validPosition( queensPosition.white, 0) 
        && validPosition( queensPosition.black, 0)){

        this.white = queensPosition.white;
        this.black = queensPosition.black;

        if( this.white.join( "" ) == this.black.join( "" ))
            return new Error( "Queens cannot share the same space" );
    }else{
        this.white = [ 0, 3 ];
        this.black = [ 7, 3 ];
    }
};

function validPosition( vector, iterator ){
    if( iterator == vector.length ) return true;
    if( vector[ iterator ] > -1 && vector[ iterator ] < 8 )
        return validPosition( vector, iterator + 1 );
    else
        return false;
}

Queens.prototype.toString = function(){
    let board = "";
    for( let i = 0; i < 8; i++ ){
        for( let j = 0; j < 8; j++ ){
            if( this.white[ 0 ] == i && this.white[ 1 ] == j )
                board += "W ";
            else if( this.black[ 0 ] == i && this.black[ 1 ] == j )
                board += "B ";
            else 
                board += "_ ";
        }
        board = board.trim() + "\n";
    }
    return board;
};

Queens.prototype.canAttack = function(){
    if( this.white[ 0 ] == this.black[ 0 ]) return true;
    if( this.white[ 1 ] == this.black [1 ]) return true;
    const row = Math.abs( this.white[ 0 ] - this.black[ 0 ]);
    const column = Math.abs( this.white[ 1 ] - this.black[ 1 ]);
    if( row == column ) return true;

    return false;
};

module.exports = Queens;
