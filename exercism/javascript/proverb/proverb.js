const Proverb = function(){
    let args = [ ...arguments ];
    const objIndex = args.findIndex( item => typeof item == "object" );
    const qualifier = objIndex > -1 ? args.splice( objIndex, 1 )[ 0 ].qualifier : null;
    const firstArg = args[ 0 ];
    const string = generateLines( args, "" ) 
        + lastLine( firstArg, qualifier );

    return { 
        toString(){
            return string;
        }
    };
};

function generateLines( args, string ){
    if( args.length < 2 ) return string;
    string += printLine( args.shift(), args[ 0 ]);
    return generateLines( args, string );
};

function printLine ( term_1, term_2 ){
    return `For want of a ${ term_1 } the ${ term_2 } was lost.\n`;
};

function lastLine ( term, qualifier ){
    const start = "And all for the want of a";
    if( qualifier ) return start + " " + qualifier + " " + term + ".";
    return start + " " + term + ".";
};

module.exports = Proverb;
