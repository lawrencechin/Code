const Binary = function( binaryNumber ){
    this.binaryNumber = binaryNumber;
};

Binary.prototype.toDecimal = function(){
    if( this.validBinary( this.binaryNumber )) return 0;

    function decimalRecursive( bin, dec, length ){
        if( length == -1 ) return dec;
        dec += +bin.slice( 0, 1 ) * Math.pow( 2, length );
        return decimalRecursive( bin.slice( 1 ), dec, length - 1 );
    }

    return decimalRecursive( this.binaryNumber, 0, this.binaryNumber.length - 1 );
};

Binary.prototype.validBinary = function( bin ){
    return bin.replace( /[01]/g, "" ).length;
};

module.exports = Binary;

