const BigInt = require("./big-integer");
const Grains = function(){};

Grains.prototype.lookUp = [ BigInt( 1 ) ]; // available for all instances

Grains.prototype.square = function( num ){
    if( num > this.lookUp.length )
        this.updateLookUpArr( num );
    return this.lookUp[ num - 1 ].toString();
};

Grains.prototype.updateLookUpArr = function( num ){
    /* let start = this.lookUp.length;
    let end = num;
    for( start; start != num; start++ ){
        const previousValue = this.lookUp[ start - 1 ];
        this.lookUp.push( previousValue.add( previousValue ));
    } */
    this.lookUp = this.lookUp.concat( 
        this.sequence(
            this.lookUp.length,
            num,
            this.lookUp[ this.lookUp.length - 1 ],
            []
        )
    );
};

Grains.prototype.sequence = function( start, end, value, arr ){
    if( start == end ) return arr;
    let newValue = value.add( value );
    arr.push( newValue );
    return this.sequence( start + 1, end, newValue, arr );
};

Grains.prototype.total = function(){
    return this.lookUp.reduce(( start, next ) => {
        return start.add( next );
    }).toString();
};

module.exports = Grains;
