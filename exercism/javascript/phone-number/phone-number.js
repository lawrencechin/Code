let PhoneNumber = function( number ){
    this.digits = number;
};

PhoneNumber.prototype.areaCode = function(){
    return this.number().slice( 0, 3 );
};

PhoneNumber.prototype.toString = function(){
    return this.formatNumber( this.number( this.digits ));
};

PhoneNumber.prototype.number = function(){
    return this.digitLength( this.removeNonDigits( this.digits ));
};

PhoneNumber.prototype.formatNumber = function( number ){
    return number.match( /(\d{3})(\d{3})(\d{4})/ ).reduce(( start, num, pos ) => {
        if( pos == 1 ) return "(" + num + ") ";
        else if( pos == 2 ) return start + num + "-";
        else if( pos == 3 ) return start + num;
    });
};

PhoneNumber.prototype.digitLength = function( number ){
    return number.length == 10 ? number 
    : number.length == 11 && number[ 0 ] == 1 ? number.slice( 1 )
    : "0000000000";
};

PhoneNumber.prototype.removeNonDigits = function( number ){
    return number.replace( /[^\d]/g, "" );
};

module.exports = PhoneNumber;
