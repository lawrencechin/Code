const Robot = function(){
    this.bearing = null;
    this.coordinates = [ 0, 0 ];
};

Robot.prototype.directions = { 
    name : [ "north", "east", "south", "west" ],
    value : [ 1, 1, -1, -1 ]
};

Robot.prototype.instructionCodes = {
    L : "turnLeft",
    R : "turnRight",
    A : "advance"
};

Robot.prototype.orient = function( direction ){
    if( this.directions.name.indexOf( direction ) > -1 )
        this.bearing = direction;
    else throw new Error( "Invalid Robot Bearing" );
};

Robot.prototype.turnLeft = function(){
    let position = this.directions.name.indexOf( this.bearing );
    if( position == 0 ) position = this.directions.name.length;
    this.bearing = this.directions.name[ position - 1 ];
};

Robot.prototype.turnRight = function(){
    let position = this.directions.name.indexOf( this.bearing );
    this.bearing = this.directions.name[( position + 1 ) % 4 ];
};

Robot.prototype.advance = function(){
    const position = this.directions.name.indexOf( this.bearing );
    if( position % 2 == 0 )
        this.coordinates[ 1 ] += this.directions.value[ position ];
    else 
        this.coordinates[ 0 ] += this.directions.value[ position ];
};

Robot.prototype.at = function( x, y ){
    this.coordinates = [ x, y ];
};

Robot.prototype.instructions = function( instructions ){
    return instructions.split( "" ).reduce(( arr, code ) => {
        const instruction = this.instructionCodes[ code ] || null;
        if( !instruction ) throw new Error( "Incorrect instruction passed" );
        arr.push( instruction );
        return arr;
    }, []);
};

Robot.prototype.place = function( options ){
    // should verify the options before using them
    // maybe write some tests for this purpose
    this.at( options.x, options.y );
    this.orient( options.direction );
};

Robot.prototype.evaluate = function( instructions ){
    this.instructions( instructions ).forEach( action => {
        this[ action ]();
    });
};

module.exports = Robot;
