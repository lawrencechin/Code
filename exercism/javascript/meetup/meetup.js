const days = {
    "Monday" : 1,
    "Tuesday" : 2,
    "Wednesday" : 3,
    "Thursday" : 4,
    "Friday" : 5,
    "Saturday" : 6,
    "Sunday" : 7
};

const descriptions = {
    "teenth" : 0,
    "last" : 0,
    "1st" : 0,
    "2nd" : 7,
    "3rd" : 14,
    "4th" : 21,
    "5th" : 28
};

const meetupDay = ( year, month, day, description ) => {
    const desc = descriptions[ description ];

    const startDay = description === "last"
        ? new Date( year, month + 1, 0 ) // last day
        : description === "teenth"
            ? new Date( year, month , 10 ) // tenth day
            : new Date( year, month ) // first day
    ;

    const gapBetweenDays = days[ day ] - startDay.getDay();
    const gapToFirstDay = description === "last" && gapBetweenDays > 0
        ? gapBetweenDays - 7
        : description !== "last" && gapBetweenDays < 0
            ? gapBetweenDays + 7
            : gapBetweenDays
    ;

    startDay.setDate( startDay.getDate() + gapToFirstDay + desc );

    if( startDay.getMonth() !== month ) throw new Error( "Not a valid date" );
    return startDay;
};

export default meetupDay;
