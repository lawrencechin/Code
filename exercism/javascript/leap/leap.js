var Year = function( year ){
    this.year = year;
};

Year.prototype.isLeap = function(){
    // Year must be evenly divisible by 4 and not 100 unless
    // it is also evenly divisible by 400
    // Return value expected is a boolean

    return this.year % 4 == 0 && this.year % 100 != 0 || this.year % 400 == 0;
};

module.exports = Year;
