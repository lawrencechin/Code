const checkPotentialWinner = board => {
    const length = board[ 0 ].length - 1;

    const O = /O/.test( board[ 0 ])
        && /O/.test( board[ board.length - 1 ]);
    const X = !( board.every( row => row[ 0 ] !== "X" )
        && board.every( row => row[ length ] !== "X" ));

    if( O && X ) return "OX";
    if( O && !X ) return "O";
    if( !O && X ) return "X";
    return false;
};

const getStartingPos = ( player, board ) => {
    let O, X;

    if( player === "O" || player === "OX" ){
        O = board[ 0 ].split( "" ).reduce(( arr, item, pos ) => {
            if( item === "O" ) arr.push([ pos, 0 ]);
            return arr;
        }, []);
    }

    if( player === "X" || player === "OX" ){
        X = board.reduce(( arr, item, pos ) => {
            if( item[ 0 ] === "X" ) arr.push([ 0, pos ]);
            return arr;
        }, []);
    }

    return { O, X };
};

const getCharAtPos = ( x, y, board ) => board[ y ][ x ]; 

const withinBounds = ( posX, posY, x, y ) => 
    posX > -1 
    && posY > -1 
    && posX <= x
    && posY <= y
;

const previouslyVisited = ( prevObj, x, y ) => {
    if( prevObj[ y ]){
        if( prevObj[ y ].indexOf( x ) !== -1 ) return true;
        prevObj[ y ].push( x );
    } else {
        prevObj[ y ] = [ x ];
        return false;
    }
};

const validCharacter = ( x, y, bX, bY, player, board ) => 
    withinBounds( x, y, bX, bY ) && getCharAtPos( x, y, board ) === player;

const winningPos = ( x, y, bX, bY, player, board ) =>
    validCharacter( x, y, bX, bY, player, board ) && ( player === "O" && y === bY || x == bX ); 

const checkPath = ([ startX, startY ], player, board ) => {
    const boundY = board.length - 1;
    const boundX = board[ 0 ].length - 1;
    const prevObj = {};
    let winner = false;

    const $checkPath = ( x, y ) => {
        if( winner ) return;
        if( previouslyVisited( prevObj, x, y )) return;

        if( winningPos( x, y, boundX, boundY, player, board )){
            winner = true;
            return;
        }

        if( validCharacter( x, y - 1, boundX, boundY, player, board ))
            $checkPath( x, y - 1 );

        if( validCharacter( x + 1, y - 1, boundX, boundY, player, board ))
            $checkPath( x + 1, y - 1 );

        if( validCharacter( x - 1, y, boundX, boundY, player, board ))
            $checkPath( x - 1, y );

        if( validCharacter( x + 1, y, boundX, boundY, player, board ))
            $checkPath( x + 1, y );

        if( validCharacter( x - 1, y + 1, boundX, boundY, player, board ))
            $checkPath( x - 1, y + 1 );

        if( validCharacter( x, y + 1, boundX, boundY, player, board ))
            $checkPath( x, y + 1 );
    };

    $checkPath( startX, startY );
    return winner;
};

const determineWinner = board => {
    const player = checkPotentialWinner( board );
    if( !player ) return "";
    const startingPos = getStartingPos( player, board );
    let winnerO, winnerX;
    if( startingPos[ "O" ])
        winnerO = startingPos[ "O" ].map( sP => checkPath( sP, "O", board )).some( result => result );
    if( startingPos[ "X" ])
        winnerX = startingPos[ "X" ].map( sP => checkPath( sP, "X", board )).some( result => result );

    return winnerO 
        ? "O" 
        : winnerX
            ? "X"
            : ""
    ;
};

const Board = function( board ){
    this.board = board.map( row => row.replace( /\s/g, "" ));
    this.winner = function(){
        return determineWinner( this.board );
    };
};

export default Board;
