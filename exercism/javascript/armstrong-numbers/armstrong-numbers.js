const validate = n => {
    const nArr = n.toString().split( "" );
    const length = nArr.length;

    const total = nArr.reduce(( t, n ) => t + Math.pow( n, length ), 0 );

    return n === total;
};

export { validate };
