const bracketDict = {
    "}" : "{",
    "]" : "[",
    ")" : "("
};

const bracketPush = str => {
    const $bracketPush = ( sArr, arr ) => {
        if( sArr.length < 1 ){
            if( arr.length < 1 ) return true;
            else return false;
        }

        const current = sArr[ 0 ];
        
        if( !bracketDict[ current ]){
            arr.unshift( current );
            return $bracketPush( sArr.slice( 1, ), arr );
        }

        if( bracketDict[ current ] !== arr[ 0 ]) return false;
        arr.shift();
        return $bracketPush( sArr.slice( 1, ), arr );
    };

    return $bracketPush( str.split( "" ), []);

};

export { bracketPush };
