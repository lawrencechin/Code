let Anagram = function( word ){
    this.word = word;
};

Anagram.prototype.matches = function( ...args ){
    // not very robust! Will break with mixed type variables submitted
    let arr = args;
    if( args[ 0 ] instanceof Array ) arr = args[ 0 ];
    return arr.filter( word => {
        return !this.sameWord( this.word, word ) 
            && this.equalLength( this.word, word )
            && this.compare( this.word, word );
    });
};

/* Anagram.prototype.compare = function( word_1, word_2 ){
    let wordArr = word_1.toLowerCase().split( "" );
    word_2.toLowerCase().split( "" ).forEach( char => {
        const index = wordArr.indexOf( char );
        if( index < 0)
            return false; // doesn't break loop 😔
        else
            wordArr.splice( index, 1 );
    });
    return !wordArr.length;
};
*/

Anagram.prototype.compare = function( word_1, word_2 ){
    let wordArr = word_1.toLowerCase().split( "" );
    return word_2.toLowerCase().split( "" ).every( char => {
        const index = wordArr.indexOf( char );
        if( index < 0 )
            return false;
        else wordArr.splice( index, 1 );
        return true;
    });
};

Anagram.prototype.sameWord = function( word_1, word_2 ){
    return word_1.toLowerCase() === word_2.toLowerCase();
};

Anagram.prototype.equalLength = function( word_1, word_2 ){
    return word_1.length === word_2.length;
};

module.exports = Anagram;
