const Scrabble = function(){
    this.scores = generateScoreTable();
    return function( term ){
        if( !term || term.length < 1 )
            return 0;
        return parseTerm( term, this.scores );
    };
}();

module.exports = Scrabble;

function parseTerm( term, scores ){
    return term.replace( /(\w):double/g, "$1$1" )
    .replace( /(\w):triple/g, "$1$1$1" )
    .split( "" ).reduce(( total, char ) => {
        return total += scores[ char.toLowerCase()];
    }, 0 );
}

function generateScoreTable(){
    // annoying!
    return [
        [[ "a", "e", "i", "o", "u", "l", "n", "r", "s", "t" ], 1 ],
        [[ "d", "g" ], 2 ],
        [[ "b", "c", "m", "p" ], 3 ],
        [[ "f", "h", "v", "w", "y" ], 4 ],
        [[ "k" ], 5 ],
        [[ "j", "x" ], 8 ],
        [[ "q", "z" ], 10 ]
    ].reduce(( obj, item ) => {
        item[ 0 ].forEach( letter => {
            obj[ letter ] = item[ 1 ];
        });
        return obj;
    }, {});
}
