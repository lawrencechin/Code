var score = require('./scrabble-score');

describe('Scrabble', function() {
  it('scores an empty word as zero',function() {
    expect(score('')).toEqual(0);
  });

  it('scores a null as zero',function() {
    expect(score(null)).toEqual(0);
  });

  it('scores a very short word',function() {
    expect(score('a')).toEqual(1);
  });

  it('scores the word by the number of letters',function() {
    expect(score('street')).toEqual(6);
  });

  it('scores more complicated words with more',function() {
    expect(score('quirky')).toEqual(22);
  });

  it('scores case insensitive words',function() {
    expect(score('OXYPHENBUTAZONE')).toEqual(41);
  });
    // new tests for doubles and trebles
    it( "scores include doubles", function(){
        expect( score( "Tray:doubleectoria" )).toEqual( 20 );
    });
    it( "scores include trebles", function(){
        expect( score( "Definitiv:tripleamente" )).toEqual( 32 );
    });
    it( "scores includes doubles and trebles", function(){
        expect( score( "Mara:doublev:tripleillos:doubleo" )).toEqual( 26 );
    });
});
